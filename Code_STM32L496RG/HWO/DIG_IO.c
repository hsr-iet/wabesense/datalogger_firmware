/**
 ****************************************************************
 @file    DIG_IO.c
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        all the available digital In/Output pins on the data-logger.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-10
 ****************************************************************
 */

/** @addtogroup HWO
 * @{
 */

/** @addtogroup IO
 * @{
 */

// --- Includes
#include "DIG_IO.h"

#include <assert.h>
#include <stdbool.h>
#include "main.h"
#include "gpio.h"
#include "SYS.h"

// --- Defines

// --- Typedefs

// --- Variables
static pFV_V_t _digIO_cbFct[10] =
{
  NULL
};

static bool _initDone = false;
static bool _otgDeviceMode = true;
static uint32_t _digIO_3v3SwState = 0x00;
static uint32_t _digIO_17vSwState = 0x00;

// --- Local Function Prototypes
void _digIO_cbFctEXTI (digIO_pin_t pin);

/**
 ****************************************************************
 @brief   Initialize the digital in/outputs.
 @param   -
 @return  -
 ****************************************************************
 */
void digIO_init (void)
{
  GPIO_InitTypeDef GPIO_InitStruct =
  {
    0
  };

  if (!_initDone)
  {
    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();

    //setup onBoard data logger inputs (not anymore as wakeUp pins in this case)
    //configure GPIO pin : PA0 - VDD USB
    GPIO_InitStruct.Pin = GPIO_PIN_0;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    //configure GPIO pin : PA2 - SDCard Detection
    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    //configure GPIO pin : PC5 - UserButton_1
    GPIO_InitStruct.Pin = GPIO_PIN_5;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    //configure GPIO pin : PC13 - UserButton_2
    GPIO_InitStruct.Pin = GPIO_PIN_13;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    //PA.15
    HAL_GPIO_WritePin(ENA_5V_EXT_GPIO_Port, ENA_5V_EXT_Pin, GPIO_PIN_RESET);
    //PB.9
    HAL_GPIO_WritePin(ENA_3V3_GPIO_Port, ENA_3V3_Pin, GPIO_PIN_RESET);
    //PA.10
    HAL_GPIO_WritePin(ENA_5V_USB_GPIO_Port, ENA_5V_USB_Pin, GPIO_PIN_RESET);
    //PC.4
    HAL_GPIO_WritePin(ENA_17V_GPIO_Port, ENA_17V_Pin, GPIO_PIN_RESET);

    //initialization of PA.10 & PA.15
    GPIO_InitStruct.Pin = ENA_5V_USB_Pin | ENA_5V_EXT_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    //initialization of PB.9
    GPIO_InitStruct.Pin = ENA_3V3_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    //initialization of PC.4
    GPIO_InitStruct.Pin = ENA_17V_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ENA_17V_GPIO_Port, &GPIO_InitStruct);

    _initDone = true;
  }
}

/**
 ****************************************************************
 @brief   Get the digital input state on the user button 1 pin.
 @param   -
 @return  True, if the user button 1 is pressed, otherwise false
 ****************************************************************
 */
bool digIO_usrBt1Pressed (void)
{
  return HAL_GPIO_ReadPin(SYS_WKUP5_USR_BT1_GPIO_Port, SYS_WKUP5_USR_BT1_Pin);
}

/**
 ****************************************************************
 @brief   Get the digital input state on the user button 2 pin.
 @param   -
 @return  True, if the user button 2 is pressed, otherwise false
 ****************************************************************
 */
bool digIO_usrBt2Pressed (void)
{
  return HAL_GPIO_ReadPin(SYS_WKUP2_USR_BT2_GPIO_Port, SYS_WKUP2_USR_BT2_Pin);
}

/**
 ****************************************************************
 @brief   Get the digital input state of the USB power detection
 @param   -
 @return  True, if the USB power is present, otherwise false
 ****************************************************************
 */
bool digIO_usbDetected (void)
{
  return HAL_GPIO_ReadPin(SYS_WKUP1_VDD_USB_GPIO_Port, SYS_WKUP1_VDD_USB_Pin);
}

/**
 ****************************************************************
 @brief   Get the digital input state on the SDCard detection pin
 @param   -
 @return  True, if the SDCard is detected, otherwise false
 ****************************************************************
 */
bool digIO_sdCardDetected (void)
{
  return HAL_GPIO_ReadPin(SYS_WKUP4_SDCARD_GPIO_Port, SYS_WKUP4_SDCARD_Pin);
}

/**
 ****************************************************************
 @brief   Turn the supply voltage measurement path on.
 @param   -
 @return  -
 ****************************************************************
 */
void digIO_supplyMeasPathEna (void)
{
  HAL_GPIO_WritePin(V_SUPPLY_MEAS_ENA_GPIO_Port, V_SUPPLY_MEAS_ENA_Pin, GPIO_PIN_SET);

  //a small delay, that parts are correctly turned on
  sys_sleepForUs(10); //current RC-Tau is 8.2us
}

/**
 ****************************************************************
 @brief   Turn the supply voltage measurement path off.
 @param   -
 @return  -
 ****************************************************************
 */
void digIO_supplyMeasPathDis (void)
{
  HAL_GPIO_WritePin(V_SUPPLY_MEAS_ENA_GPIO_Port, V_SUPPLY_MEAS_ENA_Pin, GPIO_PIN_RESET);
}

/**
 ****************************************************************
 @brief   Change the resistors viewed at the USB_CC1/USB_CC2 lines.
 @param   device  If true, the resistors are switched to DEVICE mode, otherwise to HOST mode
 @return  -
 ****************************************************************
 */
void digIO_otgDeviceModeSet (bool device)
{
  if (device)
  {
    _otgDeviceMode = true;
    HAL_GPIO_WritePin(OTG_Device_GPIO_Port, OTG_Device_Pin, GPIO_PIN_SET);
  }
  else
  {
    _otgDeviceMode = false;
    HAL_GPIO_WritePin(OTG_Device_GPIO_Port, OTG_Device_Pin, GPIO_PIN_RESET);
  }
}

/**
 ****************************************************************
 @brief   Get the current OTG Role (DEVICE or HOST)
 @param   -
 @return  True if OTG interacts as DEVICE, otherwise false
 ****************************************************************
 */
bool digIO_otgDeviceModeGet (void)
{
  return _otgDeviceMode;
}

/**
 ****************************************************************
 @brief   Close the 5V switch from system voltage to USB --> so 5V-USB power comes from datalogger
 *        Take care, only allowed, if VDD is not already present from external USB Host!
 @param   -
 @return  -
 ****************************************************************
 */
void digIO_5VtoUSBSwitchOn (void)
{
  HAL_GPIO_WritePin(ENA_5V_USB_GPIO_Port, ENA_5V_USB_Pin, GPIO_PIN_SET);

  //additional delay, so 5v to reaches correct output voltage value
  //at least 98us from 5V Switched (10-90%)
  sys_sleepForUs(125);
}

/**
 ****************************************************************
 @brief   Open the 5V switch from system voltage to USB (USB Device mode) --> so 5V-USB comes from external
 @param   -
 @return  -
 ****************************************************************
 */
void digIO_5VtoUSBSwitchOff (void)
{
  HAL_GPIO_WritePin(ENA_5V_USB_GPIO_Port, ENA_5V_USB_Pin, GPIO_PIN_RESET);
}

/**
 ****************************************************************
 @brief   Turn the 5V generation from external (battery) on (normal case / default).
 @param   -
 @return  -
 ****************************************************************
 */
void digIO_5VExtPwrOn (void)
{
  HAL_GPIO_WritePin(ENA_5V_EXT_GPIO_Port, ENA_5V_EXT_Pin, GPIO_PIN_RESET);
}

/**
 ****************************************************************
 @brief   Turn the 5V generation from external (battery) off. Device completely powered from USB Host
 @param   -
 @return  -
 ****************************************************************
 */
void digIO_5VExtPwrOff (void)
{
  HAL_GPIO_WritePin(ENA_5V_EXT_GPIO_Port, ENA_5V_EXT_Pin, GPIO_PIN_SET);
}

/**
 ****************************************************************
 @brief   Turn on the switched 3.3V power supply.
 @note    Used by FLASH IC (AT25SF321B)
 @note    Used by SD-Card (without SD-Card detection)
 @note    Could be used by RGB-LED
 @note    Could be used by external I2C communication (I2C1-Unit)
 @param   unit  One of the possible 3V3 Units
 @return  -
 ****************************************************************
 */
void digIO_3V3PwrOn (digIO_3V3Unit_t unit)
{
  uint32_t digIO_3v3SwState_old = _digIO_3v3SwState;

  assert_param(
    (unit == DIG_IO_3V3UNIT_FLASH) || (unit == DIG_IO_3V3UNIT_SDCARD) || (unit == DIG_IO_3V3UNIT_EXT_I2C1)
      || (unit == DIG_IO_3V3UNIT_EXT_J5));

  //mechanisms to check which parts are needed the 3v3
  _digIO_3v3SwState |= unit;

  if (!digIO_3v3SwState_old)
  {
    HAL_GPIO_WritePin(ENA_3V3_GPIO_Port, ENA_3V3_Pin, GPIO_PIN_SET);

    //additional delay, so 3v3 to reaches correct output voltage value
    //at least 98us from 3.3V Switched (10-90%)
    sys_sleepForUs(125);
  }
}

/**
 ****************************************************************
 @brief   Request to shutdown the switched 3.3V power supply.
 @param   unit  One of the possible 3V3 Units
 @return  -
 ****************************************************************
 */
void digIO_3V3PwrReqOff (digIO_3V3Unit_t unit)
{
  assert_param(
    (unit == DIG_IO_3V3UNIT_FLASH) || (unit == DIG_IO_3V3UNIT_SDCARD) || (unit == DIG_IO_3V3UNIT_EXT_I2C1)
      || (unit == DIG_IO_3V3UNIT_EXT_J5));

  //mechanisms to check which parts are needed the 3v3
  _digIO_3v3SwState &= ~unit;
  if (_digIO_3v3SwState == 0)
  {
    HAL_GPIO_WritePin(ENA_3V3_GPIO_Port, ENA_3V3_Pin, GPIO_PIN_RESET);
  }
}

/**
 ****************************************************************
 @brief   Turn on the switched 17V power supply.
 @note    Used by external sensor connector (J6 screw terminal)
 @param   unit  One of the possible 17V Units
 @return  -
 ****************************************************************
 */
void digIO_17VPwrOn (digIO_17VUnit_t unit)
{
  uint32_t digIO_17vSwState_old = _digIO_17vSwState;

  assert_param(
    (unit == DIG_IO_17VUNIT_EXT_SENSOR1) || (unit == DIG_IO_17VUNIT_EXT_SENSOR2) || (unit == DIG_IO_17VUNIT_EXT_J5));

  //mechanisms to check which parts are needed the 17V
  _digIO_17vSwState |= unit;

  if (!digIO_17vSwState_old)
  {
    HAL_GPIO_WritePin(ENA_17V_GPIO_Port, ENA_17V_Pin, GPIO_PIN_SET);

    //additional delay, so 17v to reaches correct output voltage value
    //at least 98us from 17V Switched (10-90%)
    sys_sleepForUs(125);
  }
}

/**
 ****************************************************************
 @brief   Request to shutdown the switched 17V power supply.
 @param   unit  One of the possible 17V Units
 @return  -
 ****************************************************************
 */
void digIO_17VPwrReqOff (digIO_17VUnit_t unit)
{
  assert_param(
    (unit == DIG_IO_17VUNIT_EXT_SENSOR1) || (unit == DIG_IO_17VUNIT_EXT_SENSOR2) || (unit == DIG_IO_17VUNIT_EXT_J5));

  //mechanisms to check which parts are needed the 17V
  _digIO_17vSwState &= ~unit;
  if (_digIO_17vSwState == 0)
  {
    HAL_GPIO_WritePin(ENA_17V_GPIO_Port, ENA_17V_Pin, GPIO_PIN_RESET);
  }
}

/**
 ****************************************************************
 @brief   Setup one of the digital in/output pins.
 @param   pin One of the possible digital in/output pins from enumeration
 @param   config  The to be used configuration struct (mode, pull, speed)
 @return  -
 ****************************************************************
 */
void digIO_setup (digIO_pin_t pin, digIO_cfg_t config)
{
  GPIO_TypeDef *pGPIO_unit = NULL;
  GPIO_InitTypeDef GPIO_InitStruct =
  {
    0
  };

  switch (pin)
  {
    case DIG_IO_PB10:
      pGPIO_unit = GPIOB;
      GPIO_InitStruct.Pin = GPIO_PIN_10;
      break;
    case DIG_IO_PB11:
      pGPIO_unit = GPIOB;
      GPIO_InitStruct.Pin = GPIO_PIN_11;
      break;
    case DIG_IO_PB12:
      pGPIO_unit = GPIOB;
      GPIO_InitStruct.Pin = GPIO_PIN_12;
      break;
    case DIG_IO_PB13:
      pGPIO_unit = GPIOB;
      GPIO_InitStruct.Pin = GPIO_PIN_13;
      break;
    case DIG_IO_PB14:
      pGPIO_unit = GPIOB;
      GPIO_InitStruct.Pin = GPIO_PIN_14;
      break;
    case DIG_IO_PB15:
      pGPIO_unit = GPIOB;
      GPIO_InitStruct.Pin = GPIO_PIN_15;
      break;
    case DIG_IO_PC6:
      pGPIO_unit = GPIOC;
      GPIO_InitStruct.Pin = GPIO_PIN_6;
      break;
    case DIG_IO_PC7:
      pGPIO_unit = GPIOC;
      GPIO_InitStruct.Pin = GPIO_PIN_7;
      break;
    case DIG_IO_PC8:
      pGPIO_unit = GPIOC;
      GPIO_InitStruct.Pin = GPIO_PIN_8;
      break;
    case DIG_IO_PC9:
      pGPIO_unit = GPIOC;
      GPIO_InitStruct.Pin = GPIO_PIN_9;
      break;
    case DIG_IO_PC10:
      pGPIO_unit = GPIOC;
      GPIO_InitStruct.Pin = GPIO_PIN_10;
      break;
    case DIG_IO_PC11:
      pGPIO_unit = GPIOC;
      GPIO_InitStruct.Pin = GPIO_PIN_11;
      break;
    case DIG_IO_PC12:
      pGPIO_unit = GPIOC;
      GPIO_InitStruct.Pin = GPIO_PIN_12;
      break;
    default:
      assert_param(0);
      break;
  }
  GPIO_InitStruct.Mode = config.mode;
  GPIO_InitStruct.Pull = config.pull;
  GPIO_InitStruct.Speed = config.speed;
  GPIO_InitStruct.Alternate = 0;

  if (pGPIO_unit == GPIOA)
  {
    __HAL_RCC_GPIOA_CLK_ENABLE();
  }
  else if (pGPIO_unit == GPIOB)
  {
    __HAL_RCC_GPIOB_CLK_ENABLE();
  }
  else
  {
    assert_param(0);
  }

  HAL_GPIO_Init(pGPIO_unit, &GPIO_InitStruct);

  //parts for interrupt detection
  if ((config.mode == DIG_IO_MODE_IT_RISING) || (config.mode == DIG_IO_MODE_IT_FALLING)
    || (config.mode == DIG_IO_MODE_IT_RISING_FALLING))
  {
    switch (pin)
    {
      case DIG_IO_PC6:
      case DIG_IO_PC7:
      case DIG_IO_PC8:
      case DIG_IO_PC9:
        //EXTI interrupt initialization
        HAL_NVIC_SetPriority(EXTI9_5_IRQn, DIG_IO_INTERRUPT_PRIO, 0);
        HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
        break;
      case DIG_IO_PB12:
      case DIG_IO_PB13:
      case DIG_IO_PB14:
      case DIG_IO_PB15:
      case DIG_IO_PC10:
      case DIG_IO_PC11:
        HAL_NVIC_SetPriority(EXTI15_10_IRQn, DIG_IO_INTERRUPT_PRIO, 0);
        HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
        break;
      case DIG_IO_PB10:
      case DIG_IO_PB11:
      case DIG_IO_PC12:
        assert_param(0);  //no interrupt possibilities
        break;
      default:
        assert_param(0);
        break;
    }
  }
}

/**
 ****************************************************************
 @brief   Register a callback function (only throw on interrupt configurations).
 @param   pin One of the possible digital in/output pins from enumeration
 @param   pCbFct  The to be called function on corresponding interrupt
 @return  -
 ****************************************************************
 */
void digIO_cbRegister (digIO_pin_t pin, pFV_V_t pCbFct)
{
  assert_param(
    (pin == DIG_IO_PB12) || (pin == DIG_IO_PB13) || (pin == DIG_IO_PB14) || (pin == DIG_IO_PB15) || (pin == DIG_IO_PC6)
      || (pin == DIG_IO_PC7) || (pin == DIG_IO_PC8) || (pin == DIG_IO_PC9) || (pin == DIG_IO_PC10)
      || (pin == DIG_IO_PC11));
  _digIO_cbFct[pin] = pCbFct;
}

/**
 ****************************************************************
 @brief   Read the pin level of a digital in/output pin.
 @param   pin One of the possible digital in/output pins from enumeration
 @return  The digital level of the corresponding in/output
 ****************************************************************
 */
bool digIO_read (digIO_pin_t pin)
{
  GPIO_TypeDef *pGPIO_unit = NULL;
  uint32_t pinNr;

  switch (pin)
  {
    case DIG_IO_PB10:
      pGPIO_unit = GPIOB;
      pinNr = GPIO_PIN_10;
      break;
    case DIG_IO_PB11:
      pGPIO_unit = GPIOB;
      pinNr = GPIO_PIN_11;
      break;
    case DIG_IO_PB12:
      pGPIO_unit = GPIOB;
      pinNr = GPIO_PIN_12;
      break;
    case DIG_IO_PB13:
      pGPIO_unit = GPIOB;
      pinNr = GPIO_PIN_13;
      break;
    case DIG_IO_PB14:
      pGPIO_unit = GPIOB;
      pinNr = GPIO_PIN_14;
      break;
    case DIG_IO_PB15:
      pGPIO_unit = GPIOB;
      pinNr = GPIO_PIN_15;
      break;
    case DIG_IO_PC6:
      pGPIO_unit = GPIOC;
      pinNr = GPIO_PIN_6;
      break;
    case DIG_IO_PC7:
      pGPIO_unit = GPIOC;
      pinNr = GPIO_PIN_7;
      break;
    case DIG_IO_PC8:
      pGPIO_unit = GPIOC;
      pinNr = GPIO_PIN_8;
      break;
    case DIG_IO_PC9:
      pGPIO_unit = GPIOC;
      pinNr = GPIO_PIN_9;
      break;
    case DIG_IO_PC10:
      pGPIO_unit = GPIOC;
      pinNr = GPIO_PIN_10;
      break;
    case DIG_IO_PC11:
      pGPIO_unit = GPIOC;
      pinNr = GPIO_PIN_11;
      break;
    case DIG_IO_PC12:
      pGPIO_unit = GPIOC;
      pinNr = GPIO_PIN_12;
      break;
    default:
      pinNr = 0;
      assert_param(0);
      break;
  }
  return HAL_GPIO_ReadPin(pGPIO_unit, pinNr);
}

/**
 ****************************************************************
 @brief   Write a digital level to an digital output
 @param   pin One of the possible digital in/output pins from enumeration
 @param   state  If true, set the output to a high level, otherwise to low
 @return  -
 ****************************************************************
 */
void digIO_write (digIO_pin_t pin, bool state)
{
  GPIO_TypeDef *pGPIO_unit = NULL;
  uint32_t pinNr;

  switch (pin)
  {
    case DIG_IO_PB10:
      pGPIO_unit = GPIOB;
      pinNr = GPIO_PIN_10;
      break;
    case DIG_IO_PB11:
      pGPIO_unit = GPIOB;
      pinNr = GPIO_PIN_11;
      break;
    case DIG_IO_PB12:
      pGPIO_unit = GPIOB;
      pinNr = GPIO_PIN_12;
      break;
    case DIG_IO_PB13:
      pGPIO_unit = GPIOB;
      pinNr = GPIO_PIN_13;
      break;
    case DIG_IO_PB14:
      pGPIO_unit = GPIOB;
      pinNr = GPIO_PIN_14;
      break;
    case DIG_IO_PB15:
      pGPIO_unit = GPIOB;
      pinNr = GPIO_PIN_15;
      break;
    case DIG_IO_PC6:
      pGPIO_unit = GPIOC;
      pinNr = GPIO_PIN_6;
      break;
    case DIG_IO_PC7:
      pGPIO_unit = GPIOC;
      pinNr = GPIO_PIN_7;
      break;
    case DIG_IO_PC8:
      pGPIO_unit = GPIOC;
      pinNr = GPIO_PIN_8;
      break;
    case DIG_IO_PC9:
      pGPIO_unit = GPIOC;
      pinNr = GPIO_PIN_9;
      break;
    case DIG_IO_PC10:
      pGPIO_unit = GPIOC;
      pinNr = GPIO_PIN_10;
      break;
    case DIG_IO_PC11:
      pGPIO_unit = GPIOC;
      pinNr = GPIO_PIN_11;
      break;
    case DIG_IO_PC12:
      pGPIO_unit = GPIOC;
      pinNr = GPIO_PIN_12;
      break;
    default:
      pinNr = 0;
      assert_param(0);
      break;
  }
  HAL_GPIO_WritePin(pGPIO_unit, pinNr, state);
}

/**
 ****************************************************************
 @brief   Interrupt callback function for the possible external interrupts.
 @param   pin One of the possible digital in/output pins from enumeration
 @return  -
 ****************************************************************
 */
void _digIO_cbFctEXTI (digIO_pin_t pin)
{
  assert_param(
    (pin == DIG_IO_PB12) || (pin == DIG_IO_PB13) || (pin == DIG_IO_PB14) || (pin == DIG_IO_PB15) || (pin == DIG_IO_PC6)
      || (pin == DIG_IO_PC7) || (pin == DIG_IO_PC8) || (pin == DIG_IO_PC9) || (pin == DIG_IO_PC10)
      || (pin == DIG_IO_PC11));

  if (_digIO_cbFct[pin] != NULL)
  {
    _digIO_cbFct[pin]();
  }
}
/**
 * @}
 */

/**
 * @}
 */
