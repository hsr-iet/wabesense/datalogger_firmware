/**
 ****************************************************************
 @file    RG_LED.c
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        a red-green (yellow) LED (Q8F3BZZRYG02E).
 *        Timer1 is used to generate the PWM
 *          Channel 1: Red LED
 *          Cahnnel 2: Green LED
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.1
 @date    2021-09-20
 ****************************************************************
 */

/** @addtogroup HWO
 * @{
 */

/** @addtogroup RG_LED
 * @{
 */

// --- Includes
#include "RG_LED.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include "tim.h"

// --- Defines

// --- Typedefs

// --- Variables
static bool _initDone = false;
static volatile uint32_t _luminosity = 0;
static volatile rgColor_t _color = RGLED_COLOR_NONE;

// --- Local Function Prototypes

/**
 ****************************************************************
 @brief   Initialize the red-green LED
 @param   -
 @return  -
 ****************************************************************
 */
void rgLED_init (void)
{
  if (!_initDone)
  {
    //setup timer1 (used for PWM generation)
    MX_TIM1_Init();

    _color = RGLED_COLOR_NONE;
    rgLED_luminositySet(0);

    _initDone = true;
  }
}

/**
 ****************************************************************
 @brief   Setup the luminosity value for the red-green LED
 @param   percent The to be used percent value [0..99]
 @return  -
 ****************************************************************
 */
void rgLED_luminositySet (uint8_t percent)
{
  if (percent > 99)
  {
    percent = 99;
  }
  _luminosity = percent;

  rgLED_colorSet(_color);
}

/**
 ****************************************************************
 @brief   Set the active red-green LED color
 @param   color One of the possible colors from enumeration
 @return  -
 ****************************************************************
 */
void rgLED_colorSet (rgColor_t color)
{
  HAL_TIMEx_PWMN_Stop(&htim1, TIM_CHANNEL_1);
  HAL_TIMEx_PWMN_Stop(&htim1, TIM_CHANNEL_2);

  HAL_TIM_OC1_Set(&htim1, _luminosity);
  HAL_TIM_OC2_Set(&htim1, _luminosity);
  switch (color)
  {
    default:
      assert_param(0);
      break;
    case RGLED_COLOR_NONE:
      _color = RGLED_COLOR_NONE;
      break;
    case RGLED_COLOR_RED:
      if (_luminosity)
      {
        HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_1);
      }
      break;
    case RGLED_COLOR_GREEN:
      if (_luminosity)
      {
        HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_2);
      }
      break;
    case RGLED_COLOR_YELLOW:
      if (_luminosity)
      {
        HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_1);
        HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_2);
      }
      break;
  }
  _color = color;
}

/**
 * @}
 */

/**
 * @}
 */
