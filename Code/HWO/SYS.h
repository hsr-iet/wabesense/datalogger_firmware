/**
 ****************************************************************
 @file    SYS.h
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the system (clock).
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-10-28
 ****************************************************************
 */
#ifndef __SYS_H
#define __SYS_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdbool.h>
#include <stdint.h>
#include "rtc.h"
#include "../DEF/GLOB_Types.h"

//===============================================================
//defines

//===============================================================
//typedefs
//! unique system identifier
typedef struct
{
  unsigned int systemID[3];     //!< unique device identification number (96bit)
} sysID_t;

//! System wakeUp events
typedef enum
{
  SYS_WAKEUP_HW_RESET = 0,  //!< hardware based reset
  SYS_WAKEUP_SW_RESET,      //!< software based reset
  SYS_WAKEUP_WATCHDOG,      //!< watch-dog based reset
  SYS_WAKEUP_USB_VDD,       //!< USB VDD voltage detected
  SYS_WAKEUP_SDCARD,        //!< SDCard insert detected
  SYS_WAKEUP_USR_BT1,       //!< User Button 1 detected
  SYS_WAKEUP_USR_BT2,       //!< User Button 2 detected
  SYS_WAKEUP_RTC,           //!< RTC Unit generate the system wakeUp
} sysWakeUp_t;

typedef enum
{
  SYS_BACKUP_REG_FLASH_LOGDATA_TAIL = 0,  //!< The FLASH logData tail memory address
  SYS_BACKUP_REG_FLASH_LOGDATA_HEAD = 1,  //!< The FLASH logData head memory address
  SYS_BACKUP_REG_FLASH_RESERVED_TAIL = 2, //!< The FLASH reserved tail memory address
  SYS_BACKUP_REG_FLASH_RESERVED_HEAD = 3, //!< The FLASH reserved head memory address
  SYS_BACKUP_REG_FLASH_LOGHIST_TAIL = 4,  //!< The FLASH log history tail memory address
  SYS_BACKUP_REG_FLASH_LOGHIST_HEAD = 5,  //!< The FLASH log history head memory address
  SYS_BACKUP_REG_AUTO_MEAS_ACTIVE = 6,    //!< Flag for automatic measurement loop control
  SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT = 7,  //!< The FLASH logData tail memory address of readout data
  SYS_BACKUP_REG_RTC_BAT_CHECK_MONTH = 8, //!< Hold the month information about the last RTC Battery check done
} sysBackupReg_t;

//===============================================================
//function prototypes
void sys_init (void);

void sys_uniqueIdGet (sysID_t *pSysId);

void sys_dwtCpuClockCntReset (void);
uint32_t sys_dwtCpuClockCntGet (void);

void sys_sleepForUs (uint32_t sleepTime_us);
void sys_standbyEnter (void);
void sys_usbBootloaderEnter (void);
void sys_sdCardBootloaderEnter (void);

void sys_sysTickReset (void);
void sys_sysTickTickGet (uint32_t *pTick);
void sys_sysTickTickDeltaGet (uint32_t oldTick, uint32_t *pDeltaTick);
void sys_sysTickTickDeltaUpdateGet (uint32_t *pOldTick, uint32_t *pDeltaTick);
void sys_sysTickRegCallbackFct (pFV_V_t pSysTickCbFct);

sysWakeUp_t sys_wakupReasonGet (void);
bool sys_wakeupTimerActive (void);
void sys_wakeupTimerSet (uint32_t wakeupTime_s);
bool sys_wakeupTimerExpired (void);

void sys_rtcDateTimeSet (rtcDateTime_t *pDateTime);
void sys_rtcDateTimeGet (rtcDateTime_t *pDateTime);
void sys_backupRegisterSet (sysBackupReg_t reg, uint32_t value);
uint32_t sys_backupRegisterGet (sysBackupReg_t reg);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* #ifndef __SYS_H */
