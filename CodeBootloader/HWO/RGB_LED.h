/**
 ****************************************************************
 @file    RGB_LED.h
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the onBoard RGB_LED.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.1
 @date    2020-10-28
 ****************************************************************
 */
#ifndef __RGB_LED_H
#define __RGB_LED_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdint.h>

//===============================================================
//defines

//===============================================================
//typedefs
//! RGB color type
typedef enum
{
  RGBLED_COLOR_NONE = 0,  //!< RGB LED OFF
  RGBLED_COLOR_RED,       //!< RGB LED red only
  RGBLED_COLOR_GREEN,     //!< RGB LED green only
  RGBLED_COLOR_BLUE,      //!< RGB LED blue only
  RGBLED_COLOR_YELLOW,    //!< RGB LED yellow (red & green)
  RGBLED_COLOR_PURPLE,    //!< RGB LED purple (red & blue)
  RGBLED_COLOR_BLUEGREEN, //!< RGB LED blue-green (green & blue)
} rgbColor_t;

//===============================================================
//function prototypes
void rgbLED_init (void);
void rgbLED_luminositySet (uint8_t percent);
void rgbLED_colorSet (rgbColor_t color);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /*__RGB_LED_H */
