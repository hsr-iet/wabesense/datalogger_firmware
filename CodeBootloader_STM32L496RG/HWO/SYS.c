/**
 ****************************************************************
 @file    SYS.c
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the system (clock).
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-10-28
 ****************************************************************
 */

/** @addtogroup HWO
 * @{
 */

/** @addtogroup SYS
 * @{
 */

// --- Includes
#include "SYS.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <cmsis_gcc.h>
#include "../DEF/GLOB_Types.h"
#include "main.h"
#include "rtc.h"
#include "tim.h"
#include "DIG_IO.h"

// --- Defines

// --- Typedefs

// --- Variables
static volatile bool _sleepTimeExp = false;

// --- Local Function Prototypes
static void _sys_sleepTimeExpCb (void);

/**
 ****************************************************************
 @brief   Initialize the system module.
 @param   -
 @return  -
 ****************************************************************
 */
void sys_init (void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct =
  {
    0
  };
  RCC_ClkInitTypeDef RCC_ClkInitStruct =
  {
    0
  };
  RCC_PeriphCLKInitTypeDef PeriphClkInit =
  {
    0
  };

  HAL_SetTickFreq(HAL_TICK_FREQ_DEFAULT);    //default tick frequency is 1kHz

  //configure LSE Drive Capability
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  //configure the main internal regulator output voltage
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  //initializes the CPU, AHB and APB busses clocks
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48 | RCC_OSCILLATORTYPE_HSI | RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  //initializes the CPU, AHB and APB busses clocks
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_USB | RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_SYSCLK;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

  SystemCoreClockUpdate();
}

/**
 *
 ****************************************************************
 @brief   Erase the complete FLASH area of the user application.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
bool sys_flashUserAppErase (void)
{
  FLASH_EraseInitTypeDef flashEraseInit;
  uint32_t pageError;
  HAL_StatusTypeDef status;
  uint16_t flash_size_kb = READ_REG(*((uint32_t *)FLASHSIZE_BASE));
  uint32_t flash_nrOfPages = flash_size_kb >> 1;  //each page is 2kByte in Size

  while(HAL_FLASH_Unlock() != HAL_OK);

  //clear Flash status register
  __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_ALL_ERRORS);

  flashEraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
  flashEraseInit.Banks = FLASH_BANK_1;
  flashEraseInit.Page = 64;                 //initial flash Page is 64 (start of userApplication)
  if (flash_nrOfPages >= 256)
  {
    flashEraseInit.NbPages = 256 - flashEraseInit.Page;
  }
  else
  {
    flashEraseInit.NbPages = flash_nrOfPages - flashEraseInit.Page;
  }
  //erase complete memory area where the userApplication is located at
  status = HAL_FLASHEx_Erase(&flashEraseInit, &pageError);
  if (status != HAL_OK)
  {
    HAL_FLASH_Lock();
    return false;
  }
  else
  {
    //check if device has two banks (if more than 512kByte in size)
    if (flash_nrOfPages > 256)
    {
      //erase second Flash Bank
      while(HAL_FLASH_Unlock() != HAL_OK);

      //clear Flash status register
      __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_ALL_ERRORS);

      flashEraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
      flashEraseInit.Banks = FLASH_BANK_2;
      flashEraseInit.Page = 0;                 //initial flash Page is 0
      flashEraseInit.NbPages = 256;
      //erase complete memory area where the userApplication is located at (complete Flash Bank 2)
      status = HAL_FLASHEx_Erase(&flashEraseInit, &pageError);
    }

    HAL_FLASH_Lock();
    return true;
  }
}

/**
 *
 ****************************************************************
 @brief   Write a couple of bytes to the memory in FLASH where the
 *        userApplication is located.
 @param   memAddr   The memory adress where the bytes should be stored
 @param   pMemData  Pointer to the be written data
 @param   len       The number of bytes to be written
 @return  Return true on success, otherwise false
 ****************************************************************
 */
bool sys_flashUserAppWrite (uint32_t memAddr, uint8_t *pMemData, uint32_t len)
{
  HAL_StatusTypeDef status;
  uint32_t i;
  uint64_t data;

  status = HAL_FLASH_Unlock();
  if (status != HAL_OK)
  {
    return false;
  }

  for (i = 0; i < len; i += 8)
  {
    memcpy(&data, pMemData, 8);
    //64bit is each transfer
    status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, memAddr, data);
    memAddr += 8;
    pMemData += 8;

    if (status != HAL_OK)
    {
      HAL_FLASH_Lock();
      return false;
    }
  }

  HAL_FLASH_Lock();
  return true;
}

/**
 ****************************************************************
 @brief   Change VectorTableAddress and jump to the user Application.
 @param   vTableAddr Address of the new vector-table
 @return  -
 ****************************************************************
 */
inline void sys_userAppStart (uint32_t vTableAddr)
{
  uint32_t jumpAddr;
  pFV_V_t appResetHandler;

  __disable_irq();
  //do all the deInitializations
  HAL_TIM1_DeInit();
  HAL_TIM6_DeInit();

  HAL_DeInit();

  SysTick->CTRL = 0;
  SysTick->LOAD = 0;
  SysTick->VAL = 0;
  __enable_irq();

  jumpAddr = *(uint32_t*) (vTableAddr + 4);
  appResetHandler = (pFV_V_t) jumpAddr;
  //initialize user application's Stack Pointer
  __set_MSP(*(uint32_t*) vTableAddr);

  //jump to user application
  appResetHandler();
  //does not return here
  while (1)
  {

  }
}

/**
 ****************************************************************
 @brief   Set the system to sleep mode for an amount of time in microseconds..
 @param   sleepTime_us  The time to set the system to sleep mode in microseconds.
 @return  -
 ****************************************************************
 */
void sys_sleepForUs (uint32_t sleepTime_us)
{
  _sleepTimeExp = false;

  if (sleepTime_us < 20)
  {
    HAL_TIM6_StartInterrupt(_sys_sleepTimeExpCb, sleepTime_us);
    //no sleep of the system
    while (!_sleepTimeExp);
  }
  else
  {
    HAL_TIM6_StartInterrupt(_sys_sleepTimeExpCb, sleepTime_us);
    //set system to sleep in lowPower sleep mode (chapter 5.3.5)
    HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);

    while (!_sleepTimeExp);
  }
}

/**
 ****************************************************************
 @brief   Put the system to standby mode (wakeUp over internal RTC or external wakeUp-Pins)
 @param   -
 @return  -
 ****************************************************************
 */
void sys_standbyEnter (void)
{
  //switch OFF 3.3V switched power supply
  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_FLASH);
  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_SDCARD);
  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_EXT_I2C1);
  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_EXT_J5);
  //switch OFF 17V switched power supply
  digIO_17VPwrReqOff(DIG_IO_17VUNIT_EXT_SENSOR1);
  digIO_17VPwrReqOff(DIG_IO_17VUNIT_EXT_SENSOR2);
  digIO_17VPwrReqOff(DIG_IO_17VUNIT_EXT_J5);

  //switch all USB parts OFF (no power output on the VUSB)
  digIO_5VtoUSBSwitchOff();
  digIO_otgDeviceModeSet(true);  //interacts as USB device

  __HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);
  __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);

  //wait while a user button is still pressed!
  while (digIO_usrBt1Pressed() || digIO_usrBt2Pressed());
  sys_sleepForUs(10e3);  //10ms delay

  HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN1); //VDD_USB
  HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN2); //User button 2
//  HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN4); //SDCardDetection
  HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN5); //User button 1

  HAL_PWR_EnterSTANDBYMode();
}

/**
 ****************************************************************
 @brief   Register a callback function, which should be executed in
 *        sysTick interrupt service routine.
 @param   pSysTickCbFct Pointer to the callback function to be registered
 @return  -
 ****************************************************************
 */
void sys_sysTickRegCallbackFct (pFV_V_t pSysTickCbFct)
{
  HAL_SYSTICK_RegCallbackFct(pSysTickCbFct);
}

/**
 ****************************************************************
 @brief   Suspend the sysTick interrupts and the counting of the sysTick unit.
 @param   -
 @return  -
 ****************************************************************
 */
void sys_sysTickSuspend (void)
{
  HAL_SuspendTick();
}

/**
 ****************************************************************
 @brief   Resume the sysTick interrupts and the counting of the sysTick unit.
 @param   -
 @return  -
 ****************************************************************
 */
void sys_sysTickResume (void)
{
  HAL_ResumeTick();
}

/**
 ****************************************************************
 @brief   Get the current RTC date/time information (binary format).
 @param   pDateTime Pointer to the current date/time information.
 @return  -
 ****************************************************************
 */
void sys_rtcDateTimeGet (rtcDateTime_t *pDateTime)
{
  HAL_RTC_DateTimeGet(pDateTime);
}

/**
 ****************************************************************
 @brief   Internal callback function on Timer6 timeout done.
 @param   -
 @return  -
 ****************************************************************
 */
static void _sys_sleepTimeExpCb (void)
{
  //indicate to exit sleep mode after interrupt handler return
  HAL_PWR_DisableSleepOnExit();

  _sleepTimeExp = true;
}

/**
 * @}
 */

/**
 * @}
 */
