/**
 ****************************************************************
 @file    MMC.h
 ****************************************************************
 @brief   MMC/SDC (in SPI mode) control module.
 @brief   Low level disk interface module include file   (C)ChaN, 2013
 ****************************************************************
 @note    The used interface is the SPI1 unit (also used for FLASH and BME280)!
 *        SPI Interface of the sensor is compatible to (max. 20MHz)
 *          mode 00
 *            CPOL = 0, CPHA = 0
 *          or mode 11
 *            CPOL = 1, CPHA = 1
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-16
 ****************************************************************
 */
#ifndef __MMC_H
#define __MMC_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdint.h>
#include "diskio.h"

//===============================================================
//defines
#define _USE_WRITE    1       //!<1: Enable mmc_write function
#define _USE_IOCTL    1       //!<1: Enable mmc_ioctl function

//Disk Status Bits (DSTATUS)
#define STA_NOINIT    0x01    //!<Drive not initialized
#define STA_NODISK    0x02    //!<No medium in the drive
#define STA_PROTECT   0x04    //!<Write protected

//Command code for mmc_ioctrl function
//Generic command (used by FatFs)
#define CTRL_SYNC         0   //!<Flush disk cache (for write functions)
#define GET_SECTOR_COUNT  1   //!<Get media size (for only f_mkfs())
#define GET_SECTOR_SIZE   2   //!<Get sector size (for multiple sector size (_MAX_SS >= 1024))
#define GET_BLOCK_SIZE    3   //!<Get erase block size (for only f_mkfs())
#define CTRL_ERASE_SECTOR 4   //!<Force erased a block of sectors (for only _USE_ERASE)

//Generic command (not used by FatFs)
#define CTRL_POWER        5   //!<Get/Set power status
#define CTRL_LOCK         6   //!<Lock/Unlock media removal
#define CTRL_EJECT        7   //!<Eject media
#define CTRL_FORMAT       8   //!<Create physical format on the media

//MMC/SDC specific ioctl command
#define MMC_GET_TYPE      10  //!<Get card type
#define MMC_GET_CSD       11  //!<Get CSD
#define MMC_GET_CID       12  //!<Get CID
#define MMC_GET_OCR       13  //!<Get OCR
#define MMC_GET_SDSTAT    14  //!<Get SD status

//ATA/CF specific ioctl command
#define ATA_GET_REV       20  //!<Get F/W revision
#define ATA_GET_MODEL     21  //!<Get model name
#define ATA_GET_SN        22  //!<Get serial number

//MMC card type flags (MMC_GET_TYPE)
#define CT_MMC    0x01        //!<MMC version 3
#define CT_SD1    0x02        //!<SD version 1
#define CT_SD2    0x04        //!<SD version 2
#define CT_SDC    (CT_SD1|CT_SD2) //!<SD
#define CT_BLOCK  0x08        //!<Block addressing

//===============================================================
//typedefs

//===============================================================
//function prototypes
DSTATUS mmc_initialize (uint8_t drive);
DSTATUS mmc_status (uint8_t drive);
DRESULT mmc_read (uint8_t drive, uint8_t *buff, uint32_t sector, uint8_t count);
DRESULT mmc_write (uint8_t drive, const uint8_t *buff, uint32_t sector, uint8_t count);
DRESULT mmc_ioctl (uint8_t drive, uint8_t cmd, void *buff);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* __MMC_H */
