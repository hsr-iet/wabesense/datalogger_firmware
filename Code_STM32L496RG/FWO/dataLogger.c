/**
 ****************************************************************
 @file    dataLogger.c
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the complete dataLogger device.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-23
 ****************************************************************
 */

/** @addtogroup FWO
 * @{
 */

/** @addtogroup DATALOGGER
 * @{
 */

// --- Includes
#include "dataLogger.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "../DEF/GLOB_Types.h"
#include "../HWO/SYS.h"
#include "../HWO/DIG_IO.h"
#include "../HWO/A_IN.h"
#include "../HWO/BME280.h"
#include "../FWO/HMI.h"
#include "../FWO/dataManager.h"
#include "../FWO/COMM.h"
#include "../FWO/stringConverter.h"
#include "usbh_def.h"
#include "usbh_core.h"
#include "usb_host.h"
#include "usbh_msc.h"

// --- Defines
#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#define USR_BT_SHORT_PRESS_CNT_CMP      50
#define USR_BT_LONG_PRESS_CNT_CMP       1000

#define V_ANALOG_MEAS_LOOPS_MANUAL      4               //!< same number of measurements are done for vBAT (for manual based measurements)
#define V_ANALOG_MEAS_LOOPS_RTC         4               //!< same number of measurements are done for vBAT (for RTC based measurements)

//! external battery, connected on J1 to the datalogger
#define V_BAT_PA1_MIN                   ((float) 4.5f)  //!< From Hardware design (2.5V is bottom range on V_BAT input)
#define V_BAT_VBAT_MIN                  ((float) 2.5f)  //! VBAT (CR2032) minimal battery voltage

#define SLEEP_TIME_BETWEEN_ADC_MEAS_US  (100)

// --- Typedefs
typedef enum
{
  BATTERY_GOOD = 0,
  BATTERY_TO_REPLACE,
  BATTERY_EMPTY
} batteryState_t;

// --- Variables
//!constant configuration for ADC-Channel internal VBAT (CR2032)
const aIn_oversampling_t _VBAT_VBAT_OSR = A_IN_OVERSAMPLING_NONE;
const aIn_samplingTime_t _VBAT_VBAT_ST = A_IN_SAMPLETIME_24_5_CLK;

//!constant configuration for ADC-Channel IN6 V_BAT
const aIn_oversampling_t _VBAT_PA1_OSR = A_IN_OVERSAMPLING_2;
const aIn_samplingTime_t _VBAT_PA1_ST = A_IN_SAMPLETIME_24_5_CLK;

//!constant configuration for ADC-Channel IN8
const aIn_oversampling_t _AIN8_OSR = A_IN_OVERSAMPLING_16;
const aIn_samplingTime_t _AIN8_ST = A_IN_SAMPLETIME_640_5_CLK;

//!constant configuration for ADC-Channel IN9
const aIn_oversampling_t _AIN9_OSR = A_IN_OVERSAMPLING_16;
const aIn_samplingTime_t _AIN9_ST = A_IN_SAMPLETIME_640_5_CLK;

//!constant configuration for ADC-Channel IN10
const aIn_oversampling_t _AIN10_OSR = A_IN_OVERSAMPLING_16;
const aIn_samplingTime_t _AIN10_ST = A_IN_SAMPLETIME_640_5_CLK;

//!constant configuration for ADC-Channel IN11
const aIn_oversampling_t _AIN11_OSR = A_IN_OVERSAMPLING_16;
const aIn_samplingTime_t _AIN11_ST = A_IN_SAMPLETIME_640_5_CLK;

//!constant configuration for BME sensor
const bme280_config_t _BME_CFG_RTC =
{
  .osTemp = OS_16, .osPres = OS_16, .osHumi = OS_16, .standbyTime = TSB_1000_MS, .filter = FILTER_OFF
};

const bme280_config_t _BME_CFG_MANUAL =
{
  .osTemp = OS_16, .osPres = OS_16, .osHumi = OS_16, .standbyTime = TSB_125_MS, .filter = FILTER_OFF
};

const bme280_config_t _BME_CFG_STREAMING =
{
  .osTemp = OS_1, .osPres = OS_1, .osHumi = OS_1, .standbyTime = TSB_0_5_MS, .filter = FILTER_OFF
};

static bool _measStreamingEna = false;
extern ApplicationTypeDef Appli_state;
extern USBH_HandleTypeDef hUsbHostFS;

static dataForBootloader_t bootloaderData __attribute__ ((section ("dataToBootloaderFromUserApplication"))) =
{
  0
};

// --- Local Function Prototypes
static void _dataLogger_measAndCheckVBAT (const rtcDateTime_t *const pDateTime);

/**
 ****************************************************************
 @brief   Initialize the dataLogger module.
 @param   -
 @return  -
 ****************************************************************
 */
void dataLogger_init (void)
{
  rtcDateTime_t rtcDateTime;
  _measStreamingEna = false;
  fwVersion_t fwVersion;
  char additionalTxt[48];

  digIO_init();
  hmi_init();

  HAL_RTC_init();
  if (!__HAL_RTC_WAKEUPTIMER_GET_FLAG(&hrtc, RTC_FLAG_INITS))
  {
    MX_RTC_Init();
  }

  dataManager_init();

  //check if automatic measurement is requested to be on and it is not already on
  if (dataManager_cfgAutoMeasActiveGet())
  {
    if (!sys_wakeupTimerActive())
    {
      dataLogger_measAutoCycleActivate(true);   //activate it
    }
  }
  else
  {
    if (sys_wakeupTimerActive())
    {
      dataLogger_measAutoCycleActivate(false);  //deactivate it
    }
  }

  //check bootloader state
  if (bootloaderData.tag == BOOTLOADING_TAG_SUCCESS)
  {
    //mark this event in the log data
    sys_rtcDateTimeGet(&rtcDateTime);
    dataLogger_fwVersionGet(&fwVersion);
    sprintf(additionalTxt, "successful - new:%c.%c.%c", fwVersion.major, fwVersion.minor, fwVersion.patch);
    dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_FW_UPDATE, additionalTxt);

    bootloaderData.tag = BOOTLOADING_TAG_NONE;

    hmi_ledPatternSet(HMI_LED_PATTERN_BLINK_10HZ, RGLED_COLOR_YELLOW);

    if (dataLogger_storeToUsbStick())
    {
      //successful data exporting to USB-Stick
      hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_GREEN);
      sys_sleepForUs(1e6);  //1s delay
      hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
    }
    else
    {
      //error during data exporting to USB-Stick
      hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_RED);
      sys_sleepForUs(1e6);  //1s delay
      hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
    }
  }
}

/**
 ****************************************************************
 @brief   Get the version of the firmware of this dataLogger.
 @param   pfwVersion  Pointer to the firmware version struct information.
 @return  -
 ****************************************************************
 */
void dataLogger_fwVersionGet (fwVersion_t *pfwVersion)
{
  char fwVersionMajor[] = STR(FW_VERSION_MAJOR);
  char fwVersionMinor[] = STR(FW_VERSION_MINOR);
  char fwVersionPatch[] = STR(FW_VERSION_PATCH);

  pfwVersion->major = fwVersionMajor[0];
  pfwVersion->minor = fwVersionMinor[0];
  pfwVersion->patch = fwVersionPatch[0];
}

/**
 ****************************************************************
 @brief   Setup all parts for the system running with USB connection.
 @param   -
 @return  -
 ****************************************************************
 */
void dataLogger_setupWithUSB (void)
{
  //add a one second delay
  sys_sleepForUs(1e6);

  if (digIO_usbDetected())
  {
    //digIO_5VExtPwrOff();
    comm_init();
    //setup of measurement parts
    //(is done in first measurement loop itself - check is integrated in every initialization-function itself)
  }
}

/**
 ****************************************************************
 @brief   Handle the system with the USB connection.
 @param   -
 @return  Return true, USB is still connected, otherwise false.
 ****************************************************************
 */
bool dataLogger_handleWithUSB (void)
{
  static bool usrBt1PressedOld = false;
  static bool usrBt2PressedOld = false;
  static uint32_t usrBt1PressedCnt = 0;
  static uint32_t usrBt2PressedCnt = 0;
  bool input;

  //***************************************************//
  //****************** UserButton Events **************//
  //***************************************************//
  //check for userButton1 rising edge (pressed down event)
  input = digIO_usrBt1Pressed();
  if ((!usrBt1PressedOld) && input)
  {
    //rising edge
    usrBt1PressedCnt = 0;
  }
  else
  {
    if (input)
    {
      //button still pressed
      if (usrBt1PressedCnt < USR_BT_LONG_PRESS_CNT_CMP)
      {
        usrBt1PressedCnt++;
      }
    }
    else
    {
      //no button presses
      usrBt1PressedCnt = 0;
    }
  }
  usrBt1PressedOld = input;

  //check for userButton2 rising edge (pressed down event)
  input = digIO_usrBt2Pressed();
  if ((!usrBt2PressedOld) && input)
  {
    //rising edge
    usrBt2PressedCnt = 0;
  }
  else
  {
    if (input)
    {
      //button still pressed
      if (usrBt2PressedCnt < USR_BT_LONG_PRESS_CNT_CMP)
      {
        usrBt2PressedCnt++;
      }
    }
    else
    {
      //no button presses
      usrBt2PressedCnt = 0;
    }
  }
  usrBt2PressedOld = input;

  //do userButton1 handling
  if ((usrBt1PressedCnt >= USR_BT_SHORT_PRESS_CNT_CMP) && (usrBt1PressedCnt <= USR_BT_LONG_PRESS_CNT_CMP))
  {
    //check other button
    if (usrBt2PressedCnt)
    {
      //nothing to do currently, if both buttons are pressed

      //mark that this event is already handled
      usrBt1PressedCnt = USR_BT_LONG_PRESS_CNT_CMP + 1;
      usrBt2PressedCnt = USR_BT_LONG_PRESS_CNT_CMP + 1;
    }
    else
    {
      //only userButton1 pressed
      //do a manual next measurement cycle
      dataLogger_measCylce(MEAS_REQUEST_MANUAL);

      //mark that this event is already handled
      usrBt1PressedCnt = USR_BT_LONG_PRESS_CNT_CMP + 1;
      usrBt2PressedCnt = USR_BT_LONG_PRESS_CNT_CMP + 1;
    }
  }
  //do userButton2 handling
  if ((usrBt2PressedCnt >= USR_BT_SHORT_PRESS_CNT_CMP) && (usrBt2PressedCnt <= USR_BT_LONG_PRESS_CNT_CMP))
  {
    //check other button
    if (usrBt1PressedCnt)
    {
      //nothing to do currently, if both buttons are pressed

      //mark that this event is already handled
      usrBt1PressedCnt = USR_BT_LONG_PRESS_CNT_CMP + 1;
      usrBt2PressedCnt = USR_BT_LONG_PRESS_CNT_CMP + 1;
    }
    else
    {
      //only userButton2 pressed
      //do a system check
      dataLogger_sysCheck();

      //mark that this event is already handled
      usrBt1PressedCnt = USR_BT_LONG_PRESS_CNT_CMP + 1;
      usrBt2PressedCnt = USR_BT_LONG_PRESS_CNT_CMP + 1;
    }
  }

  //***************************************************//
  //******************** RTC Events *******************//
  //***************************************************//
  //check if wakeUpTimerEvent occurred --> do next measurement
  if (sys_wakeupTimerExpired())
  {
    //do a next measurement cycle
    dataLogger_measCylce(MEAS_REQUEST_RTC);
  }

  //***************************************************//
  //**************** USB-Communication ****************//
  //***************************************************//
  //USB-Communication message handling (parsing)
  comm_parseRx();
  //streaming of measurement data
  if (_measStreamingEna)
  {
    dataLogger_measCylce(MEAS_REQUEST_STREAMING_ONLY);
  }

  return digIO_usbDetected();
}

/**
 ****************************************************************
 @brief   Activate or deactivate the automatic measurement loop functionality.
 @param   active  Boolean parameter to indicate the new automatic measurement cycle activation
 @return  -
 ****************************************************************
 */
void dataLogger_measAutoCycleActivate (bool active)
{
  rtcDateTime_t rtcDateTime;

  sys_rtcDateTimeGet(&rtcDateTime);

  dataManager_cfgAutoMeasActiveSet(active);

  if (active)
  {
    //save the timeStamp of this autoMeas event in the log
    dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_AUTOMEAS, "activate");
  }
  else
  {
    //save the timeStamp of this autoMeas event in the log
    dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_AUTOMEAS, "deactivate");
  }

  //update with current measurement interval time
  dataLogger_measIntervalSet(dataManager_cfgMeasIntervalGet());
}

/**
 ****************************************************************
 @brief   Set the to be used measurement interval in minutes.
 @param   minutes The to be used measurement interval (1...1092).
 @return  -
 ****************************************************************
 */
void dataLogger_measIntervalSet (uint32_t minutes)
{
  rtcDateTime_t rtcDateTime;
  uint32_t wakeUpTime = minutes * 60;
  char logInfo[32];

  if (minutes == 0)
  {
    wakeUpTime = 1; //every second
  }

  if (dataManager_cfgAutoMeasActiveGet())
  {
    //start a new interval time based measurement cycle
    sys_wakeupTimerSet(wakeUpTime);
  }
  else
  {
    sys_wakeupTimerSet(0);    //disable
  }

  sys_rtcDateTimeGet(&rtcDateTime);
  //save the timeStamp of this autoMeas event in the log
  snprintf(logInfo, sizeof(logInfo), "interval [min]: %u", (unsigned int) minutes);
  dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_AUTOMEAS, logInfo);
}
/**
 ****************************************************************
 @brief   Do a complete system check of the dataLogger
 @param   -
 @return  Returns true, if the system check was successful, otherwise false
 ****************************************************************
 */
bool dataLogger_sysCheck (void)
{
  float vBat;
  float measSumValue1;
  bool success = false;
  uint32_t measSumIdx;
  uint32_t nominalCellVoltage;
  uint32_t nrOfCells;
  float vNominal;
  float vEmpty;
  float vGood;
  batteryState_t batteryState_external = BATTERY_EMPTY;
  bme280_measData_t bme280MeasData;
  bool measDataAvailable;
  rtcDateTime_t rtcDateTime;

  dataManager_cfgBatterySettingGet(&nominalCellVoltage, &nrOfCells);

  aIN_init();

  if (nrOfCells)
  {
    vNominal = ((float) (nrOfCells * nominalCellVoltage)) * 0.001f;

    //sample and get battery voltage (connected to J1)
    measSumValue1 = 0.0f;
    for (measSumIdx = 0; measSumIdx < V_ANALOG_MEAS_LOOPS_MANUAL; measSumIdx++)
    {
      measSumValue1 += aIN_sampleAndGetVBat(BAT_SOURCE_PA1, _VBAT_PA1_OSR, _VBAT_PA1_ST);
      sys_sleepForUs(SLEEP_TIME_BETWEEN_ADC_MEAS_US);
    }
    vBat = measSumValue1 / (float) measSumIdx;

    //check depending on the battery type the fill level of it
    if (nominalCellVoltage < 2000)
    {
      //Alkali Battery (1.5V nominal, 0.9V end voltage)
      vEmpty = vNominal * 0.75f;  //1.125V / cell
      vGood = vNominal * 0.9f;
    }
    else if (nominalCellVoltage < 4000)
    {
      //Lithium Battery (3.6V nominal, 3.0V end voltage)
      vEmpty = vNominal * 0.85f;  //3.06V / cell
      vGood = vNominal * 0.95f;
    }
    else
    {
      //for example plumbum Battery
      vEmpty = vNominal * 0.85f;
      vGood = vNominal * 0.9f;
    }

    //check battery against voltage thresholds
    if (vBat < V_BAT_PA1_MIN)
    {
      batteryState_external = BATTERY_EMPTY;
    }
    else
    {
      if (vBat > vGood)
      {
        batteryState_external = BATTERY_GOOD;
      }
      else if (vBat > vEmpty)
      {
        batteryState_external = BATTERY_TO_REPLACE;
      }
      else
      {
        batteryState_external = BATTERY_EMPTY;
      }
    }
  }
  else
  {
    batteryState_external = BATTERY_GOOD;
  }

  sys_rtcDateTimeGet(&rtcDateTime);
  //VBAT (CR2032) battery measurement & check
  _dataLogger_measAndCheckVBAT(&rtcDateTime);

  //check onBoard sensor and check limitations
  //environmental sensor
  bme280_init(_BME_CFG_MANUAL);
  //do a single measurement
  bme280_modeSet(MODE_FORCED);
  //wait until measurement is done
  sys_sleepForUs(bme280_measTimeGet());
  measDataAvailable = bme280_dataGet(&bme280MeasData);

  if (measDataAvailable)
  {
    //check measurement data for plausibility

    //temperature in range of -10.0 ... +50.0°C
    if ((bme280MeasData.temperature < -10.0f) || (bme280MeasData.temperature > 50.0f))
    {
      success = false;
    }
    //pressure in range of 30.0hPa ... 1100.0hPa
    else if ((bme280MeasData.pressure < 3000.0f) || (bme280MeasData.pressure > 110000.0f))
    {
      success = false;
    }
    //humidity in range of 0 ... 100%
    else if ((bme280MeasData.humidity < 0.0f) || (bme280MeasData.humidity > 100.0f))
    {
      success = false;
    }
    else
    {
      success = true;
    }
  }
  else
  {
    //log BME280 sensor Timeout
    dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_BME_ERROR, "Timeout during sysCheck");

    success = false;
  }

  if (success)
  {
    //successful
    switch (batteryState_external)
    {
      case BATTERY_GOOD:
        hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_GREEN);
        break;
      case BATTERY_TO_REPLACE:
        hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_YELLOW);
        break;
      default:
      case BATTERY_EMPTY:
        hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_RED);
        break;
    }
  }
  else
  {
    //error
    hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_RED);
  }
  sys_sleepForUs(1e6);  //1s delay
  hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
  return success;
}

/**
 ****************************************************************
 @brief   Enter the USB DFU Bootloader, that is part of the microcontroller itself.
 @param   -
 @return  -
 ****************************************************************
 */
void dataLogger_firmwareUpdateOverUsbDFU (void)
{
  rtcDateTime_t rtcDateTime;
  fwVersion_t fwVersion;
  char additionalTxt[32];

  hmi_ledPatternSet(HMI_LED_PATTERN_BLINK_10HZ, RGLED_COLOR_RED);

  sys_rtcDateTimeGet(&rtcDateTime);
  dataLogger_fwVersionGet(&fwVersion);
  sprintf(additionalTxt, "USB - old:%c.%c.%c", fwVersion.major, fwVersion.minor, fwVersion.patch);
  dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_FW_UPDATE, additionalTxt);

  sys_bootloaderUsbDfuEnter();
}

/**
 ****************************************************************
 @brief   Check if a USB device is detected (USB-Stick).
 @note    If a stick is inserted, the complete detection takes around 690ms!
 @param   -
 @return  Return true, if a USB-Device is detected.
 ****************************************************************
 */
bool dataLogger_checkUSBDeviceInserted (void)
{
  const uint32_t deltaTickMax = 1000; //maximum tick timeout (equals 1000ms)
  bool usbDeviceDetected = false;
  uint32_t sysTime;
  uint32_t deltaTick = 0;

  //change to USB-Host
  digIO_otgDeviceModeSet(false);

  if (!digIO_usbDetected())
  {
    //start as host
    MX_USB_HOST_Init();
    sys_sysTickTickGet(&sysTime);
    while ((USBH_MSC_IsReady(&hUsbHostFS) == 0) && (deltaTick < deltaTickMax))
    {
      MX_USB_HOST_Process();
      sys_sysTickTickDeltaGet(sysTime, &deltaTick);
    }
    if (USBH_Stop(&hUsbHostFS) != USBH_OK)
    {
      Error_Handler();
    }
    if (USBH_DeInit(&hUsbHostFS) != USBH_OK)
    {
      Error_Handler();
    }

    if (deltaTick < deltaTickMax)
    {
      usbDeviceDetected = true;
    }
  }

  //change to USB-Device
  digIO_otgDeviceModeSet(true);
  return usbDeviceDetected;
}

/**
 ****************************************************************
 @brief   Check if an other firmware is available for this device.
 @param   pPathAndFileName  Pointer to the character array with the
 *        path and file name of the firmware detected.
 @return  True, if a other firmware file was located on the device
 ****************************************************************
 */
bool dataLogger_checkForFirmwareOnUsbStick (char *pPathAndFileName)
{
  if (dataManager_usbStickCheckForNewFirmwareFile(pPathAndFileName))
  {
    return true;
  }
  else
  {
    return false; //no new firmware file detected
  }
}

/**
 ****************************************************************
 @brief   Check if an other firmware is available for this device.
 *        Execute the update, if a version is available.
 @param   pPathAndFileName  Pointer to the character array with the
 *        path and file name of the firmware detected.
 @return  -
 ****************************************************************
 */
void dataLogger_firmwareUpdateByUsbStick (char *pPathAndFileName)
{
  rtcDateTime_t rtcDateTime;
  fwVersion_t fwVersion;
  char additionalTxt[48];

  if (pPathAndFileName)
  {
    bootloaderData.tag = BOOTLOADING_TAG_REQUEST;
    strcpy(bootloaderData.pathAndFileName, pPathAndFileName);
    bootloaderData.len = strlen(pPathAndFileName);

    sys_rtcDateTimeGet(&rtcDateTime);
    dataLogger_fwVersionGet(&fwVersion);
    sprintf(additionalTxt, "USBStick - actual:%c.%c.%c", fwVersion.major, fwVersion.minor, fwVersion.patch);
    dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_FW_UPDATE, additionalTxt);

    sys_bootloaderAppEnter();
  }
}

/**
 ****************************************************************
 @brief   Activate or deactivate measurement streaming mode
 @param   active  If true, system will do measurements and stream data out over USB
 @return  -
 ****************************************************************
 */
void dataLogger_measStreaming (bool active)
{
  _measStreamingEna = active;

  if (_measStreamingEna)
  {
    //power up the needed parts
    //turn on switched 17V powerSupply (temperature sensor needs 200ms after powerOn)
    digIO_17VPwrOn(DIG_IO_17VUNIT_EXT_SENSOR1);
    sys_sleepForUs(200e3);
  }
  else
  {
    //turn off 17V powerSupply
    digIO_17VPwrReqOff(DIG_IO_17VUNIT_EXT_SENSOR1);

    //end of data streaming
    hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
  }
}

/**
 ****************************************************************
 @brief   Do a measurement on all active sensor interfaces and store
 *        the resulting data on local FLASH.
 @note    The needed initialization functions are called as well
 @param   requestType One of the possible request types for system measurements
 @return  -
 ****************************************************************
 */
void dataLogger_measCylce (measRequest_t requestType)
{
  bool success;
  bool measDataAvailable;
  dataManager_dataPoint_t dataPoint;
  float vBat;
  float measSumValue1;
  float measSumValue2;
  float measSumValue3;
  float measSumValue4;
  float measSumValue5;
  uint32_t measSumIdx;
  uint32_t measNrOfLoops = 1;
  uint32_t deltaTimeMs = 0;
  rtcDateTime_t rtcDateTime_17VPowerUp;
  rtcDateTime_t rtcDateTime;

  if (!_measStreamingEna)
  {
    //turn on switched 17V powerSupply (temperature sensor needs 200ms after powerOn)
    digIO_17VPwrOn(DIG_IO_17VUNIT_EXT_SENSOR1);
    //get RTC time, used for delta time calculation
    sys_rtcDateTimeGet(&rtcDateTime_17VPowerUp);

    if (requestType == MEAS_REQUEST_MANUAL)
    {
      measNrOfLoops = V_ANALOG_MEAS_LOOPS_MANUAL;
    }
    else
    {
      measNrOfLoops = V_ANALOG_MEAS_LOOPS_RTC;
    }
  }

  //Signalizes measurement active
  hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_YELLOW);

  //environmental sensor
  if (!_measStreamingEna)
  {
    if (requestType == MEAS_REQUEST_MANUAL)
    {
      bme280_init(_BME_CFG_MANUAL);
    }
    else
    {
      bme280_init(_BME_CFG_RTC);
    }
  }
  else
  {
    bme280_init(_BME_CFG_STREAMING);
  }
  //do a single measurement
  bme280_modeSet(MODE_FORCED);
  //get RTC time
  sys_rtcDateTimeGet(&dataPoint.timeStamp);

  aIN_init();

  //sample and get VBAT (CR2032) battery voltage (only once a month)
  if (dataPoint.timeStamp.month != sys_backupRegisterGet(SYS_BACKUP_REG_RTC_BAT_CHECK_MONTH))
  {
    _dataLogger_measAndCheckVBAT(&dataPoint.timeStamp);

    sys_backupRegisterSet(SYS_BACKUP_REG_RTC_BAT_CHECK_MONTH, dataPoint.timeStamp.month);
  }

  //sample and get battery voltage
  if (!_measStreamingEna)
  {
    measSumValue1 = 0.0f;
    for (measSumIdx = 0; measSumIdx < measNrOfLoops; measSumIdx++)
    {
      measSumValue1 += aIN_sampleAndGetVBat(BAT_SOURCE_PA1, _VBAT_PA1_OSR, _VBAT_PA1_ST);

      sys_sleepForUs(SLEEP_TIME_BETWEEN_ADC_MEAS_US);
    }
    vBat = measSumValue1 / (float) measSumIdx;

    //check if 17V powerSupply is active for at least 200ms
    //get RTC time, used for delta time calculation
    sys_rtcDateTimeGet(&rtcDateTime);
    if (rtcDateTime.ms >= rtcDateTime_17VPowerUp.ms)
    {
      deltaTimeMs = rtcDateTime.ms - rtcDateTime_17VPowerUp.ms;
    }
    else
    {
      //a time change in seconds
      deltaTimeMs = (1000 - rtcDateTime_17VPowerUp.ms) + rtcDateTime.ms;
    }
    if (deltaTimeMs < 200)
    {
      sys_sleepForUs((200 - deltaTimeMs) * 1000);
    }

    measSumValue1 = 0.0f;
    measSumValue2 = 0.0f;
    measSumValue3 = 0.0f;
    measSumValue4 = 0.0f;
    measSumValue5 = 0.0f;
    for (measSumIdx = 0; measSumIdx < measNrOfLoops; measSumIdx++)
    {
      //sample and get analogInput8 voltage
      measSumValue1 += aIN_sampleAndGet(PA3_ADC_IN8, _AIN8_OSR, _AIN8_ST);
      //sample and get analogInput9 voltage
      measSumValue2 += aIN_sampleAndGet(PA4_ADC_IN9, _AIN9_OSR, _AIN9_ST);
      //sample and get analogInput10 voltage
      measSumValue3 += aIN_sampleAndGet(PA5_ADC_IN10, _AIN10_OSR, _AIN10_ST);
      //sample and get analogInput11 voltage
      measSumValue4 += aIN_sampleAndGet(PA6_ADC_IN11, _AIN11_OSR, _AIN11_ST);
      //sample and get battery voltage
      measSumValue5 += aIN_sampleAndGetVBat(BAT_SOURCE_PA1, _VBAT_PA1_OSR, _VBAT_PA1_ST);

      sys_sleepForUs(SLEEP_TIME_BETWEEN_ADC_MEAS_US);
    }
    dataPoint.adc_in8 = measSumValue1 / (float) measSumIdx;
    dataPoint.adc_in9 = measSumValue2 / (float) measSumIdx;
    dataPoint.adc_in10 = measSumValue3 / (float) measSumIdx;
    dataPoint.adc_in11 = measSumValue4 / (float) measSumIdx;
    vBat += (measSumValue5 / (float) measSumIdx);
    dataPoint.vBat = (vBat * 0.5f);

    //turn off 17V powerSupply
    digIO_17VPwrReqOff(DIG_IO_17VUNIT_EXT_SENSOR1);
  }
  else  //part if device is in streaming mode
  {
    vBat = aIN_sampleAndGetVBat(BAT_SOURCE_PA1, _VBAT_PA1_OSR, _VBAT_PA1_ST);

    //sample and get analogInput8 voltage
    dataPoint.adc_in8 = aIN_sampleAndGet(PA3_ADC_IN8, _AIN8_OSR, _AIN8_ST);
    //sample and get analogInput9 voltage
    dataPoint.adc_in9 = aIN_sampleAndGet(PA4_ADC_IN9, _AIN9_OSR, _AIN9_ST);
    //sample and get analogInput10 voltage
    dataPoint.adc_in10 = aIN_sampleAndGet(PA5_ADC_IN10, _AIN10_OSR, _AIN10_ST);
    //sample and get analogInput11 voltage
    dataPoint.adc_in11 = aIN_sampleAndGet(PA6_ADC_IN11, _AIN11_OSR, _AIN11_ST);

    //sample and get battery voltage
    vBat += aIN_sampleAndGetVBat(BAT_SOURCE_PA1, _VBAT_PA1_OSR, _VBAT_PA1_ST);
    dataPoint.vBat = vBat * 0.5f;
  }

  //check if needed measurement time for BME280 sensor is okay, otherwise wait
  sys_rtcDateTimeGet(&rtcDateTime);
  if (rtcDateTime.ms >= dataPoint.timeStamp.ms)
  {
    deltaTimeMs = rtcDateTime.ms - dataPoint.timeStamp.ms;
  }
  else
  {
    //a time change in seconds
    deltaTimeMs = (1000 - dataPoint.timeStamp.ms) + rtcDateTime.ms;
  }
  if ((deltaTimeMs * 1000) < bme280_measTimeGet())
  {
    //wait until measurement is done
    sys_sleepForUs(bme280_measTimeGet() - (deltaTimeMs * 1000));
  }
  measDataAvailable = bme280_dataGet(&dataPoint.bme280);
  if (!measDataAvailable)
  {
    //log BME280 sensor Timeout
    dataManager_flashLogEvent(&dataPoint.timeStamp, LOG_EVT_BME_ERROR, "Timeout");
  }

  switch (requestType)
  {
    default:
      assert_param(0);
      break;
    case MEAS_REQUEST_MANUAL:
      dataPoint.timeBasedMeas = false;

      //store new measurement dataPoint
      success = dataManager_flashNextMeasDataPointStore(&dataPoint);

      if ((!success) || (!measDataAvailable))  //shown up error
      {
        //only signalizes on RGB LED, if a manual measurement was requested
        //error
        hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_RED);

        sys_sleepForUs(10e3);  //10ms delay
      }
      hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
      break;
    case MEAS_REQUEST_RTC:
      dataPoint.timeBasedMeas = true;

      //store new measurement dataPoint
      success = dataManager_flashNextMeasDataPointStore(&dataPoint);

      if ((!success) || (!measDataAvailable))  //shown up error
      {
        //only signalizes on RGB LED, if a manual measurement was requested
        //error
        hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_RED);

        sys_sleepForUs(10e3);  //10ms delay
      }
      hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
      break;
    case MEAS_REQUEST_STREAMING_ONLY:
      comm_streamMeasDataPoint(&dataPoint);
      break;
  }
}

/**
 ****************************************************************
 @brief   Read out all valid data from the FLASH and store them on the USB-Stick.
 @param   -
 @return  True on success, otherwise false
 ****************************************************************
 */
bool dataLogger_storeToUsbStick (void)
{
  //store all measurement data to USB-Stick
  if (dataManager_usbStickDataStore())
  {
    //successful
    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   First clear the configuration on the FLASH and than store the current
 *        configuration from RAM to the FLASH configuration page.
 *        Set RAM values back to default configuration and then read it back
 *        from FLASH, so complete loop is closed.
 @param   defaultValues Boolean flag, to indicate if the default values
 *                      are stored or user specific (over serial port).
 @return  -
 ****************************************************************
 */
void dataLogger_flashConfigStore (bool defaultValues)
{
  rtcDateTime_t rtcDateTime;

  hmi_ledPatternSet(HMI_LED_PATTERN_BLINK_10HZ, RGLED_COLOR_YELLOW);

  dataManager_flashClearCfg();
  dataManager_cfgStoreToFlash();

  //save the timeStamp of this configuration store action in the log
  sys_rtcDateTimeGet(&rtcDateTime);
  if (defaultValues)
  {
    dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_CONFIGSTORE, "- default");
  }
  else
  {
    dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_CONFIGSTORE, "- new config over serial Interface");
  }

  hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
}

/**
 ****************************************************************
 @brief   Clear the data and history informations on the FLASH.
 @param   -
 @return  -
 ****************************************************************
 */
void dataLogger_flashClearDataAndHistory (void)
{
  rtcDateTime_t rtcDateTime;

  hmi_ledPatternSet(HMI_LED_PATTERN_BLINK_10HZ, RGLED_COLOR_YELLOW);

  dataManager_flashClearMeasDataAndHistory();

  //save the timeStamp of this data & history clear action in the log
  sys_rtcDateTimeGet(&rtcDateTime);
  dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_CLEARDATA, "");

  hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
}

/**
 ****************************************************************
 @brief   Internal function to measure and check the VBAT voltage (CR2032 battery)
 @param   pDateTime  Pointer to the actual date & time information
 @return  -
 ****************************************************************
 */
static void _dataLogger_measAndCheckVBAT (const rtcDateTime_t *const pDateTime)
{
  float measSumValue1;
  float vBat;
  uint32_t measSumIdx;
  char additionalTxt[48];
  uint32_t float_strLen;
  uint32_t strLen;

  measSumValue1 = 0.0f;
  for (measSumIdx = 0; measSumIdx < V_ANALOG_MEAS_LOOPS_MANUAL; measSumIdx++)
  {
    measSumValue1 += aIN_sampleAndGetVBat(BAT_SOURCE_VBAT, _VBAT_VBAT_OSR, _VBAT_VBAT_ST);
    sys_sleepForUs(SLEEP_TIME_BETWEEN_ADC_MEAS_US);
  }
  vBat = measSumValue1 / (float) measSumIdx;
  //check battery against voltage thresholds
  if (vBat < V_BAT_VBAT_MIN)
  {
    if (vBat < 1.0f)
    {
      snprintf (&additionalTxt[0], 48, "not inserted or empty: ");

    }
    else
    {
      snprintf (&additionalTxt[0], 48, "low: ");
    }

    strLen = strlen(additionalTxt);
    //convert floating point numbers to strings (nothing, if NaN)
    strConv_floatToString_6(vBat, (char*) &additionalTxt[strLen], &float_strLen);
    additionalTxt[strLen + float_strLen] = 0x00;  //terminate string

    //add a VBAT log on dataManager module
    dataManager_flashLogEvent(pDateTime, LOG_EVT_RTCBATTERY, additionalTxt);
  }
}
/**
 * @}
 */

/**
 * @}
 */
