/**
 ****************************************************************
 @file    test_flash.h
 ****************************************************************
 @brief   This module implements some tests for the external FLASH memory.
 *        The first memory block is erased, then erase is checked
 *        Some data are written to the first 64 bytes, then compared with the read back values
 *        The first memory block is erased & checked again
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-16
 ****************************************************************
 */
#ifndef TEST_FLASH_H_
#define TEST_FLASH_H_

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

// --- Includes

// --- Defines

// --- Typedefs

// --- Function Prototypes
void test_flash (void);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* TEST_FLASH_H_ */
