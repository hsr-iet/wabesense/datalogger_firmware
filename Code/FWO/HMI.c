/**
 ****************************************************************
 @file    HMI.c
 ****************************************************************
 @brief   This module offers a set of functions to handle the
 *        Human Machine visible interface.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-12-09
 ****************************************************************
 */

/** @addtogroup FWO
 * @{
 */

/** @addtogroup HMI
 * @{
 */

// --- Includes
#include "HMI.h"

#include <stdbool.h>
#include <stdint.h>
#include "../HWO/SYS.h"
#include "../HWO/RGB_LED.h"

// --- Defines

// --- Typedefs
//! data type definition for possible color signalization on the RGB-LED
typedef struct
{
  ledPattern_t pattern; //!< one of the possible patterns
  rgbColor_t color;     //!< The selected color (from enumeration)
  bool onState;         //!< Flag to indicate if the LED is currently ON (true) or OFF (false)
} _led_Pattern_t;

// --- Variables
static _led_Pattern_t _rgbLED;

// --- Local Function Prototypes
static void _hmi_ledTick (void);

/**
 ****************************************************************
 @brief   Initialize the hmi rgb LED.
 @param   -
 @return  -
 ****************************************************************
 */
void hmi_init (void)
{
  _rgbLED.pattern = HMI_LED_PATTERN_OFF;
  _rgbLED.color = RGBLED_COLOR_NONE;
  _rgbLED.onState = false;

  rgbLED_init();
  rgbLED_luminositySet(10);
  rgbLED_colorSet(_rgbLED.color);

  sys_sysTickRegCallbackFct(_hmi_ledTick);
}

/**
 ****************************************************************
 @brief   RGB LED pattern set function
 @param   pattern One of the possible patterns from enumeration
 @param   color   One of the possible colors from enumeration
 @return  -
 ****************************************************************
 */
void hmi_ledPatternSet (ledPattern_t pattern, rgbColor_t color)
{
  //disable sysTick interrupts

  _rgbLED.pattern = HMI_LED_PATTERN_OFF;
  switch (pattern)
  {
    default:
    case HMI_LED_PATTERN_OFF:
      _rgbLED.color = RGBLED_COLOR_NONE;
      pattern = HMI_LED_PATTERN_OFF;
      break;
    case HMI_LED_PATTERN_ON:
      _rgbLED.color = color;
      break;
    case HMI_LED_PATTERN_BLINK_10HZ:
      _rgbLED.color = color;
      break;
  }
  _rgbLED.onState = true;
  rgbLED_colorSet(_rgbLED.color);
  _rgbLED.pattern = pattern;

  //enable sysTick interrupts
}

/**
 ****************************************************************
 @brief   LED Ticker Routine, called by sysTick interrupt (every 100 milliseconds)
 @param   -
 @return  Error-Code from OBJ
 ****************************************************************
 */
static void _hmi_ledTick (void)
{
  switch (_rgbLED.pattern)
  {
    default:
    case HMI_LED_PATTERN_OFF:
    case HMI_LED_PATTERN_ON:
      //nothing to do
      break;
    case HMI_LED_PATTERN_BLINK_10HZ:
      //toggle ON/OFF state
      _rgbLED.onState = !_rgbLED.onState;
      if (_rgbLED.onState)
      {
        rgbLED_colorSet(_rgbLED.color);
      }
      else
      {
        rgbLED_colorSet(RGBLED_COLOR_NONE);
      }
      break;
  }
}

/**
 * @}
 */

/**
 * @}
 */
