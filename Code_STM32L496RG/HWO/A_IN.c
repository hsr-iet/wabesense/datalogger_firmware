/**
 ****************************************************************
 @file    A_IN.c
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        all the available analog input pins on the data-logger.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-10
 ****************************************************************
 */

/** @addtogroup HWO
 * @{
 */

/** @addtogroup IO
 * @{
 */

// --- Includes
#include "A_IN.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include "adc.h"
#include "SYS.h"
#include "DIG_IO.h"

// --- Defines
#define VDDA_MEAS_LOOPS                 4
#define SLEEP_TIME_BETWEEN_ADC_MEAS_US  (100)

// --- Typedefs

// --- Variables
static bool _initDone = false;
static bool adcMeasDone = false;
static uint32_t adcResult = 0;
static float vdda;

// --- Local Function Prototypes
static uint32_t _aIN_vddaMeas (void);
static uint32_t _aIN_calcSampleAndConversationTime_us (aIn_oversampling_t osr, aIn_samplingTime_t samplingTime);

/**
 ****************************************************************
 @brief   Initialize the analog inputs, measure the VDDA voltage and calculate
 *        the corresponding voltage in mV (with factory calibration constants).
 @param   -
 @return  -
 ****************************************************************
 */
void aIN_init (void)
{
  HAL_StatusTypeDef status;
  uint32_t measSumIdx;
  uint32_t vddaMeasSumValue;

  if (!_initDone)
  {
    //initialize the ADC unit
    MX_ADC1_Init();

    //stop ADC and disable the ADC peripheral
    status = HAL_ADC_Stop_IT(&hadc1);
    assert_param(status == HAL_OK);

    _initDone = true;
  }

  //measure the VDDA voltage
  vddaMeasSumValue = 0;
  for (measSumIdx = 0; measSumIdx < VDDA_MEAS_LOOPS; measSumIdx++)
  {
    vddaMeasSumValue += _aIN_vddaMeas();
    sys_sleepForUs(SLEEP_TIME_BETWEEN_ADC_MEAS_US);
  }
  vdda = ((float) vddaMeasSumValue) / (1000.0f * ((float) VDDA_MEAS_LOOPS));
}

/**
 ****************************************************************
 @brief   Sample and get the resulting battery/powerSupply voltage (on pin PA1)
 *        or the voltage of the CR2032 battery.
 @param   source  One of the possible battery monitoring sources
 @param   osr   The to be used oversampling ratio from enumeration
 @param   samplingTime  The to be used sampling time for each sample
 @return  The resulting measured analog voltage [V] (0 .. 30,36V)
 ****************************************************************
 */
float aIN_sampleAndGetVBat (aIn_batSrc_t source, aIn_oversampling_t osr, aIn_samplingTime_t samplingTime)
{
  const float res_up_pa1 = 8.2e3f;   //upper resistor
  const float res_down_pa1 = 1.0e3f; //lower resistor
  float voltage;
  uint32_t sampleTime_us;
  ADC_ChannelConfTypeDef sConfig =
  {
    0
  };

  if (osr == A_IN_OVERSAMPLING_NONE)
  {
    hadc1.Init.OversamplingMode = DISABLE;
    hadc1.Init.Oversampling.RightBitShift = ADC_RIGHTBITSHIFT_NONE;
  }
  else
  {
    hadc1.Init.OversamplingMode = ENABLE;
    hadc1.Init.Oversampling.Ratio = (osr - 1) << 2;
    hadc1.Init.Oversampling.RightBitShift = osr << 5;
  }
  assert_param(HAL_ADC_Init(&hadc1) == HAL_OK);

  //configure regular channel
  if (source == BAT_SOURCE_PA1)
  {
    //activate supply measurement path
    digIO_supplyMeasPathEna();

    sConfig.Channel = ADC_CHANNEL_6;
  }
  else if (source == BAT_SOURCE_VBAT)
  {
    sConfig.Channel = ADC_CHANNEL_VBAT;
  }
  else
  {
    assert_param(0);
  }
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = samplingTime;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  assert_param(HAL_ADC_ConfigChannel(&hadc1, &sConfig) == HAL_OK);

  sampleTime_us = _aIN_calcSampleAndConversationTime_us(osr, samplingTime);

  adcMeasDone = false;
  HAL_ADC_Start_IT(&hadc1); //start measurement
  sys_sleepForUs(sampleTime_us);  //set system to sleep for the sampling time
  while (!adcMeasDone);

  voltage = adcResult * (vdda / 4095.0f);
  if (source == BAT_SOURCE_PA1)
  {
    //deactivate supply measurement path
    digIO_supplyMeasPathDis();

    voltage = voltage * ((res_down_pa1 + res_up_pa1) / res_down_pa1);
    //0.3V comes additional from Q2
    voltage += 0.3f;
  }
  else if (source == BAT_SOURCE_VBAT)
  {
    voltage *= 3.0f;  //internal scaling, to meet all time the VDDA limitations
  }
  else
  {
    assert_param(0);
  }

  return voltage;
}

/**
 ****************************************************************
 @brief   Sample and get the resulting USB_CC voltage (on pin PC2 or PC3).
 @param   source  One of the possible USB_CC monitoring signals
 @return  The resulting measured analog voltage [V] (0 .. 3,3V)
 ****************************************************************
 */
float aIN_sampleAndGetVUsbCc (aIn_usbCc_t source)
{
  const aIn_oversampling_t osr = A_IN_OVERSAMPLING_NONE;
  const aIn_samplingTime_t samplingTime = A_IN_SAMPLETIME_24_5_CLK;
  float voltage;
  uint32_t sampleTime_us;
  ADC_ChannelConfTypeDef sConfig =
  {
    0
  };

  if (osr == A_IN_OVERSAMPLING_NONE)
  {
    hadc1.Init.OversamplingMode = DISABLE;
    hadc1.Init.Oversampling.RightBitShift = ADC_RIGHTBITSHIFT_NONE;
  }
  else
  {
    hadc1.Init.OversamplingMode = ENABLE;
    hadc1.Init.Oversampling.Ratio = (osr - 1) << 2;
    hadc1.Init.Oversampling.RightBitShift = osr << 5;
  }

  //configure regular channel
  if (source == USB_CC1)
  {
    sConfig.Channel = ADC_CHANNEL_3;
  }
  else if (source == USB_CC2)
  {
    sConfig.Channel = ADC_CHANNEL_4;
  }
  else
  {
    assert_param(0);
  }

  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = samplingTime;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  assert_param(HAL_ADC_ConfigChannel(&hadc1, &sConfig) == HAL_OK);

  sampleTime_us = _aIN_calcSampleAndConversationTime_us(osr, samplingTime);

  adcMeasDone = false;
  HAL_ADC_Start_IT(&hadc1); //start measurement
  sys_sleepForUs(sampleTime_us);  //set system to sleep for the sampling time
  while (!adcMeasDone);

  voltage = adcResult * (vdda / 4095.0f);
  return voltage;
}

/**
 ****************************************************************
 @brief   Sample and get the resulting analog to digital converted value.
 @param   input One of the possible analog inputs.
 @param   osr   The to be used oversampling ratio from enumeration
 @param   samplingTime  The to be used sampling time for each sample
 @return  The resulting measured voltage.
 ****************************************************************
 */
float aIN_sampleAndGet (aIn_pin_t input, aIn_oversampling_t osr, aIn_samplingTime_t samplingTime)
{
  const float res_up = 8.2e3;     //upper resistor
  const float res_down = 15.0e3;  //lower resistor
  float voltage;
  uint32_t sampleTime_us;
  ADC_ChannelConfTypeDef sConfig =
  {
    0
  };

  if (osr == A_IN_OVERSAMPLING_NONE)
  {
    hadc1.Init.OversamplingMode = DISABLE;
    hadc1.Init.Oversampling.RightBitShift = ADC_RIGHTBITSHIFT_NONE;
  }
  else
  {
    hadc1.Init.OversamplingMode = ENABLE;
    hadc1.Init.Oversampling.Ratio = (osr - 1) << 2;
    hadc1.Init.Oversampling.RightBitShift = osr << 5;
  }
  assert_param(HAL_ADC_Init(&hadc1) == HAL_OK);

  //configure regular channel
  switch (input)
  {
    default:
      assert_param(0);
      break;
    case PA3_ADC_IN8:
      sConfig.Channel = ADC_CHANNEL_8;
      break;
    case PA4_ADC_IN9:
      sConfig.Channel = ADC_CHANNEL_9;
      break;
    case PA5_ADC_IN10:
      sConfig.Channel = ADC_CHANNEL_10;
      break;
    case PA6_ADC_IN11:
      sConfig.Channel = ADC_CHANNEL_11;
      break;
  }
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = samplingTime;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  assert_param(HAL_ADC_ConfigChannel(&hadc1, &sConfig) == HAL_OK);

  adcMeasDone = false;
  HAL_ADC_Start_IT(&hadc1); //start measurement

  sampleTime_us = _aIN_calcSampleAndConversationTime_us(osr, samplingTime);

  sys_sleepForUs(sampleTime_us);  //set system to sleep for the sampling time
  while (!adcMeasDone);

  voltage = adcResult * (vdda / 4095.0f);
  voltage = voltage * ((res_down + res_up) / res_down);
  return voltage;
}

/**
 ****************************************************************
 @brief   Calculate the VDDA voltage depending on the internal reference
 *        which the factory calibration values (ADC_CHANNEL_VREFINT).
 @param   -
 @return  Return the resulting VDDA voltage in millivolts.
 ****************************************************************
 */
static uint32_t _aIN_vddaMeas (void)
{
  const aIn_samplingTime_t samplingTime = A_IN_SAMPLETIME_640_5_CLK;
  uint32_t sampleTime_us;
  uint32_t vdda_mV;
  ADC_ChannelConfTypeDef sConfig =
  {
    0
  };

  hadc1.Init.OversamplingMode = DISABLE;
  hadc1.Init.Oversampling.RightBitShift = ADC_RIGHTBITSHIFT_NONE;

  assert_param(HAL_ADC_Init(&hadc1) == HAL_OK);

  sConfig.Channel = ADC_CHANNEL_VREFINT;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = samplingTime;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  assert_param(HAL_ADC_ConfigChannel(&hadc1, &sConfig) == HAL_OK);

  adcMeasDone = false;
  assert_param(HAL_ADC_Start_IT(&hadc1) == HAL_OK);   //start measurement

  sampleTime_us = _aIN_calcSampleAndConversationTime_us(A_IN_OVERSAMPLING_NONE, samplingTime);

  sys_sleepForUs(sampleTime_us);  //set system to sleep for the sampling time
  while (!adcMeasDone);

  vdda_mV = __LL_ADC_CALC_VREFANALOG_VOLTAGE(adcResult, LL_ADC_RESOLUTION_12B);

  return vdda_mV;
}

/**
 ****************************************************************
 @brief   Calculate the resulting samplingAndConversion time in
 *        microseconds depending on the settings.
 @param   osr   The to be used oversampling ratio from enumeration
 @param   samplingTime  The to be used sampling time for each sample
 @return  The resulting sample time in microseconds.
 ****************************************************************
 */
static uint32_t _aIN_calcSampleAndConversationTime_us (aIn_oversampling_t osr, aIn_samplingTime_t samplingTime)
{
  uint32_t nrOfAdcClocks;
  uint32_t sampleTime_us;

  switch (samplingTime)
  {
    default:
      nrOfAdcClocks = 0;
      assert_param(0);
      break;
    case A_IN_SAMPLETIME_2_5_CLK:
      nrOfAdcClocks = 3;
      break;
    case A_IN_SAMPLETIME_6_5_CLK:
      nrOfAdcClocks = 7;
      break;
    case A_IN_SAMPLETIME_12_5_CLK:
      nrOfAdcClocks = 13;
      break;
    case A_IN_SAMPLETIME_24_5_CLK:
      nrOfAdcClocks = 25;
      break;
    case A_IN_SAMPLETIME_47_5_CLK:
      nrOfAdcClocks = 48;
      break;
    case A_IN_SAMPLETIME_92_5_CLK:
      nrOfAdcClocks = 93;
      break;
    case A_IN_SAMPLETIME_247_5_CLK:
      nrOfAdcClocks = 248;
      break;
    case A_IN_SAMPLETIME_640_5_CLK:
      nrOfAdcClocks = 641;
      break;
  }

  //add conversion time (depends only on the number of ADC bits)
  nrOfAdcClocks += 13;

  switch (osr)
  {
    default:
      assert_param(0);
      break;
    case A_IN_OVERSAMPLING_NONE:
      break;
    case A_IN_OVERSAMPLING_2:
      nrOfAdcClocks *= 2;
      nrOfAdcClocks--;
      break;
    case A_IN_OVERSAMPLING_4:
      nrOfAdcClocks *= 4;
      nrOfAdcClocks -= 2;
      break;
    case A_IN_OVERSAMPLING_8:
      nrOfAdcClocks *= 8;
      nrOfAdcClocks -= 4;
      break;
    case A_IN_OVERSAMPLING_16:
      nrOfAdcClocks *= 16;
      nrOfAdcClocks -= 8;
      break;
  }

  sampleTime_us = nrOfAdcClocks / (HAL_ADC1_clockFreqGet() / 1000000);
  sampleTime_us++;                    //simple round up

  return sampleTime_us;
}

/**
 ****************************************************************
 @brief   Conversion complete callback in non-blocking mode.
 @param   hadc ADC handle
 @return  -
 ****************************************************************
 */
void HAL_ADC_ConvCpltCallback (ADC_HandleTypeDef *hadc)
{
  HAL_StatusTypeDef status;

  if (hadc->Instance == ADC1)
  {
    adcResult = HAL_ADC_GetValue(hadc);
    status = HAL_ADC_Stop_IT(hadc);
    assert_param(status == HAL_OK);

    adcMeasDone = true;
  }
  else
  {
    assert_param(0);
  }
}
/**
 * @}
 */

/**
 * @}
 */
