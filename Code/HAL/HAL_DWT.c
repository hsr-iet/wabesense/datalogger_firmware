/**
 ****************************************************************
 @file    HAL_DWT.c
 ****************************************************************
 @brief   This module offers a set of functions to interact with
 *        the data watchpoint unit, used for low level debugging.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2021-02-12
 ****************************************************************
 */

/** @addtogroup HAL
 * @{
 */

/** @addtogroup DWT
 * @{
 */

// --- Includes
#include "HAL_DWT.h"

#include <stdint.h>
#include <main.h>

// --- Defines

// --- Typedefs

// --- Variables

// --- Local Function Prototypes

/**
 ****************************************************************
 @brief   Initialize the data watchpoint module.
 @param   -
 @return  -
 ****************************************************************
 */
void hal_dwt_init (void)
{
  CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk; //enable Cortex-M4's DWT CYCCNT register
  DWT->CYCCNT = 0;                                //reset counter
  DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;            //enable the counter
}

/**
 ****************************************************************
 @brief  Reset the data watchpoint counter value
 @param  -
 @return -
 ****************************************************************
 */
void hal_dwt_cpuClockCntReset (void)
{
  DWT->CYCCNT = 0;                                //reset counter
}

/**
 ****************************************************************
 @brief  Get the current data watchpoint counter value
 @param  -
 @return The current data watchpoint counter value (number of CPU cycles)
 ****************************************************************
 */
uint32_t hal_dwt_cpuClockCntGet (void)
{
  return DWT->CYCCNT;
}

/**
 * @}
 */

/**
 * @}
 */
