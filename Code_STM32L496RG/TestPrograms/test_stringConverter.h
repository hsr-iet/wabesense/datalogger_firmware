/**
 ****************************************************************
 @file    test_stringConverter.h
 ****************************************************************
 @brief   This module implements some tests for the stringConverter FWO-module.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2021-02-12
 ****************************************************************
 */
#ifndef TEST_STRINGCONVERTER_H_
#define TEST_STRINGCONVERTER_H_

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

// --- Includes

// --- Defines

// --- Typedefs

// --- Function Prototypes
void test_stringConverter (void);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* TEST_STRINGCONVERTER_H_ */
