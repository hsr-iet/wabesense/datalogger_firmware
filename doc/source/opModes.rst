===============
Operating modes
===============

.. contents:: :local:

Active mode
===========
With USB-Cable connected
------------------------
In this operating mode, the system stays active (microcontroller is running all time), until the USB-Cable is removed. Afterwards, the
system goes to the standby mode.

The datalogger can be accessed over the serial interface (see `Serial-UI reference manual`_). The interacting with the datalogger by the Buttons is the same as in nomal operation 
mode (without USB cable). Only the Data-Export is not possible, because USB Interface is allready in use.

	- in data-streaming mode over serial interface, all power-sources are turned on and stay on until data-streaming mode is exited

Without USB-Cable connected
---------------------------
The time durration where the system stay in active mode, depends on the wakeup event, but it ends after a defined time.

	- Manual Measurement: Just around 200ms
	- Data Export only: Depends on the number of dataPoints that has to be exported to the USB-Stick (couple of seconds until some minutes)
	- Data Export with firmware update: An additional time of approximately 45 seconds is needed for the firmware update process.
	- System-Check: Just around 1 second

Standby mode
============
Normaly the system stays in the standby operating mode. There, only the absolute minimal system parts are powered.
	* RTC with the BackupRegisters
	* 5V buck converter
	* 3V3 buck converter

These are possible wake-up events to bring the system into active mode:
	* One of the two user Buttons
	* Insert detection of the SD-Card (not used in current firmware)
	* RTC Wakeup (internal of microcontroller)
		* used for time based measurement/datalogging in a defined interval
	* PowerUp-detection of possible power-supply devices
		* VUSB available (system is powered by USB, means e.g. by a laptop/pc)
		* Main power source connected to the DC-plug

Links
=====
.. _Serial-UI reference manual: https://hsr-iet.gitlab.io/wabesense/dataloggerui/

.. target-notes::