/**
 ****************************************************************
 @file    test_bme280.h
 ****************************************************************
 @brief   This module implements some tests for the BME280 sensor (onBoard).
 *
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-16
 ****************************************************************
 */
#ifndef TEST_BME280_H_
#define TEST_BME280_H_

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

// --- Includes

// --- Defines

// --- Typedefs

// --- Function Prototypes
void test_bme280 (void);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* TEST_FLASH_H_ */
