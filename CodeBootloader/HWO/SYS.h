/**
 ****************************************************************
 @file    SYS.h
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the system (clock).
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-10-28
 ****************************************************************
 */
#ifndef __SYS_H
#define __SYS_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdbool.h>
#include <stdint.h>
#include "rtc.h"
#include "../DEF/GLOB_Types.h"

//===============================================================
//defines

//===============================================================
//typedefs

//===============================================================
//function prototypes
void sys_init (void);

bool sys_flashUserAppErase (void);
bool sys_flashUserAppWrite (uint32_t memAddr, uint8_t *pMemData, uint32_t len);
void sys_userAppStart (uint32_t vTableAddr);

void sys_sleepForUs (uint32_t sleepTime_us);
void sys_standbyEnter (void);

void sys_sysTickRegCallbackFct (pFV_V_t pSysTickCbFct);

void sys_rtcDateTimeGet (rtcDateTime_t *pDateTime);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* #ifndef __SYS_H */
