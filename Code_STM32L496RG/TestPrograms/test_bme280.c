/**
 ****************************************************************
 @file    test_bme280.c
 ****************************************************************
 @brief   This module implements some tests for the BME280 sensor (onBoard).
 *
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-16
 ****************************************************************
 */

/** @addtogroup TESTPROGRAMMS
 * @{
 */

// --- Includes
#include "test_bme280.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "../HWO/BME280.h"
#include "../HWO/SYS.h"
#include "../HWO/RG_LED.h"

// --- Defines

// --- Typedefs

// --- Variables

// --- Local Function Prototypes
void test_bme280_measDataValidate (bme280_measData_t *pMeasData);

/**
 ****************************************************************
 @brief	  Do all possible tests with the BME280 sensor.
 @param   -
 @return  -
 ****************************************************************
 */
void test_bme280 (void)
{
  bme280_config_t cfg;
  bme280_measData_t measData;
  bool measDataAvailable;
  uint32_t nrOfMeasurements;

  cfg.osTemp = OS_1;
  cfg.osPres = OS_1;
  cfg.osHumi = OS_1;
  cfg.standbyTime = TSB_1000_MS;
  cfg.filter = FILTER_OFF;

#ifdef SEMI_HOSTING
  printf("BME280_Test: start\n\r");
#endif

  bme280_init(cfg);

#ifdef SEMI_HOSTING
  printf("BME280_Test: single measurement");
#endif
  //do a single measurement (9.225ms for this setting)
  bme280_modeSet(MODE_FORCED);
  //wait some time
  sys_sleepForUs(bme280_measTimeGet());

  measDataAvailable = bme280_dataGet(&measData);
  assert_param(measDataAvailable == true);
#ifdef SEMI_HOSTING
  printf(" pass\n\r");
#endif

#ifdef SEMI_HOSTING
  printf("BME280_Test: validate data");
#endif
  //validate measurement data
  test_bme280_measDataValidate(&measData);
#ifdef SEMI_HOSTING
  printf(" pass\n\r");
#endif

#ifdef SEMI_HOSTING
  printf("BME280_Test: loop measurement");
#endif
  //do a loop measurement (9.225ms for this setting)
  nrOfMeasurements = 4;
  bme280_modeSet(MODE_NORMAL);
  do
  {
    sys_sleepForUs(bme280_measTimeGet());

    measDataAvailable = bme280_dataGet(&measData);
    assert_param(measDataAvailable == true);
    nrOfMeasurements--;

    //validate measurement data
    test_bme280_measDataValidate(&measData);

    //one second (standby time before next measurement cycle)
    sys_sleepForUs(1e6);
  } while (nrOfMeasurements);
  bme280_modeSet(MODE_SLEEP);
#ifdef SEMI_HOSTING
  printf(" pass\n\r");
#endif

#ifdef SEMI_HOSTING
  printf("BME280_Test: oversampling setup");
#endif
  //try another configuration
  cfg.osTemp = OS_16;
  cfg.osPres = OS_16;
  cfg.osHumi = OS_16;
  cfg.standbyTime = TSB_10_MS;
  cfg.filter = FILTER_OFF;

  bme280_init(cfg);
#ifdef SEMI_HOSTING
  printf(" pass\n\r");
#endif

#ifdef SEMI_HOSTING
  printf("BME280_Test: single measurement (with oversampling)");
#endif
  //do a single measurement (78.3ms for this setting)
  bme280_modeSet(MODE_FORCED);
  //wait some time
  sys_sleepForUs(bme280_measTimeGet());

  measDataAvailable = bme280_dataGet(&measData);
  assert_param(measDataAvailable == true);
#ifdef SEMI_HOSTING
  printf(" pass\n\r");
#endif

#ifdef SEMI_HOSTING
  printf("BME280_Test: validate data");
#endif
  //validate measurement data
  test_bme280_measDataValidate(&measData);
#ifdef SEMI_HOSTING
  printf(" pass\n\r");
#endif

#ifdef SEMI_HOSTING
  printf("BME280_Test: ends\n\n\r");
#endif

  //mark test as okay
  rgLED_init();
  rgLED_luminositySet(10);
  rgLED_colorSet(RGLED_COLOR_GREEN);
  sys_sleepForUs(1e6);  //1s delay
  rgLED_luminositySet(0);
  rgLED_colorSet(RGLED_COLOR_NONE);
}

/**
 ****************************************************************
 @brief   Internal function to validate the measurement data for plausibility.
 @param   pMeasData   Pointer to the currently read measurement data.
 @return  -
 ****************************************************************
 */
void test_bme280_measDataValidate (bme280_measData_t *pMeasData)
{
  //temperature in range of 10.0 - 30.0°C
  assert_param((pMeasData->temperature >= 10.0f) && (pMeasData->temperature <= 30.0f));
  //pressure in range of 30.0hPa - 1100.0hPa
  assert_param((pMeasData->pressure >= 3000.0f) && (pMeasData->pressure <= 110000.0f));
  //humidity in range of 0 - 100%
  assert_param((pMeasData->humidity >= 0.0f) && (pMeasData->humidity <= 100.0f));
}

/**
 * @}
 */
