/**
 ****************************************************************
 @file    stringConverter.c
 ****************************************************************
 @brief   This module offers a set of functions to convert numbers
 *        to a representing string.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2021-02-10
 ****************************************************************
 */

/** @addtogroup FWO
 * @{
 */

/** @addtogroup STRING_CONVERTER
 * @{
 */

// --- Includes
#include "stringConverter.h"

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

// --- Defines

// --- Typedefs

// --- Variables

// --- Local Function Prototypes

/**
 ****************************************************************
 @brief   Function to convert a float number to a representing string.
 @note    3 digits followed after the dot.
 @param   value The to be converted float value
 @param   pString Pointer to the resulting string
 @param   pStringLen  Pointer to the resulting string length (zero on NaN or infinite numbers)
 @return  -
 ****************************************************************
 */
void strConv_floatToString_3 (float value, char *pString, uint32_t *pStringLen)
{
  char str[32];
  char *s = &str[31];    // go to end of buffer
  uint32_t fractional;  // variable to store the decimals
  int integer;          // variable to store the units (part to left of decimal place)
  bool negativeNumber;  // flag to indicate if it is a negative number of not
  uint32_t strLen = 0;

  if (isnanf(value))
  {
    //not a number
    *pStringLen = 0;
    return;
  }
  else if (isinff(value))
  {
    //infinity number
    *pStringLen = 0;
    return;
  }

  if (value < 0.0f)
  {
    value -= 0.0005f;
    // take care of negative numbers
    negativeNumber = true;
    integer = (int) (-value);
    fractional = (int) (((-value) - (float) integer) * 1000.0f); // make 100 for 2 decimals etc.

  }
  else
  {
    value += 0.0005f;
    // positive numbers
    negativeNumber = false;
    integer = (int) value;
    fractional = (int) ((value - (float) integer) * 1000.0f); // make 100 for 2 decimals etc.
  }

  *s-- = (fractional % 10) + '0';
  strLen++;
  fractional /= 10; // repeat for as many decimal places as you need
  *s-- = (fractional % 10) + '0';
  strLen++;
  fractional /= 10;
  *s-- = (fractional % 10) + '0';
  strLen++;
  *s-- = '.';
  strLen++;

  if (integer)
  {
    while ((integer > 0) && (strLen < (sizeof(str) - 1)))
    {
      *s-- = (integer % 10) + '0';
      strLen++;
      integer /= 10;
    }
  }
  else
  {
    //only add leading zero, number is 0.fractionalPart)
    *s-- = '0';
    strLen++;
  }

  if (negativeNumber)
  {
    *s-- = '-'; // unary minus sign for negative numbers
    strLen++;
  }
  memcpy(pString, &str[sizeof(str) - strLen], strLen);
  *pStringLen = strLen;
}

/**
 ****************************************************************
 @brief   Function to convert a float number to a representing string.
 @note    6 digits followed after the dot.
 @param   value The to be converted float value
 @param   pString Pointer to the resulting string
 @param   pStringLen  Pointer to the resulting string length (zero on NaN or infinite numbers)
 @return  -
 ****************************************************************
 */
void strConv_floatToString_6 (float value, char *pString, uint32_t *pStringLen)
{
  char str[64];
  char *s = &str[63];    // go to end of buffer
  uint32_t fractional;  // variable to store the decimals
  int integer;          // variable to store the units (part to left of decimal place)
  bool negativeNumber;  // flag to indicate if it is a negative number of not
  uint32_t strLen = 0;

  if (isnanf(value))
  {
    //not a number
    *pStringLen = 0;
    return;
  }
  else if (isinff(value))
  {
    //infinity number
    *pStringLen = 0;
    return;
  }

  if (value < 0.0f)
  {
    value -= 0.0000005f;
    // take care of negative numbers
    negativeNumber = true;
    integer = (int) (-value);
    fractional = (int) (((-value) - (float) integer) * 1000000.0f); // make 100 for 2 decimals etc.

  }
  else
  {
    value += 0.0000005f;
    // positive numbers
    negativeNumber = false;
    integer = (int) value;
    fractional = (int) ((value - (float) integer) * 1000000.0f); // make 100 for 2 decimals etc.
  }

  *s-- = (fractional % 10) + '0';
  strLen++;
  fractional /= 10; // repeat for as many decimal places as you need
  *s-- = (fractional % 10) + '0';
  strLen++;
  fractional /= 10;
  *s-- = (fractional % 10) + '0';
  strLen++;
  fractional /= 10;
  *s-- = (fractional % 10) + '0';
  strLen++;
  fractional /= 10;
  *s-- = (fractional % 10) + '0';
  strLen++;
  fractional /= 10;
  *s-- = (fractional % 10) + '0';
  strLen++;
  *s-- = '.';
  strLen++;

  if (integer)
  {
    while ((integer > 0) && (strLen < (sizeof(str) - 1)))
    {
      *s-- = (integer % 10) + '0';
      strLen++;
      integer /= 10;
    }
  }
  else
  {
    //only add leading zero, number is 0.fractionalPart)
    *s-- = '0';
    strLen++;
  }

  if (negativeNumber)
  {
    *s-- = '-'; // unary minus sign for negative numbers
    strLen++;
  }
  memcpy(pString, &str[sizeof(str) - strLen], strLen);
  *pStringLen = strLen;
}
/**
 * @}
 */

/**
 * @}
 */
