/**
 ****************************************************************
 @file    stringConverter.h
 ****************************************************************
 @brief   This module offers a set of functions to convert numbers
 *        to a representing string.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2021-02-10
 ****************************************************************
 */
#ifndef __FWO_STRINGCONVERTER_H
#define __FWO_STRINGCONVERTER_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdint.h>

//===============================================================
//defines

//===============================================================
//typedefs

//===============================================================
//function prototypes
void strConv_floatToString_3 (float value, char *pString, uint32_t *pStringLen);
void strConv_floatToString_6 (float value, char *pString, uint32_t *pStringLen);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* #ifndef __FWO_STRINGCONVERTER_H */
