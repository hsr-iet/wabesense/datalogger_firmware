/**
 ****************************************************************
 @file    GLOB_Types.h
 ****************************************************************
 @brief   Defines common types to be used in project.
 ****************************************************************
 @author  Adrian Tuescher, IMES/OST
 @version 00.00
 @date    2020-10-30
 ****************************************************************
 */
#ifndef GLOB_TYPES_H
#define GLOB_TYPES_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdbool.h>
#include <stdint.h>

//===============================================================
//defines
#define BOOTLOADING_TAG_NONE      ((uint64_t) 0x0011223344556677)
#define BOOTLOADING_TAG_REQUEST   ((uint64_t) 0x1234567890123456)
#define BOOTLOADING_TAG_SUCCESS   ((uint64_t) 0x9876543210987654)
#define BOOTLOADING_TAG_ERROR     ((uint64_t) 0x1122334455667788)

//===============================================================
//typedefs
typedef void (*pFV_V_t) (void);  //!< Pointer to a function in the form: void func(void)
typedef bool (*pFB_V_t) (void);  //!< Pointer to a function in the form: bool func(void)

//! firmware version type struct
typedef struct
{
  char major;
  char minor;
  char patch;
} fwVersion_t;

//! type struct definition for data to passed to the bootloader
typedef struct
{
  uint64_t tag;    //0x1234567890123456 key is used to signalizes pending bootloading
  uint32_t len;
  char pathAndFileName[256];
} dataForBootloader_t;

//!	firmware error code type enumeration
typedef enum
{
  ERROR_NONE = 0,
} fwError_t;       //Firmware Error Codes

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* #ifndef GLOB_TYPES_H */
