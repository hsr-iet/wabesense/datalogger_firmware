/**
 ****************************************************************
 @file    DIG_IO.c
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        all the available digital In/Output pins on the data-logger.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-10
 ****************************************************************
 */

/** @addtogroup HWO
 * @{
 */

/** @addtogroup IO
 * @{
 */

// --- Includes
#include "DIG_IO.h"

#include <assert.h>
#include <stdbool.h>
#include "main.h"
#include "gpio.h"

// --- Defines

// --- Typedefs

// --- Variables
static bool _initDone = false;
static uint32_t _digIO_3v3SwState = 0x00;

// --- Local Function Prototypes

/**
 ****************************************************************
 @brief   Initialize the digital in/outputs.
 @param   -
 @return  -
 ****************************************************************
 */
void digIO_init (void)
{
  GPIO_InitTypeDef GPIO_InitStruct =
  {
    0
  };

  if (!_initDone)
  {
    //setup onBoard data logger inputs (not anymore as wakeUp pins in this case)
    //configure GPIO pin : PA0 - VDD USB
    GPIO_InitStruct.Pin = GPIO_PIN_0;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    //configure GPIO pin : PA2 - SDCard Detection
    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    _initDone = true;
  }
}

/**
 ****************************************************************
 @brief   Get the digital input state on the user button 1 pin.
 @param   -
 @return  True, if the user button 1 is pressed, otherwise false
 ****************************************************************
 */
bool digIO_usrBt1Pressed (void)
{
  return HAL_GPIO_ReadPin(USR_BT1_GPIO_Port, USR_BT1_Pin);
}

/**
 ****************************************************************
 @brief   Get the digital input state on the user button 2 pin.
 @param   -
 @return  True, if the user button 2 is pressed, otherwise false
 ****************************************************************
 */
bool digIO_usrBt2Pressed (void)
{
  return HAL_GPIO_ReadPin(USR_BT2_GPIO_Port, USR_BT2_Pin);
}

/**
 ****************************************************************
 @brief   Get the digital input state of the USB power detection
 @param   -
 @return  True, if the USB power is present, otherwise false
 ****************************************************************
 */
bool digIO_usbDetected (void)
{
  return HAL_GPIO_ReadPin(SYS_WKUP1_VDD_USB_GPIO_Port, SYS_WKUP1_VDD_USB_Pin);
}

/**
 ****************************************************************
 @brief   Get the digital input state on the SDCard detection pin
 @param   -
 @return  True, if the SDCard is detected, otherwise false
 ****************************************************************
 */
bool digIO_sdCardDetected (void)
{
  return HAL_GPIO_ReadPin(SYS_WKUP4_SDCARD_GPIO_Port, SYS_WKUP4_SDCARD_Pin);
}

/**
 ****************************************************************
 @brief   Turn on the switched 3.3V power supply.
 @note    Used by FLASH IC (AT25SF321B)
 @note    Used by SD-Card (without SD-Card detection)
 @note    Could be used by RGB-LED
 @note    Could be used by external I2C communication (I2C1-Unit)
 @param   unit  One of the possible 3V3 Units
 @return  -
 ****************************************************************
 */
void digIO_3V3PwrOn (digIO_3V3Unit_t unit)
{
  assert(
    (unit == DIG_IO_3V3UNIT_FLASH) || (unit == DIG_IO_3V3UNIT_SDCARD) || (unit == DIG_IO_3V3UNIT_RGB_LED)
      || (unit == DIG_IO_3V3UNIT_17VSW) || (unit == DIG_IO_3V3UNIT_EXT_I2C1));

  //mechanisms to check which parts are needed the 3v3
  _digIO_3v3SwState |= unit;

  HAL_GPIO_WritePin(ENA_3V3_GPIO_Port, ENA_3V3_Pin, GPIO_PIN_SET);
}

/**
 ****************************************************************
 @brief   Request to shutdown the switched 3.3V power supply.
 @param   unit  One of the possible 3V3 Units
 @return  -
 ****************************************************************
 */
void digIO_3V3PwrReqOff (digIO_3V3Unit_t unit)
{
  assert(
    (unit == DIG_IO_3V3UNIT_FLASH) || (unit == DIG_IO_3V3UNIT_SDCARD) || (unit == DIG_IO_3V3UNIT_RGB_LED)
      || (unit == DIG_IO_3V3UNIT_17VSW) || (unit == DIG_IO_3V3UNIT_EXT_I2C1));

  //mechanisms to check which parts are needed the 3v3
  _digIO_3v3SwState &= ~unit;
  if (_digIO_3v3SwState == 0)
  {
    HAL_GPIO_WritePin(ENA_3V3_GPIO_Port, ENA_3V3_Pin, GPIO_PIN_RESET);
  }
}

/**
 * @}
 */

/**
 * @}
 */
