/**
 ****************************************************************
 @file    FLASH.c
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the onBoard FLASH IC (AT25SF321B - 32Mbit).
 ****************************************************************
 @note    The used interface is the SPI1 unit (also used for BME280 and SDCard)!
 *        SPI Interface of the sensor is compatible to (max. 108MHz)
 *          mode 00
 *            CPOL = 0, CPHA = 0
 *          or mode 11
 *            CPOL = 1, CPHA = 1
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-03
 ****************************************************************
 */

/** @addtogroup HWO
 * @{
 */

/** @addtogroup FLASH
 * @{
 */

// --- Includes
#include "FLASH.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include "spi.h"
#include "SYS.h"
#include "DIG_IO.h"

// --- Defines
#define FLASH_POWER_UP_SLEEP_DELAY_US     100   //at least 70us from FLASH

//! AT25SF321B Command Table
enum
{
  //! system commands
  AT25SF321B_ENA_RESET = 0x66,
  AT25SF321B_RESET_DEV = 0x99,
  AT25SF321B_DEEP_POWER_DOWN = 0xB9,
  AT25SF321B_RELEASE_POWER_DOWN = 0xAB,
  //! read commands
  AT25SF321B_NORMAL_READ_DATA = 0x03,
  AT25SF321B_FAST_READ_DATA = 0x0B,
  //! write commands
  AT25SF321B_ENA_WRITE = 0x06,
  AT25SF321B_DIS_WRITE = 0x04,
  //! program commands
  AT25SF321B_PAGE_PROG = 0x02,
  //! erase commands
  AT25SF321B_BLOCK_4K_ERASE = 0x20,
  AT25SF321B_BLOCK_32K_ERASE = 0x52,
  AT25SF321B_BLOCK_64K_ERASE = 0xD8,
  AT25SF321B_CHIP_ERASE = 0xC7,
  //! status register commands
  AT25SF321B_STATUS_REG1_READ = 0x05,
  AT25SF321B_STATUS_REG2_READ = 0x35,
  AT25SF321B_STATUS_REG3_READ = 0x15,
  AT25SF321B_STATUS_REG1_WRITE = 0x01,
  AT25SF321B_STATUS_REG2_WRITE = 0x31,
  AT25SF321B_STATUS_REG3_WRITE = 0x11,
  //! device information commands
  AT25SF321B_DEV_ID = 0x90,
  AT25SF321B_JEDEC_ID_READ = 0x9F,
  AT25SF321B_UNIQUE_ID_READ = 0x4B,
} at25sf321b_cmd;

// --- Typedefs

// --- Variables
static bool _initDone = false;

// --- Local Function Prototypes
static bool _flash_checkBusy (void);

/**
 ****************************************************************
 @brief   Initialize the AT25SF321B FLASH IC.
 @param   -
 @return  -
 ****************************************************************
 */
void flash_init (void)
{
  //currently nothing to do
  if (!_initDone)
  {
    _initDone = true;
  }
}

/**
 ****************************************************************
 @brief   Get the size in byte of the FLASH memory.
 @param   -
 @return  The number of bytes.
 ****************************************************************
 */
uint32_t flash_memorySizeGet (void)
{
  return 4194304; //4MByte
}

/**
 ****************************************************************
 @brief   Read an amount of bytes from the FLASH memory.
 @param   address   The byte memory address to read from [0...0x3F'FFFF]
 @param   pData     Pointer to the resulting data from read out
 @param   nrOfBytes The number of bytes to be read out
 @return  -
 ****************************************************************
 */
void flash_bytesRead (uint32_t address, uint8_t *pData, uint32_t nrOfBytes)
{
  uint8_t txData[4];
  uint8_t rxData[4];
  bool busy;
  uint32_t timeoutCnt;

  assert_param(address <= 0x003FFFFF);

  digIO_3V3PwrOn(DIG_IO_3V3UNIT_FLASH);
  //a minimum of 70uS is needed before chip could be selected
  sys_sleepForUs(FLASH_POWER_UP_SLEEP_DELAY_US);

  //read status register and check if the chip is not busy (ready)
  timeoutCnt = 0;
  do
  {
    busy = _flash_checkBusy();  //check if the device is busy

    sys_sleepForUs(100);        //set system to sleep for 100us
    timeoutCnt++;               //a simple timeout counter
  } while (busy && (timeoutCnt < 20));

  assert_param(!busy);

  txData[0] = AT25SF321B_NORMAL_READ_DATA;
  txData[1] = (address >> 16) & 0x3F;
  txData[2] = (address >> 8) & 0xFF;
  txData[3] = address & 0xFF;
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_FLASH);
  HAL_SPI1_transfer(txData, rxData, 4);
  //read the requested data
  HAL_SPI1_receive(pData, nrOfBytes);
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_FLASH);
}

/**
 ****************************************************************
 @brief   Write an amount of bytes to the FLASH memory.
 @note    The to be programmed page needs to be erased before written to it is possible
 @note    To program 256 Bytes, the needed time is typical 0.4ms, max. 3.4ms
 @param   address   The byte memory address to be written to [0...0x3F'FFFF]
 @param   pData     Pointer to the to be written data
 @param   nrOfBytes The number of bytes to be written [1 ... 256]
 @return  -
 ****************************************************************
 */
void flash_bytesWrite (uint32_t address, uint8_t *pData, uint32_t nrOfBytes)
{
  uint8_t txData[4];
  bool busy;
  uint32_t timeoutCnt;

  assert_param(address <= 0x003FFFFF);
  assert_param((nrOfBytes <= 256) && (nrOfBytes > 0));

  digIO_3V3PwrOn(DIG_IO_3V3UNIT_FLASH);
  //a minimum of 70uS is needed before chip could be selected
  sys_sleepForUs(FLASH_POWER_UP_SLEEP_DELAY_US);

  //read status register and check if the chip is not busy (ready)
  timeoutCnt = 0;
  do
  {
    busy = _flash_checkBusy();  //check if the device is busy

    sys_sleepForUs(100);        //set system to sleep for 100us
    timeoutCnt++;               //a simple timeout counter
  } while (busy && (timeoutCnt < 20));

  assert_param(!busy);

  //the write enable command is needed before any program command
  txData[0] = AT25SF321B_ENA_WRITE;
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_FLASH);
  HAL_SPI1_transmit(txData, 1);
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

  txData[0] = AT25SF321B_PAGE_PROG;
  txData[1] = (address >> 16) & 0x3F;
  txData[2] = (address >> 8) & 0xFF;
  txData[3] = address & 0xFF;
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_FLASH);
  HAL_SPI1_transmit(txData, 4);
  HAL_SPI1_transmit(pData, nrOfBytes);
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

  //read status register and check if the write has been completed
  timeoutCnt = 0;
  do
  {
    sys_sleepForUs(1000);       //set system to sleep for 1ms

    busy = _flash_checkBusy();  //check if the device is busy
    timeoutCnt++;               //a simple timeout counter
  } while (busy && (timeoutCnt < 5));

  assert_param(!busy);

  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_FLASH);
}

/**
 ****************************************************************
 @brief   Erase one of the 1024 4kByte Blocks of memory.
 @note    4kbyte block erase time is typical 55ms, max. 250ms
 @param   blockNr One of the possible memory blocks [0...1023]
 @return  -
 ****************************************************************
 */
void flash_block4kErase (uint32_t blockNr)
{
  uint8_t txData[4];
  uint32_t address;
  bool busy;
  uint32_t timeoutCnt;

  assert_param(blockNr <= 1023);

  //generate memory address out of the block number
  address = blockNr << 12;

  digIO_3V3PwrOn(DIG_IO_3V3UNIT_FLASH);
  //a minimum of 70uS is needed before chip could be selected
  sys_sleepForUs(FLASH_POWER_UP_SLEEP_DELAY_US);

  //read status register and check if the chip is not busy (ready)
  timeoutCnt = 0;
  do
  {
    busy = _flash_checkBusy();  //check if the device is busy

    sys_sleepForUs(100);        //set system to sleep for 100us
    timeoutCnt++;               //a simple timeout counter
  } while (busy && (timeoutCnt < 20));

  assert_param(!busy);

  //the write enable command is needed before chip erase
  txData[0] = AT25SF321B_ENA_WRITE;
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_FLASH);
  HAL_SPI1_transmit(txData, 1);
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

  txData[0] = AT25SF321B_BLOCK_4K_ERASE;
  txData[1] = (address >> 16) & 0x3F;
  txData[2] = (address >> 8) & 0xFF;
  txData[3] = address & 0xFF;
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_FLASH);
  HAL_SPI1_transmit(txData, 4);
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

  //read status register and check if the erase has been completed
  timeoutCnt = 0;
  do
  {
    busy = _flash_checkBusy();  //check if the device is busy

    sys_sleepForUs(100e3);      //set system to sleep for 100ms
    timeoutCnt++;               //a simple timeout counter
  } while (busy && (timeoutCnt < 4));

  assert_param(!busy);

  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_FLASH);
}

/**
 ****************************************************************
 @brief   Erase the complete chip memory.
 @note    Chip erase time is typical 10 seconds, max. 30 seconds, timeout is set to 40 seconds
 @param   -
 @return  -
 ****************************************************************
 */
void flash_chipErase (void)
{
  uint8_t txData[1];
  bool busy;
  uint32_t timeoutCnt;

  digIO_3V3PwrOn(DIG_IO_3V3UNIT_FLASH);
  //a minimum of 70uS is needed before chip could be selected
  sys_sleepForUs(FLASH_POWER_UP_SLEEP_DELAY_US);

  //read status register and check if the chip is not busy (ready)
  timeoutCnt = 0;
  do
  {
    busy = _flash_checkBusy();  //check if the device is busy

    sys_sleepForUs(100);        //set system to sleep for 100us
    timeoutCnt++;               //a simple timeout counter
  } while (busy && (timeoutCnt < 20));

  assert_param(!busy);

  //the write enable command is needed before chip erase
  txData[0] = AT25SF321B_ENA_WRITE;
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_FLASH);
  HAL_SPI1_transmit(txData, 1);
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

  //erase chip command
  txData[0] = AT25SF321B_CHIP_ERASE;
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_FLASH);
  HAL_SPI1_transmit(txData, 1);
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

  //read status register and check if the erase has been completed
  timeoutCnt = 0;
  do
  {
    busy = _flash_checkBusy();  //check if the device is busy

    sys_sleepForUs(2e6);        //set system to sleep for two seconds
    timeoutCnt++;               //a simple timeout counter
  } while (busy && (timeoutCnt < 20));

  assert_param(!busy);

  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_FLASH);
}

/**
 ****************************************************************
 @brief   Check if the flash is busy (internal operation ongoing)
 @param   -
 @return  True if the device is busy, otherwise false
 ****************************************************************
 */
static bool _flash_checkBusy (void)
{
  uint8_t txData[2];
  uint8_t rxData[2];

  txData[0] = AT25SF321B_STATUS_REG1_READ;
  txData[1] = 0xFF;
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_FLASH);
  HAL_SPI1_transfer(txData, rxData, 2);
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

  //check if the device is busy)
  if (rxData[1] & 0x01)
  {
    return true;  //busy
  }
  else
  {
    return false;
  }
}

/**
 * @}
 */

/**
 * @}
 */
