/**
 ****************************************************************
 @file    test_sdCard.h
 ****************************************************************
 @brief   This module implements some tests for the SdCard interface.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-16
 ****************************************************************
 */
#ifndef TEST_SDCARD_H_
#define TEST_SDCARD_H_

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

// --- Includes

// --- Defines

// --- Typedefs

// --- Function Prototypes
void test_sdCard (void);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* TEST_SDCARD_H_ */
