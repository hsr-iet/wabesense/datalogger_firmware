/**
 ****************************************************************
 @file    HMI.c
 ****************************************************************
 @brief   This module offers a set of functions to handle the
 *        Human Machine visible interface.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.1
 @date    2021-09-20
 ****************************************************************
 */

/** @addtogroup FWO
 * @{
 */

/** @addtogroup HMI
 * @{
 */

// --- Includes
#include "HMI.h"

#include <stdbool.h>
#include <stdint.h>
#include "../HWO/SYS.h"
#include "../HWO/RG_LED.h"

// --- Defines

// --- Typedefs
//! data type definition for possible color signalization on the RGB-LED
typedef struct
{
  ledPattern_t pattern; //!< one of the possible patterns
  rgColor_t color;      //!< The selected color (from enumeration)
  bool onState;         //!< Flag to indicate if the LED is currently ON (true) or OFF (false)
  uint32_t tickCnt;     //!< a simple tick counter, to reduce frequency from sysTick callback
} _led_Pattern_t;

// --- Variables
static _led_Pattern_t _rgLED;

// --- Local Function Prototypes
static void _hmi_ledTick (void);

/**
 ****************************************************************
 @brief   Initialize the hmi rgb LED.
 @param   -
 @return  -
 ****************************************************************
 */
void hmi_init (void)
{
  _rgLED.pattern = HMI_LED_PATTERN_OFF;
  _rgLED.color = RGLED_COLOR_NONE;
  _rgLED.tickCnt = 100;
  _rgLED.onState = false;

  rgLED_init();
  rgLED_luminositySet(10);
  rgLED_colorSet(_rgLED.color);

  sys_sysTickRegCallbackFct(_hmi_ledTick);
}

/**
 ****************************************************************
 @brief   RGB LED pattern set function
 @param   pattern One of the possible patterns from enumeration
 @param   color   One of the possible colors from enumeration
 @return  -
 ****************************************************************
 */
void hmi_ledPatternSet (ledPattern_t pattern, rgColor_t color)
{
  //disable sysTick interrupts
  sys_sysTickSuspend();

  _rgLED.pattern = HMI_LED_PATTERN_OFF;
  switch (pattern)
  {
    default:
    case HMI_LED_PATTERN_OFF:
      _rgLED.color = RGLED_COLOR_NONE;
      pattern = HMI_LED_PATTERN_OFF;
      break;
    case HMI_LED_PATTERN_ON:
      _rgLED.color = color;
      break;
    case HMI_LED_PATTERN_BLINK_10HZ:
      _rgLED.color = color;
      break;
  }
  _rgLED.onState = true;
  rgLED_colorSet(_rgLED.color);
  _rgLED.pattern = pattern;
  _rgLED.tickCnt = 100;

  //reEnable sysTick interrupts
  sys_sysTickResume();
}

/**
 ****************************************************************
 @brief   LED Ticker Routine, called by sysTick interrupt (every 1 milliseconds)
 @param   -
 @return  Error-Code from OBJ
 ****************************************************************
 */
static void _hmi_ledTick (void)
{
  switch (_rgLED.pattern)
  {
    default:
    case HMI_LED_PATTERN_OFF:
    case HMI_LED_PATTERN_ON:
      _rgLED.tickCnt = 100;
      //nothing to do
      break;
    case HMI_LED_PATTERN_BLINK_10HZ:
      _rgLED.tickCnt--;
      if (_rgLED.tickCnt == 0)
      {
        _rgLED.tickCnt = 100;

        //toggle ON/OFF state
        _rgLED.onState = !_rgLED.onState;
        if (_rgLED.onState)
        {
          rgLED_colorSet(_rgLED.color);
        }
        else
        {
          rgLED_colorSet(RGLED_COLOR_NONE);
        }
      }
      break;
  }
}

/**
 * @}
 */

/**
 * @}
 */
