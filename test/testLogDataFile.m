%%
% matlab script for the WABEsense project
% mainly used to check the measurement integrity of different dataloggers
% possible comparision of the BME280 pressure values over all dataLogger
% units
%
% Project: WABEsense
% Autor: Adrian Tuescher, IMES / OST
% Date:  19.03.2021
%%
clc
clear
close all;

%test a logFile
srcPath = 'C:\Daten\WABEsense\_WABEsense_SwitchDrive\data\reporters\';
locations = {'bonaduz', 'hergiswil', 'oberriet', 'zug'};
buildings = {'', '', '', ''};
wabeID =    {'', '', '', ''};

for idx = 1:length(locations)
    fileName = cell2mat(['*', locations(idx)]);
    files = dir(cell2mat([srcPath, locations(idx), '\', fileName, '*.csv']));
    %sort on descending filename
    [~,index] = sortrows({files.name}.'); files = files(index(end:-1:1)); clear index
    file = [files(1).folder, '\', files(1).name];

    dataSrc{idx}.location = cell2mat(locations(idx));
    dataSrc{idx}.data = readtable(file);

    data = dataSrc{idx}.data;
    %get date information out
    date = datetime(data.RTC_Date_yyyy_mm_dd_, 'InputFormat', 'yyyy.MM.dd');
    time = data.RTC_Time_hh_mm_ss_;
    dateTime = date + time;
    dataSrc{idx}.dateTime = dateTime;
    dateTimeString = datestr(dateTime, 'dd-mmm-yyyy HH:MM:SS');

    %build timeseries
    ts.autoMeas = timeseries(data.AutoMeas, dateTimeString, 'Name', 'AutoMeas');
    ts.vBat = timeseries(data.VBat_V_, dateTimeString, 'Name', 'VBat [V]');

    ts.bme280.temp = timeseries(data.BME_Temperature_C_, dateTimeString, 'Name', 'BME Temperature [�C]');
    ts.bme280.pressure = timeseries(data.BME_Pressure_Pa_ / 100, dateTimeString, 'Name', 'BME Pressure [hPa]');
    ts.bme280.humidity = timeseries(data.BME_Humidity___, dateTimeString, 'Name', 'BME Humidity [%]');

    ts.sensor.temp = timeseries(table2array(data(:,9))/5.0*90-40, dateTimeString, 'Name', 'Sensor Temperature [�C]');
    ts.sensor.pressure = timeseries(table2array(data(:,8))/5.0*100.0, dateTimeString, 'Name', 'Sensor Pressure [mBar]');

    figure('Name',cell2mat(['Measured Data - location: ', locations(idx)]),'NumberTitle','off');
    %first row
    subplot(3,3,1);
    plot(ts.autoMeas);
    subplot(3,3,2);
    plot(ts.vBat);

    %second row
    subplot(3,3,4);
    plot(ts.bme280.temp);
    subplot(3,3,5);
    plot(ts.bme280.pressure);
    subplot(3,3,6);
    plot(ts.bme280.humidity);

    %third row
    subplot(3,3,7);
    plot(ts.sensor.temp);
    subplot(3,3,8);
    plot(ts.sensor.pressure);
    
    dataSrc{idx}.ts = ts;
end

%%
%start of  data integrity check
figure('Name','Data integrity','NumberTitle','off');
for idx = 1:length(locations)
    dateTime = dataSrc{idx}.dateTime;
    deltaTime=diff(dateTime);

    subplot(4,1,idx);
    plot(deltaTime);
    ylim([duration(0,0,0) duration(0,30,0)])
    title(locations(idx));
end

%%
% BME280 pressure compare
figure('Name','BME280 - Pressure compare','NumberTitle','off');
for idx = 1:length(locations)
    pressure_hPa = dataSrc{idx}.ts.bme280.pressure;

    plot(pressure_hPa);
    hold on
    legendNames(idx) = locations(idx);
end
legend(legendNames);