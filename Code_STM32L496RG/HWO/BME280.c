/**
 ****************************************************************
 @file    BME280.c
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the onBoard BME280 sensor (temperature, pressure and humidity).
 @brief   The sensor is normally in sleep mode and wake up for a forced
 *        measurement (T, P & H).
 @brief   The maximum measurement time is:
 *        1.25ms + (2.3 * T_oversampling) + [(2.3 * P_oversampling) + 0.575] +
 *        [(2.3 * H_oversampling) + 0.5]
 *        Example:
 *        osTemp = 1, osPres = 1, osHumidity = 1 (fix)
 *        t_meas = 1.25ms + 2.3ms + 2.875ms + 2.8ms = 9.225ms
 ****************************************************************
 @note    The used interface is the SPI1 unit (also used for FLASH and SDCard)!
 *        SPI Interface of the sensor is compatible to (max. 10MHz)
 *          mode 00
 *            CPOL = 0, CPHA = 0
 *          or mode 11
 *            CPOL = 1, CPHA = 1
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.2
 @date    2020-11-02
 ****************************************************************
 */

/** @addtogroup HWO
 * @{
 */

/** @addtogroup BME280
 * @{
 */

// --- Includes
#include "BME280.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "spi.h"
#include "SYS.h"

// --- Defines
//! Chip IDs for samples and mass production parts
enum bme280_chipId
{
  BME280_CHIP_ID = 0x60   //!< chip ID
} bme280_chipId;

//! BME280 register addresses (only 7 bit used in SPI Mode, bit 8 is read/write)
enum bme280_regAddr
{
  BME280_DIG_T1_LSB_ADDR = 0x08,  //0x88
  BME280_DIG_H1_ADDR = 0x21,      //0xA1
  BME280_CHIP_ID_ADDR = 0x50,     //0xD0
  BME280_SOFT_RESET_ADDR = 0x60,  //0xE0
  BME280_DIG_H2_LSB_ADDR = 0x61,  //0xE1
  BME280_CTRL_HUM_ADDR = 0x72,    //0xF2
  BME280_STATUS_ADDR = 0x73,      //0xF3
  BME280_CTRL_MEAS_ADDR = 0x74,   //0xF4
  BME280_CONFIG_ADDR = 0x75,      //0xF5
  BME280_PRES_MSB_ADDR = 0x77,    //0xF7
  BME280_PRES_LSB_ADDR = 0x78,    //0xF8
  BME280_PRES_XLSB_ADDR = 0x79,   //0xF9
  BME280_TEMP_MSB_ADDR = 0x7A,    //0xFA
  BME280_TEMP_LSB_ADDR = 0x7B,    //0xFB
  BME280_TEMP_XLSB_ADDR = 0x7C,   //0xFC
} bme280_regAddr;

//! possible power modes for the BME280 sensor
enum bme280_powerModes
{
  BME280_SLEEP_MODE = 0x00,
  BME280_FORCED_MODE = 0x01,
  BME280_NORMAL_MODE = 0x03,
} bme280_powerModes;

//! implemented commands for the BME280 sensor
enum bme280_commands
{
  BME280_SOFT_RESET_CMD = 0xB6, //!< soft reset command
  BME280_WRITE = 0x00,          //!< write (MSB must be zero)
  BME280_READ = 0x80,           //!< read (MSB must be one)
} bme280_commands;

// --- Typedefs
//! data type to pack all calibration parameters together of the BME280 sensor
typedef union
{
  struct __attribute__((packed))
  {
    uint16_t t1;  //!< unsigned short T1 calibration value for temperature
    int16_t t2;   //!< signed short T2 calibration value for temperature
    int16_t t3;   //!< signed short T3 calibration value for temperature
    uint16_t p1;  //!< unsigned short P1 calibration value for pressure
    int16_t p2;   //!< signed short P2 calibration value for pressure
    int16_t p3;   //!< signed short P3 calibration value for pressure
    int16_t p4;   //!< signed short P4 calibration value for pressure
    int16_t p5;   //!< signed short P5 calibration value for pressure
    int16_t p6;   //!< signed short P6 calibration value for pressure
    int16_t p7;   //!< signed short P7 calibration value for pressure
    int16_t p8;   //!< signed short P8 calibration value for pressure
    int16_t p9;   //!< signed short P9 calibration value for pressure
    uint8_t h1;   //!< unsigned char H1 calibration value for humidity
    int16_t h2;   //!< signed short H2 calibration value for humidity
    uint8_t h3;   //!< unsigned char H3 calibration value for humidity
    int16_t h4;   //!< signed short H4 calibration value for humidity
    int16_t h5;   //!< signed short H5 calibration value for humidity
    int8_t h6;    //!< signed char H6 calibration value for humidity
  } data;
  uint8_t BYTE[33];
} bme280_calib_t;

// --- Variables
static bool _initDone = false;
static bme280_calib_t _bme280_calib;
static bme280_config_t _bme280_config;
static int32_t _bme280_tFine;

// --- Local Function Prototypes
static float _bme280_temperatureCompensateFixedPoint (int32_t uncomp_temp) __attribute__((unused));
static float _bme280_pressureCompensateFixedPoint (int32_t uncomp_press) __attribute__((unused));
static float _bme280_humidityCompensateFixedPoint (int32_t uncomp_hum) __attribute__((unused));
static float _bme280_temperatureCompensateFloat (int32_t uncomp_temp);
static float _bme280_pressureCompensateFloat (int32_t uncomp_press);
static float _bme280_humidityCompensateFloat (int32_t uncomp_hum);
static float _bme280_temperatureCompensateDouble (int32_t uncomp_temp) __attribute__((unused));
static float _bme280_pressureCompensateDouble (int32_t uncomp_press) __attribute__((unused));
static float _bme280_humidityCompensateDouble (int32_t uncomp_hum) __attribute__((unused));

/**
 ****************************************************************
 @brief   Initialize the BME280 sensor.
 @brief   It reads the chip-id and calibration data from the sensor.
 @param   config  The to be setup sensor configuration (structure)
 @return  -
 ****************************************************************
 */
void bme280_init (bme280_config_t config)
{
  uint8_t txData[32];
  uint8_t rxData[32];
  uint32_t idx;

  if (!_initDone)
  {
    txData[0] = BME280_CHIP_ID_ADDR | BME280_READ;
    txData[1] = 0xFF;
    HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_BME280);
    HAL_SPI1_transfer(txData, rxData, 2);
    HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

    //check for chip id validity
    if (rxData[1] == BME280_CHIP_ID)
    {
      //do a softReset of the sensor
      txData[0] = BME280_SOFT_RESET_ADDR | BME280_WRITE;
      txData[1] = BME280_SOFT_RESET_CMD;
      HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_BME280);
      HAL_SPI1_transfer(txData, rxData, 2);
      HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

      //a maximum start-up time of 2ms is required
      sys_sleepForUs(2e3);

      for (idx = 1; idx < 25; idx++)
      {
        txData[idx] = 0xFF;
      }
      //read out calibration parameters for temperature and pressure
      txData[0] = BME280_DIG_T1_LSB_ADDR | BME280_READ;
      HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_BME280);
      HAL_SPI1_transfer(txData, rxData, 25);
      HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);
      memcpy(&_bme280_calib.data.t1, &rxData[1], 24);

      //read out calibration parameters for humidity
      txData[0] = BME280_DIG_H1_ADDR | BME280_READ;
      HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_BME280);
      HAL_SPI1_transfer(txData, rxData, 2);
      HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);
      _bme280_calib.data.h1 = rxData[1];

      txData[0] = BME280_DIG_H2_LSB_ADDR | BME280_READ;
      HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_BME280);
      HAL_SPI1_transfer(txData, rxData, 8);
      HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);
      _bme280_calib.data.h2 = (int16_t) (((uint16_t) rxData[1]) | (((uint16_t) rxData[2]) << 8));
      _bme280_calib.data.h3 = rxData[3];
      _bme280_calib.data.h4 = (int16_t) ((((uint16_t) rxData[4]) << 4) | ((uint16_t) (rxData[5] & 0x0F)));
      _bme280_calib.data.h5 = (int16_t) ((((uint16_t) (rxData[5] & 0xF0)) >> 4) | (((uint16_t) rxData[6]) << 4));
      _bme280_calib.data.h6 = (int8_t) rxData[7];
    }
    else
    {
      assert_param(0);
    }

    //setup the sensor configuration
    _bme280_config = config;
    //setup humidity oversampling
    txData[0] = BME280_CTRL_HUM_ADDR | BME280_WRITE;
    txData[1] = _bme280_config.osHumi;
    HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_BME280);
    HAL_SPI1_transfer(txData, rxData, 2);
    HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

    bme280_modeSet(MODE_SLEEP);
    //setup configuration register
    txData[0] = BME280_CONFIG_ADDR | BME280_WRITE;
    txData[1] = (_bme280_config.standbyTime << 5) | (_bme280_config.filter << 2);
    HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_BME280);
    HAL_SPI1_transfer(txData, rxData, 2);
    HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

    _initDone = true;
  }
}

/**
 ****************************************************************
 @brief   Setup the operating mode of the BME280 sensor.
 @param   mode  One of the possible operating modes
 @return  -
 ****************************************************************
 */
void bme280_modeSet (bme280_mode_t mode)
{
  uint8_t txData[2];
  uint8_t rxData[2];

  //setup the sensor mode
  txData[0] = BME280_CTRL_MEAS_ADDR | BME280_WRITE;
  txData[1] = (_bme280_config.osTemp << 5) | (_bme280_config.osPres << 2) | (mode);
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_BME280);
  HAL_SPI1_transfer(txData, rxData, 2);
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);
}

/**
 ****************************************************************
 @brief   Return the needed measurement time depending on the current configuration.
 @param   -
 @return  The resulting measurement time in uSeconds.
 ****************************************************************
 */
uint32_t bme280_measTimeGet (void)
{
  uint32_t oversamplingNr = 0;
  uint32_t time_us;

  if (_bme280_config.osTemp != OS_NONE)
  {
    oversamplingNr += 1 << (_bme280_config.osTemp - 1);
  }
  if (_bme280_config.osPres != OS_NONE)
  {
    oversamplingNr += 1 << (_bme280_config.osPres - 1);
  }
  if (_bme280_config.osHumi != OS_NONE)
  {
    oversamplingNr += 1 << (_bme280_config.osHumi - 1);
  }
  time_us = 2300 * oversamplingNr;
  time_us += (1250 + 575 + 500);

  return time_us;
}

/**
 ****************************************************************
 @brief   Read out the last sensor measurement data and do the corresponding
 *        compensation afterwards.
 @param   pMeasData   Pointer to the resulting measurement datas.
 @return  Return true, if new valid data are available (measuring is zero)
 ****************************************************************
 */
bool bme280_dataGet (bme280_measData_t *pMeasData)
{
  uint8_t txData[12];
  uint8_t rxData[12];
  uint32_t idx;
  int32_t uncomp_press;
  int32_t uncomp_temp;
  int32_t uncomp_hum;

  //read the status register
  txData[0] = BME280_STATUS_ADDR | BME280_READ;
  txData[1] = 0xFF;
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_BME280);
  HAL_SPI1_transfer(txData, rxData, 2);
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

  if (rxData[1] & 0x08)
  {
    //mark values as not a number
    pMeasData->temperature = NAN;
    pMeasData->pressure = NAN;
    pMeasData->humidity = NAN;

    return false;
  }
  else
  {
    //read out all measurement registers
    txData[0] = BME280_PRES_MSB_ADDR | BME280_READ;
    for (idx = 2; idx < 9; idx++)
    {
      txData[idx] = 0xFF;
    }
    HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_BME280);
    HAL_SPI1_transfer(txData, rxData, 9);
    HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

    uncomp_press = (int32_t) ((((uint32_t) (rxData[1])) << 12) | (((uint32_t) (rxData[2])) << 4)
      | ((uint32_t) rxData[3] >> 4));
    uncomp_temp = (int32_t) ((((int32_t) (rxData[4])) << 12) | (((int32_t) (rxData[5])) << 4)
      | (((int32_t) (rxData[6])) >> 4));
    uncomp_hum = (int32_t) ((((int32_t) (rxData[7])) << 8) | ((int32_t) rxData[8]));

    //call the compensations functions
    pMeasData->temperature = _bme280_temperatureCompensateFloat(uncomp_temp);
    pMeasData->pressure = _bme280_pressureCompensateFloat(uncomp_press);
    pMeasData->humidity = _bme280_humidityCompensateFloat(uncomp_hum);

    return true;
  }
}

/**
 ****************************************************************
 @brief   Internally function to calculate the compensated correct temperature value.
 @note    All the calculations are done by fixpoint unit
 @param   uncomp_temp The uncompensated temperature value.
 @return  The compensated temperature in °C unit.
 ****************************************************************
 */
static float _bme280_temperatureCompensateFixedPoint (int32_t uncomp_temp)
{
  int32_t var1;
  int32_t var2;
  int32_t temperature;
  const int32_t temperature_min = -4000;
  const int32_t temperature_max = 8500;

  var1 = (int32_t) ((uncomp_temp / 8) - ((int32_t) _bme280_calib.data.t1 * 2));
  var1 = (var1 * ((int32_t) _bme280_calib.data.t2)) / 2048;
  var2 = (int32_t) ((uncomp_temp / 16) - ((int32_t) _bme280_calib.data.t1));
  var2 = (((var2 * var2) / 4096) * ((int32_t) _bme280_calib.data.t3)) / 16384;
  _bme280_tFine = var1 + var2;
  temperature = (_bme280_tFine * 5 + 128) / 256;

  if (temperature < temperature_min)
  {
    temperature = temperature_min;
  }
  else if (temperature > temperature_max)
  {
    temperature = temperature_max;
  }

  return ((float) temperature) / 100.0f;
}

/**
 ****************************************************************
 @brief   Internally function to calculate the compensated correct pressure value.
 @note    All the calculations are done by fixpoint unit
 @param   uncomp_press The uncompensated pressure value.
 @return  The compensated pressure in Pa unit.
 ****************************************************************
 */
static float _bme280_pressureCompensateFixedPoint (int32_t uncomp_press)
{
  int64_t var1;
  int64_t var2;
  int64_t var3;
  int64_t var4;
  uint32_t pressure;
  const uint32_t pressure_min = 3000000;
  const uint32_t pressure_max = 11000000;

  var1 = ((int64_t) _bme280_tFine) - 128000;
  var2 = var1 * var1 * (int64_t) _bme280_calib.data.p6;
  var2 = var2 + ((var1 * (int64_t) _bme280_calib.data.p5) * 131072);
  var2 = var2 + (((int64_t) _bme280_calib.data.p4) * 34359738368);
  var1 = ((var1 * var1 * (int64_t) _bme280_calib.data.p3) / 256) + ((var1 * ((int64_t) _bme280_calib.data.p2) * 4096));
  var3 = ((int64_t) 1) * 140737488355328;
  var1 = (var3 + var1) * ((int64_t) _bme280_calib.data.p1) / 8589934592;

  /* To avoid divide by zero exception */
  if (var1 != 0)
  {
    var4 = 1048576 - uncomp_press;
    var4 = (((var4 * INT64_C(2147483648)) - var2) * 3125) / var1;
    var1 = (((int64_t) _bme280_calib.data.p9) * (var4 / 8192) * (var4 / 8192)) / 33554432;
    var2 = (((int64_t) _bme280_calib.data.p8) * var4) / 524288;
    var4 = ((var4 + var1 + var2) / 256) + (((int64_t) _bme280_calib.data.p7) * 16);
    pressure = (uint32_t) (((var4 / 2) * 100) / 128);

    if (pressure < pressure_min)
    {
      pressure = pressure_min;
    }
    else if (pressure > pressure_max)
    {
      pressure = pressure_max;
    }
  }
  else
  {
    pressure = pressure_min;
  }

  return ((float) pressure) / 100.0f;
}

/**
 ****************************************************************
 @brief   Internally function to calculate the compensated correct humidity value.
 @note    All the calculations are done by fixpoint unit
 @param   uncomp_hum The uncompensated humidity value.
 @return  The compensated humidity in % unit.
 ****************************************************************
 */
static float _bme280_humidityCompensateFixedPoint (int32_t uncomp_hum)
{
  int32_t v_x1_u32r;
  uint32_t h_res_q22_10;

  v_x1_u32r = (_bme280_tFine - ((int32_t) 76800));

  v_x1_u32r = (((((uncomp_hum << 14) - (((int32_t) _bme280_calib.data.h4) << 20)
    - (((int32_t) _bme280_calib.data.h5) * v_x1_u32r)) + ((int32_t) 16384)) >> 15)
    * (((((((v_x1_u32r * ((int32_t) _bme280_calib.data.h6)) >> 10)
      * (((v_x1_u32r * ((int32_t) _bme280_calib.data.h3)) >> 11) + ((int32_t) 32768))) >> 10) + ((int32_t) 2097152))
      * ((int32_t) _bme280_calib.data.h2) + 8192) >> 14));
  v_x1_u32r = (v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) * ((int32_t) _bme280_calib.data.h1)) >> 4));
  v_x1_u32r = (v_x1_u32r < 0 ? 0 : v_x1_u32r);
  v_x1_u32r = (v_x1_u32r > 419430400 ? 419430400 : v_x1_u32r);
  h_res_q22_10 = (uint32_t) (v_x1_u32r >> 12);

  return ((float) h_res_q22_10) / 1024.0f;
}

/**
 ****************************************************************
 @brief   Internally function to calculate the compensated correct temperature value.
 @note    All the calculations are done by float unit
 @param   uncomp_temp The uncompensated temperature value.
 @return  The compensated temperature in °C unit.
 ****************************************************************
 */
static float _bme280_temperatureCompensateFloat (int32_t uncomp_temp)
{
  float var1;
  float var2;
  float temperature;
  const float temperature_min = -40.0f;
  const float temperature_max = 85.0f;

  var1 = ((float) uncomp_temp) / 16384.0f - ((float) _bme280_calib.data.t1) / 1024.0f;
  var1 = var1 * ((float) _bme280_calib.data.t2);
  var2 = (((float) uncomp_temp) / 131072.0f - ((float) _bme280_calib.data.t1) / 8192.0f);
  var2 = (var2 * var2) * ((float) _bme280_calib.data.t3);
  _bme280_tFine = (int32_t) (var1 + var2);
  temperature = (var1 + var2) / 5120.0f;

  if (temperature < temperature_min)
  {
    temperature = temperature_min;
  }
  else if (temperature > temperature_max)
  {
    temperature = temperature_max;
  }

  return temperature;
}

/**
 ****************************************************************
 @brief   Internally function to calculate the compensated correct pressure value.
 @note    All the calculations are done by float unit
 @param   uncomp_press The uncompensated pressure value.
 @return  The compensated pressure in Pa unit.
 ****************************************************************
 */
static float _bme280_pressureCompensateFloat (int32_t uncomp_press)
{
  float var1;
  float var2;
  float var3;
  float pressure;
  const float pressure_min = 30000.0f;
  const float pressure_max = 110000.0f;

  var1 = ((float) _bme280_tFine / 2.0f) - 64000.0f;
  var2 = var1 * var1 * ((float) _bme280_calib.data.p6) / 32768.0f;
  var2 = var2 + var1 * ((float) _bme280_calib.data.p5) * 2.0f;
  var2 = (var2 / 4.0f) + (((float) _bme280_calib.data.p4) * 65536.0f);
  var3 = ((float) _bme280_calib.data.p3) * var1 * var1 / 524288.0f;
  var1 = (var3 + ((float) _bme280_calib.data.p2) * var1) / 524288.0f;
  var1 = (1.0f + var1 / 32768.0f) * ((float) _bme280_calib.data.p1);

  /* avoid exception caused by division by zero */
  if (var1 > (0.0f))
  {
    pressure = 1048576.0f - (float) uncomp_press;
    pressure = (pressure - (var2 / 4096.0f)) * 6250.0f / var1;
    var1 = ((float) _bme280_calib.data.p9) * pressure * pressure / 2147483648.0f;
    var2 = pressure * ((float) _bme280_calib.data.p8) / 32768.0f;
    pressure = pressure + (var1 + var2 + ((float) _bme280_calib.data.p7)) / 16.0f;

    if (pressure < pressure_min)
    {
      pressure = pressure_min;
    }
    else if (pressure > pressure_max)
    {
      pressure = pressure_max;
    }
  }
  else /* Invalid case */
  {
    pressure = pressure_min;
  }

  return pressure;
}

/**
 ****************************************************************
 @brief   Internally function to calculate the compensated correct humidity value.
 @note    All the calculations are done by float unit
 @param   uncomp_hum The uncompensated humidity value.
 @return  The compensated humidity in % unit.
 ****************************************************************
 */
static float _bme280_humidityCompensateFloat (int32_t uncomp_hum)
{
  float humidity;
  float humidity_min = 0.0f;
  float humidity_max = 100.0f;
  float var1;
  float var2;
  float var3;
  float var4;
  float var5;
  float var6;

  var1 = ((float) _bme280_tFine) - 76800.0f;
  var2 = (((float) _bme280_calib.data.h4) * 64.0f + (((float) _bme280_calib.data.h5) / 16384.0f) * var1);
  var3 = ((float) uncomp_hum) - var2;
  var4 = ((float) _bme280_calib.data.h2) / 65536.0f;
  var5 = (1.0f + (((float) _bme280_calib.data.h3) / 67108864.0f) * var1);
  var6 = 1.0f + (((float) _bme280_calib.data.h6) / 67108864.0f) * var1 * var5;
  var6 = var3 * var4 * (var5 * var6);
  humidity = var6 * (1.0f - ((float) _bme280_calib.data.h1) * var6 / 524288.0f);

  if (humidity > humidity_max)
  {
    humidity = humidity_max;
  }
  else if (humidity < humidity_min)
  {
    humidity = humidity_min;
  }

  return humidity;
}

/**
 ****************************************************************
 @brief   Internally function to calculate the compensated correct temperature value.
 @note    All the calculations are done by double unit
 @param   uncomp_temp The uncompensated temperature value.
 @return  The compensated temperature in °C unit.
 ****************************************************************
 */
static float _bme280_temperatureCompensateDouble (int32_t uncomp_temp)
{
  double var1;
  double var2;
  double temperature;
  const double temperature_min = -40.0;
  const double temperature_max = 85.0;

  var1 = ((double) uncomp_temp) / 16384.0 - ((double) _bme280_calib.data.t1) / 1024.0;
  var1 = var1 * ((double) _bme280_calib.data.t2);
  var2 = (((double) uncomp_temp) / 131072.0 - ((double) _bme280_calib.data.t1) / 8192.0);
  var2 = (var2 * var2) * ((double) _bme280_calib.data.t3);
  _bme280_tFine = (int32_t) (var1 + var2);
  temperature = (var1 + var2) / 5120.0;

  if (temperature < temperature_min)
  {
    temperature = temperature_min;
  }
  else if (temperature > temperature_max)
  {
    temperature = temperature_max;
  }

  return ((float) temperature);
}

/**
 ****************************************************************
 @brief   Internally function to calculate the compensated correct pressure value.
 @note    All the calculations are done by double unit
 @param   uncomp_press The uncompensated pressure value.
 @return  The compensated pressure in Pa unit.
 ****************************************************************
 */
static float _bme280_pressureCompensateDouble (int32_t uncomp_press)
{
  double var1;
  double var2;
  double var3;
  double pressure;
  const double pressure_min = 30000.0;
  const double pressure_max = 110000.0;

  var1 = ((double) _bme280_tFine / 2.0) - 64000.0;
  var2 = var1 * var1 * ((double) _bme280_calib.data.p6) / 32768.0;
  var2 = var2 + var1 * ((double) _bme280_calib.data.p5) * 2.0;
  var2 = (var2 / 4.0) + (((double) _bme280_calib.data.p4) * 65536.0);
  var3 = ((double) _bme280_calib.data.p3) * var1 * var1 / 524288.0;
  var1 = (var3 + ((double) _bme280_calib.data.p2) * var1) / 524288.0;
  var1 = (1.0 + var1 / 32768.0) * ((double) _bme280_calib.data.p1);

  /* avoid exception caused by division by zero */
  if (var1 > (0.0))
  {
    pressure = 1048576.0 - (double) uncomp_press;
    pressure = (pressure - (var2 / 4096.0)) * 6250.0 / var1;
    var1 = ((double) _bme280_calib.data.p9) * pressure * pressure / 2147483648.0;
    var2 = pressure * ((double) _bme280_calib.data.p8) / 32768.0;
    pressure = pressure + (var1 + var2 + ((double) _bme280_calib.data.p7)) / 16.0;

    if (pressure < pressure_min)
    {
      pressure = pressure_min;
    }
    else if (pressure > pressure_max)
    {
      pressure = pressure_max;
    }
  }
  else /* Invalid case */
  {
    pressure = pressure_min;
  }

  return ((float) pressure);
}

/**
 ****************************************************************
 @brief   Internally function to calculate the compensated correct humidity value.
 @note    All the calculations are done by double unit
 @param   uncomp_hum The uncompensated humidity value.
 @return  The compensated humidity in % unit.
 ****************************************************************
 */
static float _bme280_humidityCompensateDouble (int32_t uncomp_hum)
{
  double humidity;
  double humidity_min = 0.0;
  double humidity_max = 100.0;
  double var1;
  double var2;
  double var3;
  double var4;
  double var5;
  double var6;

  var1 = ((double) _bme280_tFine) - 76800.0;
  var2 = (((double) _bme280_calib.data.h4) * 64.0 + (((double) _bme280_calib.data.h5) / 16384.0) * var1);
  var3 = ((double) uncomp_hum) - var2;
  var4 = ((double) _bme280_calib.data.h2) / 65536.0;
  var5 = (1.0 + (((double) _bme280_calib.data.h3) / 67108864.0) * var1);
  var6 = 1.0 + (((double) _bme280_calib.data.h6) / 67108864.0) * var1 * var5;
  var6 = var3 * var4 * (var5 * var6);
  humidity = var6 * (1.0 - ((double) _bme280_calib.data.h1) * var6 / 524288.0);

  if (humidity > humidity_max)
  {
    humidity = humidity_max;
  }
  else if (humidity < humidity_min)
  {
    humidity = humidity_min;
  }

  return ((float) humidity);
}

/**
 * @}
 */

/**
 * @}
 */
