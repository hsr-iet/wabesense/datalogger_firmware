/**
 ****************************************************************
 @file    SYS.c
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the system (clock).
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-10-28
 ****************************************************************
 */

/** @addtogroup HWO
 * @{
 */

/** @addtogroup SYS
 * @{
 */

// --- Includes
#include "sys.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "../DEF/GLOB_Types.h"
#include "../HAL/HAL_DWT.h"
#include "main.h"
#include "adc.h"
#include "gpio.h"
#include "rtc.h"
#include "tim.h"
#include "spi.h"
#include "DIG_IO.h"
#include "usb_device.h"

// --- Defines

// --- Typedefs

// --- Variables
static volatile bool _sleepTimeExp = false;

// --- Local Function Prototypes
static void _sys_sleepTimeExpCb (void);

/**
 ****************************************************************
 @brief   Initialize the system module.
 @param   -
 @return  -
 ****************************************************************
 */
void sys_init (void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct =
  {
    0
  };
  RCC_ClkInitTypeDef RCC_ClkInitStruct =
  {
    0
  };
  RCC_PeriphCLKInitTypeDef PeriphClkInit =
  {
    0
  };

  HAL_SetTickFreq(HAL_TICK_FREQ_10HZ);

  digIO_init();
  //check if USB is connected or not (Power Range 2 only possible without USB)
  if (digIO_usbDetected())
  {
    //configure LSE Drive Capability
    HAL_PWR_EnableBkUpAccess();
    __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
    //configure the main internal regulator output voltage
    if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
    {
      Error_Handler();
    }
    //initializes the CPU, AHB and APB busses clocks
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48 | RCC_OSCILLATORTYPE_HSI | RCC_OSCILLATORTYPE_LSE;
    RCC_OscInitStruct.LSEState = RCC_LSE_ON;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
    RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
      Error_Handler();
    }
    //initializes the CPU, AHB and APB busses clocks
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
    {
      Error_Handler();
    }
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_USB | RCC_PERIPHCLK_ADC;
    PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
    PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
    {
      Error_Handler();
    }
  }
  else  //no USB detected
  {
    //configure LSE Drive Capability
    HAL_PWR_EnableBkUpAccess();
    __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
    //configure the main internal regulator output voltage
    if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE2) != HAL_OK)
    {
      Error_Handler();
    }
    //initializes the CPU, AHB and APB busses clocks
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI | RCC_OSCILLATORTYPE_LSE;
    RCC_OscInitStruct.LSEState = RCC_LSE_ON;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSI48State = RCC_HSI48_OFF;
    RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
      Error_Handler();
    }
    //initializes the CPU, AHB and APB busses clocks
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
    {
      Error_Handler();
    }
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_USB | RCC_PERIPHCLK_ADC;
    PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
    PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
    {
      Error_Handler();
    }
  }

  SystemCoreClockUpdate();

  //enable data watch and trace unit
  hal_dwt_init();
}

/**
 ****************************************************************
 @brief   Get the unique ID of the microcontroller unit.
 @param   pSysId  Pointer to the unique identification number
 @return  -
 ****************************************************************
 */
void sys_uniqueIdGet (sysID_t *pSysId)
{
  pSysId->systemID[0] = HAL_GetUIDw0();
  pSysId->systemID[1] = HAL_GetUIDw1();
  pSysId->systemID[2] = HAL_GetUIDw2();
}

/**
 ****************************************************************
 @brief  Reset the data watchpoint counter value
 @param  -
 @return -
 ****************************************************************
 */
void sys_dwtCpuClockCntReset (void)
{
  hal_dwt_cpuClockCntReset();
}

/**
 ****************************************************************
 @brief  Get the current data watchpoint counter value
 @param  -
 @return The current data watchpoint counter value (number of CPU cycles)
 ****************************************************************
 */
uint32_t sys_dwtCpuClockCntGet (void)
{
  return hal_dwt_cpuClockCntGet();
}

/**
 ****************************************************************
 @brief   Set the system to sleep mode for an amount of time in microseconds..
 @param   sleepTime_us  The time to set the system to sleep mode in microseconds.
 @return  -
 ****************************************************************
 */
void sys_sleepForUs (uint32_t sleepTime_us)
{
  _sleepTimeExp = false;

  if (sleepTime_us < 20)
  {
    HAL_TIM6_StartInterrupt(_sys_sleepTimeExpCb, sleepTime_us);
    //no sleep of the system

    while (!_sleepTimeExp);
  }
  else
  {
    //check if USB is connected or not (Power Range 2 only possible without USB)
    if (digIO_usbDetected())
    {
      HAL_TIM6_StartInterrupt(_sys_sleepTimeExpCb, sleepTime_us);
      //set system to sleep but with main regulator on
      HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);

      while (!_sleepTimeExp);
    }
    else
    {
      HAL_TIM6_StartInterrupt(_sys_sleepTimeExpCb, sleepTime_us);
      //set system to sleep in lowPower sleep mode (chapter 5.3.5)
      HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);

      while (!_sleepTimeExp);
    }
  }
}

/**
 ****************************************************************
 @brief   Put the system to standby mode (wakeUp over internal RTC or external wakeUp-Pins)
 @param   -
 @return  -
 ****************************************************************
 */
void sys_standbyEnter (void)
{
  //switch OFF 3.3V switched power supply
  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_EXT_I2C1);
  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_FLASH);
  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_RGB_LED);
  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_17VSW);
  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_SDCARD);
  //switch OFF 17V switched power supply
  digIO_17VPwrReqOff(DIG_IO_17VUNIT_EXT_SENSOR);

  ///@TODO check if USB or SD-Card is inserted, maybe do not clear flags from below
  __HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);
  __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);

  //wait while a user button is still pressed!
  while (digIO_usrBt1Pressed() || digIO_usrBt2Pressed());
  sys_sleepForUs(10e3);  //10ms delay

  HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN1); //VDD_USB
  HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN2); //User button 1 or 2
  HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN4); //SDCardDetection

  HAL_PWR_EnterSTANDBYMode();
}

/**
 ****************************************************************
 @brief   Enter the usb bootloader mode
 @param   -
 @return  -
 ****************************************************************
 */
void sys_usbBootloaderEnter (void)
{
  volatile uint32_t jumpAddr;
  volatile pFV_V_t JumpToApplication;

  //shutdown USB
  USB_DEVICE_DeInit();

  __disable_irq();  //disable all interrupts

  //do all the deInitializations
  HAL_DeInit();

  SysTick->CTRL = 0;
  SysTick->LOAD = 0;
  SysTick->VAL = 0;

  //ARM Cortex-M Programming Guide to Memory Barrier Instructions.
  __DSB();

  __HAL_SYSCFG_REMAPMEMORY_SYSTEMFLASH();

  //Remap is not visible at once. Execute some unrelated command!
  __DSB();
  __ISB();

  __enable_irq();
  //SYSTEM_MEMORY_ADDRESS 0x1FFF0000
  jumpAddr = *(uint32_t*) (0x1FFF0000 + 4);
  JumpToApplication = (pFV_V_t) jumpAddr;
  //Initialize user application's Stack Pointer
  __set_MSP(*(uint32_t*) 0x1FFF0000);

  JumpToApplication();

  while (1); //we will never come back to here
}

/**
 ****************************************************************
 @brief   Enter the sdCard bootloader mode
 @param   -
 @return  -
 ****************************************************************
 */
void sys_sdCardBootloaderEnter (void)
{
  uint32_t vTableAddr = 0x08000000;
  uint32_t jumpAddr;
  pFV_V_t bootloaderResetHandler;

  //check if USB is connected, then shut down interface
  if (digIO_usbDetected())
  {
    //shutdown USB
    USB_DEVICE_DeInit();
  }

  //do all the deInitializations
  HAL_GPIO_deInit();
  HAL_ADC1_deInit();
  HAL_SPI1_deInit();
  HAL_TIM1_deInit();
  HAL_TIM6_deInit();
  HAL_RTC_deInit();   //only disable interrupt of RTC

  __disable_irq();
  HAL_DeInit();

  SysTick->CTRL = 0;
  SysTick->LOAD = 0;
  SysTick->VAL = 0;

  //set vector table address
  SCB->VTOR = FLASH_BASE;
  __DSB();
  __ISB();

  __enable_irq();

  jumpAddr = *(uint32_t*) (vTableAddr + 4);
  bootloaderResetHandler = (pFV_V_t) jumpAddr;
  //initialize user application's Stack Pointer
  __set_MSP(*(uint32_t*) vTableAddr);

  //jump to custom SD-Card bootloader
  bootloaderResetHandler();
  //does not return here
  while (1)
  {

  }
}

/**
 ****************************************************************
 @brief   Reset the absolute tick counter value.
 @param   -
 @return  -
 ****************************************************************
 */
void sys_sysTickReset (void)
{
  HAL_SYSTICK_Reset();
}

/**
 ****************************************************************
 @brief   Get the absolute tick counter value since last sys_sysTickReset().
 @param   pTick  Pointer to the actual TickCounter value.
 @return  -
 ****************************************************************
 */
void sys_sysTickTickGet (uint32_t *pTick)
{
  HAL_SYSTICK_TickGet(pTick);
}

/**
 ****************************************************************
 @brief   Get the difference between a former TickCounter value and the
 *        actual TickCounter value.
 @param   oldTick    Former TickCounter value
 @param   pDeltaTick Pointer to the resulting TickCounter difference.
 @return  -
 ****************************************************************
 */
void sys_sysTickTickDeltaGet (uint32_t oldTick, uint32_t *pDeltaTick)
{
  HAL_SYSTICK_TickDeltaGet(oldTick, pDeltaTick);
}

/**
 ****************************************************************
 @brief   Get the difference between a former TickCounter value and the
 *        actual TickCounter value. Update pOldTick to actual TickCounter value.
 @param   pOldTick   Pointer to a former TickCounter value, updated to actual TickCounter value.
 @param   pDeltaTick Pointer to the resulting TickCounter difference.
 @return  -
 ****************************************************************
 */
void sys_sysTickTickDeltaUpdateGet (uint32_t *pOldTick, uint32_t *pDeltaTick)
{
  HAL_SYSTICK_TickDeltaUpdateGet(pOldTick, pDeltaTick);
}

/**
 ****************************************************************
 @brief   Register a callback function, which should be executed in
 *        sysTick interrupt service routine.
 @param   pSysTickCbFct Pointer to the callback function to be registered
 @return  -
 ****************************************************************
 */
void sys_sysTickRegCallbackFct (pFV_V_t pSysTickCbFct)
{
  HAL_SYSTICK_RegCallbackFct(pSysTickCbFct);
}

/**
 ****************************************************************
 @brief   Get the reason for the system wakeUp.
 @param   -
 @return  One of the possible wakeUp reasons from enumeration.
 ****************************************************************
 */
sysWakeUp_t sys_wakupReasonGet (void)
{
  sysWakeUp_t wakeUpReson = SYS_WAKEUP_HW_RESET;

  //Get the pending status of the WAKEUPTIMER Interrupt
  if (__HAL_RTC_WAKEUPTIMER_GET_FLAG(&hrtc, RTC_FLAG_WUTF) != 0U)
  {
    //Clear the WAKEUPTIMER interrupt pending bit
    __HAL_RTC_WAKEUPTIMER_CLEAR_FLAG(&hrtc, RTC_FLAG_WUTF);

    wakeUpReson = SYS_WAKEUP_RTC;
  }
  else if (__HAL_PWR_GET_FLAG(PWR_FLAG_WUF1) != RESET)
  {
    //WAKEUP_PIN1 -> VDD_USB
    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF1);
    wakeUpReson = SYS_WAKEUP_USB_VDD;
  }
  else if (__HAL_PWR_GET_FLAG(PWR_FLAG_WUF2) != RESET)
  {
    //WAKEUP_PIN2 -> USR_BT1 or USR_BT2
    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF2);

    if (HAL_GPIO_ReadPin(USR_BT1_GPIO_Port, USR_BT1_Pin) == GPIO_PIN_SET)
    {
      wakeUpReson = SYS_WAKEUP_USR_BT1;
    }
    else if (HAL_GPIO_ReadPin(USR_BT2_GPIO_Port, USR_BT2_Pin) == GPIO_PIN_SET)
    {
      wakeUpReson = SYS_WAKEUP_USR_BT2;
    }
  }
  else if (__HAL_PWR_GET_FLAG(PWR_FLAG_WUF4) != RESET)
  {
    //WAKEUP_PIN4 -> SDCardDetection
    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WUF4);
    wakeUpReson = SYS_WAKEUP_SDCARD;
  }
  else if ((__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST) != RESET) || (__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST) != RESET))
  {
    //Independent or Window Watchdog reset
    __HAL_RCC_CLEAR_RESET_FLAGS();
    wakeUpReson = SYS_WAKEUP_WATCHDOG;
  }
  else if (__HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST) != RESET)
  {
    //software based reset
    __HAL_RCC_CLEAR_RESET_FLAGS();
    wakeUpReson = SYS_WAKEUP_SW_RESET;
  }
  else
  {
    //all other reset reasons are currently marked as hardware reset
    __HAL_RCC_CLEAR_RESET_FLAGS();
    wakeUpReson = SYS_WAKEUP_HW_RESET;
  }

  __HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);
  __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);

  return wakeUpReson;
}

/**
 ****************************************************************
 @brief   Check if the wakeUpTimer is active or not
 @param   -
 @return  Return true, if the timer is activated, otherwise false
 ****************************************************************
 */
bool sys_wakeupTimerActive (void)
{
  return HAL_RTC_WakeUpTimerActivated();
}

/**
 ****************************************************************
 @brief   Set the next wakeUp time in seconds
 @param   wakeupTime_s  The wakeUp time in seconds (1...65536), 0 means deactivate
 @return  -
 ****************************************************************
 */
void sys_wakeupTimerSet (uint32_t wakeupTime_s)
{
  HAL_StatusTypeDef status;

  assert_param(wakeupTime_s <= 65536);
  //ck_spre frequency = RTCCLK / ((PREDIV_A + 1) * (PREDIV_S + 1))

  //RTCCLK is 32.768kHz
  //PREDIV_A set to 127
  //PREDIV_S set to 255
  //ck_spre frequency = 1Hz

  status = HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);
  assert_param(status == HAL_OK);

  if (wakeupTime_s)
  {
    //no auto clear of the flag
    status = HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, wakeupTime_s - 1, RTC_WAKEUPCLOCK_CK_SPRE_16BITS, 0);
    assert_param(status == HAL_OK);
  }
}

/**
 ****************************************************************
 @brief   Check if the wakeUp timer has expired. clear flag on expiration.
 @param   -
 @return  Return true if the wakeUp timer has expired.
 ****************************************************************
 */
bool sys_wakeupTimerExpired (void)
{
  return HAL_RTC_WakeUpEvtGetAndClear();
}

/**
 ****************************************************************
 @brief   Set a new RTC date/time information  (binary format).
 @param   pDateTime Pointer to the new date/time information data.
 @return  -
 ****************************************************************
 */
void sys_rtcDateTimeSet (rtcDateTime_t *pDateTime)
{
  HAL_RTC_DateTimeSet(pDateTime);
}

/**
 ****************************************************************
 @brief   Get the current RTC date/time information (binary format).
 @param   pDateTime Pointer to the current date/time information.
 @return  -
 ****************************************************************
 */
void sys_rtcDateTimeGet (rtcDateTime_t *pDateTime)
{
  HAL_RTC_DateTimeGet(pDateTime);
}

/**
 ****************************************************************
 @brief   Set the value of one of the possible backup registers, which
 *        are part of the RTC module.
 @param   reg     One of the possible system defined backup registers (from enumeration)
 @param   value   The to be set value
 @return  -
 ****************************************************************
 */
void sys_backupRegisterSet (sysBackupReg_t reg, uint32_t value)
{
  HAL_RTC_BackupRegisterSet(reg, value);
}

/**
 ****************************************************************
 @brief   Get a value out of one of the possible backup registers, which
 *        are part of the RTC module.
 @param   reg     One of the possible system defined backup registers (from enumeration)
 @return  The value read out of this register.
 ****************************************************************
 */
uint32_t sys_backupRegisterGet (sysBackupReg_t reg)
{
  return HAL_RTC_BackupRegisterGet(reg);
}

/**
 ****************************************************************
 @brief   Internal callback function on Timer6 timeout done.
 @param   -
 @return  -
 ****************************************************************
 */
static void _sys_sleepTimeExpCb (void)
{
  //indicate to exit sleep mode after interrupt handler return
  HAL_PWR_DisableSleepOnExit();

  _sleepTimeExp = true;
}

/**
 * @}
 */

/**
 * @}
 */
