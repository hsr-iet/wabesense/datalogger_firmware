/**
 ****************************************************************
 @file    test_dataManager.c
 ****************************************************************
 @brief   This module implements some tests for the dataManager FWO-module.
 *
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 1.0
 @date    2021-10-11
 ****************************************************************
 */

/** @addtogroup TESTPROGRAMMS
 * @{
 */

// --- Includes
#include "test_dataManager.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "../HWO/SYS.h"
#include "../HWO/DIG_IO.h"
#include "../HWO/RG_LED.h"
#include "../HWO/FLASH.h"
#include "../FWO/dataManager.h"

// --- Defines
#define FLASH_BLOCKSTARTADDR_LOGDATA        0     //!< 4kByte Block number 0 to 1018
#define FLASH_BLOCKSTARTADDR_SYSCONFIG      1019  //!< 4kByte Block number 1019
#define FLASH_BLOCKSTARTADDR_RESERVED       1020  //!< 4kByte Block number 1020-1021 (reserved)
#define FLASH_BLOCKSTARTADDR_LOGHIST        1022  //!< 4kByte Block number 1022-1023 (64Bytes each entry: gives 128 log events to be stored)

// --- Typedefs
//! all data of a the system configuration packed together in a small memory footprint structure
typedef union
{
  struct __attribute__((packed))  //total 362 bytes used, 4096 bytes blocks reserved for system configuration
  {
    const uint32_t tag;                         //!< a simple 32bit Tag used for a simple validation check    4 Bytes
    char devName[DEV_NAME_MAX_LEN + 1];         //!< the device name                                         93 Bytes
    uint16_t measInterval_minutes;              //!< the measurement interval [minutes]                       2 Bytes
    uint16_t bat_nominalCellMilivoltage;        //!< the nominal battery cell voltage [mV]                    2 Bytes
    uint8_t bat_nrOfCells;                      //!< the number of installed battery cells                    1 Bytes
    char analogChannel0Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 0 name                         65 Bytes
    char analogChannel1Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 1 name                         65 Bytes
    char analogChannel2Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 2 name                         65 Bytes
    char analogChannel3Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 3 name                         65 Bytes
  };
  uint8_t BYTE[362];
} dataManager_sysConfigSmall_t;

// --- Variables
static const uint32_t flashStartAddr_sysConfig = FLASH_BLOCKSTARTADDR_SYSCONFIG * 4096;

// --- Local Function Prototypes

/**
 ****************************************************************
 @brief	  Do all possible tests with the dataManager module.
 @param   -
 @return  -
 ****************************************************************
 */
void test_dataManager (void)
{
  uint32_t nrOfMeasDataPoints;
  //uint32_t nrOfMeasDataPointsExpected;
  dataManager_dataPoint_t dataPoint;
  dataManager_dataPoint_t dataPointRead;
  uint8_t flashDataRead[256];
  uint32_t dataPointNr;
  uint32_t dataPointsStored;
  uint32_t logEventNr;
  uint32_t logEventsStored;
  uint32_t logEventsGet;
  dataManager_sysConfigSmall_t sysCfg_bevore;
  dataManager_sysConfigSmall_t sysCfg_after;
  rtcDateTime_t rtcDateTime;

#ifdef SEMI_HOSTING
  printf("DataManager_Test: start\n\r");
#endif

  digIO_init();

  //get RTC time
  sys_rtcDateTimeGet(&dataPoint.timeStamp);
  //rest of dataPoint values are dummy data
  dataPoint.timeBasedMeas = false;
  dataPoint.vBat = 5.123f;                  //internally stored as format 8.8, unsigned --> 1'311.488
  dataPoint.adc_in8 = 2.0;
  dataPoint.adc_in9 = 2.1;
  dataPoint.adc_in10 = 2.2;
  dataPoint.adc_in11 = 2.3;
  dataPoint.bme280.temperature = 25.123f;   //internally stored as format 8.8, signed --> 3'215.744
  dataPoint.bme280.pressure = 101300.123f;  //internally stored as float --> 1013.123
  dataPoint.bme280.humidity = 50.123f;      //internally stored as format 7.9, unsigned --> 25'662.976

  dataManager_init();

#ifdef SEMI_HOSTING
  printf("DataManager_Test: clear data & check");
#endif
  //read current sysConfiguration out of the flash
  flash_bytesRead(flashStartAddr_sysConfig, sysCfg_bevore.BYTE, sizeof(dataManager_sysConfigSmall_t));

  //***** Test: clear complete data and check the result of that
  dataManager_flashClearMeasDataAndHistory();

  //read current sysConfiguration out of the flash
  flash_bytesRead(flashStartAddr_sysConfig, sysCfg_after.BYTE, sizeof(dataManager_sysConfigSmall_t));

  //compare, if the system configuration is the same!
  assert_param(memcmp(sysCfg_bevore.BYTE, sysCfg_after.BYTE, sizeof(dataManager_sysConfigSmall_t)) == 0);

  nrOfMeasDataPoints = dataManager_flashNrOfMeasDataPointsGet(true);
  assert_param(nrOfMeasDataPoints == 0);
  nrOfMeasDataPoints = dataManager_flashNrOfMeasDataPointsGet(false);
  assert_param(nrOfMeasDataPoints == 0);

  //read FLASH data itself
  flash_bytesRead(0, flashDataRead, sizeof(flashDataRead));
  assert_param(flashDataRead[0] == 255);
  assert_param(flashDataRead[sizeof(flashDataRead) - 1] == 255);

#ifdef SEMI_HOSTING
  printf(" pass\n\r");
#endif

#ifdef SEMI_HOSTING
  printf("DataManager_Test: write and read one dataPoint");
#endif
  //***** Test: write and read dataPoint's
  //  130431 measData Points could be stored as maximum
  //  then, the first page should be cleared and the next point will be stored
  dataPoint.timeStamp.ms = 0;
  dataPoint.timeStamp.sec = 0;
  dataPoint.timeStamp.min = 0;
  dataPoint.timeStamp.hrs = 0;

  dataPointsStored = 0;
  for (dataPointNr = 0; dataPointNr < 300000; dataPointNr++)
  {
    dataPoint.timeStamp.ms++;
    if (dataPoint.timeStamp.ms > 999)
    {
      dataPoint.timeStamp.ms = 0;
      dataPoint.timeStamp.sec++;

      if (dataPoint.timeStamp.sec > 59)
      {
        dataPoint.timeStamp.sec = 0;
        dataPoint.timeStamp.min++;

        if (dataPoint.timeStamp.min > 59)
        {
          dataPoint.timeStamp.min = 0;
          dataPoint.timeStamp.hrs++;

          if (dataPoint.timeStamp.hrs > 23)
          {
            dataPoint.timeStamp.hrs = 0;
            dataPoint.timeStamp.day++;
          }
        }
      }
    }

    //write a dataPoint
    assert_param(dataManager_flashNextMeasDataPointStore(&dataPoint) == true);

    dataPointsStored++;
    if (dataPointsStored > 130431)
    {
      dataPointsStored -= 128;  //a page was cleared
    }

    //check if this point is stored correctly (absolute number of data points available)
    nrOfMeasDataPoints = dataManager_flashNrOfMeasDataPointsGet(true);
    assert_param(nrOfMeasDataPoints == dataPointsStored);
    //check if this point is stored correctly
    nrOfMeasDataPoints = dataManager_flashNrOfMeasDataPointsGet(false);
    assert_param(nrOfMeasDataPoints == dataPointsStored);

    //read out dataPoint
    dataManager_flashMeasDataPointGet((dataPointsStored - 1), &dataPointRead);

#ifdef SEMI_HOSTING
    printf(" pass\n\r");
  #endif

#ifdef SEMI_HOSTING
    printf("DataManager_Test: verify read out data");
  #endif
    //verify read out data, weekday is not stored on dataManager dataPoint!
    //RTC time
    assert_param(memcmp(&dataPoint.timeStamp.year, &dataPointRead.timeStamp.year, 2) == 0);
    assert_param(memcmp(&dataPoint.timeStamp.month, &dataPointRead.timeStamp.month, 1) == 0);
    assert_param(memcmp(&dataPoint.timeStamp.day, &dataPointRead.timeStamp.day, 1) == 0);
    assert_param(memcmp(&dataPoint.timeStamp.hrs, &dataPointRead.timeStamp.hrs, 1) == 0);
    assert_param(memcmp(&dataPoint.timeStamp.min, &dataPointRead.timeStamp.min, 1) == 0);
    assert_param(memcmp(&dataPoint.timeStamp.sec, &dataPointRead.timeStamp.sec, 1) == 0);
    assert_param(memcmp(&dataPoint.timeStamp.ms, &dataPointRead.timeStamp.ms, 2) == 0);
    //timeBasedMeasurement flag
    assert_param(dataPointRead.timeBasedMeas == dataPoint.timeBasedMeas);
    //VBat (format 8.8, unsigned)
    assert_param(dataPointRead.vBat == (((uint16_t )(dataPoint.vBat * 256.0f)) / 256.0f));
    //ADC_IN8 (format 3.13, unsigned)
    assert_param(dataPointRead.adc_in8 == (((uint16_t )(dataPoint.adc_in8 * 8192.0f)) / 8192.0f));
    //ADC_IN9 (format 3.13, unsigned)
    assert_param(dataPointRead.adc_in9 == (((uint16_t )(dataPoint.adc_in9 * 8192.0f)) / 8192.0f));
    //ADC_IN10 (format 3.13, unsigned)
    assert_param(dataPointRead.adc_in10 == (((uint16_t )(dataPoint.adc_in10 * 8192.0f)) / 8192.0f));
    //ADC_IN11 (format 3.13, unsigned)
    assert_param(dataPointRead.adc_in11 == (((uint16_t )(dataPoint.adc_in11 * 8192.0f)) / 8192.0f));
    //BME280_temperature (format 8.8, signed)
    assert_param(dataPointRead.bme280.temperature == (((int16_t )(dataPoint.bme280.temperature * 256.0f)) / 256.0f));
    //BME280_pressure (format float)
    assert_param(dataPointRead.bme280.pressure == dataPoint.bme280.pressure);
    //BME280_humidity (format 7.9, unsigned)
    assert_param(dataPointRead.bme280.humidity == (((uint16_t )(dataPoint.bme280.humidity * 512.0f)) / 512.0f));
#ifdef SEMI_HOSTING
      printf(" pass\n\r");
    #endif
  }

#ifdef SEMI_HOSTING
    printf("DataManager_Test: USB-Stick file storing");
  #endif

  //check if this point is stored correctly (absolute number of data points available)
  nrOfMeasDataPoints = dataManager_flashNrOfMeasDataPointsGet(true);
  assert_param(nrOfMeasDataPoints == dataPointsStored);
  //check if this point is stored correctly
  nrOfMeasDataPoints = dataManager_flashNrOfMeasDataPointsGet(false);
  assert_param(nrOfMeasDataPoints == dataPointsStored);
  //test the USB-Stick over the dataManager unit handling
  // takes around 200seconds to write a file of 130400 dataPoints @Release v0.3.0, File: 11’501’568 Bytes
  assert_param(dataManager_usbStickDataStore() == true);
#ifdef SEMI_HOSTING
    printf(" pass\n\r");
  #endif

  //***********************************************//
  //  TEST dataManager_flashLogEvent()
  //***********************************************//
#ifdef SEMI_HOSTING
  printf("DataManager_Test: logHistory data");
#endif
  //***** clear complete data and check the result of that
  dataManager_flashClearMeasDataAndHistory();

  logEventsGet = dataManager_flashNrOfLogEventsGet();
  assert_param(logEventsGet == 0);

  logEventsStored = 0;
  for (logEventNr = 0; logEventNr < 400; logEventNr++)
  {
    //mark this event in the log data
    sys_rtcDateTimeGet(&rtcDateTime);

    dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_SYSRESTART, "test");
    logEventsStored++;
    if (logEventsStored > 127)
    {
      //limit reached
      logEventsStored -= 64;  //one page has been cleared
    }

    logEventsGet = dataManager_flashNrOfLogEventsGet();
    assert_param(logEventsGet == logEventsStored);
  }
#ifdef SEMI_HOSTING
    printf(" pass\n\r");
  #endif

  //test the USB-Stick over the dataManager unit handling
  assert_param(dataManager_usbStickDataStore() == true);
#ifdef SEMI_HOSTING
    printf(" pass\n\r");
  #endif

#ifdef SEMI_HOSTING
  printf("DataManager_Test: ends\n\n\r");
#endif

  //mark test as okay
  rgLED_init();
  rgLED_luminositySet(10);
  rgLED_colorSet(RGLED_COLOR_GREEN);
  sys_sleepForUs(1e6);  //1s delay
  rgLED_luminositySet(0);
  rgLED_colorSet(RGLED_COLOR_NONE);
}

/**
 * @}
 */
