/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SYS_CLK 16000000
#define DIG_IO_INTERRUPT_PRIO 3
#define DEV_NAME_MAX_LEN 92
#define ANALOG_NAME_MAX_LEN 64
#define SYS_WKUP2_USR_BT2_Pin GPIO_PIN_13
#define SYS_WKUP2_USR_BT2_GPIO_Port GPIOC
#define ADC_IN3_USB_CC1_Pin GPIO_PIN_2
#define ADC_IN3_USB_CC1_GPIO_Port GPIOC
#define ADC_IN4_USB_CC2_Pin GPIO_PIN_3
#define ADC_IN4_USB_CC2_GPIO_Port GPIOC
#define SYS_WKUP1_VDD_USB_Pin GPIO_PIN_0
#define SYS_WKUP1_VDD_USB_GPIO_Port GPIOA
#define ADC1_IN6_V_BAT_Pin GPIO_PIN_1
#define ADC1_IN6_V_BAT_GPIO_Port GPIOA
#define SYS_WKUP4_SDCARD_Pin GPIO_PIN_2
#define SYS_WKUP4_SDCARD_GPIO_Port GPIOA
#define TIM1_CH1N_LED_R_Pin GPIO_PIN_7
#define TIM1_CH1N_LED_R_GPIO_Port GPIOA
#define ENA_17V_Pin GPIO_PIN_4
#define ENA_17V_GPIO_Port GPIOC
#define SYS_WKUP5_USR_BT1_Pin GPIO_PIN_5
#define SYS_WKUP5_USR_BT1_GPIO_Port GPIOC
#define TIM1_CH2N_LED_G_Pin GPIO_PIN_0
#define TIM1_CH2N_LED_G_GPIO_Port GPIOB
#define V_SUPPLY_MEAS_ENA_Pin GPIO_PIN_8
#define V_SUPPLY_MEAS_ENA_GPIO_Port GPIOA
#define ENA_5V_USB_Pin GPIO_PIN_10
#define ENA_5V_USB_GPIO_Port GPIOA
#define ENA_5V_EXT_Pin GPIO_PIN_15
#define ENA_5V_EXT_GPIO_Port GPIOA
#define OTG_Device_Pin GPIO_PIN_2
#define OTG_Device_GPIO_Port GPIOD
#define SPI1_FLASH_CSN_Pin GPIO_PIN_6
#define SPI1_FLASH_CSN_GPIO_Port GPIOB
#define SPI1_BME280_CSN_Pin GPIO_PIN_7
#define SPI1_BME280_CSN_GPIO_Port GPIOB
#define BOOT0_Pin GPIO_PIN_3
#define BOOT0_GPIO_Port GPIOH
#define SPI1_SDCARD_CSN_Pin GPIO_PIN_8
#define SPI1_SDCARD_CSN_GPIO_Port GPIOB
#define ENA_3V3_Pin GPIO_PIN_9
#define ENA_3V3_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
