/**
  ******************************************************************************
  * File Name          : RTC.h
  * Description        : This file provides code for the configuration
  *                      of the RTC instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __rtc_H
#define __rtc_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */
#include <stdbool.h>
/* USER CODE END Includes */

extern RTC_HandleTypeDef hrtc;

/* USER CODE BEGIN Private defines */
typedef enum
{
  RTC_MONDAY = 0x1,   //!< monday
  RTC_TUESDAY = 2,    //!< tuesday
  RTC_WEDNESDAY = 3,  //!< wednesday
  RTC_THURSDAY = 4,   //!< thursday
  RTC_FRIDAY = 5,     //!< friday
  RTC_SATURDAY = 6,   //!< saturday
  RTC_SUNDAY = 7      //!< sunday
} rtcWeekday_t;

typedef struct
{
  uint16_t year;  //!< The RTC year number (0..65535)
  uint8_t month;  //!< The RTC month information
  uint8_t day;    //!< The RTC day number (1..31)
  rtcWeekday_t weekday; //!< The RTC weekday information
  uint8_t hrs;    //!< The RTC time hours (0..23)
  uint8_t min;    //!< The RTC time minutes (0..59)
  uint8_t sec;    //!< The RTC time seconds (0..59)
  uint16_t ms;    //!< The RTC time milliseconds (0..999)
} rtcDateTime_t;
/* USER CODE END Private defines */

void MX_RTC_Init(void);

/* USER CODE BEGIN Prototypes */
void HAL_RTC_init (void);
void HAL_RTC_deInit (void);
bool HAL_RTC_PowerUpReconfigGetAndClear (void);
bool HAL_RTC_WakeUpEvtGetAndClear (void);
bool HAL_RTC_WakeUpTimerActivated (void);
void HAL_RTC_SmoothCalibrationSet (uint32_t nrOfPulses, bool substitute);
void HAL_RTC_CalibrationOutputEnable (bool enable);
void HAL_RTC_DateTimeSet (rtcDateTime_t *pDateTime);
void HAL_RTC_DateTimeGet (rtcDateTime_t *pDateTime);
void HAL_RTC_BackupRegisterSet (uint32_t backupRegNr, uint32_t value);
uint32_t HAL_RTC_BackupRegisterGet (uint32_t backupRegNr);
/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ rtc_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
