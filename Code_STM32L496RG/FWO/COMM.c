/**
 ****************************************************************
 @file    COMM.c
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        a smart _protRxocol over a USB interface.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-24
 ****************************************************************
 */

/** @addtogroup FWO
 * @{
 */

/** @addtogroup COMM
 * @{
 */

// --- Includes
#include "COMM.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "usb_device.h"
#include "usbd_cdc_if.h"
#include "../DEF/GLOB_Types.h"
#include "../HWO/SYS.h"
#include "../FWO/HMI.h"
#include "../FWO/dataManager.h"
#include "../FWO/dataLogger.h"
#include "../FWO/stringConverter.h"

// --- Defines
#define CMD_START_CHAR  ((uint8_t) 0x02)
#define CMD_END_CHAR    ((uint8_t) 0x03)

// --- Typedefs
//! A complete command (requested pattern and the callback function)
typedef struct
{
  uint8_t cmd[3];     //!< command pattern "xyz"
  pFB_V_t pCbFct;     //!< handler function
} command_t;

//! enumeration for the parsing states of a command
typedef enum
{
  STATE_IDLE = 0,
  STATE_LEN,
  STATE_CMD,
  STATE_DATA,
  STATE_END
} state_t;

//! data type for a complete command abstraction (used at command receive)
typedef union
{
  struct __attribute__((packed))
  {
    uint8_t startChar;              //!< the start character
    uint8_t cmd[3];                 //!< the three bytes for the command representation
    uint8_t data_len;               //!< the number of bytes of data
    uint8_t data[DEV_NAME_MAX_LEN]; //!< the data of this command
    uint8_t endChar;                //!< the enc character
  };
  uint8_t BYTE[1];
} _protRxocol_t;

// --- Variables
static bool _initDone = false;
static uint8_t txData[256];
static _protRxocol_t _protRx;

// --- Local Function _protRxotypes
//_protRxocol handler function _protRxotypes
//functions for action requests
static bool _comm_configStoreAction (void);
static bool _comm_dataClearAction (void);
static bool _comm_dataReadoutCursorRewindAction (void);
static bool _comm_usbBootloaderAction (void);     //USB DFU device class
static bool _comm_manualMeasurementAction (void);
static bool _comm_realTimeClockCalibrationAction (void);
static bool _comm_realTimeClockCalibrationOutputAction (void);
//replay functions for getter requests
static bool _comm_sendAnswerDeviceNameGet (void);
static bool _comm_sendAnswerMeasIntervalGet (void);
static bool _comm_sendAnswerMeasNrOfDataPointsGet (void);
static bool _comm_sendAnswerMeasValueGet (void);
static bool _comm_sendAnswerRealTimeGet (void);
static bool _comm_sendAnswerBatteryConfigGet (void);
static bool _comm_sendAnswerAnalogChannelNameGet (void);
static bool _comm_sendAnswerFirmwareVersionGet (void);
static bool _comm_sendAnswerAutoSamplingGet (void);
static bool _comm_sendAnswerUniqueIdGet (void);
//internal setter functions
static bool _comm_deviceNameSet (void);
static bool _comm_measurementIntervalSet (void);
static bool _comm_rtcTimeSet (void);
static bool _comm_batteryConfigSet (void);
static bool _comm_analogChannelNameSet (void);
static bool _comm_dataStreamSet (void);
static bool _comm_autoSamplingSet (void);
//generic ACK/NACK replays
static void _comm_sendACK (void);
static void _comm_sendNACK (void);

static void _comm_sendMeasValue (dataManager_dataPoint_t *pDataPoint);

//_protRxocol Command definitions
const command_t _command[] =
{
  //actions
  {     //store the current system configuration in FLASH
    .cmd = "CSA", .pCbFct = _comm_configStoreAction
  },
  {     //clear the data & history stored in FLASH
    .cmd = "DCA", .pCbFct = _comm_dataClearAction
  },
  {     //rewind the data readout cursor in the FLASH
    .cmd = "CRA", .pCbFct = _comm_dataReadoutCursorRewindAction
  },
  {     //enter USB bootloader action
    .cmd = "UBA", .pCbFct = _comm_usbBootloaderAction
  },
  {     //perform a manual triggered measurement
    .cmd = "MMA", .pCbFct = _comm_manualMeasurementAction
  },
  {     //realTimeClockCalibration action
    .cmd = "RTA", .pCbFct = _comm_realTimeClockCalibrationAction
  },
  {     //realTimeClockOutput action
    .cmd = "ROA", .pCbFct = _comm_realTimeClockCalibrationOutputAction
  },
  //getter
  {     //device name get
    .cmd = "DNG", .pCbFct = _comm_sendAnswerDeviceNameGet
  },
  {     //measurement interval get
    .cmd = "MIG", .pCbFct = _comm_sendAnswerMeasIntervalGet
  },
  {     //number of measurement data points get
    .cmd = "MNG", .pCbFct = _comm_sendAnswerMeasNrOfDataPointsGet
  },
  {     //measurement data values get
    .cmd = "MVG", .pCbFct = _comm_sendAnswerMeasValueGet
  },
  {     //RTC time get
    .cmd = "RTG", .pCbFct = _comm_sendAnswerRealTimeGet
  },
  {     //Battery info get
    .cmd = "BCG", .pCbFct = _comm_sendAnswerBatteryConfigGet
  },
  {     //Analog channel name get
    .cmd = "ANG", .pCbFct = _comm_sendAnswerAnalogChannelNameGet
  },
  {     //firmware version info get
    .cmd = "FVG", .pCbFct = _comm_sendAnswerFirmwareVersionGet
  },
  {     //auto sampling get (enable/disable)
    .cmd = "ASG", .pCbFct = _comm_sendAnswerAutoSamplingGet
  },
  {     //unique ID get
    .cmd = "UIG", .pCbFct = _comm_sendAnswerUniqueIdGet
  },
  //setter
  {     //device name set
    .cmd = "DNS", .pCbFct = _comm_deviceNameSet
  },
  {     //measurement interval set
    .cmd = "MIS", .pCbFct = _comm_measurementIntervalSet
  },
  {     //RTC time set
    .cmd = "RTS", .pCbFct = _comm_rtcTimeSet
  },
  {     //battery configuration set
    .cmd = "BCS", .pCbFct = _comm_batteryConfigSet
  },
  {     //analog name configuration set
    .cmd = "ANS", .pCbFct = _comm_analogChannelNameSet
  },
  {     //data streaming set (enable/disable)
    .cmd = "DSS", .pCbFct = _comm_dataStreamSet
  },
  {     //auto sampling set (enable/disable)
    .cmd = "ASS", .pCbFct = _comm_autoSamplingSet
  },
};

const uint32_t _numCommands = sizeof(_command) / sizeof(command_t);

/**
 ****************************************************************
 @brief   Initialize the communication module.
 @param   -
 @return  -
 ****************************************************************
 */
void comm_init (void)
{
  if (!_initDone)
  {
    MX_USB_DEVICE_Init();

    _initDone = true;
  }
}

/**
 ****************************************************************
 @brief   Parse UART interface and check if a new command received
 @param   -
 @return  -
 ****************************************************************
 */
void comm_parseRx (void)
{
  static state_t state = STATE_IDLE;
  static uint32_t sys_tickStart = 0;
  uint32_t deltaTicks;  //one tick is currently 0.001s
  uint32_t len;
  uint32_t i;
  bool cmdFound;

  //handling for timeout
  if (state != STATE_IDLE)
  {
    sys_sysTickTickDeltaGet(sys_tickStart, &deltaTicks);
    if (deltaTicks >= 500)
    {
      //timeout - this is an error
      _comm_sendNACK();

      state = STATE_IDLE;
    }
  }

  switch (state)
  {
    default:
    case STATE_IDLE:
      len = CDC_Receive_Data(&_protRx.startChar, 1);
      if (len == 1)
      {
        if (_protRx.startChar == CMD_START_CHAR)
        {
          sys_sysTickTickGet(&sys_tickStart);

          _protRx.data_len = 0;
          state = STATE_CMD;
        }
        else
        {
          //this is an error
          _comm_sendNACK();
        }
      }
      break;
    case STATE_CMD:   //start character received, now get command-bytes
      len = CDC_Receive_Data(&_protRx.cmd[0], 3);
      if (len == 3)
      {
        state = STATE_LEN;
      }
      break;
    case STATE_LEN:   //command-bytes received, not get data length
      len = CDC_Receive_Data(&_protRx.data_len, 1);
      if ((len == 1) && (_protRx.data_len < sizeof(_protRx.data)))
      {
        if (_protRx.data_len)
        {
          state = STATE_DATA;
        }
        else
        {
          //no data for this command
          state = STATE_END;
        }
      }
      break;
    case STATE_DATA:
      len = CDC_Receive_Data(&_protRx.data[0], _protRx.data_len);
      if (len == _protRx.data_len)
      {
        state = STATE_END;
      }
      break;
    case STATE_END:   //receive ending character
      len = CDC_Receive_Data(&_protRx.endChar, 1);
      if (len == 1)
      {
        if (_protRx.endChar == CMD_END_CHAR)
        {
          //search throw command table for the specific command
          cmdFound = false;                 //search pattern in command table
          for (i = 0; ((i < _numCommands) && !(cmdFound)); i++)
          {
            if (0 == memcmp(&_protRx.cmd[0], &_command[i].cmd[0], 3))
            {
              cmdFound = true;                //break search-loop
            }
          }
          i--;
          if (cmdFound)                       //command-pattern found
          {
            if (_command[i].pCbFct())         //execute corresponding callback function
            {
              if (_command[i].cmd[2] != 'G')  //no ACK on successful getter function feedbacks
              {
                _comm_sendACK();
              }
            }
            else
            {
              //some internal function error (_protRxocol parameters, ...)
              _comm_sendNACK();
            }
          }
          else
          {
            _comm_sendNACK();
          }
          //sequence done successful
          state = STATE_IDLE;
        }
        else
        {
          //wrong ending character
          state = STATE_IDLE;
        }
      }
      break;
  }
}

/**
 ****************************************************************
 @brief   Stream measurement data out over virtual communication port.
 @param   pDataPoint  The to be sent out measurement data.
 @return  -
 ****************************************************************
 */
void comm_streamMeasDataPoint (dataManager_dataPoint_t *pDataPoint)
{
  _comm_sendMeasValue(pDataPoint);
}

/**
 ****************************************************************
 @brief   _protRxocol handler function to store the current system configuration in FLASH.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_configStoreAction (void)
{
  if (_protRx.data_len == 0)
  {
    dataLogger_flashConfigStore(false);

    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function to clear measurement data & history in FLASH.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_dataClearAction (void)
{
  if (_protRx.data_len == 0)
  {
    dataLogger_flashClearDataAndHistory();

    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function to rewind the data readout cursor.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_dataReadoutCursorRewindAction (void)
{
  if (_protRx.data_len == 0)
  {
    dataManager_flashRewindDataReadoutCursor();

    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   Enter the USB bootloader mode (USB DFU)
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_usbBootloaderAction (void)
{
  const uint32_t key = 0x78345612;

  if (_protRx.data_len == 4)
  {
    if (memcmp(&key, _protRx.data, 4) == 0)
    {
      dataLogger_firmwareUpdateOverUsbDFU();

      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   Perform a manual triggered measurement.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_manualMeasurementAction (void)
{
  if (_protRx.data_len == 0)
  {
    dataLogger_measCylce(MEAS_REQUEST_MANUAL);

    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   Execute a real time clock calibration.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_realTimeClockCalibrationAction (void)
{
  uint32_t nrOfPulses;

  if (_protRx.data_len == 4)
  {
    nrOfPulses = ((_protRx.data[1] - '0') * 100) + ((_protRx.data[2] - '0') * 10) + (_protRx.data[3] - '0');

    if (nrOfPulses <= 511)
    {
      if (_protRx.data[0] == '-')
      {
        HAL_RTC_SmoothCalibrationSet(nrOfPulses, true);

        return true;
      }
      else if (_protRx.data[0] == '+')
      {
        HAL_RTC_SmoothCalibrationSet(nrOfPulses, false);

        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   Enable or disable the RTC calibration clock output
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_realTimeClockCalibrationOutputAction (void)
{
  if (_protRx.data_len == 1)
  {
    if (_protRx.data[0] == '0')
    {
      HAL_RTC_CalibrationOutputEnable(false);

      return true;
    }
    else if (_protRx.data[0] == '1')
    {
      HAL_RTC_CalibrationOutputEnable(true);

      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function for send device name information out.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_sendAnswerDeviceNameGet (void)
{
  uint8_t res;
  uint8_t devNameLen;

  if (_protRx.data_len == 0)
  {
    txData[0] = CMD_START_CHAR;
    txData[1] = 'D';
    txData[2] = 'N';
    txData[3] = 'G';
    dataManager_cfgDeviceNameGet((char*) &txData[5], &devNameLen);
    txData[4] = devNameLen;
    txData[5 + devNameLen] = CMD_END_CHAR;

    do
    {
      res = CDC_Transmit_FS(txData, 6 + devNameLen);
      if (res != USBD_OK)
      {
        //set system to sleep for some time
        sys_sleepForUs(2e3);
      }
    } while (res != USBD_OK);
    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function for send measurement interval time in minutes out.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_sendAnswerMeasIntervalGet (void)
{
  uint8_t res;

  if (_protRx.data_len == 0)
  {
    txData[0] = CMD_START_CHAR;
    txData[1] = 'M';
    txData[2] = 'I';
    txData[3] = 'G';
    txData[4] = 4;
    sprintf((char*) &txData[5], "%04u", (unsigned int) dataManager_cfgMeasIntervalGet());
    txData[9] = CMD_END_CHAR;

    do
    {
      res = CDC_Transmit_FS(txData, 10);
      if (res != USBD_OK)
      {
        //set system to sleep for some time
        sys_sleepForUs(2e3);
      }
    } while (res != USBD_OK);
    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function for send the number of stored measurement numbers out (absolute, new).
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_sendAnswerMeasNrOfDataPointsGet (void)
{
  uint8_t res;

  if (_protRx.data_len == 0)
  {
    txData[0] = CMD_START_CHAR;
    txData[1] = 'M';
    txData[2] = 'N';
    txData[3] = 'G';
    txData[4] = 6;
    sprintf((char*) &txData[5], "%06u,%06u", (unsigned int) dataManager_flashNrOfMeasDataPointsGet(true),
      (unsigned int) dataManager_flashNrOfMeasDataPointsGet(false));
    txData[18] = CMD_END_CHAR;

    do
    {
      res = CDC_Transmit_FS(txData, 19);
      if (res != USBD_OK)
      {
        //set system to sleep for some time
        sys_sleepForUs(2e3);
      }
    } while (res != USBD_OK);
    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function for send measurement values out.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_sendAnswerMeasValueGet (void)
{
  uint32_t nrOfValidMeasDataPoints = dataManager_flashNrOfMeasDataPointsGet(true);
  uint32_t nrOfMeasDataPoint;
  dataManager_dataPoint_t dataPoint;

  if (nrOfValidMeasDataPoints == 0)
  {
    return false;
  }

  if (_protRx.data_len == 0)
  {
    //last measurement data point
    nrOfMeasDataPoint = nrOfValidMeasDataPoints - 1;
  }
  else if (_protRx.data_len == 6)
  {
    nrOfMeasDataPoint = ((_protRx.data[0] - '0') * 100000) + ((_protRx.data[1] - '0') * 10000)
      + ((_protRx.data[2] - '0') * 1000) + ((_protRx.data[3] - '0') * 100) + ((_protRx.data[4] - '0') * 10)
      + (_protRx.data[5] - '0');

    //specific measurement data point
    if (nrOfMeasDataPoint >= nrOfValidMeasDataPoints)
    {
      //not a valid measurement data point
      return false;
    }
  }
  else
  {
    return false;
  }

  dataManager_flashMeasDataPointGet(nrOfMeasDataPoint, &dataPoint);

  _comm_sendMeasValue(&dataPoint);

  return true;
}

/**
 ****************************************************************
 @brief   _protRxocol handler function for send the time information out.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_sendAnswerRealTimeGet (void)
{
  uint8_t res;
  rtcDateTime_t rtc;

  if (_protRx.data_len == 0)
  {
    sys_rtcDateTimeGet(&rtc);

    txData[0] = CMD_START_CHAR;
    txData[1] = 'R';
    txData[2] = 'T';
    txData[3] = 'G';
    txData[4] = 25;
    sprintf((char*) &txData[5], "%04d.%02d.%02d %d %02d:%02d:%02d.%03d", rtc.year, rtc.month, rtc.day, rtc.weekday,
      rtc.hrs, rtc.min, rtc.sec, rtc.ms);
    txData[30] = CMD_END_CHAR;

    do
    {
      res = CDC_Transmit_FS(txData, 31);
      if (res != USBD_OK)
      {
        //set system to sleep for some time
        sys_sleepForUs(2e3);
      }
    } while (res != USBD_OK);
    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function for send the battery configuration out.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_sendAnswerBatteryConfigGet (void)
{
  uint8_t res;
  uint32_t nominalCellVoltage;
  uint32_t nrOfCells;

  if (_protRx.data_len == 0)
  {
    dataManager_cfgBatterySettingGet(&nominalCellVoltage, &nrOfCells);

    txData[0] = CMD_START_CHAR;
    txData[1] = 'B';
    txData[2] = 'C';
    txData[3] = 'G';
    txData[4] = 6;
    sprintf((char*) &txData[5], "%04u,%01u", (unsigned int) nominalCellVoltage, (unsigned int) nrOfCells);
    txData[11] = CMD_END_CHAR;

    do
    {
      res = CDC_Transmit_FS(txData, 12);
      if (res != USBD_OK)
      {
        //set system to sleep for some time
        sys_sleepForUs(2e3);
      }
    } while (res != USBD_OK);
    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function for send the analog channel name configuration out.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_sendAnswerAnalogChannelNameGet (void)
{
  uint8_t channelNameLen;
  uint8_t channelNr;
  uint8_t res;
  uint32_t idx;

  if ((_protRx.data_len == 1) && ((_protRx.data[0] >= '0') && (_protRx.data[0] <= '3')))
  {
    channelNr = (_protRx.data[0] - '0');
    if (!dataManager_cfgAnalogNameGet(channelNr, (char*) &txData[5], &channelNameLen))
    {
      return false;
    }

    txData[0] = CMD_START_CHAR;
    txData[1] = 'A';
    txData[2] = 'N';
    txData[3] = 'G';
    txData[4] = channelNameLen;
    idx = 5 + channelNameLen;
    txData[idx] = CMD_END_CHAR;

    do
    {
      res = CDC_Transmit_FS(txData, idx + 1);
      if (res != USBD_OK)
      {
        //set system to sleep for some time
        sys_sleepForUs(2e3);
      }
    } while (res != USBD_OK);
    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function for send the firmware version information out.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_sendAnswerFirmwareVersionGet (void)
{
  uint8_t res;
  fwVersion_t fwVersion;

  if (_protRx.data_len == 0)
  {
    dataLogger_fwVersionGet(&fwVersion);

    txData[0] = CMD_START_CHAR;
    txData[1] = 'F';
    txData[2] = 'V';
    txData[3] = 'G';
    txData[4] = 5;
    txData[5] = fwVersion.major;
    txData[6] = '.';
    txData[7] = fwVersion.minor;
    txData[8] = '.';
    txData[9] = fwVersion.patch;
    txData[10] = CMD_END_CHAR;

    do
    {
      res = CDC_Transmit_FS(txData, 11);
      if (res != USBD_OK)
      {
        //set system to sleep for some time
        sys_sleepForUs(2e3);
      }
    } while (res != USBD_OK);
    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function for send the autoSampling information out.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_sendAnswerAutoSamplingGet (void)
{
  uint8_t res;
  bool active;

  if (_protRx.data_len == 0)
  {
    active = dataManager_cfgAutoMeasActiveGet();

    txData[0] = CMD_START_CHAR;
    txData[1] = 'A';
    txData[2] = 'S';
    txData[3] = 'G';
    txData[4] = 1;
    if (active)
    {
      txData[5] = '1';
    }
    else
    {
      txData[5] = '0';
    }
    txData[6] = CMD_END_CHAR;

    do
    {
      res = CDC_Transmit_FS(txData, 7);
      if (res != USBD_OK)
      {
        //set system to sleep for some time
        sys_sleepForUs(2e3);
      }
    } while (res != USBD_OK);
    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function for send the unique ID information out.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_sendAnswerUniqueIdGet (void)
{
  uint8_t res;
  sysID_t sysID;

  if (_protRx.data_len == 0)
  {
    sys_uniqueIdGet(&sysID);

    txData[0] = CMD_START_CHAR;
    txData[1] = 'U';
    txData[2] = 'I';
    txData[3] = 'G';
    txData[4] = 26;
    sprintf((char*) &txData[5], "0x%08X%08X%08X", sysID.systemID[0], sysID.systemID[1], sysID.systemID[2]);
    txData[32] = CMD_END_CHAR;

    do
    {
      res = CDC_Transmit_FS(txData, 33);
      if (res != USBD_OK)
      {
        //set system to sleep for some time
        sys_sleepForUs(2e3);
      }
    } while (res != USBD_OK);
    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function to set the device name (1 up to DEV_NAME_MAX_LEN characters).
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_deviceNameSet (void)
{
  if ((_protRx.data_len >= 1) && (_protRx.data_len <= DEV_NAME_MAX_LEN))
  {
    if (dataManager_cfgDeviceNameSet((char*) _protRx.data, _protRx.data_len))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function to set the measurement interval time in minutes.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_measurementIntervalSet (void)
{
  uint32_t minutes;

  if (_protRx.data_len == 4)
  {
    minutes = ((_protRx.data[0] - '0') * 1000) + ((_protRx.data[1] - '0') * 100) + ((_protRx.data[2] - '0') * 10)
      + (_protRx.data[3] - '0');

    if (dataManager_cfgMeasIntervalSet(minutes))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function to set the time information.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_rtcTimeSet (void)
{
  rtcDateTime_t rtc;

  if (_protRx.data_len == 21)
  {
    if ((_protRx.data[4] == '.') && (_protRx.data[7] == '.') && (_protRx.data[10] == ' ')
      && ((_protRx.data[11] >= '1') && (_protRx.data[11] <= '7')) && (_protRx.data[12] == ' ')
      && (_protRx.data[15] == ':') && (_protRx.data[18] == ':'))
    {
      rtc.year = ((_protRx.data[0] - '0') * 1000) + ((_protRx.data[1] - '0') * 100) + ((_protRx.data[2] - '0') * 10)
        + (_protRx.data[3] - '0');
      rtc.month = ((_protRx.data[5] - '0') * 10) + (_protRx.data[6] - '0');
      rtc.day = ((_protRx.data[8] - '0') * 10) + (_protRx.data[9] - '0');
      rtc.weekday = (rtcWeekday_t) (_protRx.data[11] - '0');
      rtc.hrs = ((_protRx.data[13] - '0') * 10) + (_protRx.data[14] - '0');
      rtc.min = ((_protRx.data[16] - '0') * 10) + (_protRx.data[17] - '0');
      rtc.sec = ((_protRx.data[19] - '0') * 10) + (_protRx.data[20] - '0');

      sys_rtcDateTimeSet(&rtc);

      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function to set the battery configuration.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_batteryConfigSet (void)
{
  uint32_t nominalCellVoltage;
  uint32_t nrOfCells;

  if ((_protRx.data_len == 6) && (_protRx.data[4] == ','))
  {
    nominalCellVoltage = ((_protRx.data[0] - '0') * 1000) + ((_protRx.data[1] - '0') * 100)
      + ((_protRx.data[2] - '0') * 10) + (_protRx.data[3] - '0');
    nrOfCells = (_protRx.data[5] - '0');

    return dataManager_cfgBatterySettingSet(nominalCellVoltage, nrOfCells);
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function to set a analog channel name.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_analogChannelNameSet (void)
{
  if (((_protRx.data_len >= 3) && (_protRx.data_len <= (ANALOG_NAME_MAX_LEN + 2)))
    && ((_protRx.data[0] >= '0') && (_protRx.data[0] <= '3')) && (_protRx.data[1] = ','))
  {
    if (dataManager_cfgAnalogNameSet(_protRx.data[0] - '0', (char*) &_protRx.data[2], _protRx.data_len - 2))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function to enable/disable dataStreaming.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_dataStreamSet (void)
{
  if (_protRx.data_len == 1)
  {
    if (_protRx.data[0] == '0')
    {
      //disable streaming
      dataLogger_measStreaming(false);

      return true;
    }
    else if (_protRx.data[0] == '1')
    {
      //enable streaming
      dataLogger_measStreaming(true);

      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   _protRxocol handler function to enable/disable autoSampling (RTC-based).
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _comm_autoSamplingSet (void)
{
  if (_protRx.data_len == 1)
  {
    if (_protRx.data[0] == '0')
    {
      //disable autoSampling
      dataLogger_measAutoCycleActivate(false);

      return true;
    }
    else if (_protRx.data[0] == '1')
    {
      //enable autoSampling
      dataLogger_measAutoCycleActivate(true);

      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   Acknowledge command sent out.
 @param   -
 @return  -
 ****************************************************************
 */
static void _comm_sendACK (void)
{
  uint8_t res;

  txData[0] = CMD_START_CHAR;
  txData[1] = 'A';
  txData[2] = 'C';
  txData[3] = 'K';
  txData[4] = 0;
  txData[5] = CMD_END_CHAR;

  do
  {
    res = CDC_Transmit_FS(txData, 6);
    if (res != USBD_OK)
    {
      //set system to sleep for some time
      sys_sleepForUs(2e3);
    }
  } while (res != USBD_OK);
}

/**
 ****************************************************************
 @brief   Not acknowledge command sent out.
 @param   -
 @return  -
 ****************************************************************
 */
static void _comm_sendNACK (void)
{
  uint8_t res;

  txData[0] = CMD_START_CHAR;
  txData[1] = 'N';
  txData[2] = 'A';
  txData[3] = 'K';
  txData[4] = 0;
  txData[5] = CMD_END_CHAR;

  do
  {
    res = CDC_Transmit_FS(txData, 6);
    if (res != USBD_OK)
    {
      //set system to sleep for some time
      sys_sleepForUs(2e3);
    }
  } while (res != USBD_OK);
}

/**
 ****************************************************************
 @brief   Internal function used, to sent out a measurement data point.
 @param   pDataPoint  Pointer to the to be sent out dataPoint structure.
 @return  -
 ****************************************************************
 */
static void _comm_sendMeasValue (dataManager_dataPoint_t *pDataPoint)
{
  uint32_t strLen;
  uint32_t dataLen;
  uint8_t res;

  txData[0] = CMD_START_CHAR;
  txData[1] = 'M';
  txData[2] = 'V';
  txData[3] = 'G';

  dataLen = snprintf((char*) &txData[5], DEV_NAME_MAX_LEN, "%04u.%02u.%02u %02u:%02u:%02u.%03u;%d;",
    pDataPoint->timeStamp.year, pDataPoint->timeStamp.month, pDataPoint->timeStamp.day, pDataPoint->timeStamp.hrs,
    pDataPoint->timeStamp.min, pDataPoint->timeStamp.sec, pDataPoint->timeStamp.ms, pDataPoint->timeBasedMeas);

  //convert floating point numbers to strings (nothing, if NaN)
  strConv_floatToString_6(pDataPoint->vBat, (char*) &txData[5 + dataLen], &strLen);
  dataLen += strLen;
  txData[5 + dataLen] = ';';
  dataLen++;
  strConv_floatToString_6(pDataPoint->bme280.temperature, (char*) &txData[5 + dataLen], &strLen);
  dataLen += strLen;
  txData[5 + dataLen] = ';';
  dataLen++;
  strConv_floatToString_6(pDataPoint->bme280.pressure, (char*) &txData[5 + dataLen], &strLen);
  dataLen += strLen;
  txData[5 + dataLen] = ';';
  dataLen++;
  strConv_floatToString_6(pDataPoint->bme280.humidity, (char*) &txData[5 + dataLen], &strLen);
  dataLen += strLen;
  txData[5 + dataLen] = ';';
  dataLen++;
  strConv_floatToString_6(pDataPoint->adc_in8, (char*) &txData[5 + dataLen], &strLen);
  dataLen += strLen;
  txData[5 + dataLen] = ';';
  dataLen++;
  strConv_floatToString_6(pDataPoint->adc_in9, (char*) &txData[5 + dataLen], &strLen);
  dataLen += strLen;
  txData[5 + dataLen] = ';';
  dataLen++;
  strConv_floatToString_6(pDataPoint->adc_in10, (char*) &txData[5 + dataLen], &strLen);
  dataLen += strLen;
  txData[5 + dataLen] = ';';
  dataLen++;
  strConv_floatToString_6(pDataPoint->adc_in11, (char*) &txData[5 + dataLen], &strLen);
  dataLen += strLen;

  txData[4] = dataLen;
  txData[5 + dataLen] = CMD_END_CHAR;

  do
  {
    res = CDC_Transmit_FS(txData, 6 + dataLen);
    if (res != USBD_OK)
    {
      //set system to sleep for some time
      sys_sleepForUs(2e3);
    }
  } while (res != USBD_OK);
}

/**
 * @}
 */

/**
 * @}
 */
