/**
 ****************************************************************
 @file    dataManager.c
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the log data management (FLASH / USB-Memory-Stick).
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 1.0
 @date    2021-10-04
 ****************************************************************
 */

/** @addtogroup FWO
 * @{
 */

/** @addtogroup DATAMANAGER
 * @{
 */

// --- Includes
#include "dataManager.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "fatfs.h"
#include "usb_host.h"
#include "../HWO/DIG_IO.h"
#include "../HWO/FLASH.h"
#include "../HWO/SYS.h"
#include "../FWO/dataLogger.h"
#include "../FWO/stringConverter.h"
#include "../FWO/HMI.h"

// --- Defines
#define FLASH_BLOCKSTARTADDR_LOGDATA        0     //!< 4kByte Block number 0 to 1018
#define FLASH_BLOCKSTARTADDR_SYSCONFIG      1019  //!< 4kByte Block number 1019
#define FLASH_BLOCKSTARTADDR_RESERVED       1020  //!< 4kByte Block number 1020-1021 (reserved)
#define FLASH_BLOCKSTARTADDR_LOGHIST        1022  //!< 4kByte Block number 1022-1023 (64Bytes each entry: gives 128 log events to be stored)

#define USB_STICK_BLOCK_SIZE                4096  //!< The number of bytes to be written as complete block

#define SYS_VBAT_ISNAN                      ((uint16_t) 0xFFFF) //!< internal compressed system NaN value for the battery voltage (valid 0.0 up to +28.0), format 8.8, unsigned
#define ADC_IN_ISNAN                        ((uint16_t) 0xFFFF) //!< internal compressed ADC NaN value for a input voltage (valid 0.0 up to +5.0), format 3.13, unsigned
#define BME280_TEMPERATURE_ISNAN            ((int16_t) 0x7FFF)  //!< internal compressed BME280 NaN value for the temperature (valid -40.0 up to +80.0), format 8.8, signed
#define BME280_HUMIDITY_ISNAN               ((uint16_t) 0xFFFF) //!< internal compressed BME280 NaN value for the humidity (valid 0.0 up to 100.0), format 7.9, unsigned

// --- Typedefs
//! reduced timeStamp information (bitCoded to reduce size) - 6 Bytes
typedef struct __attribute__((packed))
{
  uint32_t year :6;       //!< the reduced year information (0..63)
  uint32_t month :4;      //!< the month information (1..12)
  uint32_t day :5;        //!< the day information (1..31)
  uint32_t hours :5;      //!< the hours information (0..23)
  uint32_t minutes :6;    //!< the minutes information (0..59)
  uint32_t seconds :6;    //!< the second information (0..59)
  uint16_t milliseconds;  //!< the millisecond information (0..999)
} dataManager_timeStampSmall_t;

//! all data of a single data point packed together in a small memory footprint structure
typedef union
{
  struct __attribute__((packed))  //total 25 bytes used, 32 bytes blocks used
  {
    dataManager_timeStampSmall_t timeStamp; //!< the bitwise coded timeStamp (6Byte)
    bool timeBasedMeas;                     //!< Flag to indicate, if this is a timeBaseMeasurement or not (1Byte)
    uint16_t vBat;                          //!< battery voltage [V] (2Byte), format 8.8
    uint16_t adc_in8;                       //!< voltage on ADC-input8 [V] (2Byte), format 3.13
    uint16_t adc_in9;                       //!< voltage on ADC-input9 [V] (2Byte), format 3.13
    uint16_t adc_in10;                      //!< voltage on ADC-input10 [V] (2Byte), format 3.13
    uint16_t adc_in11;                      //!< voltage on ADC-input11 [V] (2Byte), format 3.13
    int16_t bme280_temperature;             //!< temperature of BME280 sensor in °C, format sign8.8 (2Byte)
    float bme280_pressure;                  //!< pressure of BME280 in Pa, format float (4Byte)
    uint16_t bme280_humidity;               //!< humidity of BME280 in %, format 7.9 (2Byte)
  };
  uint8_t BYTE[32];
} dataManager_dataPointSmall_t;

//! all data of a the system configuration packed together in a small memory footprint structure
typedef union
{
  struct __attribute__((packed))  //total 363 bytes used, 4096 bytes blocks reserved for system configuration
  {
    const uint32_t tag;                         //!< a simple 32bit Tag used for a simple validation check    4 Bytes
    char devName[DEV_NAME_MAX_LEN + 1];         //!< the device name                                         93 Bytes
    uint16_t measInterval_minutes;              //!< the measurement interval [minutes]                       2 Bytes
    uint16_t bat_nominalCellMilivoltage;        //!< the nominal battery cell voltage [mV]                    2 Bytes
    uint8_t bat_nrOfCells;                      //!< the number of installed battery cells                    1 Byte
    char analogChannel0Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 0 name                         65 Bytes
    char analogChannel1Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 1 name                         65 Bytes
    char analogChannel2Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 2 name                         65 Bytes
    char analogChannel3Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 3 name                         65 Bytes
    bool autoMeasActive;                        //!< the autoMeasurement active flag                          1 Byte
  };
  uint8_t BYTE[363];
} dataManager_sysConfigSmall_t;

//! all data of a single log history packed together in a small memory footprint structure
typedef union
{
  struct __attribute__((packed))  //64 bytes blocks used
  {
    dataManager_timeStampSmall_t timeStamp; //!< the bitwise coded timeStamp (6 Bytes)
    uint8_t evt;                            //!< the to be logged event, based on dataManager_logEvt_t
    char addInfo[57];                       //!< additional text with a maximum length of 57 characters could be added
  };
  uint8_t BYTE[64];
} dataManager_logHistorySmall_t;

// --- Variables
static bool _initDone = false;
//! system configuration struct with default parameters
static const dataManager_sysConfigSmall_t _sysCfgDefault =
{
  .tag = 0x21110800,                    //!< simple format as yymmdd and to bytes for different versions of this day
  .devName = "",                        //!< default device name (will be set dynamically, depending on Unique ID)
  .measInterval_minutes = 10,           //!< default measurement interval in minutes [0: deactivated, 1..1092]
  .bat_nominalCellMilivoltage = 3700,   //!< default battery nominal voltage
  .bat_nrOfCells = 3,                   //!< default number of battery cells installed
  .analogChannel0Name = "ADC_IN8(V)",   //!< default analog channel 0 name
  .analogChannel1Name = "ADC_IN9(V)",   //!< default analog channel 1 name
  .analogChannel2Name = "ADC_IN10(V)",  //!< default analog channel 2 name
  .analogChannel3Name = "ADC_IN11(V)",  //!< default analog channel 3 name
  .autoMeasActive = true,
//!< default auto measurement flag
  };

extern USBH_HandleTypeDef hUsbHostFS;
static dataManager_sysConfigSmall_t _sysCfg;
static FIL file;
static DIR dj;         //Directory object
static FILINFO fno;    //file information
static TCHAR path[128] = "";
static char usbStickDataBuffer[USB_STICK_BLOCK_SIZE << 1];
static uint8_t _flashReadData[USB_STICK_BLOCK_SIZE];

//start addresses of the different FLASH blocks
static const uint32_t flashStartAddr_logData = FLASH_BLOCKSTARTADDR_LOGDATA * 4096;
static const uint32_t flashStartAddr_sysConfig = FLASH_BLOCKSTARTADDR_SYSCONFIG * 4096;
static const uint32_t flashStartAddr_logHist = FLASH_BLOCKSTARTADDR_LOGHIST * 4096;
//block sizes for the different kind of memory areas
static const uint32_t flashSize_logData = (FLASH_BLOCKSTARTADDR_SYSCONFIG - FLASH_BLOCKSTARTADDR_LOGDATA) * 4096;
static const uint32_t flashSize_sysConfig = (FLASH_BLOCKSTARTADDR_RESERVED - FLASH_BLOCKSTARTADDR_SYSCONFIG) * 4096;
static const uint32_t flashSize_reserved = (FLASH_BLOCKSTARTADDR_LOGHIST - FLASH_BLOCKSTARTADDR_RESERVED) * 4096;
static const uint32_t flashSize_logHist = (1024 - FLASH_BLOCKSTARTADDR_LOGHIST) * 4096;

static uint32_t flashMaxNrOfLogDataPoints = 0;
static uint32_t flashMaxNrOfLogHistPoints = 0;

// --- Local Function Prototypes
static bool _dataManager_usbStickMount (void);
static void _dataManager_usbStickUnmount (void);
static bool _dataManager_measDataFileStore (TCHAR *pPath, uint32_t nrOfDataPoints, uint32_t nrOfNewDataPoints);
static bool _dataManager_metaDataFileStore (TCHAR *pPath);
static bool _dataManager_logDataFileStore (TCHAR *pPath);
static bool _dataManager_writeBlockToOpenedFile (bool forceWrite, UINT *pBytesLeftToWrite);
static void _dataManager_writeDefaultCfgToRamConfig (void);
static void _dataManager_restoreLogDataCursors (void);
static void _dataManager_restoreLogHistCursors (void);

/**
 ****************************************************************
 @brief   Initialize the data-manager module.
 @param   -
 @return  -
 ****************************************************************
 */
void dataManager_init (void)
{
  uint32_t flashSize;
  rtcDateTime_t rtcDateTime;

  if (!_initDone)
  {
    flash_init();
    flashSize = flash_memorySizeGet();
    assert_param(flashSize == (flashSize_logData + flashSize_sysConfig + flashSize_reserved + flashSize_logHist));

    //check if memory size and dataPoint size gives no remaining bytes
    //  Integer number for number of dataPoints to be stored in FLASH memory
    assert_param((flashSize % sizeof(dataManager_dataPointSmall_t)) == 0);

    //check if dataPoint size and FLASH memory block matches
    static_assert((4096 % sizeof(dataManager_dataPointSmall_t)) == 0,
      "dataManager: wrong size of dataManager_dataPoint_t!");
    static_assert(32 == sizeof(dataManager_dataPointSmall_t), "dataManager: wrong size of dataManager_dataPoint_t!");

    //check if dataPoint size and FLASH memory block matches
    static_assert((4096 % sizeof(dataManager_logHistorySmall_t)) == 0,
      "dataManager: wrong size of dataManager_logHistorySmall_t!");
    static_assert(64 == sizeof(dataManager_logHistorySmall_t),
      "dataManager: wrong size of dataManager_logHistorySmall_t!");

    assert_param(flashSize_logData > sizeof(dataManager_dataPointSmall_t));
    flashMaxNrOfLogDataPoints = (flashSize_logData / sizeof(dataManager_dataPointSmall_t)) - 1;
    assert_param(flashSize_logHist > sizeof(dataManager_logHistorySmall_t));
    flashMaxNrOfLogHistPoints = (flashSize_logHist / sizeof(dataManager_logHistorySmall_t)) - 1;

    //read current sysConfiguration out of the flash
    flash_bytesRead(flashStartAddr_sysConfig, _sysCfg.BYTE, sizeof(dataManager_sysConfigSmall_t));

    sys_rtcDateTimeGet(&rtcDateTime);
    //or the read out flash configuration is older than the default one from the current firmware
    if (_sysCfg.tag == 0xFFFFFFFF)
    {
      //log this event
      dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_CONFIGSTORE, "- default all (RAM only)");

      //write default parameters to RAM configuration
      _dataManager_writeDefaultCfgToRamConfig();
    }
    else
    {
      //check if some of the system configuration in FLASH are in correct range
      if (((uint8_t)_sysCfg.autoMeasActive) > 1)
      {
        //log this event
        dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_CONFIGSTORE, "- default autoMeasActive (RAM only)");

        _sysCfg.autoMeasActive = _sysCfgDefault.autoMeasActive;
      }
      if (_sysCfg.measInterval_minutes > 1092)
      {
        //log this event
        dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_CONFIGSTORE, "- default measInterval (RAM only)");

        _sysCfg.measInterval_minutes = _sysCfgDefault.measInterval_minutes;
      }
    }

    //RTC Backup registers are not valid anymore at this time!
    if (HAL_RTC_PowerUpReconfigGetAndClear())
    {
      //blink yellow red
      hmi_ledPatternSet(HMI_LED_PATTERN_BLINK_10HZ, RGLED_COLOR_RED);

      //restore data cursors by read FLASH part by part and check empty block
      //used to be able to restore correct addresses
      _dataManager_restoreLogDataCursors();
      _dataManager_restoreLogHistCursors();

      //last RTC battery check (month)
      sys_backupRegisterSet(SYS_BACKUP_REG_RTC_BAT_CHECK_MONTH, 1);

      //successful restore data cursors
      hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_GREEN);
      sys_sleepForUs(1e6);  //1s delay
      hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);

      //log this event
      sys_rtcDateTimeGet(&rtcDateTime);
      //save the timeStamp of this autoMeas event in the log
      dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_CURSORRESTORE, "done");

      //add system reset (otherwise USB is not correct handled!)
      sys_reset();
    }

    _initDone = true;
  }
}

/**
 ****************************************************************
 @brief   Erased all stored data on the FLASH, write default system configuration parameters
 @param   -
 @return  -
 ****************************************************************
 */
void dataManager_flashClearAll (void)
{
  //clear all backup registers
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL, flashStartAddr_logData);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT, flashStartAddr_logData);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_HEAD, flashStartAddr_logData);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGHIST_TAIL, flashStartAddr_logHist);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGHIST_HEAD, flashStartAddr_logHist);

  flash_chipErase();
  //check chip erase
  flash_bytesRead(0, _flashReadData, 8);
  assert_param(_flashReadData[0] == 0xFF);
  assert_param(_flashReadData[7] == 0xFF);
  //read some data belong to first 4kB and some to second 4kB
  flash_bytesRead(4092, _flashReadData, 8);
  assert_param(_flashReadData[0] == 0xFF);
  assert_param(_flashReadData[7] == 0xFF);
}

/**
 ****************************************************************
 @brief   Erased the stored configuration data on the FLASH.
 @param   -
 @return  -
 ****************************************************************
 */
void dataManager_flashClearCfg (void)
{
  flash_block4kErase(FLASH_BLOCKSTARTADDR_SYSCONFIG);
}

/**
 ****************************************************************
 @brief   Erased the stored data and history data on the FLASH.
 @param   -
 @return  -
 ****************************************************************
 */
void dataManager_flashClearMeasDataAndHistory (void)
{
  //clear all backup registers
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL, flashStartAddr_logData);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT, flashStartAddr_logData);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_HEAD, flashStartAddr_logData);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGHIST_TAIL, flashStartAddr_logHist);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGHIST_HEAD, flashStartAddr_logHist);

  //erase complete chip (much faster with typical 15 seconds, instead of
  //  1019 * 4kByte Blocks of each typical 50ms = 50.95 seconds (data section)
  //  2 * 4kByte Blocks of each typical 50ms = 0.1 seconds (log history section)
  //the disadvantage of this is, if a lost of the power source occurs, system configuration is lost as well
  flash_chipErase();

  //store back the system configuration from RAM to FLASH
  dataManager_cfgStoreToFlash();
}

/**
 ****************************************************************
 @brief   Rewind the data readout cursor for the log data on the FLASH.
 @param   -
 @return  -
 ****************************************************************
 */
void dataManager_flashRewindDataReadoutCursor (void)
{
  uint32_t tail = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL);

  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT, tail);
}

/**
 ****************************************************************
 @brief   Store the device name in the RAM _sysCfg struct
 @param   pDevName  Pointer to the device name with max length of DEV_NAME_MAX_LEN
 @param   len       The length of the name (1...DEV_NAME_MAX_LEN)
 @return  Return true, if successful, otherwise false
 ****************************************************************
 @note    After all system configuration parameters has been written,
 *        these settings has to be stored in FLASH by calling dataManager_cfgStoreToFlash()
 ****************************************************************
 */
bool dataManager_cfgDeviceNameSet (char *pDevName, uint8_t len)
{
  uint32_t idx;

  if ((len > DEV_NAME_MAX_LEN) || (len == 0))
  {
    return false;
  }

  memcpy(_sysCfg.devName, pDevName, len);
  _sysCfg.devName[len] = '\0';  //string terminator

  //clear other parts of the deviceName
  if (len < DEV_NAME_MAX_LEN)
  {
    for (idx = len + 1; idx <= DEV_NAME_MAX_LEN; idx++)
    {
      _sysCfg.devName[idx] = '\0';
    }
  }

  return true;
}

/**
 ****************************************************************
 @brief   Get the current device name stored in the RAM _sysCfg struct.
 @param   pDevName  Pointer to the device name with length 1...DEV_NAME_MAX_LEN
 @param   pLen      Pointer to the length of the device name.
 @return  -
 ****************************************************************
 */
void dataManager_cfgDeviceNameGet (char *pDevName, uint8_t *pLen)
{
  uint8_t len = strlen(_sysCfg.devName);

  //length limitation check!
  if (len > DEV_NAME_MAX_LEN)
  {
    len = DEV_NAME_MAX_LEN;
  }

  memcpy(pDevName, _sysCfg.devName, len);
  *pLen = len;
}

/**
 ****************************************************************
 @brief   Set the to be used measurement interval in minutes in the RAM _sysCfg struct.
 @param   minutes The to be used measurement interval (0...1092). 0 means, sampling every second.
 @return  Return true if okay, otherwise false (no new interval set)
 ****************************************************************
 @note    After all system configuration parameters has been written,
 *        these settings has to be stored in FLASH by calling dataManager_cfgStoreToFlash()
 ****************************************************************
 */
bool dataManager_cfgMeasIntervalSet (uint32_t minutes)
{
  if (minutes <= 1092)
  {
    _sysCfg.measInterval_minutes = minutes;

    //update the informations in the dataLogger
    dataLogger_measIntervalSet(minutes);

    return true;
  }
  return false;
}

/**
 ****************************************************************
 @brief   Get the currently set measurement interval in minutes.
 @param   -
 @return  Returns the measurement interval in unit minutes.
 ****************************************************************
 */
uint32_t dataManager_cfgMeasIntervalGet (void)
{
  return _sysCfg.measInterval_minutes;
}

/**
 ****************************************************************
 @brief   Store the battery configuration in the RAM _sysCfg struct
 @param   nominalCellMiliVoltage  The nominal cell voltage [mV]
 @param   nrOfCells The number of installed battery cells.
 @return  Return true, if successful, otherwise false
 ****************************************************************
 @note    After all system configuration parameters has been written,
 *        these settings has to be stored in FLASH by calling dataManager_cfgStoreToFlash()
 ****************************************************************
 */
bool dataManager_cfgBatterySettingSet (uint32_t nominalCellMiliVoltage, uint32_t nrOfCells)
{
  if (nrOfCells > 9)
  {
    return false;
  }
  if (nominalCellMiliVoltage > 9999)
  {
    return false;
  }
  _sysCfg.bat_nominalCellMilivoltage = nominalCellMiliVoltage;
  _sysCfg.bat_nrOfCells = nrOfCells;

  return true;
}

/**
 ****************************************************************
 @brief   Get the current battery configuration stored in the RAM _sysCfg struct.
 @param   pNominalCellMiliVoltage  Pointer to the nominal cell voltage [mV]
 @param   pNrOfCells Pointer to the number of installed battery cells.
 @return  -
 ****************************************************************
 */
void dataManager_cfgBatterySettingGet (uint32_t *pNominalCellMiliVoltage, uint32_t *pNrOfCells)
{
  *pNominalCellMiliVoltage = _sysCfg.bat_nominalCellMilivoltage;
  *pNrOfCells = _sysCfg.bat_nrOfCells;
}

/**
 ****************************************************************
 @brief   Store the name of a analog channel in the RAM _sysCfg struct
 @param   analogChannelNr One of the possible analog channels (0..3)
 @param   pAnalogChannelName  Pointer to the name to be set for this channel
 @param   len   The length of the name to be set (1..ANALOG_NAME_MAX_LEN)
 @return  Return true, if successful, otherwise false
 ****************************************************************
 @note    After all system configuration parameters has been written,
 *        these settings has to be stored in FLASH by calling dataManager_cfgStoreToFlash()
 ****************************************************************
 */
bool dataManager_cfgAnalogNameSet (uint8_t analogChannelNr, char *pAnalogChannelName, uint8_t len)
{
  char *pTarget;

  if ((len > ANALOG_NAME_MAX_LEN) || (len == 0) || (analogChannelNr > 3))
  {
    return false;
  }
  else
  {
    switch (analogChannelNr)
    {
      default:
        return false;
        break;
      case 0:
        pTarget = _sysCfg.analogChannel0Name;
        break;
      case 1:
        pTarget = _sysCfg.analogChannel1Name;
        break;
      case 2:
        pTarget = _sysCfg.analogChannel2Name;
        break;
      case 3:
        pTarget = _sysCfg.analogChannel3Name;
        break;
    }
    memcpy(pTarget, pAnalogChannelName, len);
    pTarget[len] = '\0';  //string terminator

    return true;
  }
}

/**
 ****************************************************************
 @brief   Get the current name of the analog channel stored in the RAM _sysCfg struct.
 @param   analogChannelNr One of the possible analog channels (0..3)
 @param   pAnalogChannelName  Pointer to the name of this channel
 @param   pLen   Pointer to the length of the name (1..ANALOG_NAME_MAX_LEN)
 @return  -
 ****************************************************************
 */
bool dataManager_cfgAnalogNameGet (uint8_t analogChannelNr, char *pAnalogChannelName, uint8_t *pLen)
{
  char *pSource;
  uint8_t len;

  if (analogChannelNr > 3)
  {
    return false;
  }
  else
  {
    switch (analogChannelNr)
    {
      default:
        return false;
        break;
      case 0:
        pSource = _sysCfg.analogChannel0Name;
        break;
      case 1:
        pSource = _sysCfg.analogChannel1Name;
        break;
      case 2:
        pSource = _sysCfg.analogChannel2Name;
        break;
      case 3:
        pSource = _sysCfg.analogChannel3Name;
        break;
    }
    len = strlen(pSource);
    if (len > ANALOG_NAME_MAX_LEN)
    {
      //this string length is not possible
      assert_param(0);
    }
    else if (len == 0)
    {
      //this string length is not possible
      assert_param(0);
    }
    memcpy(pAnalogChannelName, pSource, len);
    *pLen = len;

    return true;
  }
}

/**
 ****************************************************************
 @brief   Activate or deactivate the automatic measurement loop functionality.
 @param   active  Boolean parameter to indicate the new automatic measurement cycle activation
 @return  -
 ****************************************************************
 */
void dataManager_cfgAutoMeasActiveSet (bool active)
{
  _sysCfg.autoMeasActive = active;
}

/**
 ****************************************************************
 @brief   Check if the automatic measurement loop is activated or not.
 @param   -
 @return  Return true, if the automatic measurement cycle is active, otherwise false
 ****************************************************************
 */
bool dataManager_cfgAutoMeasActiveGet (void)
{
  return _sysCfg.autoMeasActive;
}

/**
 ****************************************************************
 @brief   Store the current system configuration in FLASH
 *        (on sysCfg memory block)
 @param   -
 @return  -
 ****************************************************************
 */
void dataManager_cfgStoreToFlash (void)
{
  const uint32_t maxMemoryBlockSize = 256;
  uint32_t flashAddr = flashStartAddr_sysConfig;
  uint32_t idx = 0;
  uint32_t sysCfgSize = sizeof(dataManager_sysConfigSmall_t);

  for (; sysCfgSize > 0;)
  {
    if (sysCfgSize > maxMemoryBlockSize)
    {
      //store configuration by memory block limitations (256Bytes)
      flash_bytesWrite(flashAddr, &_sysCfg.BYTE[idx], maxMemoryBlockSize);

      flashAddr += maxMemoryBlockSize;
      idx += maxMemoryBlockSize;
      sysCfgSize -= maxMemoryBlockSize;
    }
    else
    {
      //write last memory block/or parts of it
      flash_bytesWrite(flashAddr, &_sysCfg.BYTE[idx], sysCfgSize);

      flashAddr += sysCfgSize;
      idx += sysCfgSize;
      sysCfgSize -= sysCfgSize;
    }
  }

  //read current sysConfiguration out of the flash
  flash_bytesRead(flashStartAddr_sysConfig, _sysCfg.BYTE, sizeof(dataManager_sysConfigSmall_t));
}

/**
 ****************************************************************
 @brief   Store the next measurement data point on the FLASH.
 @param   pDataPoint  Pointer to the to be stored measurement data.
 @return  True if the data point was correct stored, otherwise return false
 ****************************************************************
 @note    The weekday of the RTC timeStamp is not stored!
 ****************************************************************
 */
bool dataManager_flashNextMeasDataPointStore (dataManager_dataPoint_t *pDataPoint)
{
  uint32_t tail;
  uint32_t tailReadOut;
  uint32_t head;
  uint32_t head_new;
  uint32_t blockNrTail;
  uint32_t blockNrTailReadOut;
  uint32_t blockNrHead;
  uint32_t blockNrHead_new;
  dataManager_dataPointSmall_t dataPointSmall;

  //get the current memory addresses for the FLASH (circular memory)
  tail = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL);
  tailReadOut = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT);
  head = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_HEAD);

  blockNrTail = tail >> 12;               //4kByte block size
  blockNrTailReadOut = tailReadOut >> 12; //4kByte block size
  blockNrHead = head >> 12;               //4kByte block size

  head_new = head + sizeof(dataManager_dataPointSmall_t);
  if (head_new >= (flashStartAddr_logData + flashSize_logData))
  {
    //wrap around
    head_new = flashStartAddr_logData;
  }
  blockNrHead_new = head_new >> 12;         //4kByte block size

  //check if there is space left between tail and new-head (not in same block)
  if ((blockNrHead_new == blockNrTail) && (dataManager_flashNrOfMeasDataPointsGet(true) >= flashMaxNrOfLogDataPoints))
  {
    //erase 4kByte memory block (oldest data)
    flash_block4kErase(blockNrTail);

    tail = (blockNrTail + 1) << 12;
    if (tail >= (flashStartAddr_logData + flashSize_logData))
    {
      tail = flashStartAddr_logData; //wrap around
    }
    sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL, tail);

    //check if tail of readOut needs to be set new as well
    if (blockNrTailReadOut == blockNrTail)
    {
      //in the same block as old tail block number, so update it as well to new tail
      sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT, tail);
    }
  }
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_HEAD, head_new);

  //compact data first and then store them
  dataPointSmall.timeStamp.year = pDataPoint->timeStamp.year - 2000;
  dataPointSmall.timeStamp.month = pDataPoint->timeStamp.month;
  dataPointSmall.timeStamp.day = pDataPoint->timeStamp.day;
  dataPointSmall.timeStamp.hours = pDataPoint->timeStamp.hrs;
  dataPointSmall.timeStamp.minutes = pDataPoint->timeStamp.min;
  dataPointSmall.timeStamp.seconds = pDataPoint->timeStamp.sec;
  dataPointSmall.timeStamp.milliseconds = pDataPoint->timeStamp.ms;
  dataPointSmall.timeBasedMeas = pDataPoint->timeBasedMeas;

  //internal handling of NaN values for battery voltage (needed because of data compression)
  if (__isnanf(pDataPoint->vBat))
  {
    dataPointSmall.vBat = SYS_VBAT_ISNAN;
  }
  else
  {
    dataPointSmall.vBat = (uint16_t) (pDataPoint->vBat * 256.0f);                           //format 8.8, unsigned
  }

  //internal handling of NaN values for ADC_IN8 voltage (needed because of data compression)
  if (__isnanf(pDataPoint->adc_in8))
  {
    dataPointSmall.adc_in8 = ADC_IN_ISNAN;
  }
  else
  {
    dataPointSmall.adc_in8 = (uint16_t) (pDataPoint->adc_in8 * 8192.0f);                    //format 3.13, unsigned
  }

  //internal handling of NaN values for ADC_IN9 voltage (needed because of data compression)
  if (__isnanf(pDataPoint->adc_in9))
  {
    dataPointSmall.adc_in9 = ADC_IN_ISNAN;
  }
  else
  {
    dataPointSmall.adc_in9 = (uint16_t) (pDataPoint->adc_in9 * 8192.0f);                    //format 3.13, unsigned
  }

  //internal handling of NaN values for ADC_IN10 voltage (needed because of data compression)
  if (__isnanf(pDataPoint->adc_in10))
  {
    dataPointSmall.adc_in10 = ADC_IN_ISNAN;
  }
  else
  {
    dataPointSmall.adc_in10 = (uint16_t) (pDataPoint->adc_in10 * 8192.0f);                  //format 3.13, unsigned
  }

  //internal handling of NaN values for ADC_IN11 voltage (needed because of data compression)
  if (__isnanf(pDataPoint->adc_in11))
  {
    dataPointSmall.adc_in11 = ADC_IN_ISNAN;
  }
  else
  {
    dataPointSmall.adc_in11 = (uint16_t) (pDataPoint->adc_in11 * 8192.0f);                  //format 3.13, unsigned
  }

  //internal handling of NaN values for temperature (needed because of data compression)
  if (__isnanf(pDataPoint->bme280.temperature))
  {
    dataPointSmall.bme280_temperature = BME280_TEMPERATURE_ISNAN;
  }
  else
  {
    dataPointSmall.bme280_temperature = (int16_t) (pDataPoint->bme280.temperature * 256.0f);  //format 8.8, signed
  }

  dataPointSmall.bme280_pressure = pDataPoint->bme280.pressure;                             //format float

  //internal handling of NaN values for humidity (needed because of data compression)
  if (__isnanf(pDataPoint->bme280.humidity))
  {
    dataPointSmall.bme280_humidity = BME280_HUMIDITY_ISNAN;
  }
  else
  {
    dataPointSmall.bme280_humidity = (uint16_t) (pDataPoint->bme280.humidity * 512.0f);     //format 7.9, unsigned
  }

  //address check for memory location correctness
  if (head <= (flashSize_logData - sizeof(dataManager_dataPointSmall_t)))
  {
    flash_bytesWrite(head, &dataPointSmall.BYTE[0], sizeof(dataManager_dataPointSmall_t));

    return true;
  }
  else
  {
    //dataPoint could not be written, incorrect head cursor
    return false;
  }
}

/**
 ****************************************************************
 @brief   Get the number of not already read out measurement data points stored on the FLASH.
 @param   absolute  If true, the absolute number of measurement data points are returned,
 *        otherwise only the not already read out number
 @return  Return the number of measurement data points (0 ... flashMaxNrOfLogDataPoints)
 ****************************************************************
 */
uint32_t dataManager_flashNrOfMeasDataPointsGet (bool absolute)
{
  uint32_t tailAbsolute;
  uint32_t tailReadout;
  uint32_t head;
  uint32_t nrOfMeasDataPointsAbsolute;
  uint32_t nrOfMeasDataPointsReadout;

  //get the current memory addresses for the FLASH (circular memory)
  head = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_HEAD);
  tailAbsolute = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL);
  tailReadout = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT);

  //calculate the absolute number of measDataPoints
  if (head >= tailAbsolute)
  {
    nrOfMeasDataPointsAbsolute = head - tailAbsolute;
  }
  else
  {
    nrOfMeasDataPointsAbsolute = (flashSize_logData - tailAbsolute) + head;
  }
  nrOfMeasDataPointsAbsolute /= sizeof(dataManager_dataPointSmall_t);

  //calculate the newReadout number of measDataPoints
  if (head >= tailReadout)
  {
    nrOfMeasDataPointsReadout = head - tailReadout;
  }
  else
  {
    nrOfMeasDataPointsReadout = (flashSize_logData - tailReadout) + head;
  }
  nrOfMeasDataPointsReadout /= sizeof(dataManager_dataPointSmall_t);

  if (absolute)
  {
    return nrOfMeasDataPointsAbsolute;
  }
  else
  {
    //check for some error!
    if (nrOfMeasDataPointsReadout > nrOfMeasDataPointsAbsolute)
    {
      //this should never happens, otherwise there was a wrong handling with dataPointers!
      nrOfMeasDataPointsReadout = nrOfMeasDataPointsAbsolute;
    }

    return nrOfMeasDataPointsReadout;
  }
}

/**
 ****************************************************************
 @brief   Get a measurement data point out from the FLASH.
 @param   dataPointNr One of the possible dataPoint numbers (0 ... dataManager_nrOfMeasDataPointsGet())
 @param   pDataPoint  Pointer to the read out measurement data.
 @return  -
 ****************************************************************
 @note    The weekday of the RTC timeStamp is not stored!
 ****************************************************************
 */
void dataManager_flashMeasDataPointGet (uint32_t dataPointNr, dataManager_dataPoint_t *pDataPoint)
{
  uint32_t tail;
  uint32_t address;
  dataManager_dataPointSmall_t dataPointSmall;

  //get the current memory addresses for the FLASH (circular memory)
  tail = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL);

  address = tail + (sizeof(dataManager_dataPointSmall_t) * dataPointNr);
  if (address >= flashSize_logData)
  {
    address -= flashSize_logData; //wrap around
  }

  //read data
  flash_bytesRead(address, &dataPointSmall.BYTE[0], sizeof(dataManager_dataPointSmall_t)); //takes around 9699 clocks @Release_v0.3.0

  //and convert back the compact data system units
  pDataPoint->timeStamp.year = ((uint16_t) dataPointSmall.timeStamp.year) + 2000;
  pDataPoint->timeStamp.month = ((uint8_t) dataPointSmall.timeStamp.month);
  pDataPoint->timeStamp.day = ((uint8_t) dataPointSmall.timeStamp.day);
  pDataPoint->timeStamp.hrs = ((uint8_t) dataPointSmall.timeStamp.hours);
  pDataPoint->timeStamp.min = ((uint8_t) dataPointSmall.timeStamp.minutes);
  pDataPoint->timeStamp.sec = ((uint8_t) dataPointSmall.timeStamp.seconds);
  pDataPoint->timeStamp.ms = ((uint16_t) dataPointSmall.timeStamp.milliseconds);
  pDataPoint->timeBasedMeas = dataPointSmall.timeBasedMeas;

  //NaN number handling
  if (dataPointSmall.vBat == SYS_VBAT_ISNAN)
  {
    pDataPoint->vBat = NAN;
  }
  else
  {
    pDataPoint->vBat = ((float) dataPointSmall.vBat) / 256.0f;                            //format 8.8, unsigned
  }

  //NaN number handling
  if (dataPointSmall.adc_in8 == ADC_IN_ISNAN)
  {
    pDataPoint->adc_in8 = NAN;
  }
  else
  {
    pDataPoint->adc_in8 = ((float) dataPointSmall.adc_in8) / 8192.0f;                     //format 3.13, unsigned
  }

  //NaN number handling
  if (dataPointSmall.adc_in9 == ADC_IN_ISNAN)
  {
    pDataPoint->adc_in9 = NAN;
  }
  else
  {
    pDataPoint->adc_in9 = ((float) dataPointSmall.adc_in9) / 8192.0f;                     //format 3.13, unsigned
  }

  //NaN number handling
  if (dataPointSmall.adc_in10 == ADC_IN_ISNAN)
  {
    pDataPoint->adc_in10 = NAN;
  }
  else
  {
    pDataPoint->adc_in10 = ((float) dataPointSmall.adc_in10) / 8192.0f;                   //format 3.13, unsigned
  }

  //NaN number handling
  if (dataPointSmall.adc_in11 == ADC_IN_ISNAN)
  {
    pDataPoint->adc_in11 = NAN;
  }
  else
  {
    pDataPoint->adc_in11 = ((float) dataPointSmall.adc_in11) / 8192.0f;                   //format 3.13, unsigned
  }

  //NaN number handling
  if (dataPointSmall.bme280_temperature == BME280_TEMPERATURE_ISNAN)
  {
    pDataPoint->bme280.temperature = NAN;
  }
  else
  {
    pDataPoint->bme280.temperature = ((float) dataPointSmall.bme280_temperature) / 256.0f;  //format 8.8, signed
  }

  pDataPoint->bme280.pressure = dataPointSmall.bme280_pressure;                           //format float

  //NaN number handling
  if (dataPointSmall.bme280_humidity == BME280_HUMIDITY_ISNAN)
  {
    pDataPoint->bme280.humidity = NAN;
  }
  else
  {
    pDataPoint->bme280.humidity = ((float) dataPointSmall.bme280_humidity) / 512.0f;        //format 7.9, unsigned
  }
}

/**
 ****************************************************************
 @brief   Store a new event to the log data located in the corresponding FLASH section.
 @param   pTimeStamp  Pointer to the actual time information
 @param   evt One of the possible events
 @param   pAdditionalInfo Pointer to a string based additional information to be stored (max. 57 characters).
 @return  True if the logEvent was correct stored, otherwise return false
 ****************************************************************
 */
bool dataManager_flashLogEvent (const rtcDateTime_t *const pTimeStamp, dataManager_logEvt_t evt, char *pAdditionalInfo)
{
  uint32_t tail;
  uint32_t head;
  uint32_t head_new;
  uint32_t blockNrTail;
  uint32_t blockNrHead_new;
  dataManager_logHistorySmall_t logHistorySmall;

  //get the current memory addresses for the FLASH (circular memory)
  tail = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGHIST_TAIL);
  head = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGHIST_HEAD);

  blockNrTail = tail >> 12;         //4kByte block size

  head_new = head + sizeof(dataManager_logHistorySmall_t);
  if (head_new >= (flashStartAddr_logHist + flashSize_logHist))
  {
    //wrap around
    head_new = flashStartAddr_logHist;
  }
  blockNrHead_new = head_new >> 12; //4kByte block size

  //check if there is space left between tail and new-head
  if ((blockNrHead_new == blockNrTail) && (dataManager_flashNrOfLogEventsGet() >= flashMaxNrOfLogHistPoints))
  {
    //erase 4kByte memory block (oldest data)
    flash_block4kErase(blockNrTail);

    blockNrTail++;
    tail = blockNrTail << 12;
    if (tail >= (flashStartAddr_logHist + flashSize_logHist))
    {
      tail = flashStartAddr_logHist; //wrap around
    }
    sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGHIST_TAIL, tail);
  }
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGHIST_HEAD, head_new);

  //compact log informations and then store them
  logHistorySmall.timeStamp.year = pTimeStamp->year - 2000;
  logHistorySmall.timeStamp.month = pTimeStamp->month;
  logHistorySmall.timeStamp.day = pTimeStamp->day;
  logHistorySmall.timeStamp.hours = pTimeStamp->hrs;
  logHistorySmall.timeStamp.minutes = pTimeStamp->min;
  logHistorySmall.timeStamp.seconds = pTimeStamp->sec;
  logHistorySmall.timeStamp.milliseconds = pTimeStamp->ms;
  logHistorySmall.evt = (uint8_t) evt;
  strncpy(logHistorySmall.addInfo, pAdditionalInfo, sizeof(logHistorySmall.addInfo));

  //check if address is in correct range
  if ((head >= flashStartAddr_logHist)
    && (head <= ((flashStartAddr_logHist + flashSize_logHist) - sizeof(dataManager_logHistorySmall_t))))
  {
    flash_bytesWrite(head, &logHistorySmall.BYTE[0], sizeof(dataManager_logHistorySmall_t));

    return true;
  }
  else
  {
    //logEvent could not be written, incorrect head cursor
    return false;
  }
}

/**
 ****************************************************************
 @brief   Get the number of log events in the stored corresponding section on the FLASH.
 @param   -
 @return  Return the number of log events (0 ... flashMaxNrOfLogHistPoints)
 ****************************************************************
 */
uint32_t dataManager_flashNrOfLogEventsGet (void)
{
  uint32_t tail;
  uint32_t head;
  uint32_t nrOfLogEvents;

  //get the current memory addresses for the FLASH (circular memory)
  tail = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGHIST_TAIL);
  head = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGHIST_HEAD);

  if (head >= tail)
  {
    nrOfLogEvents = head - tail;
  }
  else
  {
    nrOfLogEvents = (flashSize_logHist - tail) + head;
  }
  nrOfLogEvents /= sizeof(dataManager_logHistorySmall_t);

  return nrOfLogEvents;
}

/**
 ****************************************************************
 @brief   Check if an other firmware version for this device is available
 *        on the USB-memory stick.
 *        File must be named: datalogger_firmware.bin
 *        File must be located on root directory on USB-Stick
 @param   pFileName The resulting path/filename of the new firmware image (max. 128Byte)
 @return  True if a other image was found, otherwise false
 ****************************************************************
 */
bool dataManager_usbStickCheckForNewFirmwareFile (char *pFileName)
{
  FRESULT fr;
  bool firmwareFileFound = false;

  if (!_dataManager_usbStickMount())
  {
    return false;
  }

  //search for binary files in root directory that matches "datalogger_firmware.bin"
  fr = f_findfirst(&dj, &fno, "", "datalogger_firmware.bin");

  //Repeat through all items that matches
  if ((fr == FR_OK) && (fno.fname[0]))
  {
    //a file found
    firmwareFileFound = true;

    //generate pathAndFileName
    strcpy(pFileName, fno.fname);
  }
  f_closedir(&dj);

  _dataManager_usbStickUnmount();

  return firmwareFileFound;
}

/**
 ****************************************************************
 @brief   Store all not already readout measurement data from the FLASH to different files on the USB-memory stick.
 @param   -
 @return  True on success, otherwise false
 ****************************************************************
 */
bool dataManager_usbStickDataStore (void)
{
  uint32_t idx;
  rtcDateTime_t rtcDateTime;
  uint32_t nrOfNewDataPoints = dataManager_flashNrOfMeasDataPointsGet(false);
  uint32_t nrOfDataPoints = dataManager_flashNrOfMeasDataPointsGet(true);
  uint8_t len;
  bool success = true;
  char logText[48];

  if (!_dataManager_usbStickMount())  //takes around 14596160 clocks at DEBUG
  {
    //unmount is called in mount function itself if an error occurs or no USB-Stick is detected
    return false;
  }

  sys_rtcDateTimeGet(&rtcDateTime);

  idx = 0;

  //get the deviceName
  dataManager_cfgDeviceNameGet(&path[idx], &len);
  idx += len;

  //extend path by file ending
  path[idx++] = '.';
  path[idx++] = 'c';
  path[idx++] = 's';
  path[idx++] = 'v';
  path[idx] = '\0'; //string termination
  assert_param(idx < sizeof(path));

  if (!_dataManager_measDataFileStore(path, nrOfDataPoints, nrOfNewDataPoints))
  {
    //rewind data cursors to the start, if a error occurs
    dataManager_flashRewindDataReadoutCursor();

    sprintf(logText, "fail on *.csv: abs: %06u, new: %06u", (unsigned int) nrOfDataPoints,
      (unsigned int) nrOfNewDataPoints);
    dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_DATAREADOUT, logText);

    success = false;
  }

  //create the metaData file path
  idx -= 3;
  //extend path by file ending
  path[idx++] = 't';
  path[idx++] = 'x';
  path[idx] = 't';
  assert_param(idx < sizeof(path));

  if (!_dataManager_metaDataFileStore(path))
  {
    dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_DATAREADOUT, "fail on *.txt");

    success = false;
  }

  //create the logData file path
  idx -= 2;
  //extend path by file ending
  path[idx++] = 'l';
  path[idx++] = 'o';
  path[idx] = 'g';
  assert_param(idx < sizeof(path));

  //save the timeStamp of this read out, save in log history if not already marked with an error for this readout
  if (success)
  {
    sprintf(logText, "success: abs: %06u, new: %06u", (unsigned int) nrOfDataPoints, (unsigned int) nrOfNewDataPoints);
    dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_DATAREADOUT, logText);
  }

  if (!_dataManager_logDataFileStore(path))
  {
    dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_DATAREADOUT, "fail on *.log");

    success = false;
  }

  _dataManager_usbStickUnmount();

  return success;
}

/**
 ****************************************************************
 @brief   Internally function to mount the usbStick memory, initialize the
 *        FatFileSystem and turn on the 5V to the USB-Stick.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _dataManager_usbStickMount (void)
{
  const uint32_t deltaTickMax = 1000; //maximum tick timeout (equals 1000ms)
  uint32_t sysTime;
  uint32_t deltaTick = 0;
  FRESULT iFResult;

  //check if VUSB is not present actually
  if (!digIO_usbDetected())
  {
    //USB could interacts as host and should try to power the V-USB
    digIO_otgDeviceModeSet(false);
    digIO_5VExtPwrOn();       //turn power source V_Ext (connector J1) ON --> Device powered from J1 source
    digIO_5VtoUSBSwitchOn();  //use internal 5V to supply V-USB

    MX_USB_HOST_Init();
    sys_sysTickTickGet(&sysTime);
    while ((USBH_MSC_IsReady(&hUsbHostFS) == 0) && (deltaTick < deltaTickMax))
    {
      MX_USB_HOST_Process();
      sys_sysTickTickDeltaGet(sysTime, &deltaTick);
    }

    if (deltaTick >= deltaTickMax)
    {
      return false;
    }

    MX_FATFS_Init();
    MX_USB_HOST_Process();

    //Mount the file system immediately, using logical disk 0
    iFResult = f_mount(&USBHFatFS, (TCHAR const*) USBHPath, 0);
    if (iFResult != FR_OK)
    {
      _dataManager_usbStickUnmount();

      return false;
    }
    return true;
  }
  else
  {
    return false; //VUSB is already present --> so Host mode not possible!
  }
}

/**
 ****************************************************************
 @brief   Internally function to unmount the usbStick memory, deInit the
 *        FatFileSystem and turn off the 5V VUSB generation.
 @param   -
 @return  -
 ****************************************************************
 */
static void _dataManager_usbStickUnmount (void)
{
  //unmount file system
  f_mount(0, (TCHAR const*) USBHPath, 1);
  MX_USB_HOST_Process();

  //wait again, in case some more buffers need flushing
  while (USBH_MSC_IsReady(&hUsbHostFS) == 0)
  {
    MX_USB_HOST_Process();
  }

  //unlink driver
  FATFS_DeInit();
  MX_USB_HOST_Process();

  //and wait again, just in case
  while (USBH_MSC_IsReady(&hUsbHostFS) == 0)
  {
    MX_USB_HOST_Process();
  }

  if (USBH_Stop(&hUsbHostFS) != USBH_OK)
  {
    Error_Handler();
  }
  if (USBH_DeInit(&hUsbHostFS) != USBH_OK)
  {
    Error_Handler();
  }

  //switch the power OFF to VUSB
  digIO_otgDeviceModeSet(true);   //interact as Device
  digIO_5VtoUSBSwitchOff();
}

/**
 ****************************************************************
 @brief   Internally function to build the measurementData file and store it on the memory drive.
 @param   pPath Pointer to the path of the file (including the file name/type)
 @param   nrOfDataPoints     The absolute number of available dataPoints
 @param   nrOfNewDataPoints  The number of new available dataPoints (if 0: only header will be stored)
 @return  -
 ****************************************************************
 @note    If nrOfDataPoints == nrOfNewDataPoints, a new file will be created and everything will be stored
 ****************************************************************
 */
static bool _dataManager_measDataFileStore (TCHAR *pPath, uint32_t nrOfDataPoints, uint32_t nrOfNewDataPoints)
{
  uint32_t dataPointNr;
  dataManager_dataPoint_t dataPoint;
  UINT bytesLeftToWrite = 0;
  uint32_t strLen;
  FRESULT iFResultExists = FR_NO_FILE;
  FRESULT iFResult;
  uint8_t len;
  uint32_t tailReadOut;

  if (nrOfDataPoints == nrOfNewDataPoints)
  {
    //always create a new .csv-File
    iFResult = f_open(&file, pPath, FA_CREATE_ALWAYS | FA_WRITE);
    if ((iFResult != FR_OK) && (iFResult != FR_EXIST))
    {
      return false;
    }
  }
  else
  {
    //create a new .csv-File only if not already exists, otherwise append new data
    iFResultExists = f_stat(pPath, &fno);
    iFResult = f_open(&file, pPath, FA_OPEN_APPEND | FA_WRITE); //takes around 612550 clock cycles at DEBUG
    if ((iFResult != FR_OK) && (iFResult != FR_EXIST))
    {
      return false;
    }
  }

  //only write header if file not already exists
  if (iFResultExists == FR_NO_FILE)
  {
    //write header line (logData build up)
    strcpy(&usbStickDataBuffer[bytesLeftToWrite],
      "RTC_Datetime(YYYY.mm.dd HH:MM:SS.MS+0000);Measurement_Auto;Battery_Voltage(V);BME_Temperature(C);BME_Pressure(Pa);BME_Humidity(%%);");
    bytesLeftToWrite += strlen(usbStickDataBuffer);

    //append by dynamic header
    //analog channel0 (ADC_IN8 in schematic)
    len = strlen(_sysCfg.analogChannel0Name);
    memcpy(&usbStickDataBuffer[bytesLeftToWrite], _sysCfg.analogChannel0Name, len);
    bytesLeftToWrite += len;
    usbStickDataBuffer[bytesLeftToWrite++] = ';';
    //analog channel1 (ADC_IN9 in schematic)
    len = strlen(_sysCfg.analogChannel1Name);
    memcpy(&usbStickDataBuffer[bytesLeftToWrite], _sysCfg.analogChannel1Name, len);
    bytesLeftToWrite += len;
    usbStickDataBuffer[bytesLeftToWrite++] = ';';
    //analog channel2 (ADC_IN10 in schematic)
    len = strlen(_sysCfg.analogChannel2Name);
    memcpy(&usbStickDataBuffer[bytesLeftToWrite], _sysCfg.analogChannel2Name, len);
    bytesLeftToWrite += len;
    usbStickDataBuffer[bytesLeftToWrite++] = ';';
    //analog channel3 (ADC_IN11 in schematic)
    len = strlen(_sysCfg.analogChannel3Name);
    memcpy(&usbStickDataBuffer[bytesLeftToWrite], _sysCfg.analogChannel3Name, len);
    bytesLeftToWrite += len;
    usbStickDataBuffer[bytesLeftToWrite++] = '\r';
    usbStickDataBuffer[bytesLeftToWrite++] = '\n';

    //write data, but only if they are reaching the block size
    if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
    {
      return false;
    }

    //store all datapoint on it, even if these are not new once!
    nrOfNewDataPoints = nrOfDataPoints;
  }

  if (nrOfNewDataPoints)
  {
    //write .csv data file
    dataPointNr = nrOfDataPoints - nrOfNewDataPoints;
    do
    {
      //write next line to .csv-File
      dataManager_flashMeasDataPointGet(dataPointNr, &dataPoint); //takes around 15210 clock cycles at DEBUG
      //format of data: year.Month.Day;Hrs:Min:Sec.MS;Boolean
      bytesLeftToWrite += (UINT) snprintf(&usbStickDataBuffer[bytesLeftToWrite], USB_STICK_BLOCK_SIZE,
        "%04u.%02u.%02u %02u:%02u:%02u.%03u;%d;", dataPoint.timeStamp.year, dataPoint.timeStamp.month,
        dataPoint.timeStamp.day, dataPoint.timeStamp.hrs, dataPoint.timeStamp.min, dataPoint.timeStamp.sec,
        dataPoint.timeStamp.ms, dataPoint.timeBasedMeas);       //takes around 7096 clock cycles at DEBUG

      //convert floating point numbers to strings (//takes around 2923 clock cycles at DEBUG)
      //vBat(format x.6);BME_T(format x.6);BME_P(format x.6);BME_H(format x.6);ADC_IN8(format x.6);ADC_IN9(format x.6);ADC_IN10(format x.6);ADC_IN11(format x.6);
      strConv_floatToString_6(dataPoint.vBat, &usbStickDataBuffer[bytesLeftToWrite], &strLen);
      bytesLeftToWrite += strLen;
      usbStickDataBuffer[bytesLeftToWrite++] = ';';
      strConv_floatToString_6(dataPoint.bme280.temperature, &usbStickDataBuffer[bytesLeftToWrite], &strLen);
      bytesLeftToWrite += strLen;
      usbStickDataBuffer[bytesLeftToWrite++] = ';';
      strConv_floatToString_6(dataPoint.bme280.pressure, &usbStickDataBuffer[bytesLeftToWrite], &strLen);
      bytesLeftToWrite += strLen;
      usbStickDataBuffer[bytesLeftToWrite++] = ';';
      strConv_floatToString_6(dataPoint.bme280.humidity, &usbStickDataBuffer[bytesLeftToWrite], &strLen);
      bytesLeftToWrite += strLen;
      usbStickDataBuffer[bytesLeftToWrite++] = ';';
      strConv_floatToString_6(dataPoint.adc_in8, &usbStickDataBuffer[bytesLeftToWrite], &strLen);
      bytesLeftToWrite += strLen;
      usbStickDataBuffer[bytesLeftToWrite++] = ';';
      strConv_floatToString_6(dataPoint.adc_in9, &usbStickDataBuffer[bytesLeftToWrite], &strLen);
      bytesLeftToWrite += strLen;
      usbStickDataBuffer[bytesLeftToWrite++] = ';';
      strConv_floatToString_6(dataPoint.adc_in10, &usbStickDataBuffer[bytesLeftToWrite], &strLen);
      bytesLeftToWrite += strLen;
      usbStickDataBuffer[bytesLeftToWrite++] = ';';
      strConv_floatToString_6(dataPoint.adc_in11, &usbStickDataBuffer[bytesLeftToWrite], &strLen);
      bytesLeftToWrite += strLen;
      usbStickDataBuffer[bytesLeftToWrite++] = '\r';
      usbStickDataBuffer[bytesLeftToWrite++] = '\n';

      //write data, but only if they are reaching the block size
      if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
      {
        return false;
      }
      dataPointNr++;
    } while (dataPointNr != nrOfDataPoints);
  }

  //write remaining data anyway
  if (!_dataManager_writeBlockToOpenedFile(true, &bytesLeftToWrite))
  {
    return false;
  }

  //close .csv data file
  iFResult = f_close(&file);  //takes around 302422 clock cycles at DEBUG
  if (iFResult != FR_OK)
  {
    return false;
  }

  //set the readout flash data pointer (tail) to the head
  tailReadOut = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_HEAD);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT, tailReadOut);

  return true;
}

/**
 ****************************************************************
 @brief   Internally function to build the metaData file and store it on the memory drive.
 @param   pPath Pointer to the path of the file (including the file name/type)
 @return  -
 ****************************************************************
 */
static bool _dataManager_metaDataFileStore (TCHAR *pPath)
{
  sysID_t sysID;
  fwVersion_t fwVersion;
  UINT bytesLeftToWrite = 0;
  FRESULT iFResult;
  uint8_t len;

  //create this file all time new
  iFResult = f_open(&file, pPath, FA_CREATE_ALWAYS | FA_WRITE);
  if ((iFResult != FR_OK) && (iFResult != FR_EXIST))
  {
    return false;
  }

  //write meta data file content (.txt file)
  //line: device name
  strcpy(&usbStickDataBuffer[bytesLeftToWrite], "Device name: ");
  bytesLeftToWrite += 13;

  dataManager_cfgDeviceNameGet(&usbStickDataBuffer[bytesLeftToWrite], &len);
  bytesLeftToWrite += len;
  usbStickDataBuffer[bytesLeftToWrite++] = '\r';
  usbStickDataBuffer[bytesLeftToWrite++] = '\n';
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: timeBasedMeasurementActive
  if (dataManager_cfgAutoMeasActiveGet())
  {
    strcpy(&usbStickDataBuffer[bytesLeftToWrite], "Time based measurement: enabled\r\n");
    bytesLeftToWrite += strlen("Time based measurement: enabled\r\n");
  }
  else
  {
    strcpy(&usbStickDataBuffer[bytesLeftToWrite], "Time based measurement: disabled\r\n");
    bytesLeftToWrite += strlen("Time based measurement: disabled\r\n");
  }
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: measurement interval
  bytesLeftToWrite += (UINT) snprintf(&usbStickDataBuffer[bytesLeftToWrite], USB_STICK_BLOCK_SIZE,
    "Measurement interval (minutes): %u\r\n", _sysCfg.measInterval_minutes);
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: battery nominal voltage
  bytesLeftToWrite += (UINT) snprintf(&usbStickDataBuffer[bytesLeftToWrite], USB_STICK_BLOCK_SIZE,
    "Battery nominal voltage (mV): %u\r\n", _sysCfg.bat_nominalCellMilivoltage);
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: battery number of cells
  bytesLeftToWrite += (UINT) snprintf(&usbStickDataBuffer[bytesLeftToWrite], USB_STICK_BLOCK_SIZE,
    "Battery number of cells: %u\r\n", _sysCfg.bat_nrOfCells);
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: analog input channel name mappings
  strcpy(&usbStickDataBuffer[bytesLeftToWrite], "Analog channel mappings:\r\n");
  bytesLeftToWrite += 26;
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: analog input channel0 name mapping
  strcpy(&usbStickDataBuffer[bytesLeftToWrite], " ADC_IN8 (HW): ");
  bytesLeftToWrite += 15;
  len = strlen(_sysCfg.analogChannel0Name);
  memcpy(&usbStickDataBuffer[bytesLeftToWrite], _sysCfg.analogChannel0Name, len);
  bytesLeftToWrite += len;
  usbStickDataBuffer[bytesLeftToWrite++] = '\r';
  usbStickDataBuffer[bytesLeftToWrite++] = '\n';
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: analog input channel1 name mapping
  strcpy(&usbStickDataBuffer[bytesLeftToWrite], " ADC_IN9 (HW): ");
  bytesLeftToWrite += 15;
  len = strlen(_sysCfg.analogChannel1Name);
  memcpy(&usbStickDataBuffer[bytesLeftToWrite], _sysCfg.analogChannel1Name, len);
  bytesLeftToWrite += len;
  usbStickDataBuffer[bytesLeftToWrite++] = '\r';
  usbStickDataBuffer[bytesLeftToWrite++] = '\n';
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: analog input channel2 name mapping
  strcpy(&usbStickDataBuffer[bytesLeftToWrite], " ADC_IN10 (HW): ");
  bytesLeftToWrite += 16;
  len = strlen(_sysCfg.analogChannel2Name);
  memcpy(&usbStickDataBuffer[bytesLeftToWrite], _sysCfg.analogChannel2Name, len);
  bytesLeftToWrite += len;
  usbStickDataBuffer[bytesLeftToWrite++] = '\r';
  usbStickDataBuffer[bytesLeftToWrite++] = '\n';
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: analog input channel3 name mapping
  strcpy(&usbStickDataBuffer[bytesLeftToWrite], " ADC_IN11 (HW): ");
  bytesLeftToWrite += 16;
  len = strlen(_sysCfg.analogChannel3Name);
  memcpy(&usbStickDataBuffer[bytesLeftToWrite], _sysCfg.analogChannel3Name, len);
  bytesLeftToWrite += len;
  usbStickDataBuffer[bytesLeftToWrite++] = '\r';
  usbStickDataBuffer[bytesLeftToWrite++] = '\n';
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: firmware version
  dataLogger_fwVersionGet(&fwVersion);
  bytesLeftToWrite += (UINT) snprintf(&usbStickDataBuffer[bytesLeftToWrite], USB_STICK_BLOCK_SIZE,
    "Firmware version: %c.%c.%c\r\n", fwVersion.major, fwVersion.minor, fwVersion.patch);
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: unique identification number
  sys_uniqueIdGet(&sysID);
  bytesLeftToWrite += (UINT) snprintf(&usbStickDataBuffer[bytesLeftToWrite], USB_STICK_BLOCK_SIZE,
    "Unique ID: 0x%08X%08X%08X\r\n", sysID.systemID[0], sysID.systemID[1], sysID.systemID[2]);
  //write remaining data anyway
  if (!_dataManager_writeBlockToOpenedFile(true, &bytesLeftToWrite))
  {
    return false;
  }

  //close metaData file
  iFResult = f_close(&file);
  if (iFResult != FR_OK)
  {
    return false;
  }
  return true;
}

/**
 ****************************************************************
 @brief   Internally function to build the logData file and store it on the memory drive.
 @param   pPath Pointer to the path of the file (including the file name/type)
 @return  -
 ****************************************************************
 */
static bool _dataManager_logDataFileStore (TCHAR *pPath)
{
  uint32_t tail;
  uint32_t head;
  FRESULT iFResult;
  UINT bytesLeftToWrite = 0;
  dataManager_logHistorySmall_t logHistorySmall;
  uint32_t nrOfMeasDataPoints;
  dataManager_dataPoint_t dataPoint;
  char evtTxt[32];
  uint32_t nrOfLogDataPoints;

  //create this file all time new
  iFResult = f_open(&file, pPath, FA_CREATE_ALWAYS | FA_WRITE);
  if ((iFResult != FR_OK) && (iFResult != FR_EXIST))
  {
    return false;
  }

  //line: info about csv-file (measurement data) - header
  strcpy(&usbStickDataBuffer[bytesLeftToWrite], "csv:\r\n");
  bytesLeftToWrite += 6;
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  nrOfMeasDataPoints = dataManager_flashNrOfMeasDataPointsGet(true);

  //line: info about .csv-file (measurement data) - time of first measurement dataPoint
  if (nrOfMeasDataPoints)
  {
    dataManager_flashMeasDataPointGet(0, &dataPoint);
  }
  else
  {
    dataPoint.timeStamp.year = 2000;
    dataPoint.timeStamp.month = 1;
    dataPoint.timeStamp.day = 1;
    dataPoint.timeStamp.hrs = 0;
    dataPoint.timeStamp.min = 0;
    dataPoint.timeStamp.sec = 0;
  }
  bytesLeftToWrite += (UINT) snprintf(&usbStickDataBuffer[bytesLeftToWrite], USB_STICK_BLOCK_SIZE,
    " start: %04u.%02u.%02u %02u:%02u:%02u.%03u\r\n", dataPoint.timeStamp.year, dataPoint.timeStamp.month,
    dataPoint.timeStamp.day, dataPoint.timeStamp.hrs, dataPoint.timeStamp.min, dataPoint.timeStamp.sec,
    dataPoint.timeStamp.ms);
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: info about csv-file (measurement data) - time of last measurement dataPoint
  if (nrOfMeasDataPoints)
  {
    dataManager_flashMeasDataPointGet(nrOfMeasDataPoints - 1, &dataPoint);
  }
  else
  {
    //nothing to do, dataPoint.timeStamp already set above
  }
  bytesLeftToWrite += (UINT) snprintf(&usbStickDataBuffer[bytesLeftToWrite], USB_STICK_BLOCK_SIZE,
    " stop: %04u.%02u.%02u %02u:%02u:%02u.%03u\r\n", dataPoint.timeStamp.year, dataPoint.timeStamp.month,
    dataPoint.timeStamp.day, dataPoint.timeStamp.hrs, dataPoint.timeStamp.min, dataPoint.timeStamp.sec,
    dataPoint.timeStamp.ms);
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: info about .csv-file (measurement data) - number of dataPoints
  bytesLeftToWrite += (UINT) snprintf(&usbStickDataBuffer[bytesLeftToWrite], USB_STICK_BLOCK_SIZE, " samples: %u\r\n",
    (unsigned int) nrOfMeasDataPoints);
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: logData read out history information
  strcpy(&usbStickDataBuffer[bytesLeftToWrite], "\r\nlog:\r\n");
  bytesLeftToWrite += 8;
  //write data, but only if they are reaching the block size
  if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
  {
    return false;
  }

  //write logHistory informations as well
  //get the current memory addresses for the FLASH (circular memory)
  tail = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGHIST_TAIL);
  head = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGHIST_HEAD);

  nrOfLogDataPoints = dataManager_flashNrOfLogEventsGet();
  if (nrOfLogDataPoints)
  {
    if (nrOfLogDataPoints > 100)
    {
      //increase tail, that only 100 logDataPoints left (newest 100)
      tail += (sizeof(dataManager_logHistorySmall_t) * (nrOfLogDataPoints - 100));
      if (tail >= (flashStartAddr_logHist + flashSize_logHist))
      {
        tail -= flashSize_logHist; //wrap around
      }
    }

    do
    {
      flash_bytesRead(tail, &logHistorySmall.BYTE[0], sizeof(dataManager_logHistorySmall_t));

      // event text information
      switch (logHistorySmall.evt)
      {
        default:
          strcpy(evtTxt, "unknown");
          break;
        case LOG_EVT_DATAREADOUT:
          strcpy(evtTxt, "readout");
          break;
        case LOG_EVT_SYSRESTART:
          strcpy(evtTxt, "sysRestart");
          break;
        case LOG_EVT_FW_UPDATE:
          strcpy(evtTxt, "fwUpdate");
          break;
        case LOG_EVT_BME_ERROR:
          strcpy(evtTxt, "bmeError");
          break;
        case LOG_EVT_RTCBATTERY:
          strcpy(evtTxt, "rtcBattery");
          break;
        case LOG_EVT_CONFIGSTORE:
          strcpy(evtTxt, "configStore");
          break;
        case LOG_EVT_CLEARDATA:
          strcpy(evtTxt, "clearData");
          break;
        case LOG_EVT_AUTOMEAS:
          strcpy(evtTxt, "autoMeas");
          break;
        case LOG_EVT_CURSORRESTORE:
          strcpy(evtTxt, "cursorRestore");
          break;
      }

      //logHistory information
      bytesLeftToWrite += (UINT) snprintf(&usbStickDataBuffer[bytesLeftToWrite], USB_STICK_BLOCK_SIZE,
        " - %04u.%02u.%02u %02u:%02u:%02u.%03u, %s", logHistorySmall.timeStamp.year + 2000,
        logHistorySmall.timeStamp.month, logHistorySmall.timeStamp.day, logHistorySmall.timeStamp.hours,
        logHistorySmall.timeStamp.minutes, logHistorySmall.timeStamp.seconds, logHistorySmall.timeStamp.milliseconds,
        evtTxt);

      if (strlen(logHistorySmall.addInfo))
      {
        bytesLeftToWrite += (UINT) snprintf(&usbStickDataBuffer[bytesLeftToWrite], USB_STICK_BLOCK_SIZE, " %s\r\n",
          logHistorySmall.addInfo);
      }
      else
      {
        //only add the carriage return and newline
        usbStickDataBuffer[bytesLeftToWrite++] = '\r';
        usbStickDataBuffer[bytesLeftToWrite++] = '\n';
      }

      //write data, but only if they are reaching the block size
      if (!_dataManager_writeBlockToOpenedFile(false, &bytesLeftToWrite))
      {
        return false;
      }

      tail += sizeof(dataManager_logHistorySmall_t);
      if (tail >= (flashStartAddr_logHist + flashSize_logHist))
      {
        tail -= flashSize_logHist; //wrap around
      }
    } while (tail != head);
  }

  //write remaining data anyway
  if (!_dataManager_writeBlockToOpenedFile(true, &bytesLeftToWrite))
  {
    return false;
  }

  //close logData file
  iFResult = f_close(&file);
  if (iFResult != FR_OK)
  {
    return false;
  }
  return true;
}

/**
 ****************************************************************
 @brief   Write a block (512 bytes) of data to the current opened file on the memory-drive.
 @param   forceWrite  If true, the data needs to be written, even they are shorter than the block size
 @param   pBytesLeftToWrite  Pointer to the number of requested bytes to be written and the resulting left bytes to be written
 @return  True on success, otherwise false
 ****************************************************************
 */
static bool _dataManager_writeBlockToOpenedFile (bool forceWrite, UINT *pBytesLeftToWrite)
{
  FRESULT iFResult;
  UINT nrOfBytesToWrite;
  UINT nrOfBytesWritten;

  nrOfBytesToWrite = *pBytesLeftToWrite;
  if (nrOfBytesToWrite >= USB_STICK_BLOCK_SIZE)
  {
    //do the write of a complete block
    iFResult = f_write(&file, &usbStickDataBuffer[0], USB_STICK_BLOCK_SIZE, &nrOfBytesWritten);

    if ((iFResult != FR_OK) || (nrOfBytesWritten != USB_STICK_BLOCK_SIZE))
    {
      return false;
    }
    nrOfBytesToWrite -= nrOfBytesWritten;

    //copy others to be written data to the beginning of the SD-DataBuffer
    memcpy(&usbStickDataBuffer[0], &usbStickDataBuffer[USB_STICK_BLOCK_SIZE], nrOfBytesToWrite);
  }

  if (forceWrite && nrOfBytesToWrite)
  {
    //check if remaining data needs to be written anyway
    iFResult = f_write(&file, &usbStickDataBuffer[0], nrOfBytesToWrite, &nrOfBytesWritten);
    if ((iFResult != FR_OK) || (nrOfBytesWritten != nrOfBytesToWrite))
    {
      return false;
    }
    nrOfBytesToWrite -= nrOfBytesWritten;
  }
  *pBytesLeftToWrite = nrOfBytesToWrite;

  return true;
}

/**
 ****************************************************************
 @brief   Write default configuration to the RAM configuration struct
 @param   -
 @return  -
 ****************************************************************
 */
static void _dataManager_writeDefaultCfgToRamConfig (void)
{
  sysID_t sysID;

  //copy default system configuration to working system configuration in RAM
  memcpy(_sysCfg.BYTE, _sysCfgDefault.BYTE, sizeof(dataManager_sysConfigSmall_t));

  //change default device name to Unique ID
  sys_uniqueIdGet(&sysID);
  snprintf(_sysCfg.devName, DEV_NAME_MAX_LEN, "0x%08X%08X%08X", sysID.systemID[0], sysID.systemID[1],
    sysID.systemID[2]);
}

/**
 ****************************************************************
 @brief   Read external FLASH part by part to restore the logData cursors for the
 *        stored data.
 @param   -
 @return  -
 ****************************************************************
 */
static void _dataManager_restoreLogDataCursors (void)
{
  uint32_t tail;
  uint32_t head;
  uint32_t address;
  uint8_t rxByte;
  uint32_t logPoint;
  bool emptyRegionFound;

  //restore log data cursors
  //find logData tail
  emptyRegionFound = false;
  address = flashStartAddr_logData;
  tail = address;
  head = address;
  for (logPoint = 0; logPoint < flashMaxNrOfLogDataPoints; logPoint++)
  {
    //read the first single byte of each data point --> if not 0xFF, a dataPoint is stored there in FLASH
    flash_bytesRead(address, &rxByte, 1);

    if (rxByte == 0xFF)
    {
      //no data point stored on this location
      emptyRegionFound = true;
    }
    else
    {
      if (emptyRegionFound)
      {
        //this is the tail
        tail = address;

        //abort loop
        logPoint = flashMaxNrOfLogDataPoints;
      }
    }
    address += sizeof(dataManager_dataPointSmall_t);
    if (address >= flashSize_logData)
    {
      address -= flashSize_logData; //wrap around
    }
  }
  //find logData head (start from tail up until empty block found)
  address = tail;
  for (logPoint = 0; logPoint < flashMaxNrOfLogDataPoints; logPoint++)
  {
    //read the first single byte of each data point --> if not 0xFF, a dataPoint is stored there in FLASH
    flash_bytesRead(address, &rxByte, 1);

    if (rxByte == 0xFF)
    {
      //no data point stored on this location --> head found
      head = address;

      //abort loop
      logPoint = flashMaxNrOfLogDataPoints;
    }
    address += sizeof(dataManager_dataPointSmall_t);
    if (address >= flashSize_logData)
    {
      address -= flashSize_logData; //wrap around
    }
  }
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL, tail);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT, tail);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_HEAD, head);
}

/**
 ****************************************************************
 @brief   Read external FLASH part by part to restore the logHistory cursors for the
 *        stored data.
 @param   -
 @return  -
 ****************************************************************
 */
static void _dataManager_restoreLogHistCursors (void)
{
  uint32_t tail;
  uint32_t head;
  uint32_t address;
  uint8_t rxByte;
  uint32_t logHistPoint;
  bool emptyRegionFound;

  //restore log history cursors
  //find logHist tail
  emptyRegionFound = false;
  address = flashStartAddr_logHist;
  tail = address;
  head = address;
  for (logHistPoint = 0; logHistPoint < flashMaxNrOfLogHistPoints; logHistPoint++)
  {
    //read the first single byte of each data point --> if not 0xFF, a dataPoint is stored there in FLASH
    flash_bytesRead(address, &rxByte, 1);

    if (rxByte == 0xFF)
    {
      //no data point stored on this location
      emptyRegionFound = true;
    }
    else
    {
      if (emptyRegionFound)
      {
        //this is the tail
        tail = address;

        //abort loop
        logHistPoint = flashMaxNrOfLogHistPoints;
      }
    }
    address += sizeof(dataManager_logHistorySmall_t);
    if (address >= (flashStartAddr_logHist + flashSize_logHist))
    {
      address -= flashSize_logHist; //wrap around
    }
  }
  //find logData head (start from tail up until empty block found)
  address = tail;
  for (logHistPoint = 0; logHistPoint < flashMaxNrOfLogHistPoints; logHistPoint++)
  {
    //read the first single byte of each data point --> if not 0xFF, a dataPoint is stored there in FLASH
    flash_bytesRead(address, &rxByte, 1);

    if (rxByte == 0xFF)
    {
      //no data point stored on this location --> head found
      head = address;

      //abort loop
      logHistPoint = flashMaxNrOfLogHistPoints;
    }
    address += sizeof(dataManager_logHistorySmall_t);
    if (address >= (flashStartAddr_logHist + flashSize_logHist))
    {
      address -= flashSize_logHist; //wrap around
    }
  }
  //log history information cursors
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGHIST_TAIL, tail);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGHIST_HEAD, head);
}

/**
 * @}
 */

/**
 * @}
 */
