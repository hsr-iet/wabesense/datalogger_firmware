===============
System Memories
===============

.. contents:: :local:

Microcontroller-FLASH
=====================
The internal FLASH of the `STM32L496RET6`_ microcontroller is 512kByte (STM32L496RGT6: 1024kByte). This memory is divided into two sections:

.. list-table:: FLASH Memory Map
	:widths: 10 10 10 10
	:header-rows: 1

	*	- Region-Name
		- Start address
		- End address
		- Size
	*	- Bootloader
		- 0x08000000
		- 0x08020000
		- 128kByte
	*	- Application
		- 0x08020000
		- 0x08080000 	(STM32L496RGT6: 0x08100000)
		- 384kByte		(STM32L496RGT6: 896kByte)

.. note::

	| The location of the Application-Start address is fix coded in the Bootloader-Firmware!
	| See in Application Firmware - Code_STM32L496RG\\Core\\Src\\system_stm32l4xx.c:
	|
	|   #define APP_START_ADDRESS   0x08020000
	
.. note::

	| The location of the Application-Start address needs also to be known for the application itself (fix coded). There the Address-Offset for the Interrupt 
	 Vector table must be set according to the Application-Start address.
	| See in Bootloader Firmware - CodeBootloader_STM32L496RG\\Core\\Src\\main.c:
	|
	|   #define VECT_TAB_OFFSET  0x00020000

Microcontroller-RAM
===================
The internal RAM of the `STM32L496RET6`_ microcontroller is 320kByte (same for STM32L496RGT6: 320kByte). This memory is divided into two sections:

.. list-table:: RAM Memory Map
	:widths: 10 10 10 10
	:header-rows: 1

	*	- Region-Name
		- Start address
		- End address
		- Size
	*	- RAM
		- 0x20000000
		- 0x2004FC00
		- 319kByte
	*	- RAM_Bootloader
		- 0x2004FC00
		- 0x20050000
		- 1kByte

The RAM_Bootloader section is used, to share informations between the application and the bootloader.

**Shared bootloader information struct**

.. code-block:: yaml

	//! type struct definition for data to share with the bootloader
	typedef struct
	{
	  uint64_t tag;    // see Bootloader tags below
	  uint32_t len;
	  char pathAndFileName[256];
	} dataForBootloader_t;


**Bootloader tags**

.. code-block:: yaml
	
	#define BOOTLOADING_TAG_NONE      ((uint64_t) 0x0011223344556677)
	#define BOOTLOADING_TAG_REQUEST   ((uint64_t) 0x1234567890123456)
	#define BOOTLOADING_TAG_SUCCESS   ((uint64_t) 0x9876543210987654)
	#define BOOTLOADING_TAG_ERROR     ((uint64_t) 0x1122334455667788)

Microcontroller-RTC Backup Registers
====================================
There are 32 backup registers on the RTC module. These and the RTC peripheral are connected to the CR2032 Backup-Battery.

* Reg0: SYS_BACKUP_REG_FLASH_LOGDATA_TAIL

	Holds the tail memory address of valid log-data in the `Datalogger-PCB FLASH`_
	
* Reg1: SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT
	
	Holds the tail memory address of already readout log-data in the `Datalogger-PCB FLASH`_
	
* Reg2: SYS_BACKUP_REG_FLASH_LOGDATA_HEAD
	
	Holds the head memory address of valid log-data in the `Datalogger-PCB FLASH`_

* Reg3: SYS_BACKUP_REG_FLASH_LOGHIST_TAIL

	Holds the tail memory address of valid log-history in the `Datalogger-PCB FLASH`_

* Reg4: SYS_BACKUP_REG_FLASH_LOGHIST_HEAD

	Holds the head memory address of valid log-history in the `Datalogger-PCB FLASH`_

* Reg5: SYS_BACKUP_REG_RESERVED

	unused (reserved for future)

* Reg6: SYS_BACKUP_REG_RTC_BAT_CHECK_MONTH
	
	Hold the month information about the last RTC Battery check done. A ADC based measurement will
	be executed only once a month. This low interval is selected, to reduce the current consumption
	needed by each ADC measurement.

Datalogger-PCB FLASH
====================
A FLASH-IC (`AT25SF321B`_) is connected to the uController over a SPI Interface. This FLASH-IC is 
used to store different type of data.
The RTC backup registers are used for the cursor handling of the logData. If the system lost the complete power, on the 
next power-up of the datalogger, first all these cursors are getting to be restored by checking the fill-state of the FLASH-IC. 
During this time, the red LED blinks with 10Hz. Wait until this step is finished, bevore you try to do some handlings on the datalogger 
unit.

See `logData`_ informations, which are part of the this firmware reference manual.

.. list-table:: `AT25SF321B`_ Memory Map
	:widths: 20 20 20 20 50
	:header-rows: 1
	
	*	- Content
		- Start block number
		- End block number
		- Size in kByte
		- Number of storable specific data points
	*	- Log-Data
		- 0
		- 1018
		- 4'076kByte
		- 130'432 (see `dataManager_dataPointSmall_t`_)
	*	- System-Configuration
		- 1019
		- 1019
		- 4kByte
		- 363Bytes used (see `dataManager_sysConfigSmall_t`_)
	*	- Reserved
		- 1020
		- 1021
		- 8kByte
		- 
	*	- Log-History data
		- 1022
		- 1023
		- 8kByte
		- 128 (see `dataManager_logHistorySmall_t`_)

dataManager_dataPointSmall_t
----------------------------

.. code-block:: yaml

	//! all data of a single data point packed together in a small memory footprint structure
	typedef union
	{
	  struct __attribute__((packed))  //total 25 bytes used, 32 bytes blocks used
	  {
		dataManager_timeStampSmall_t timeStamp; //!< the bitwise coded timeStamp (6Byte)
		bool timeBasedMeas;                     //!< Flag to indicate, if this is a timeBaseMeasurement or not (1Byte)
		uint16_t vBat;                          //!< battery voltage [V] (2Byte), format 8.8
		uint16_t adc_in8;                       //!< voltage on ADC-input8 [V] (2Byte), format 3.13
		uint16_t adc_in9;                       //!< voltage on ADC-input9 [V] (2Byte), format 3.13
		uint16_t adc_in10;                      //!< voltage on ADC-input10 [V] (2Byte), format 3.13
		uint16_t adc_in11;                      //!< voltage on ADC-input11 [V] (2Byte), format 3.13
		int16_t bme280_temperature;             //!< temperature of BME280 sensor in °C, format sign8.8 (2Byte)
		float bme280_pressure;                  //!< pressure of BME280 in Pa, format float (4Byte)
		uint16_t bme280_humidity;               //!< humidity of BME280 in %, format 7.9 (2Byte)
	  };
	  uint8_t BYTE[32];
	} dataManager_dataPointSmall_t;


dataManager_sysConfigSmall_t
----------------------------

.. code-block:: yaml

	//! all data of a the system configuration packed together in a small memory footprint structure
	typedef union
	{
	  struct __attribute__((packed))  //total 363 bytes used, 4096 bytes blocks reserved for system configuration
	  {
		const uint32_t tag;                         //!< a simple 32bit Tag used for a simple validation check    4 Bytes
		char devName[DEV_NAME_MAX_LEN + 1];         //!< the device name                                         93 Bytes
		uint16_t measInterval_minutes;              //!< the measurement interval [minutes]                       2 Bytes
		uint16_t bat_nominalCellMilivoltage;        //!< the nominal battery cell voltage [mV]                    2 Bytes
		uint8_t bat_nrOfCells;                      //!< the number of installed battery cells                    1 Byte
		char analogChannel0Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 0 name                         65 Bytes
		char analogChannel1Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 1 name                         65 Bytes
		char analogChannel2Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 2 name                         65 Bytes
		char analogChannel3Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 3 name                         65 Bytes
		bool autoMeasActive;                        //!< the autoMeasurement active flag                          1 Byte
	  };
	  uint8_t BYTE[363];
	} dataManager_sysConfigSmall_t;

dataManager_logHistorySmall_t
-----------------------------

.. code-block:: yaml

	//! all data of a single log history packed together in a small memory footprint structure
	typedef union
	{
	  struct __attribute__((packed))  //64 bytes blocks used
	  {
		dataManager_timeStampSmall_t timeStamp; //!< the bitwise coded timeStamp (6 Bytes)
		uint8_t evt;                            //!< the to be logged event, based on dataManager_logEvt_t
		char addInfo[57];                       //!< additional text with a maximum length of 57 characters could be added
	  };
	  uint8_t BYTE[64];
	} dataManager_logHistorySmall_t;

dataManager_timeStampSmall_t
----------------------------

.. code-block:: yaml

	//! reduced timeStamp information (bitCoded to reduce size) - 6 Bytes
	typedef struct __attribute__((packed))
	{
	  uint32_t year :6;       //!< the reduced year information (0..63)
	  uint32_t month :4;      //!< the month information (1..12)
	  uint32_t day :5;        //!< the day information (1..31)
	  uint32_t hours :5;      //!< the hours information (0..23)
	  uint32_t minutes :6;    //!< the minutes information (0..59)
	  uint32_t seconds :6;    //!< the second information (0..59)
	  uint16_t milliseconds;  //!< the millisecond information (0..999)
	} dataManager_timeStampSmall_t;

Links
=====
.. _STM32L496RET6: https://www.st.com/en/microcontrollers-microprocessors/stm32l496re.html
.. _AT25SF321B: https://www.dialog-semiconductor.com/sites/default/files/2021-04/DS-AT25SF321B-179E-062020.pdf
.. _logData: https://hsr-iet.gitlab.io/wabesense/datalogger_firmware/logData.html

.. target-notes::