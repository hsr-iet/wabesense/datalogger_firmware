/**
 ****************************************************************
 @file    HAL_DWT.h
 ****************************************************************
 @brief   This module offers a set of functions to interact with
 *        the data watchpoint unit, used for low level debugging.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2021-02-12
 ****************************************************************
 */
#ifndef __HAL_DWT_H
#define __HAL_DWT_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdint.h>

//===============================================================
//defines

//===============================================================
//typedefs

//===============================================================
//function prototypes
void hal_dwt_init (void);
void hal_dwt_cpuClockCntReset (void);
uint32_t hal_dwt_cpuClockCntGet (void);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* #ifndef __HAL_DWT_H */
