/**
 ****************************************************************
 @file    test_stringConverter.c
 ****************************************************************
 @brief   This module implements some tests for the stringConverter FWO-module.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2021-02-12
 ****************************************************************
 */

/** @addtogroup TESTPROGRAMMS
 * @{
 */

// --- Includes
#include "test_stringConverter.h"

#include <assert.h>
#include <stdint.h>
#include "../HWO/SYS.h"
#include "../FWO/HMI.h"
#include "../FWO/stringConverter.h"

// --- Defines

// --- Typedefs

// --- Variables

// --- Local Function Prototypes

/**
 ****************************************************************
 @brief	  Do all possible tests with the stringConverter module.
 @param   -
 @return  -
 ****************************************************************
 */
void test_stringConverter (void)
{
  volatile char txt[32];
  volatile uint32_t strLen;

#ifdef SEMI_HOSTING
  printf("StringConverter_Test: start\n\r");
#endif

  //float representation is: 0.00439999997615814208984375
  //resulting string should be 0.004
  strConv_floatToString_3(0.0044f, (char*) txt, (uint32_t*) &strLen);
  strConv_floatToString_6(0.0044f, (char*) txt, (uint32_t*) &strLen);

  //float representation is: -0.00439999997615814208984375
  //resulting string should be -0.004
  strConv_floatToString_3(-0.0044f, (char*) txt, (uint32_t*) &strLen);
  strConv_floatToString_6(-0.0044f, (char*) txt, (uint32_t*) &strLen);
  //float representation is: -0.00449999980628490447998046875
  //resulting string should be -0.005
  strConv_floatToString_3(-0.0045f, (char*) txt, (uint32_t*) &strLen);
  strConv_floatToString_6(-0.0045f, (char*) txt, (uint32_t*) &strLen);

  //float representation is: -123456.890625
  //resulting string should be -123456.890
  strConv_floatToString_3(-123456.8893f, (char*) txt, (uint32_t*) &strLen);
  strConv_floatToString_6(-123456.8893f, (char*) txt, (uint32_t*) &strLen);
  //float representation is: -123456.890625
  //resulting string should be -123456.890
  strConv_floatToString_3(-123456.8896f, (char*) txt, (uint32_t*) &strLen);
  strConv_floatToString_6(-123456.8896f, (char*) txt, (uint32_t*) &strLen);
  //float representation is: 123456.890625
  //resulting string should be 123456.890
  strConv_floatToString_3(123456.8896f, (char*) txt, (uint32_t*) &strLen);
  strConv_floatToString_6(123456.8896f, (char*) txt, (uint32_t*) &strLen);

  //mark test as okay
  hmi_init();
  hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGBLED_COLOR_GREEN);
  sys_sleepForUs(1e6);  //1s delay
  hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGBLED_COLOR_NONE);
}

/**
 * @}
 */
