/**
 ****************************************************************
 @file    HMI.h
 ****************************************************************
 @brief   This module offers a set of functions to handle the
 *        Human Machine visible interface.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.1
 @date    2021-09-20
 ****************************************************************
 */
#ifndef __HMI_H
#define __HMI_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include "../HWO/RG_LED.h"

//===============================================================
//defines

//===============================================================
//typedefs
typedef enum
{
  HMI_LED_PATTERN_OFF = 0,    //!< red-green LED is OFF
  HMI_LED_PATTERN_ON,         //!< red-green LED is static on
  HMI_LED_PATTERN_BLINK_10HZ  //!< red-green toggle ON/OFF with 10Hz
} ledPattern_t;

//===============================================================
//function prototypes
void hmi_init (void);

void hmi_ledPatternSet (ledPattern_t pattern, rgColor_t color);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* #ifndef __HMI_H */
