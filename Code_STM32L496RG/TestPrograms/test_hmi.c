/**
 ****************************************************************
 @file    test_hmi.c
 ****************************************************************
 @brief   This module implements some tests for the human machine interface.
 *        With the user button1: select next RG color
 *        With the user button2: select previous RG color
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 1.0
 @date    2021-09-24
 ****************************************************************
 */

/** @addtogroup TESTPROGRAMMS
 * @{
 */

// --- Includes
#include "test_hmi.h"

#include <assert.h>
#include <stdio.h>
#include "../HWO/DIG_IO.h"
#include "../HWO/RG_LED.h"
#include "../HWO/SYS.h"

// --- Defines

// --- Typedefs

// --- Variables

// --- Local Function Prototypes

/**
 ****************************************************************
 @brief	  Do all possible tests for the human machine interface.
 @param   -
 @return  -
 ****************************************************************
 */
void test_hmi (void)
{
  rgColor_t testColor = RGLED_COLOR_RED;

#ifdef SEMI_HOSTING
  printf("HMI_Test: start\n\r");
#endif

  digIO_init();
  rgLED_init();
  rgLED_luminositySet(10);
  rgLED_colorSet(testColor);

  while (1)
  {
    //select next base color on a user button1 press
    if (digIO_usrBt1Pressed())
    {
      testColor++;
      if (testColor > RGLED_COLOR_YELLOW)
      {
        testColor = RGLED_COLOR_NONE;
      }
      rgLED_colorSet(testColor);

      do
      {
        sys_sleepForUs(20000);  //20ms delay
      } while (digIO_usrBt1Pressed());
    }

    //exit test on a user button2 press
    if (digIO_usrBt2Pressed())
    {
      if (testColor == RGLED_COLOR_NONE)
      {
        testColor = RGLED_COLOR_YELLOW;
      }
      else
      {
        testColor--;
      }
      rgLED_colorSet(testColor);

      do
      {
        sys_sleepForUs(20000);  //20ms delay
      } while (digIO_usrBt2Pressed());
    }
  }

  //mark test as okay
  rgLED_luminositySet(10);
  rgLED_colorSet(RGLED_COLOR_GREEN);
  sys_sleepForUs(1e6);  //1s delay
  rgLED_luminositySet(0);
  rgLED_colorSet(RGLED_COLOR_NONE);

#ifdef SEMI_HOSTING
  printf("HMI_Test: ends\n\n\r");
#endif
}

/**
 * @}
 */
