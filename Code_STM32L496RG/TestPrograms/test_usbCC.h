/**
 ****************************************************************
 @file    test_usbCC.h
 ****************************************************************
 @brief   This module implements some tests for the analog input channels
 *        of the USB interface.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 1.0
 @date    2021-10-18
 ****************************************************************
 */
#ifndef TEST_USBCC_H_
#define TEST_USBCC_H_

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

// --- Includes

// --- Defines

// --- Typedefs

// --- Function Prototypes
void test_usbCC (void);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* TEST_USBCC_H_ */
