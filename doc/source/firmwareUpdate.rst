===============
Firmware update
===============

.. contents:: :local:

Bootloader-Application
======================

A specific `Bootloader-Application`_ is needed, to be able to update the firmware of a datalogger with a simple USB-Stick. Therefore, a 
Application, called Bootloader is implemented. The structure of it is the same as the firmware application itself. If a bootloading 
is requested by the application (informations are passed to the bootloader by a specific part located in RAM), then the Bootloader-Application 
execute the `Defaul Process (via USB-Stick)`_ described below. The datalogger will interact as USB-Host and check, if a correctly named firmware file is located 
on the USB-Stick.

These are the main steps inside the `Bootloader-Application`_:

.. mermaid::

	graph TD
		A[Check if a firmware update from user Application is requested] ==> B(SystemInit - USB-Host)
		B ==> C(Try to mount the USB-Stick and check if a correct firmware file is located on it)
		C ==>|file found| D(file found)
		C -->|error| Y(Show up Error: red LED on)
		D ==> E(read at max. 512Bytes from \*.bin-File)
		E ==> F(write/program the previous read data to the FLASH)
		F ==> G{nrOfBytesLeft zero?}
		G ==>|no| E
		G ==>|yes| H(close file<br>unmount USB-Stick)
		H ==> I(exit: <br>LED off, <br>jump to updated userApplication)
		Y -->|1 second timeout| Z(exit: <br>LED off, <br>jump to untouched current userApplication)

Defaul Process (via USB-Stick)
==============================

The Firmware update process/setup is part of the WABEsense User Manual (chapter Operation/Logger/Maintenance)

Link to corresponding Chapter in User-Manual:
	- https://hsr-iet.gitlab.io/wabesense/wabesense/operation.html#firmware-updates

Flashing via ST-LINK/v2
=======================

The \*.bin-Files could be directly used to programm/flashing the target microcontroller without the need of first compile the source-code. 
The possible to be used \*.bin-Files are located in the firmware repository as Releases in the Deployment location.

Parts needed:
	- `STM32CubeProgrammer`_ Application
	- `ST-LINK/v2`_ JTAG/Programmer
	- WABEsense datalogger hardware
	- Power-Source (via USB-C or DC-Plug)

Setup:
	- Connect `ST-LINK/v2`_ over USB with PC/Laptop and on the other side to the WABEsense datalogger hardware
	- Connect Power-Source to WABEsense datalogger hardware
	
	.. image:: ../pictures/firmwareFlashing/firmwareFlashing_hwSetup.jpg
	  :width: 500
	  :alt: Datalogger firmware flashing via ST-LINK/v2: hardware setup
	
	- Open `STM32CubeProgrammer`_ Application

Erasing complete microcontroller
--------------------------------

	1. Click "Erasing & Programming" Button on the left side of the `STM32CubeProgrammer`_ application
		
		.. image:: ../pictures/firmwareFlashing/STM32CubeProgrammer_Startup_annotated.png
		  :width: 500
		  :alt: Datalogger firmware flashing via ST-LINK/v2: "Erasing & Programming" Tab
	
	2. | Be sure, `ST-LINK/v2`_ is selected as programming interface
	   | Click "Connect"-Button on the top-right side, to establish the communication to the `ST-LINK/v2`_
	   
		.. image:: ../pictures/firmwareFlashing/STM32CubeProgrammer_ErasingAndProgramming_Connect_annotated.png
		  :width: 500
		  :alt: Datalogger firmware flashing via ST-LINK/v2: "Erasing & Programming" Tab - Connect over ST-LINK/v2
		  
	3. Click "Full chip erase"-Button
	
		.. image:: ../pictures/firmwareFlashing/STM32CubeProgrammer_ErasingAndProgramming_FullChipErase_annotated.png
		  :width: 500
		  :alt: Datalogger firmware flashing via ST-LINK/v2: "Erasing & Programming" Tab - Full chip erase
		  
	4. Accept full chip erase with a click on the "OK"-Button
		
		.. image:: ../pictures/firmwareFlashing/STM32CubeProgrammer_FullChipErase.jpg
		  :width: 500
		  :alt: Datalogger firmware flashing via ST-LINK/v2: "Erasing & Programming" Tab - full chip erase

Programming/Flashing Bootloader-Application
-------------------------------------------

	1. Click "Browse" to select Bootloader-Application binary-file
	2. Set Start address: 0x08000000	(default)
	3. Select option-Flag: "Skip flash erase before programming"
	4. Click "Start Programming"
	
	.. image:: ../pictures/firmwareFlashing/STM32CubeProgrammer_BootloaderProgramming_annotated.png
		  :width: 750
		  :alt: Datalogger firmware flashing via ST-LINK/v2: "Erasing & Programming" Tab - programming bootloader application

Programming/Flashing Firmware-Application
-----------------------------------------

	1. Click "Browse" to select Firmware-Application binary-file
	2. Set Start address: 0x08020000
	3. Select option-Flag: "Skip flash erase before programming"
	4. Click "Start Programming"
	
	.. image:: ../pictures/firmwareFlashing/STM32CubeProgrammer_ApplicationProgramming_annotated.png
		  :width: 750
		  :alt: Datalogger firmware flashing via ST-LINK/v2: "Erasing & Programming" Tab - programming firmware application
	
Links
=====
.. _STM32CubeProgrammer: https://www.st.com/en/development-tools/stm32cubeprog.html
.. _ST-LINK/v2: https://www.st.com/en/development-tools/st-link-v2.html

.. target-notes::