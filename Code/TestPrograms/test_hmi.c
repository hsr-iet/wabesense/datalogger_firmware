/**
 ****************************************************************
 @file    test_hmi.c
 ****************************************************************
 @brief   This module implements some tests for the human machine interface.
 *        With the user button1: select next RGB color
 *        With the user button2: exit test routine
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-16
 ****************************************************************
 */

/** @addtogroup TESTPROGRAMMS
 * @{
 */

// --- Includes
#include "test_hmi.h"

#include <assert.h>
#include <stdio.h>
#include "../HWO/DIG_IO.h"
#include "../HWO/RGB_LED.h"
#include "../HWO/SYS.h"

// --- Defines

// --- Typedefs

// --- Variables

// --- Local Function Prototypes

/**
 ****************************************************************
 @brief	  Do all possible tests for the human machine interface.
 @param   -
 @return  -
 ****************************************************************
 */
void test_hmi (void)
{
  rgbColor_t testColor = RGBLED_COLOR_RED;

#ifdef SEMI_HOSTING
  printf("HMI_Test: start\n\r");
#endif

  digIO_init();
  rgbLED_init();
  rgbLED_luminositySet(10);

  while (1)
  {
    //select next base color on a user button1 press
    if (digIO_usrBt1Pressed())
    {
      rgbLED_colorSet(testColor);
      testColor++;
      if (testColor > RGBLED_COLOR_BLUEGREEN)
      {
        testColor = RGBLED_COLOR_NONE;
      }
      do
      {
        sys_sleepForUs(50000);  //50ms delay
      } while (digIO_usrBt1Pressed());
    }

    //exit test on a user button2 press
    if (digIO_usrBt2Pressed())
    {
      break;
    }
  }

  //mark test as okay
  rgbLED_luminositySet(10);
  rgbLED_colorSet(RGBLED_COLOR_GREEN);
  sys_sleepForUs(1e6);  //1s delay
  rgbLED_luminositySet(0);
  rgbLED_colorSet(RGBLED_COLOR_NONE);

#ifdef SEMI_HOSTING
  printf("HMI_Test: ends\n\n\r");
#endif
}

/**
 * @}
 */
