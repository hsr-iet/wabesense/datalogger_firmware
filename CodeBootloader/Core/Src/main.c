/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "fatfs.h"
#include "rtc.h"
#include "spi.h"
#include "tim.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "fatfs.h"
#include "../../HWO/SYS.h"
#include "../../HWO/DIG_IO.h"
#include "../../FWO/HMI.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef enum
{
  BOOTLOADER_STATE_INIT = 0,
  BOOTLOADER_STATE_ACTIVE,
  BOOTLOADER_STATE_ERROR,
  BOOTLOADER_STATE_SUCCESSFUL,
  BOOTLOADER_STATE_EXIT
} bootloaderState_t;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define APP_START_ADDRESS   0x08008000
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
dataForBootloader_t bootloaderData __attribute__ ((section ("dataToBootloaderFromUserApplication"))) =
{
  0
};
static FATFS fs;
static FIL file;
//! data buffer for the SDCard - note: This must be a multiple of 64bits in size!
static uint8_t sdDataBuffer[512];
static bootloaderState_t bootloaderState = BOOTLOADER_STATE_INIT;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config (void);
/* USER CODE BEGIN PFP */
static void _sdCardUnmount (void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main (void)
{
  /* USER CODE BEGIN 1 */
  FRESULT iFResult;
  UINT nrOfBytesRead;
  uint32_t memAddr;
  uint32_t appResetVector;

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  /* USER CODE BEGIN 2 */
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  //check if flag for bootloading is set
  if ((bootloaderData.tag == BOOTLOADING_TAG_REQUEST) && (bootloaderData.pathAndFileName[bootloaderData.len] == '\0'))
  {
    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_SPI1_Init();
    MX_TIM6_Init();

    if (!__HAL_RTC_GET_FLAG(&hrtc, RTC_FLAG_INITS))
    {
      MX_RTC_Init();
    }

    sys_init();
    digIO_init();
    hmi_init();

    //check if a SC-Card is inserted in the slot and dataPoints available
    if (digIO_sdCardDetected())
    {
      hmi_ledPatternSet(HMI_LED_PATTERN_BLINK_10HZ, RGBLED_COLOR_BLUEGREEN);

      MX_FATFS_Init();

      //switch the power on
      digIO_3V3PwrOn(DIG_IO_3V3UNIT_SDCARD);
      //a minimum of 100ms is needed before chip could be selected
      sys_sleepForUs(100e3);

      //Mount the file system immediately, using logical disk 0
      iFResult = f_mount(&fs, "", 1);
      if (iFResult != FR_OK)
      {
        _sdCardUnmount();

        bootloaderState = BOOTLOADER_STATE_ERROR;
      }
      else
      {
        //read out file name from the RAM_BL_DATA section which is located in RAM
        //open the File
        iFResult = f_open(&file, bootloaderData.pathAndFileName, FA_OPEN_ALWAYS | FA_READ);
        if (iFResult != FR_OK)
        {
          _sdCardUnmount();

          bootloaderState = BOOTLOADER_STATE_ERROR;
        }
        else
        {
          bootloaderState = BOOTLOADER_STATE_ACTIVE;
        }
      }
    }
    else
    {
      bootloaderState = BOOTLOADER_STATE_ERROR;
    }
  }
  else
  {
    //no bootloading is requested
    bootloaderState = BOOTLOADER_STATE_EXIT;
  }

  while (1)
  {
    if (bootloaderState == BOOTLOADER_STATE_ACTIVE)
    {
      //try to clear flash pages where the user application is stored
      if (!sys_flashUserAppErase())
      {
        bootloaderState = BOOTLOADER_STATE_ERROR;
      }
      else
      {
        memAddr = APP_START_ADDRESS;
        do
        {
          //read out data from firmware file
          iFResult = f_read(&file, sdDataBuffer, sizeof(sdDataBuffer), &nrOfBytesRead);
          if (iFResult != FR_OK)
          {
            f_close(&file);
            _sdCardUnmount();

            //flash is corrupted now!

            bootloaderState = BOOTLOADER_STATE_ERROR;
          }
          else
          {
            //update the user application flash sections with the readout data
            if (sys_flashUserAppWrite(memAddr, sdDataBuffer, nrOfBytesRead))
            {
              memAddr += nrOfBytesRead;
              if (nrOfBytesRead < sizeof(sdDataBuffer))
              {
                f_close(&file);

                //delete the firmware file on SD-Card
                f_unlink(bootloaderData.pathAndFileName);

                _sdCardUnmount();

                //last bytes read from file, bootloading finished
                bootloaderState = BOOTLOADER_STATE_SUCCESSFUL;
              }
            }
            else
            {
              f_close(&file);
              _sdCardUnmount();

              bootloaderState = BOOTLOADER_STATE_ERROR;
            }
          }
        } while (bootloaderState == BOOTLOADER_STATE_ACTIVE);
      }
    }

    switch (bootloaderState)
    {
      default:
      case BOOTLOADER_STATE_ERROR:
        hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGBLED_COLOR_RED);
        sys_sleepForUs(1e6);  //1s delay
        hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGBLED_COLOR_NONE);
        break;
      case BOOTLOADER_STATE_SUCCESSFUL:
        bootloaderData.tag = BOOTLOADING_TAG_SUCCESS;
        break;
      case BOOTLOADER_STATE_EXIT:
        break;
    }

    //check if a valid firmware is installed, otherwise go to standby mode
    appResetVector = *(uint32_t*) (APP_START_ADDRESS + 4);
    if ((appResetVector == 0xFFFFFFFF) || (bootloaderState == BOOTLOADER_STATE_ERROR))
    {
      //no firmware installed, go to standby
      sys_standbyEnter();
    }
    else
    {
      //jump to user application code section
      sys_userAppStart(APP_START_ADDRESS);
    }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config (void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct =
  {
    0
  };
  RCC_ClkInitTypeDef RCC_ClkInitStruct =
  {
    0
  };
  RCC_PeriphCLKInitTypeDef PeriphClkInit =
  {
    0
  };

  /** Configure LSE Drive Capability 
   */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Configure the main internal regulator output voltage 
   */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
   */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI | RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
   */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
/**
 ****************************************************************
 @brief   Internally function to unmount the sdCard memory, deInit the
 *        FatFileSystem and request to turn off the 3v3 to the SD-Card.
 @param   -
 @return  -
 ****************************************************************
 */
static void _sdCardUnmount (void)
{
  //unmount drive
  f_mount(0, "", 0);

  FATFS_DeInit();

  //request to switch the power off
  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_SDCARD);
}
/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler (void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
