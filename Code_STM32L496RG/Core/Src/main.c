/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "fatfs.h"
#include "rtc.h"
#include "spi.h"
#include "tim.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <assert.h>
#include "../../HWO/SYS.h"
#include "../../HWO/DIG_IO.h"
#include "../../HWO/RG_LED.h"
#include "../../FWO/HMI.h"
#include "../../FWO/dataManager.h"
#include "../../FWO/dataLogger.h"
#include "../../TestPrograms/test_aIN.h"
#include "../../TestPrograms/test_usbCC.h"
#include "../../TestPrograms/test_bme280.h"
#include "../../TestPrograms/test_dataManager.h"
#include "../../TestPrograms/test_flash.h"
#include "../../TestPrograms/test_hmi.h"
#include "../../TestPrograms/test_sdCard.h"
#include "../../TestPrograms/test_stringConverter.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  rtcDateTime_t rtcDateTime;
  sysWakeUp_t wakeUpReason;
  char fwFileAndPath[128];
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN SysInit */
  sys_init();
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  /* USER CODE BEGIN 2 */
  dataLogger_init();

  //here, some test routines could be activated
//    while(1)
//    {
//      test_hmi();
//      test_aIN();
//      test_bme280();
//      test_flash();
//      test_dataManager();
//      test_usbCC();
//      while(1);
//    }

  wakeUpReason = sys_wakupReasonGet();
  switch (wakeUpReason)
  {
    default:
      //mark this event in the log data
      sys_rtcDateTimeGet(&rtcDateTime);
      //system started up, because of VDD generation (USB (J2), external DC-Plug (Battery (J1))
      dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_SYSRESTART, "VDD powerUp");

      //check if wakeUpTimerEvent occurred --> do next measurement
      if (sys_wakeupTimerExpired())
      {
        //do a next measurement cycle
        dataLogger_measCylce(MEAS_REQUEST_RTC);
      }
      //set system to standby
      sys_standbyEnter();
      break;
    case SYS_WAKEUP_USR_BT1:  //UserButton1: Green --> Data operation
      //a little delay, to check other button as well
      sys_sleepForUs(50e3);
      if (digIO_usrBt2Pressed())
      {
        hmi_ledPatternSet(HMI_LED_PATTERN_BLINK_10HZ, RGLED_COLOR_YELLOW);

        //check if a USB-Stick is inserted
        if (dataLogger_checkUSBDeviceInserted())
        {
          //try to do a data export to the USB Stick if available
          if (dataLogger_storeToUsbStick())
          {
            //check if there is a other firmware on the usbStick for this device
            if (dataLogger_checkForFirmwareOnUsbStick(fwFileAndPath))
            {
              //successful data exporting to USB-Stick
              hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_GREEN);
              sys_sleepForUs(1e6);  //1s delay

              hmi_ledPatternSet(HMI_LED_PATTERN_BLINK_10HZ, RGLED_COLOR_RED);
              sys_sleepForUs(2e6);  //2s delay
              hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_RED);
              sys_sleepForUs(500e3);  //0.5s delay
              dataLogger_firmwareUpdateByUsbStick(fwFileAndPath);
            }
            else
            {
              //successful data exporting to USB-Stick
              hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_GREEN);
              sys_sleepForUs(1e6);  //1s delay
              hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
            }
          }
          else
          {
            //error --> no USB-Stick detected
            hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_RED);
            sys_sleepForUs(1e6);  //1s delay
            hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
          }
        }
        else
        {
          //error --> no USB-Stick detected
          hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_RED);
          sys_sleepForUs(1e6);  //1s delay
          hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
        }
      }
      else
      {
        //otherwise do a manual measurement
        dataLogger_measCylce(MEAS_REQUEST_MANUAL);
      }
      //set system to standby
      sys_standbyEnter();
      break;
    case SYS_WAKEUP_USR_BT2:  //UserButton2: Red --> System operation
      //a little delay, to check other button as well
      sys_sleepForUs(50e3);
      if (digIO_usrBt1Pressed())
      {
        hmi_ledPatternSet(HMI_LED_PATTERN_BLINK_10HZ, RGLED_COLOR_YELLOW);

        //check if a USB-Stick is inserted
        if (dataLogger_checkUSBDeviceInserted())
        {
          //try to do a data export to the USB Stick if available
          if (dataLogger_storeToUsbStick())
          {
            //check if there is a other firmware on the usbStick for this device
            if (dataLogger_checkForFirmwareOnUsbStick(fwFileAndPath))
            {
              //successful data exporting to USB-Stick
              hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_GREEN);
              sys_sleepForUs(1e6);  //1s delay

              hmi_ledPatternSet(HMI_LED_PATTERN_BLINK_10HZ, RGLED_COLOR_RED);
              sys_sleepForUs(2e6);  //2s delay
              hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_RED);
              sys_sleepForUs(500e3);  //0.5s delay
              dataLogger_firmwareUpdateByUsbStick(fwFileAndPath);
            }
            else
            {
              //successful data exporting to USB-Stick
              hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_GREEN);
              sys_sleepForUs(1e6);  //1s delay
              hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
            }
          }
          else
          {
            //error --> no USB-Stick detected
            hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_RED);
            sys_sleepForUs(1e6);  //1s delay
            hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
          }
        }
        else
        {
          //error --> no USB-Stick detected
          hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_RED);
          sys_sleepForUs(1e6);  //1s delay
          hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
        }
      }
      else
      {
        //do a system check
        dataLogger_sysCheck();
      }
      //set system to standby
      sys_standbyEnter();
      break;
    case SYS_WAKEUP_RTC:
      //do a measurement cycle
      dataLogger_measCylce(MEAS_REQUEST_RTC);
      //set system to standby
      sys_standbyEnter();
      break;
    case SYS_WAKEUP_USB_VDD:
      //Datalogger interacts as CDC-Device on USB
      sys_sleepForUs(100e3);
      dataLogger_setupWithUSB();
      sys_sleepForUs(100e3);
      do
      {
        sys_sleepForUs(1e3);
      } while (dataLogger_handleWithUSB());
      //set system to standby
      sys_standbyEnter();
      break;
    case SYS_WAKEUP_SDCARD:
      //set system to standby
      sys_standbyEnter();
      break;
  }
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure LSE Drive Capability 
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSI
                              |RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USB
                              |RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_SYSCLK;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage 
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
#ifdef DEBUG
  assert(0);
#else
  ///@todo Error_Handler() at release (example system reset?)
#endif
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
   tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
#ifdef DEBUG
  assert(0);
#else
  ///@todo assert_failed() at release (example system reset?)
#endif
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
