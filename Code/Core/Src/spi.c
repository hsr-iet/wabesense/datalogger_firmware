/**
  ******************************************************************************
  * File Name          : SPI.c
  * Description        : This file provides code for the configuration
  *                      of the SPI instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "spi.h"

/* USER CODE BEGIN 0 */
#include <assert.h>
#include <stdbool.h>
/* USER CODE END 0 */

SPI_HandleTypeDef hspi1;

/* SPI1 init function */
void MX_SPI1_Init(void)
{

  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_SPI_MspInit(SPI_HandleTypeDef* spiHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(spiHandle->Instance==SPI1)
  {
  /* USER CODE BEGIN SPI1_MspInit 0 */

  /* USER CODE END SPI1_MspInit 0 */
    /* SPI1 clock enable */
    __HAL_RCC_SPI1_CLK_ENABLE();
  
    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**SPI1 GPIO Configuration    
    PB3 (JTDO/TRACESWO)     ------> SPI1_SCK
    PB4 (NJTRST)     ------> SPI1_MISO
    PB5     ------> SPI1_MOSI 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* USER CODE BEGIN SPI1_MspInit 1 */

  /* USER CODE END SPI1_MspInit 1 */
  }
}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef* spiHandle)
{

  if(spiHandle->Instance==SPI1)
  {
  /* USER CODE BEGIN SPI1_MspDeInit 0 */

  /* USER CODE END SPI1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_SPI1_CLK_DISABLE();
  
    /**SPI1 GPIO Configuration    
    PB3 (JTDO/TRACESWO)     ------> SPI1_SCK
    PB4 (NJTRST)     ------> SPI1_MISO
    PB5     ------> SPI1_MOSI 
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5);

  /* USER CODE BEGIN SPI1_MspDeInit 1 */

  /* USER CODE END SPI1_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */
/**
 ****************************************************************
 @brief   DeInitialize the SPI1 unit.
 @param   -
 @return  -
 ****************************************************************
 */
void HAL_SPI1_deInit (void)
{
  HAL_SPI_DeInit(&hspi1);
}

/**
 ****************************************************************
 @brief   Setup the SPI1 clock to one of the possible clocks.
 @param   fast  IF true, the SPI1 clock will be 8 MHz, otherwise 250kHz
 @return  -
 ****************************************************************
 */
void HAL_SPI1_clockSpeedFastSet (bool fast)
{
  uint32_t reg;

  reg = hspi1.Instance->CR1 & (~SPI_BAUDRATEPRESCALER_256);
  if (fast)
  {
    //clock prescaler with 2 (8MHz)
    reg |= SPI_BAUDRATEPRESCALER_2;
  }
  else
  {
    //clock prescaler with 64 (250kHz)
    reg |= SPI_BAUDRATEPRESCALER_64;
  }
  hspi1.Instance->CR1 = reg;
}

/**
 ****************************************************************
 @brief   Select one of the possible slaves on this SPI interface
 @param   chipSelect   One of the possible chip connected to this SPI interface
 @return  -
 ****************************************************************
 */
void HAL_SPI1_slaveSelect (spiChipSelect_t chipSelect)
{
  switch (chipSelect)
  {
    default:
    case SPI1_CHIPSELECT_NONE:
      HAL_GPIO_WritePin(SPI1_SDCARD_CSN_GPIO_Port, SPI1_SDCARD_CSN_Pin, GPIO_PIN_SET);
      HAL_GPIO_WritePin(SPI1_FLASH_CSN_GPIO_Port, SPI1_FLASH_CSN_Pin, GPIO_PIN_SET);
      HAL_GPIO_WritePin(SPI1_BME280_CSN_GPIO_Port, SPI1_BME280_CSN_Pin, GPIO_PIN_SET);
      break;
    case SPI1_CHIPSELECT_SDCARD:
      //check if FLASH is active
      if (HAL_GPIO_ReadPin(SPI1_FLASH_CSN_GPIO_Port, SPI1_FLASH_CSN_Pin) == GPIO_PIN_RESET)
      {
        assert_param(0);
      }
      //check if BME280 is active
      else if (HAL_GPIO_ReadPin(SPI1_BME280_CSN_GPIO_Port, SPI1_BME280_CSN_Pin) == GPIO_PIN_RESET)
      {
        assert_param(0);
      }
      else
      {
        HAL_GPIO_WritePin(SPI1_SDCARD_CSN_GPIO_Port, SPI1_SDCARD_CSN_Pin, GPIO_PIN_RESET);
      }
      break;
    case SPI1_CHIPSELECT_FLASH:
      //check if SDCard is active
      if (HAL_GPIO_ReadPin(SPI1_SDCARD_CSN_GPIO_Port, SPI1_SDCARD_CSN_Pin) == GPIO_PIN_RESET)
      {
        assert_param(0);
      }
      //check if BME280 is active
      else if (HAL_GPIO_ReadPin(SPI1_BME280_CSN_GPIO_Port, SPI1_BME280_CSN_Pin) == GPIO_PIN_RESET)
      {
        assert_param(0);
      }
      else
      {
        HAL_GPIO_WritePin(SPI1_FLASH_CSN_GPIO_Port, SPI1_FLASH_CSN_Pin, GPIO_PIN_RESET);
      }
      break;
    case SPI1_CHIPSELECT_BME280:
      //check if SDCard is active
      if (HAL_GPIO_ReadPin(SPI1_SDCARD_CSN_GPIO_Port, SPI1_SDCARD_CSN_Pin) == GPIO_PIN_RESET)
      {
        assert_param(0);
      }
      //check if FLASH is active
      else if (HAL_GPIO_ReadPin(SPI1_FLASH_CSN_GPIO_Port, SPI1_FLASH_CSN_Pin) == GPIO_PIN_RESET)
      {
        assert_param(0);
      }
      else
      {
        HAL_GPIO_WritePin(SPI1_BME280_CSN_GPIO_Port, SPI1_BME280_CSN_Pin, GPIO_PIN_RESET);
      }
      break;
  }
}

/**
 ****************************************************************
 @brief   Transmit an amount of data in blocking mode.
 @param   pTxData pointer to transmission data buffer
 @param   size    amount of data to be sent
 @return  -
 ****************************************************************
 */
void HAL_SPI1_transmit (uint8_t *pTxData, uint16_t size)
{
  HAL_StatusTypeDef status;

  status = HAL_SPI_Transmit(&hspi1, pTxData, size, 10);
  assert_param(status == HAL_OK);
}

/**
 ****************************************************************
 @brief   Receive an amount of data in blocking mode.
 @param   pRxData pointer to reception data buffer
 @param   size    amount of data to be received
 @return  -
 ****************************************************************
 */
void HAL_SPI1_receive (uint8_t *pRxData, uint16_t size)
{
  HAL_StatusTypeDef status;

  status = HAL_SPI_Receive(&hspi1, pRxData, size, 10);
  assert_param(status == HAL_OK);
}

/**
 ****************************************************************
 @brief   Transmit and Receive an amount of data in blocking mode.
 @param   pTxData pointer to transmission data buffer
 @param   pRxData pointer to reception data buffer
 @param   size    amount of data to be sent and received
 @return  -
 ****************************************************************
 */
void HAL_SPI1_transfer (uint8_t *pTxData, uint8_t *pRxData, uint16_t size)
{
  HAL_StatusTypeDef status;

  status = HAL_SPI_TransmitReceive(&hspi1, pTxData, pRxData, size, 10);
  assert_param(status == HAL_OK);
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
