/**
 ****************************************************************
 @file    MMC.c
 ****************************************************************
 @brief   MMC/SDC (in SPI mode) control module.
 @brief   Low level disk interface module include file   (C)ChaN, 2013
 ****************************************************************
 @note    The used interface is the SPI1 unit (also used for FLASH and BME280)!
 *        SPI Interface of the sensor is compatible to (max. 20MHz)
 *          mode 00
 *            CPOL = 0, CPHA = 0
 *          or mode 11
 *            CPOL = 1, CPHA = 1
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-16
 ****************************************************************
 */

/** @addtogroup HWO
 * @{
 */

/** @addtogroup MMC
 * @{
 */

// --- Includes
#include "MMC.h"

#include <stdbool.h>
#include <stdint.h>
#include "spi.h"
#include "SYS.h"
#include "../DEF/GLOB_Types.h"

// --- Defines
//Definitions for MMC/SDC command
#define CMD0     (0x40+0)     //GO_IDLE_STATE
#define CMD1     (0x40+1)     //SEND_OP_COND
#define CMD8     (0x40+8)     //SEND_IF_COND
#define CMD9     (0x40+9)     //SEND_CSD
#define CMD10    (0x40+10)    //SEND_CID
#define CMD12    (0x40+12)    //STOP_TRANSMISSION
#define CMD16    (0x40+16)    //SET_BLOCKLEN
#define CMD17    (0x40+17)    //READ_SINGLE_BLOCK
#define CMD18    (0x40+18)    //READ_MULTIPLE_BLOCK
#define CMD23    (0x40+23)    //SET_BLOCK_COUNT
#define CMD24    (0x40+24)    //WRITE_BLOCK
#define CMD25    (0x40+25)    //WRITE_MULTIPLE_BLOCK
#define CMD41    (0x40+41)    //SEND_OP_COND (ACMD)
#define CMD55    (0x40+55)    //APP_CMD
#define CMD58    (0x40+58)    //READ_OCR

// --- Typedefs

// --- Variables
static volatile DSTATUS Stat = STA_NOINIT;  //Disk status
static uint8_t CardType;                    //b0:MMC, b1:SDC, b2:Block addressing
static uint8_t PowerFlag = 0;               //indicates if "power" is on

// --- Local Function Prototypes
static uint8_t _mmc_wait_ready (void);
static void _mm_send_initial_clock_train (void);
static bool _mmc_rcvr_datablock (uint8_t *buff, uint32_t btr);
static bool _mmc_xmit_datablock (const uint8_t *buff, uint8_t token);
static uint8_t _mmc_send_cmd (uint8_t cmd, uint32_t arg);
static uint8_t _mmc_send_cmd12 (void);

/**
 *****************************************************************
 @brief  Initialize Disk Drive.
 @param  drive        Physical drive number (0)
 @return The disk status
 ****************************************************************
 */
DSTATUS mmc_initialize (uint8_t drive)
{
  uint8_t txData[4];
  uint8_t n;
  uint8_t ty;
  uint8_t ocr[4];
  uint32_t timeoutCnt = 100;  //initialization timeout of 1000 millisecond

  for (n = 0; n < 4; n++)
  {
    txData[n] = 0xFF;
  }

  if (drive)
  {
    return STA_NOINIT;                    //Supports only single drive
  }
  if (Stat & STA_NODISK)
  {
    return Stat;                          //No card in the socket
  }

  //Ensure the card is in SPI mode
  //Set DI and CS high and apply more than 74 pulses to SCLK for the card
  //to be able to accept a native command.
  _mm_send_initial_clock_train();
  PowerFlag = 1;

  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_SDCARD);
  ty = 0;
  if (_mmc_send_cmd(CMD0, 0) == 1)
  {
    //Enter Idle state
    if (_mmc_send_cmd(CMD8, 0x1AA) == 1)
    {
      //SDC Ver2+
      HAL_SPI1_transfer(txData, ocr, 4);

      if (ocr[2] == 0x01 && ocr[3] == 0xAA)
      {
        //The card can work at VDD range of 2.7-3.6V
        do
        {
          if (_mmc_send_cmd(CMD55, 0) <= 1 && _mmc_send_cmd(CMD41, 1UL << 30) == 0)
          {
            break;   //ACMD41 with HCS bit
          }
          sys_sleepForUs(10e3);
          timeoutCnt--;
        } while (timeoutCnt);

        if (timeoutCnt && _mmc_send_cmd(CMD58, 0) == 0)
        {
          //Check CCS bit
          HAL_SPI1_transfer(txData, ocr, 4);
          ty = (ocr[0] & 0x40) ? 6 : 2;
        }
      }
    }
    else
    {
      //SDC Ver1 or MMC
      ty = (_mmc_send_cmd(CMD55, 0) <= 1 && _mmc_send_cmd(CMD41, 0) <= 1) ? 2 : 1;   //SDC : MMC
      do
      {
        if (ty == 2)
        {
          if (_mmc_send_cmd(CMD55, 0) <= 1 && _mmc_send_cmd(CMD41, 0) == 0)
          {
            break;   //ACMD41
          }
        }
        else
        {
          if (_mmc_send_cmd(CMD1, 0) == 0)
          {
            break;   //CMD1
          }
        }
        sys_sleepForUs(10e3);
        timeoutCnt--;
      } while (timeoutCnt);

      if (!timeoutCnt || _mmc_send_cmd(CMD16, 512) != 0)  //Select R/W block length
      {
        ty = 0;
      }
    }
  }
  CardType = ty;
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);
  HAL_SPI1_transmit(txData, 1);           //Idle (Release DO)

  if (ty)
  {
    //Initialization succeeded
    Stat &= ~STA_NOINIT;                  //Clear STA_NOINIT
  }
  else
  {
    //Initialization failed
    PowerFlag = 0;
  }

  return Stat;
}

/**
 *****************************************************************
 @brief  Get Disk Status.
 @param  drive  Physical drive number (0)
 @return The disk status.
 ****************************************************************
 */
DSTATUS mmc_status (uint8_t drive)
{
  if (drive)
  {
    return STA_NOINIT;                    //Supports only single drive */
  }
  return Stat;
}

/**
 *****************************************************************
 @brief  Read Sector(s).
 @param  drive  Physical drive number (0)
 @param  buff   Pointer to the data buffer to store read data
 @param  sector Start sector number (LBA)
 @param  count  Sector count (1..255)
 @return Result of disk function from enumeration.
 ****************************************************************
 */
DRESULT mmc_read (uint8_t drive, uint8_t *buff, uint32_t sector, uint8_t count)
{
  uint8_t txData;

  if (drive || !count)
  {
    return RES_PARERR;
  }
  if (Stat & STA_NOINIT)
  {
    return RES_NOTRDY;
  }

  if (!(CardType & 4))
  {
    sector *= 512;                        //Convert to byte address if needed
  }

  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_SDCARD);

  if (count == 1)
  {
    //Single block read
    if ((_mmc_send_cmd(CMD17, sector) == 0) && _mmc_rcvr_datablock(buff, 512))
    {
      count = 0;
    }
  }
  else
  {
    //Multiple block read
    if (_mmc_send_cmd(CMD18, sector) == 0)
    {
      do
      {
        if (!_mmc_rcvr_datablock(buff, 512))
          break;
        buff += 512;
      } while (--count);
      _mmc_send_cmd12();                  //STOP_TRANSMISSION
    }
  }

  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);
  txData = 0xFF;
  HAL_SPI1_transmit(&txData, 1);           //Idle (Release DO)

  return count ? RES_ERROR : RES_OK;
}

/**
 *****************************************************************
 @brief  Write Sector(s).
 @param  drive  Physical drive number (0)
 @param  buff   Pointer to the data to be written
 @param  sector Start sector number (LBA)
 @param  count  Sector count (1..255)
 @return Result of disk function from enumeration.
 ****************************************************************
 */
#if _READONLY == 0
DRESULT mmc_write (uint8_t drive, const uint8_t *buff, uint32_t sector, uint8_t count)
{
  uint8_t txData;

  if (drive || !count)
  {
    return RES_PARERR;
  }
  if (Stat & STA_NOINIT)
  {
    return RES_NOTRDY;
  }
  if (Stat & STA_PROTECT)
  {
    return RES_WRPRT;
  }

  if (!(CardType & 4))
  {
    sector *= 512;                        //Convert to byte address if needed
  }

  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_SDCARD);

  if (count == 1)
  {
    //Single block write
    if ((_mmc_send_cmd(CMD24, sector) == 0) && _mmc_xmit_datablock(buff, 0xFE))
    {
      count = 0;
    }
  }
  else
  {
    //Multiple block write
    if (CardType & 2)
    {
      _mmc_send_cmd(CMD55, 0);
      _mmc_send_cmd(CMD23, count);   //ACMD23
    }
    if (_mmc_send_cmd(CMD25, sector) == 0)
    {
      //WRITE_MULTIPLE_BLOCK
      do
      {
        if (!_mmc_xmit_datablock(buff, 0xFC))
        {
          break;
        }
        buff += 512;
      } while (--count);
      if (!_mmc_xmit_datablock(0, 0xFD))   //STOP_TRAN token
      {
        count = 1;
      }
    }
  }

  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);
  txData = 0xFF;
  HAL_SPI1_transmit(&txData, 1);           //Idle (Release DO)

  return count ? RES_ERROR : RES_OK;
}
#endif //_READONLY

/**
 *****************************************************************
 @brief  Miscellaneous Functions.
 @param  drive  Physical drive number (0)
 @param  ctrl   Control code
 @param  buff   Buffer to send/receive control data
 @return Result of disk function from enumeration.
 ****************************************************************
 */
DRESULT mmc_ioctl (uint8_t drive, uint8_t ctrl, void *buff)
{
  DRESULT res;
  uint8_t n;
  uint8_t csd[16];
  uint8_t *ptr = buff;
  uint16_t csize;
  uint8_t txData[4];

  for (n = 0; n < 4; n++)
  {
    txData[n] = 0xFF;
  }

  if (drive)
    return RES_PARERR;

  res = RES_ERROR;

  if (ctrl == CTRL_POWER)
  {
    switch (*ptr)
    {
      case 0:   //Sub control code == 0 (POWER_OFF)
        if (PowerFlag)
        {
          PowerFlag = 0;                  //Power off
        }
        res = RES_OK;
        break;
      case 1:   //Sub control code == 1 (POWER_ON)
        //Ensure the card is in SPI mode
        //Set DI and CS high and apply more than 74 pulses to SCLK for the card
        //to be able to accept a native command.
        _mm_send_initial_clock_train();
        PowerFlag = 1;

        res = RES_OK;
        break;
      case 2:   //Sub control code == 2 (POWER_GET)
        *(ptr + 1) = PowerFlag;
        res = RES_OK;
        break;
      default:
        res = RES_PARERR;
    }
  }
  else
  {
    if (Stat & STA_NOINIT)
    {
      return RES_NOTRDY;
    }

    HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_SDCARD);

    switch (ctrl)
    {
      case GET_SECTOR_COUNT:  //Get number of sectors on the disk (uint32_t)
        if ((_mmc_send_cmd(CMD9, 0) == 0) && _mmc_rcvr_datablock(csd, 16))
        {
          if ((csd[0] >> 6) == 1)
          {
            //SDC version 2.00
            csize = csd[9] + ((uint16_t) csd[8] << 8) + 1;
            *(uint32_t*) buff = (uint32_t) csize << 10;
          }
          else
          {
            //MMC or SDC version 1.XX
            n = (csd[5] & 15) + ((csd[10] & 128) >> 7) + ((csd[9] & 3) << 1) + 2;
            csize = (csd[8] >> 6) + ((uint16_t) csd[7] << 2) + ((uint16_t) (csd[6] & 3) << 10) + 1;
            *(uint32_t*) buff = (uint32_t) csize << (n - 9);
          }
          res = RES_OK;
        }
        break;

      case GET_SECTOR_SIZE:   //Get sectors on the disk (uint16_t)
        *(uint16_t*) buff = 512;
        res = RES_OK;
        break;

      case CTRL_SYNC:         //Make sure that data has been written
        if (_mmc_wait_ready() == 0xFF)
        {
          res = RES_OK;
        }
        break;

      case MMC_GET_CSD:       //Receive CSD as a data block (16 bytes)
        //READ_CSD
        if (_mmc_send_cmd(CMD9, 0) == 0 && _mmc_rcvr_datablock(ptr, 16))
        {
          res = RES_OK;
        }
        break;

      case MMC_GET_CID:       //Receive CID as a data block (16 bytes)
        //READ_CID
        if (_mmc_send_cmd(CMD10, 0) == 0 && _mmc_rcvr_datablock(ptr, 16))
        {
          res = RES_OK;
        }
        break;

      case MMC_GET_OCR:       //Receive OCR as an R3 response (4 bytes)
        if (_mmc_send_cmd(CMD58, 0) == 0)
        {
          //READ_OCR
          HAL_SPI1_transfer(txData, ptr, 4);
          res = RES_OK;
        }
      case MMC_GET_TYPE:      //Get card type flags (1 byte)
        *ptr = CardType;
        res = RES_OK;
        break;

      default:
        res = RES_PARERR;
    }
    HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);
    HAL_SPI1_transmit(txData, 1);           //Idle (Release DO)
  }
  return res;
}

/**
 ****************************************************************
 @brief  Wait for card ready (timeout of 500ms)
 @param  -
 @return Return 0xFF if ready
 ****************************************************************
 */
static uint8_t _mmc_wait_ready (void)
{
  uint8_t res;
  uint8_t txData = 0xFF;
  uint32_t timeoutCnt = 50;   //Wait for ready in timeout of 500ms

  HAL_SPI1_transmit(&txData, 1);           //Idle (Release DO)
  do
  {
    HAL_SPI1_transfer(&txData, &res, 1);
    sys_sleepForUs(10e3);
    timeoutCnt--;
  } while ((res != 0xFF) && timeoutCnt);

  return res;
}

/**
 *****************************************************************
 @brief  Send 80 or so clock transitions with CS and DI held high.
 @brief  This is required after card power up to get it into SPI mode.
 @param  -
 @return -
 ****************************************************************
 */
static void _mm_send_initial_clock_train (void)
{
  uint8_t i;
  uint8_t txData = 0xFF;

  //Ensure CS is held high
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);

  //Send 10 bytes over the SSI. This causes the clock to wiggle the
  //required number of times
  for (i = 0; i < 10; i++)
  {
    //Write DUMMY data. SSIDataPut() waits until there is room in the FIFO
    HAL_SPI1_transmit(&txData, 1);
  }
}

/**
 *****************************************************************
 @brief   Receive a data packet from MMC
 @param   buff  Data buffer to store received data
 @param   btr   Byte count (must be even number)
 @return  Return true if successful
 ****************************************************************
 */
static bool _mmc_rcvr_datablock (uint8_t *buff, uint32_t btr)
{
  uint8_t token;
  uint8_t txData = 0xFF;
  uint32_t timeoutCnt = 10;   //Wait for ready in timeout of 100ms

  do
  {
    //Wait for data packet in timeout of 10ms
    HAL_SPI1_transfer(&txData, &token, 1);
    sys_sleepForUs(10e3);
    timeoutCnt--;
  } while ((token == 0xFF) && timeoutCnt);
  if (token != 0xFE)
  {
    return false;   //If not valid data token, return with error
  }

  do
  {
    //Receive the data block into buffer
    HAL_SPI1_transfer(&txData, buff++, 1);
    HAL_SPI1_transfer(&txData, buff++, 1);
  } while (btr -= 2);
  HAL_SPI1_transmit(&txData, 1);          //Discard CRC
  HAL_SPI1_transmit(&txData, 1);

  return true;   //Return with success
}

/**
 *****************************************************************
 @brief   Send a data packet to MMC.
 @param   buff    512 byte data block to be transmitted
 @param   token   Data/Stop token
 @return  return true if done without error
 ****************************************************************
 */
#if _READONLY == 0
static bool _mmc_xmit_datablock (const uint8_t *buff, uint8_t token)
{
  uint8_t resp;
  uint8_t txData = 0xFF;

  if (_mmc_wait_ready() != 0xFF)
  {
    return false;
  }

  HAL_SPI1_transmit(&token, 1);           //transmit data token
  if (token != 0xFD)
  {
    //Is data token
    //transmit the 512 byte data block to MMC
    HAL_SPI1_transmit((uint8_t*) buff, 512);

    HAL_SPI1_transmit(&txData, 1);        //CRC (Dummy)
    HAL_SPI1_transmit(&txData, 1);
    HAL_SPI1_transfer(&txData, &resp, 1); //Receive data response
    if ((resp & 0x1F) != 0x05)            //If not accepted, return with error
    {
      return false;
    }
  }
  return true;
}
#endif //_READONLY

/**
 *****************************************************************
 @brief   Send a command packet to MMC.
 @param   cmd     Command byte
 @param   arg     Argument
 @return  Return with the response value of the command.
 ****************************************************************
 */
static uint8_t _mmc_send_cmd (uint8_t cmd, uint32_t arg)
{
  uint8_t n;
  uint8_t res;
  uint8_t txData;

  if (_mmc_wait_ready() != 0xFF)
  {
    return 0xFF;
  }

  //Send command packet
  HAL_SPI1_transmit(&cmd, 1);     //Command
  txData = (uint8_t) (arg >> 24); //Argument[31..24]
  HAL_SPI1_transmit(&txData, 1);
  txData = (uint8_t) (arg >> 16); //Argument[23..16]
  HAL_SPI1_transmit(&txData, 1);
  txData = (uint8_t) (arg >> 8);  //Argument[15..8]
  HAL_SPI1_transmit(&txData, 1);
  txData = (uint8_t) (arg);       //Argument[7..0]
  HAL_SPI1_transmit(&txData, 1);

  if (cmd == CMD0)
  {
    txData = 0x95;   //CRC for CMD0(0)
  }
  else if (cmd == CMD8)
  {
    txData = 0x87;   //CRC for CMD8(0x1AA)
  }
  else
  {
    txData = 0xFF;
  }
  HAL_SPI1_transmit(&txData, 1);

  //Receive command response
  if (cmd == CMD12)
  {
    txData = 0xFF;
    HAL_SPI1_transmit(&txData, 1);  //Skip a stuff byte when stop reading
  }

  txData = 0xFF;
  n = 10;   //Wait for a valid response in timeout of 10 attempts
  do
  {
    HAL_SPI1_transfer(&txData, &res, 1);
  } while ((res & 0x80) && --n);

  return res;   //Return with the response value
}

/**
 *****************************************************************
 @brief  Send the special command used to terminate a multiple-sector read.
 @brief  This is the only command which can be sent while the SDCard is sending
 @brief  data. The SDCard specification indicates that the data transfer will stop 2 bytes
 @brief  after the 6 byte CMD12 command is sent and that the card will then send
 @brief  0xFF for between 2 and 6 more bytes before the R1 response byte.  This
 @brief  response will be followed by another 0xFF byte.  In testing, however, it
 @brief  seems that some cards don't send the 2 to 6 0xFF bytes between the end of
 @brief  data transmission and the response code.  This function, therefore, merely
 @brief  reads 10 bytes and, if the last one read is 0xFF, returns the value of the
 @brief  latest non-0xFF byte as the response code.
 @param  -
 @return Return with the response value
 ****************************************************************
 */
static uint8_t _mmc_send_cmd12 (void)
{
  uint8_t n;
  uint8_t res;
  uint8_t val;
  uint8_t txData;

  //For CMD12, we don't wait for the card to be idle before we send the new command.
  //Send command packet - the argument for CMD12 is ignored.
  txData = (uint8_t) CMD12;
  HAL_SPI1_transmit(&txData, 1);
  txData = 0x00;
  HAL_SPI1_transmit(&txData, 1);
  HAL_SPI1_transmit(&txData, 1);
  HAL_SPI1_transmit(&txData, 1);
  HAL_SPI1_transmit(&txData, 1);
  HAL_SPI1_transmit(&txData, 1);

  //Read up to 10 bytes from the card, remembering the value read if it's not 0xFF
  txData = 0xFF;
  for (n = 0; n < 10; n++)
  {
    HAL_SPI1_transfer(&txData, &val, 1);
    if (val != 0xFF)
    {
      res = val;
    }
  }
  return res;   //Return with the response value
}

/**
 * @}
 */

/**
 * @}
 */
