/**
 ****************************************************************
 @file    test_hmi.h
 ****************************************************************
 @brief   This module implements some tests for the human machine interface.
 *        With the user button1: select next RG color
 *        With the user button2: select previous RG color
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 1.0
 @date    2021-09-24
 ****************************************************************
 */
#ifndef TEST_HMI_H_
#define TEST_HMI_H_

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

// --- Includes

// --- Defines

// --- Typedefs

// --- Function Prototypes
void test_hmi (void);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* TEST_HMI_H_ */
