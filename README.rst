=============================================================
Firmware for the microcontroller of the WABEsense data-logger
=============================================================

The open source data-logger firmware, which is part of the WABEsense project.

The main features are:
* communly used firmware build up (layering)
 
* lowPower
  
* External channels
    * 4x Analog 0-5V
	* 2x Analog 0-3V3 (VDDA)
    * 7x Digital In/Out
    * SPI interface or additional 4x Digital In/Out
    * I2C interface or additional 2x Digital In/Out
	* V_Supply (USB or external DC-Adapter)
	* 3V3 supply
	* 3V3 supply switched by microcontroller
	* 17V supply switched by microcontroller
	* VDDA supply (3V3)

* Internal channels

    [BME280](https://www.bosch-sensortec.com/products/environmental-sensors/humidity-sensors-bme280/) sensor: pressure, temperature, humidity

* Data recording

    [Internal FLASH](https://www.dialog-semiconductor.com/sites/default/files/2021-04/DS-AT25SF321B-179E-062020.pdf) (4 MB): up to 130000 data points (> 2 years at an minute sampling interval)
 
* extendable
 
* Firmware update:
	* Via memory stick using .bin files (USB-C connector)

* Data output exported to external memory stick (USB-C connector)
    * Channel values (.csv-File) 
    * Metadata and configuration (.txt-File: Plain text file)
    * Activity log (.log-File: Plain text file)

* Configuration over USB-C connector (virtual COM-Port):
    * Device name string: max. 92 characters
    * Analog channel name string: max. 64 characters
    * Battery: nominal cell voltage, number of cells (used for system check)
    * Sampling interval: 1-1092 minutes
    * System time: RTC with back-up battery CR2032 (RTC unit is integrated in microcontroller)

## Generated files
During operation the logger dumps three files:
- `.log`: activity logs
- `.txt`: logger configuration file
- `.csv`: recordeded data

Installation
------------

For development
***************

To install all dependencies for development use the ``requirements.txt`` file::

    python -m pip install -U requirements.txt

Documentation
-------------

The documentation is hosted at https://hsr-iet.gitlab.io/wabesense/datalogger_firmware

To build the documentation you need `Sphinx <https://www.sphinx-doc.org/en/master/>`_.
Make sure you have all the dependencies, see section `For development`_ in the
installation instructions.
Once you have all the dependencies, go to the folder ``doc`` and run::

    make html

The documentation is then available inside ``doc/build/html``. To read it, open
the ``index.html`` file in that folder with your favorite internet browser.
