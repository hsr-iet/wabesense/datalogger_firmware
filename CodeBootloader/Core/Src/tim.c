/**
  ******************************************************************************
  * File Name          : TIM.c
  * Description        : This file provides code for the configuration
  *                      of the TIM instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "tim.h"

/* USER CODE BEGIN 0 */
#include <assert.h>
#include <stdbool.h>
#include "../../DEF/GLOB_Types.h"

static volatile uint32_t tickCnt = 0;          //!< TickCounter
static volatile pFV_V_t pCbFct_Tim6 = (pFV_V_t) NULL;
static volatile pFV_V_t pCbFct_SysTick = (pFV_V_t) NULL;
/* USER CODE END 0 */

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim6;

/* TIM1 init function */
void MX_TIM1_Init(void)
{
  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  htim1.Instance = TIM1;
  htim1.Init.Prescaler = (SYS_CLK/10000)-1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 99;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 50;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_LOW;
  sConfigOC.OCFastMode = TIM_OCFAST_ENABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.Pulse = 0;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
  sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
  sBreakDeadTimeConfig.Break2Filter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  HAL_TIM_MspPostInit(&htim1);

}
/* TIM6 init function */
void MX_TIM6_Init(void)
{
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  htim6.Instance = TIM6;
  htim6.Init.Prescaler = (SYS_CLK/1000000)-1;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 0;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* tim_baseHandle)
{

  if(tim_baseHandle->Instance==TIM1)
  {
  /* USER CODE BEGIN TIM1_MspInit 0 */

  /* USER CODE END TIM1_MspInit 0 */
    /* TIM1 clock enable */
    __HAL_RCC_TIM1_CLK_ENABLE();
  /* USER CODE BEGIN TIM1_MspInit 1 */

  /* USER CODE END TIM1_MspInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM6)
  {
  /* USER CODE BEGIN TIM6_MspInit 0 */

  /* USER CODE END TIM6_MspInit 0 */
    /* TIM6 clock enable */
    __HAL_RCC_TIM6_CLK_ENABLE();

    /* TIM6 interrupt Init */
    HAL_NVIC_SetPriority(TIM6_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(TIM6_IRQn);
  /* USER CODE BEGIN TIM6_MspInit 1 */

  /* USER CODE END TIM6_MspInit 1 */
  }
}
void HAL_TIM_MspPostInit(TIM_HandleTypeDef* timHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(timHandle->Instance==TIM1)
  {
  /* USER CODE BEGIN TIM1_MspPostInit 0 */

  /* USER CODE END TIM1_MspPostInit 0 */
  
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**TIM1 GPIO Configuration    
    PA7     ------> TIM1_CH1N
    PB0     ------> TIM1_CH2N
    PB1     ------> TIM1_CH3N 
    */
    GPIO_InitStruct.Pin = TIM1_CH1N_LED_R_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF1_TIM1;
    HAL_GPIO_Init(TIM1_CH1N_LED_R_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = TIM1_CH2N_LED_G_Pin|TIM1_CH3N_LED_B_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF1_TIM1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* USER CODE BEGIN TIM1_MspPostInit 1 */

  /* USER CODE END TIM1_MspPostInit 1 */
  }

}

void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef* tim_baseHandle)
{

  if(tim_baseHandle->Instance==TIM1)
  {
  /* USER CODE BEGIN TIM1_MspDeInit 0 */

  /* USER CODE END TIM1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM1_CLK_DISABLE();
  /* USER CODE BEGIN TIM1_MspDeInit 1 */

  /* USER CODE END TIM1_MspDeInit 1 */
  }
  else if(tim_baseHandle->Instance==TIM6)
  {
  /* USER CODE BEGIN TIM6_MspDeInit 0 */

  /* USER CODE END TIM6_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_TIM6_CLK_DISABLE();

    /* TIM6 interrupt Deinit */
    HAL_NVIC_DisableIRQ(TIM6_IRQn);
  /* USER CODE BEGIN TIM6_MspDeInit 1 */

  /* USER CODE END TIM6_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */
/**
 ****************************************************************
 @brief   De-Initialize the timer1 unit.
 @param   -
 @return  -
 ****************************************************************
 */
void HAL_TIM1_DeInit (void)
{
  HAL_TIM_Base_DeInit(&htim1);
}

/**
 ****************************************************************
 @brief   De-Initialize the timer6 unit.
 @param   -
 @return  -
 ****************************************************************
 */
void HAL_TIM6_DeInit (void)
{
  HAL_TIM_Base_DeInit(&htim6);
}

/**
 ****************************************************************
 @brief   Set the output compare 1 value for one of the timers
 @param   htim  One of the possible timer handlers
 @param   pulse The to be set pulse/compare value
 @return  -
 ****************************************************************
 */
void HAL_TIM_OC1_Set (TIM_HandleTypeDef *htim, uint32_t pulse)
{
  htim->Instance->CCR1 = pulse;
}

/**
 ****************************************************************
 @brief   Set the output compare 2 value for one of the timers
 @param   htim  One of the possible timer handlers
 @param   pulse The to be set pulse/compare value
 @return  -
 ****************************************************************
 */
void HAL_TIM_OC2_Set (TIM_HandleTypeDef *htim, uint32_t pulse)
{
  htim->Instance->CCR2 = pulse;
}

/**
 ****************************************************************
 @brief   Set the output compare 3 value for one of the timers
 @param   htim  One of the possible timer handlers
 @param   pulse The to be set pulse/compare value
 @return  -
 ****************************************************************
 */
void HAL_TIM_OC3_Set (TIM_HandleTypeDef *htim, uint32_t pulse)
{
  htim->Instance->CCR3 = pulse;
}

/**
 ****************************************************************
 @brief   Set a defined microseconds based timeout timer interrupt on timer 6
 @param   timeout_us  The timeout time in microseconds
 @return  -
 ****************************************************************
 */
void HAL_TIM6_StartInterrupt (pFV_V_t pCbFct, uint32_t timeout_us)
{
  HAL_StatusTypeDef status;
  uint32_t timeout;
  uint32_t sysClock;

  sysClock = HAL_RCC_GetSysClockFreq();
  status = HAL_TIM_Base_Stop_IT(&htim6);
  assert(status == HAL_OK);

  if (timeout_us > 65536)
  {
    timeout = (timeout_us + 999) / 1000;  //round up
    //ms basis unit
    htim6.Init.Prescaler = (sysClock / 1000) - 1;
  }
  else
  {
    timeout = timeout_us;
    //us basis unit
    htim6.Init.Prescaler = (sysClock / 1000000) - 1;
  }
  assert(timeout <= 0xFFFF);

  htim6.Init.Period = (timeout - 1);
  TIM_Base_SetConfig(htim6.Instance, &htim6.Init);

  pCbFct_Tim6 = pCbFct;

  __HAL_TIM_CLEAR_IT(&htim6, TIM_IT_UPDATE);
  status = HAL_TIM_Base_Start_IT(&htim6);
  assert(status == HAL_OK);
}

/**
 ****************************************************************
 @brief   Reset the absolute tick counter value.
 @param   -
 @return  -
 ****************************************************************
 */
void HAL_SYSTICK_Reset (void)
{
  SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk; //disable SysTick interrupt
  tickCnt = 0;
  SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk; //enable SysTick interrupt
}

/**
 ****************************************************************
 @brief   Get the absolute tick counter value since last HAL_SYSTICK_Reset().
 @param   pTick  Pointer to the actual TickCounter value.
 @return  -
 ****************************************************************
 */
void HAL_SYSTICK_TickGet (uint32_t *pTick)
{
  SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk; //disable SysTick interrupt
  *pTick = tickCnt;
  SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk; //enable SysTick interrupt
}

/**
 ****************************************************************
 @brief   Get the difference between a former TickCounter value and the
 *        actual TickCounter value.
 @param   oldTick    Former TickCounter value
 @param   pDeltaTick Pointer to the resulting TickCounter difference.
 @return  -
 ****************************************************************
 */
void HAL_SYSTICK_TickDeltaGet (uint32_t oldTick, uint32_t *pDeltaTick)
{
  SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk; //disable SysTick interrupt
  *pDeltaTick = tickCnt - oldTick;    //wrap around is correct
  SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk; //enable SysTick interrupt
}

/**
 ****************************************************************
 @brief   Get the difference between a former TickCounter value and the
 *        actual TickCounter value. Update pOldTick to actual TickCounter value.
 @param   pOldTick   Pointer to a former TickCounter value, updated to actual TickCounter value.
 @param   pDeltaTick Pointer to the resulting TickCounter difference.
 @return  -
 ****************************************************************
 */
void HAL_SYSTICK_TickDeltaUpdateGet (uint32_t *pOldTick, uint32_t *pDeltaTick)
{
  uint32_t actCnt;

  SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk; //disable SysTick interrupt
  actCnt = tickCnt;                   //read actual TickCounter value
  SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk; //enable SysTick interrupt
  *pDeltaTick = actCnt - *pOldTick;   //wrap around is correct
  *pOldTick = actCnt;                 //update *pOldTick value
}

/**
 ****************************************************************
 @brief   Register a callback function, which should be executed in
 *        sysTick interrupt service routine.
 @param   pSysTickCbFct Pointer to the callback function to be registered
 @return  -
 ****************************************************************
 */
void HAL_SYSTICK_RegCallbackFct (pFV_V_t pSysTickCbFct)
{
  pCbFct_SysTick = pSysTickCbFct;
}

/**
  * @brief Provide a tick value in millisecond.
  * @note This function is declared as __weak to be overwritten in case of other
  *       implementations in user file.
  * @retval tick value
  */

/**
 ****************************************************************
 @brief   Provide a tick value in millisecond.
 @param   -
 @return  The current tick value
 ****************************************************************
 */
uint32_t HAL_GetTick(void)
{
  return tickCnt;
}

/**
 ****************************************************************
 @brief   This function is called to increment a global variable "tickCnt"
 *        used as application time base.
 @note    In the default implementation, this variable is incremented each 100ms
 *        in SysTick ISR.
 @param   -
 @return  -
 ****************************************************************
 */
void HAL_IncTick (void)
{
  tickCnt++;

  if (pCbFct_SysTick)
  {
    pCbFct_SysTick();
  }
}

/**
 ****************************************************************
 @brief   Period elapsed callback in non-blocking mode
 @param   htim TIM handle
 @return  -
 ****************************************************************
 */
void HAL_TIM_PeriodElapsedCallback (TIM_HandleTypeDef *htim)
{
  HAL_StatusTypeDef status;

  if (htim->Instance == TIM6)
  {
    status = HAL_TIM_Base_Stop_IT(htim);
    assert(status == HAL_OK);

    if (pCbFct_Tim6)
    {
      pCbFct_Tim6();
    }
  }
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
