===========
Source Code
===========

.. contents:: :local:

The complete source code is documented with the doxygen tool. In GitLab, with a continous-integration step, the doxygen is build on every push to the master branch 
and the resulting documentation can be opened over this link:

Firmware `Doxygen`_ documentation
---------------------------------

	* https://hsr-iet.gitlab.io/wabesense/datalogger_firmware/doxygen/index.html
	
Bootloader `Doxygen`_ documentation
-----------------------------------

	* https://hsr-iet.gitlab.io/wabesense/datalogger_firmware/doxygen_Bootloader/index.html
	

Links
=====
.. _Doxygen: https://www.doxygen.nl/index.html

.. target-notes::