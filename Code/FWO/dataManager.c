/**
 ****************************************************************
 @file    dataManager.c
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the log data management (FLASH / SD-Card).
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-16
 ****************************************************************
 */

/** @addtogroup FWO
 * @{
 */

/** @addtogroup DATAMANAGER
 * @{
 */

// --- Includes
#include "dataManager.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "fatfs.h"
#include "../HWO/DIG_IO.h"
#include "../HWO/FLASH.h"
#include "../HWO/SYS.h"
#include "../FWO/dataLogger.h"
#include "../FWO/stringConverter.h"

// --- Defines
#define FLASH_BLOCKSTARTADDR_LOGDATA        0     //!< 4kByte Block number 0 to 1018
#define FLASH_BLOCKSTARTADDR_SYSCONFIG      1019  //!< 4kByte Block number 1019
#define FLASH_BLOCKSTARTADDR_RESERVED       1020  //!< 4kByte Block number 1020-1021 (reserved)
#define FLASH_BLOCKSTARTADDR_LOGHIST        1022  //!< 4kByte Block number 1022-1023 (64Bytes each entry: gives 128 log events to be stored)

#define SD_CARD_BLOCK_SIZE                  512   //!< The number of bytes to be written as complete block

#define SYS_VBAT_ISNAN                      ((uint16_t) 0xFFFF) //!< internal compressed system NaN value for the battery voltage (valid 0.0 up to +28.0), format 8.8, unsigned
#define ADC_IN_ISNAN                        ((uint16_t) 0xFFFF) //!< internal compressed ADC NaN value for a input voltage (valid 0.0 up to +5.0), format 3.13, unsigned
#define BME280_TEMPERATURE_ISNAN            ((int16_t) 0x7FFF)  //!< internal compressed BME280 NaN value for the temperature (valid -40.0 up to +80.0), format 8.8, signed
#define BME280_HUMIDITY_ISNAN               ((uint16_t) 0xFFFF) //!< internal compressed BME280 NaN value for the humidity (valid 0.0 up to 100.0), format 7.9, unsigned

// --- Typedefs
//! reduced timeStamp information (bitCoded to reduce size) - 6 Bytes
typedef struct __attribute__((packed))
{
  uint32_t year :6;       //!< the reduced year information (0..63)
  uint32_t month :4;      //!< the month information (1..12)
  uint32_t day :5;        //!< the day information (1..31)
  uint32_t hours :5;      //!< the hours information (0..23)
  uint32_t minutes :6;    //!< the minutes information (0..59)
  uint32_t seconds :6;    //!< the second information (0..59)
  uint16_t milliseconds;  //!< the millisecond information (0..999)
} dataManager_timeStampSmall_t;

//! all data of a single data point packed together in a small memory footprint structure
typedef union
{
  struct __attribute__((packed))  //total 21 bytes used, 32 bytes blocks used
  {
    dataManager_timeStampSmall_t timeStamp; //!< the bitwise coded timeStamp (6Byte)
    bool timeBasedMeas;                     //!< Flag to indicate, if this is a timeBaseMeasurement or not (1Byte)
    uint16_t vBat;                          //!< battery voltage [V] (2Byte), format 8.8
    uint16_t adc_in8;                       //!< voltage on ADC-input8 [V] (2Byte), format 3.13
    uint16_t adc_in9;                       //!< voltage on ADC-input9 [V] (2Byte), format 3.13
    int16_t bme280_temperature;             //!< temperature of BME280 sensor in °C, format sign8.8 (2Byte)
    float bme280_pressure;                  //!< pressure of BME280 in Pa, format float (4Byte)
    uint16_t bme280_humidity;               //!< humidity of BME280 in %, format 7.9 (2Byte)
  };
  uint8_t BYTE[32];
} dataManager_dataPointSmall_t;

//! all data of a the system configuration packed together in a small memory footprint structure
typedef union
{
  struct __attribute__((packed))  //total 362 bytes used, 4096 bytes blocks reserved for system configuration
  {
    const uint32_t tag;                         //!< a simple 32bit Tag used for a simple validation check    4 Bytes
    char devName[DEV_NAME_MAX_LEN + 1];         //!< the device name                                         93 Bytes
    uint16_t measInterval_minutes;              //!< the measurement interval [minutes]                       2 Bytes
    uint16_t bat_nominalCellMilivoltage;        //!< the nominal battery cell voltage [mV]                    2 Bytes
    uint8_t bat_nrOfCells;                      //!< the number of installed battery cells                    1 Bytes
    char analogChannel0Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 0 name                         65 Bytes
    char analogChannel1Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 1 name                         65 Bytes
    char analogChannel2Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 2 name                         65 Bytes
    char analogChannel3Name[ANALOG_NAME_MAX_LEN + 1]; //!< the analog channel 3 name                         65 Bytes
  };
  uint8_t BYTE[362];
} dataManager_sysConfigSmall_t;

//! all data of a single log history packed together in a small memory footprint structure
typedef union
{
  struct __attribute__((packed))  //64 bytes blocks used
  {
    dataManager_timeStampSmall_t timeStamp; //!< the bitwise coded timeStamp (6 Bytes)
    uint8_t evt;                            //!< the to be logged event, based on dataManager_logEvt_t
    char addInfo[57];                       //!< additional text with a maximum length of 57 characters could be added
  };
  uint8_t BYTE[64];
} dataManager_logHistorySmall_t;

// --- Variables
static bool _initDone = false;
//! system configuration struct with default parameters
static const dataManager_sysConfigSmall_t _sysCfgDefault =
{
  .tag = 0x21020200,                    //!< simple format as yymmdd and to bytes for different versions of this day
  .devName = "",                        //!< default device name (will be set dynamically, depending on Unique ID)
  .measInterval_minutes = 10,           //!< default measurement interval in minutes [0: deactivated, 1..1092]
  .bat_nominalCellMilivoltage = 3700,   //!< default battery nominal voltage
  .bat_nrOfCells = 2,                   //!< default number of battery cells installed
  .analogChannel0Name = "ADC_IN8(V)",   //!< default analog channel 0 name
  .analogChannel1Name = "ADC_IN9(V)",   //!< default analog channel 1 name
  .analogChannel2Name = "ADC_IN10(V)",  //!< default analog channel 2 name
  .analogChannel3Name = "ADC_IN11(V)",
//!< default analog channel 3 name
  };

static dataManager_sysConfigSmall_t _sysCfg;
static FATFS fs;
static FIL file;
static DIR dj;         //Directory object
static FILINFO fno;    //file information
static TCHAR path[128] = "";
static char sdDataBuffer[SD_CARD_BLOCK_SIZE << 1];
static uint8_t _flashReadData[SD_CARD_BLOCK_SIZE];

//start addresses of the different FLASH blocks
static const uint32_t flashStartAddr_logData = FLASH_BLOCKSTARTADDR_LOGDATA * 4096;
static const uint32_t flashStartAddr_sysConfig = FLASH_BLOCKSTARTADDR_SYSCONFIG * 4096;
static const uint32_t flashStartAddr_reserved = FLASH_BLOCKSTARTADDR_RESERVED * 4096;
static const uint32_t flashStartAddr_logHist = FLASH_BLOCKSTARTADDR_LOGHIST * 4096;
//block sizes for the different kind of memory areas
static const uint32_t flashSize_logData = (FLASH_BLOCKSTARTADDR_SYSCONFIG - FLASH_BLOCKSTARTADDR_LOGDATA) * 4096;
static const uint32_t flashSize_sysConfig = (FLASH_BLOCKSTARTADDR_RESERVED - FLASH_BLOCKSTARTADDR_SYSCONFIG) * 4096;
static const uint32_t flashSize_reserved = (FLASH_BLOCKSTARTADDR_LOGHIST - FLASH_BLOCKSTARTADDR_RESERVED) * 4096;
static const uint32_t flashSize_logHist = (1024 - FLASH_BLOCKSTARTADDR_LOGHIST) * 4096;

// --- Local Function Prototypes
static bool _dataManager_sdCardMount (void);
static void _dataManager_sdCardUnmount (void);
static bool _dataManager_sdCardMeasDataFileStore (TCHAR *pPath, uint32_t nrOfDataPoints, uint32_t nrOfNewDataPoints);
static bool _dataManager_sdCardMetaDataFileStore (TCHAR *pPath);
static bool _dataManager_sdCardLogDataFileStore (TCHAR *pPath);
static bool _dataManager_sdCardWriteBlock (bool forceWrite, UINT *pBytesLeftToWrite);

/**
 ****************************************************************
 @brief   Initialize the data-manager module.
 @param   -
 @return  -
 ****************************************************************
 */
void dataManager_init (void)
{
  sysID_t sysID;
  uint32_t flashSize;

  if (!_initDone)
  {
    flashSize = flash_memorySizeGet();
    assert_param(flashSize == (flashSize_logData + flashSize_sysConfig + flashSize_reserved + flashSize_logHist));

    //check if memory size and dataPoint size gives no remaining bytes
    //  Integer number for number of dataPoints to be stored in FLASH memory
    assert_param((flashSize % sizeof(dataManager_dataPointSmall_t)) == 0);

    //check if dataPoint size and FLASH memory block matches
    static_assert((4096 % sizeof(dataManager_dataPointSmall_t)) == 0,
      "dataManager: wrong size of dataManager_dataPoint_t!");
    static_assert(32 == sizeof(dataManager_dataPointSmall_t), "dataManager: wrong size of dataManager_dataPoint_t!");

    //check if dataPoint size and FLASH memory block matches
    static_assert((4096 % sizeof(dataManager_logHistorySmall_t)) == 0,
      "dataManager: wrong size of dataManager_logHistorySmall_t!");
    static_assert(64 == sizeof(dataManager_logHistorySmall_t),
      "dataManager: wrong size of dataManager_logHistorySmall_t!");

    //read current sysConfiguration out of the flash
    flash_bytesRead(flashStartAddr_sysConfig, _sysCfg.BYTE, sizeof(dataManager_sysConfigSmall_t));
    //check if the system configuration in FLASH has not been set yet
    //or the read out flash configuration is older than the default one from the current firmware
    if ((_sysCfg.tag == 0xFFFFFFFF) || (_sysCfg.tag < _sysCfgDefault.tag))
    {
      //copy default system configuration to working system configuration in RAM
      memcpy(_sysCfg.BYTE, _sysCfgDefault.BYTE, sizeof(dataManager_sysConfigSmall_t));

      //change default device name to Unique ID
      sys_uniqueIdGet(&sysID);
      snprintf(_sysCfg.devName, DEV_NAME_MAX_LEN, "0x%08X%08X%08X", sysID.systemID[0], sysID.systemID[1],
        sysID.systemID[2]);

      //store the new configuration (default)
      dataLogger_flashConfigStore(true);

      //read current sysConfiguration out of the flash
      flash_bytesRead(flashStartAddr_sysConfig, _sysCfg.BYTE, sizeof(dataManager_sysConfigSmall_t));
    }

    ///@TODO: check all data cursors (check if they match with the memory) --> next position in memory should be 0xFF

    _initDone = true;
  }
}

/**
 ****************************************************************
 @brief   Erased all stored data on the FLASH, write default system configuration parameters
 @param   -
 @return  -
 ****************************************************************
 */
void dataManager_flashClearAll (void)
{
  //clear all backup registers
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL, flashStartAddr_logData);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT, flashStartAddr_logData);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_HEAD, flashStartAddr_logData);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_RESERVED_TAIL, flashStartAddr_reserved);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_RESERVED_HEAD, flashStartAddr_reserved);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGHIST_TAIL, flashStartAddr_logHist);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGHIST_HEAD, flashStartAddr_logHist);

  flash_chipErase();
  //check chip erase
  flash_bytesRead(0, _flashReadData, 8);
  assert_param(_flashReadData[0] == 0xFF);
  assert_param(_flashReadData[7] == 0xFF);
  //read some data belong to first 4kB and some to second 4kB
  flash_bytesRead(4092, _flashReadData, 8);
  assert_param(_flashReadData[0] == 0xFF);
  assert_param(_flashReadData[7] == 0xFF);
}

/**
 ****************************************************************
 @brief   Erased the stored configuration data on the FLASH.
 @param   -
 @return  -
 ****************************************************************
 */
void dataManager_flashClearCfg (void)
{
  flash_block4kErase(FLASH_BLOCKSTARTADDR_SYSCONFIG);
}

/**
 ****************************************************************
 @brief   Erased the stored data and history data on the FLASH.
 @param   -
 @return  -
 ****************************************************************
 */
void dataManager_flashClearMeasDataAndHistory (void)
{
  //clear all backup registers
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL, flashStartAddr_logData);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT, flashStartAddr_logData);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_HEAD, flashStartAddr_logData);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_RESERVED_TAIL, flashStartAddr_reserved);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_RESERVED_HEAD, flashStartAddr_reserved);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGHIST_TAIL, flashStartAddr_logHist);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGHIST_HEAD, flashStartAddr_logHist);

  //erase complete chip (much faster with typical 15 seconds, instead of
  //  1019 * 4kByte Blocks of each typical 50ms = 50.95 seconds (data section)
  //  2 * 4kByte Blocks of each typical 50ms = 0.1 seconds (log history section)
  //the disadvantage of this is, if a lost of the power source occurs, system configuration is lost as well
  flash_chipErase();

  ///@TODO check if this is always correct!
  //store back the system configuration from RAM to FLASH
  dataManager_cfgStoreToFlash();
}

/**
 ****************************************************************
 @brief   Rewind the data readout cursor for the log data on the FLASH.
 @param   -
 @return  -
 ****************************************************************
 */
void dataManager_flashRewindDataReadoutCursor (void)
{
  uint32_t tail = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL);

  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT, tail);
}

/**
 ****************************************************************
 @brief   Store the device name in the RAM _sysCfg struct
 @param   pDevName  Pointer to the device name with max length of DEV_NAME_MAX_LEN
 @param   len       The length of the name (1...DEV_NAME_MAX_LEN)
 @return  Return true, if successful, otherwise false
 ****************************************************************
 @note    After all system configuration parameters has been written,
 *        these settings has to be stored in FLASH by calling dataManager_cfgStoreToFlash()
 ****************************************************************
 */
bool dataManager_cfgDeviceNameSet (char *pDevName, uint8_t len)
{
  uint32_t idx;

  if ((len > DEV_NAME_MAX_LEN) || (len == 0))
  {
    return false;
  }

  memcpy(_sysCfg.devName, pDevName, len);
  _sysCfg.devName[len] = '\0';  //string terminator

  //clear other parts of the deviceName
  if (len < DEV_NAME_MAX_LEN)
  {
    for (idx = len + 1; idx <= DEV_NAME_MAX_LEN; idx++)
    {
      _sysCfg.devName[idx] = '\0';
    }
  }

  return true;
}

/**
 ****************************************************************
 @brief   Get the current device name stored in the RAM _sysCfg struct.
 @param   pDevName  Pointer to the device name with length 1...DEV_NAME_MAX_LEN
 @param   pLen      Pointer to the length of the device name.
 @return  -
 ****************************************************************
 */
void dataManager_cfgDeviceNameGet (char *pDevName, uint8_t *pLen)
{
  uint8_t len = strlen(_sysCfg.devName);

  //length limitation check!
  if (len > DEV_NAME_MAX_LEN)
  {
    len = DEV_NAME_MAX_LEN;
  }

  memcpy(pDevName, _sysCfg.devName, len);
  *pLen = len;
}

/**
 ****************************************************************
 @brief   Set the to be used measurement interval in minutes in the RAM _sysCfg struct.
 @param   minutes The to be used measurement interval (0...1092). 0 means, sampling every second.
 @return  Return true if okay, otherwise false (no new interval set)
 ****************************************************************
 @note    After all system configuration parameters has been written,
 *        these settings has to be stored in FLASH by calling dataManager_cfgStoreToFlash()
 ****************************************************************
 */
bool dataManager_cfgMeasIntervalSet (uint32_t minutes)
{
  if (minutes <= 1092)
  {
    _sysCfg.measInterval_minutes = minutes;

    //update the informations in the dataLogger
    dataLogger_measIntervalSet(minutes);

    return true;
  }
  return false;
}

/**
 ****************************************************************
 @brief   Get the currently set measurement interval in minutes.
 @param   -
 @return  Returns the measurement interval in unit minutes.
 ****************************************************************
 */
uint32_t dataManager_cfgMeasIntervalGet (void)
{
  return _sysCfg.measInterval_minutes;
}

/**
 ****************************************************************
 @brief   Store the battery configuration in the RAM _sysCfg struct
 @param   nominalCellMiliVoltage  The nominal cell voltage [mV]
 @param   nrOfCells The number of installed battery cells.
 @return  Return true, if successful, otherwise false
 ****************************************************************
 @note    After all system configuration parameters has been written,
 *        these settings has to be stored in FLASH by calling dataManager_cfgStoreToFlash()
 ****************************************************************
 */
bool dataManager_cfgBatterySettingSet (uint32_t nominalCellMiliVoltage, uint32_t nrOfCells)
{
  if (nrOfCells > 9)
  {
    return false;
  }
  if (nominalCellMiliVoltage > 9999)
  {
    return false;
  }
  _sysCfg.bat_nominalCellMilivoltage = nominalCellMiliVoltage;
  _sysCfg.bat_nrOfCells = nrOfCells;

  return true;
}

/**
 ****************************************************************
 @brief   Get the current battery configuration stored in the RAM _sysCfg struct.
 @param   pNominalCellMiliVoltage  Pointer to the nominal cell voltage [mV]
 @param   pNrOfCells Pointer to the number of installed battery cells.
 @return  -
 ****************************************************************
 */
void dataManager_cfgBatterySettingGet (uint32_t *pNominalCellMiliVoltage, uint32_t *pNrOfCells)
{
  *pNominalCellMiliVoltage = _sysCfg.bat_nominalCellMilivoltage;
  *pNrOfCells = _sysCfg.bat_nrOfCells;
}

/**
 ****************************************************************
 @brief   Store the name of a analog channel in the RAM _sysCfg struct
 @param   analogChannelNr One of the possible analog channels (0..3)
 @param   pAnalogChannelName  Pointer to the name to be set for this channel
 @param   len   The length of the name to be set (1..ANALOG_NAME_MAX_LEN)
 @return  Return true, if successful, otherwise false
 ****************************************************************
 @note    After all system configuration parameters has been written,
 *        these settings has to be stored in FLASH by calling dataManager_cfgStoreToFlash()
 ****************************************************************
 */
bool dataManager_cfgAnalogNameSet (uint8_t analogChannelNr, char *pAnalogChannelName, uint8_t len)
{
  char *pTarget;

  if ((len > ANALOG_NAME_MAX_LEN) || (len == 0) || (analogChannelNr > 3))
  {
    return false;
  }
  else
  {
    switch (analogChannelNr)
    {
      default:
        return false;
        break;
      case 0:
        pTarget = _sysCfg.analogChannel0Name;
        break;
      case 1:
        pTarget = _sysCfg.analogChannel1Name;
        break;
      case 2:
        pTarget = _sysCfg.analogChannel2Name;
        break;
      case 3:
        pTarget = _sysCfg.analogChannel3Name;
        break;
    }
    memcpy(pTarget, pAnalogChannelName, len);
    pTarget[len] = '\0';  //string terminator

    return true;
  }
}

/**
 ****************************************************************
 @brief   Get the current name of the analog channel stored in the RAM _sysCfg struct.
 @param   analogChannelNr One of the possible analog channels (0..3)
 @param   pAnalogChannelName  Pointer to the name of this channel
 @param   pLen   Pointer to the length of the name (1..ANALOG_NAME_MAX_LEN)
 @return  -
 ****************************************************************
 */
bool dataManager_cfgAnalogNameGet (uint8_t analogChannelNr, char *pAnalogChannelName, uint8_t *pLen)
{
  char *pSource;
  uint8_t len;

  if (analogChannelNr > 3)
  {
    return false;
  }
  else
  {
    switch (analogChannelNr)
    {
      default:
        return false;
        break;
      case 0:
        pSource = _sysCfg.analogChannel0Name;
        break;
      case 1:
        pSource = _sysCfg.analogChannel1Name;
        break;
      case 2:
        pSource = _sysCfg.analogChannel2Name;
        break;
      case 3:
        pSource = _sysCfg.analogChannel3Name;
        break;
    }
    len = strlen(pSource);
    if (len > ANALOG_NAME_MAX_LEN)
    {
      //this string length is not possible
      assert_param(0);
    }
    else if (len == 0)
    {
      //this string length is not possible
      assert_param(0);
    }
    memcpy(pAnalogChannelName, pSource, len);
    *pLen = len;

    return true;
  }
}

/**
 ****************************************************************
 @brief   Store the current system configuration in FLASH
 *        (on sysCfg memory block)
 @param   -
 @return  -
 ****************************************************************
 */
void dataManager_cfgStoreToFlash (void)
{
  const uint32_t maxMemoryBlockSize = 256;
  uint32_t flashAddr = flashStartAddr_sysConfig;
  uint32_t idx = 0;
  uint32_t sysCfgSize = sizeof(dataManager_sysConfigSmall_t);

  for (; sysCfgSize > 0;)
  {
    if (sysCfgSize > maxMemoryBlockSize)
    {
      //store configuration by memory block limitations (256Bytes)
      flash_bytesWrite(flashAddr, &_sysCfg.BYTE[idx], maxMemoryBlockSize);

      flashAddr += maxMemoryBlockSize;
      idx += maxMemoryBlockSize;
      sysCfgSize -= maxMemoryBlockSize;
    }
    else
    {
      //write last memory block/or parts of it
      flash_bytesWrite(flashAddr, &_sysCfg.BYTE[idx], sysCfgSize);

      flashAddr += sysCfgSize;
      idx += sysCfgSize;
      sysCfgSize -= sysCfgSize;
    }
  }
}

/**
 ****************************************************************
 @brief   Store the next measurement data point on the FLASH.
 @param   pDataPoint  Pointer to the to be stored measurement data.
 @return  -
 ****************************************************************
 @note    The weekday of the RTC timeStamp is not stored!
 ****************************************************************
 */
void dataManager_flashNextMeasDataPointStore (dataManager_dataPoint_t *pDataPoint)
{
  uint32_t tail;
  uint32_t tailReadOut;
  uint32_t head;
  uint32_t head_new;
  uint32_t blockNrTail;
  uint32_t blockNrTailReadOut;
  uint32_t blockNrHead;
  uint32_t blockNrHead_new;
  dataManager_dataPointSmall_t dataPointSmall;

  //get the current memory addresses for the FLASH (circular memory)
  tail = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL);
  tailReadOut = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT);
  head = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_HEAD);

  blockNrTail = tail >> 12;         //4kByte block size
  blockNrTailReadOut = tailReadOut >> 12; //4kByte block size
  blockNrHead = head >> 12;         //4kByte block size

  head_new = head + sizeof(dataManager_dataPointSmall_t);
  if (head_new >= (flashStartAddr_logData + flashSize_logData))
  {
    //wrap around
    head_new = flashStartAddr_logData;
  }
  blockNrHead_new = head_new >> 12;         //4kByte block size

  //check if there is space left between tail and new-head (not in same block)
  if ((blockNrHead_new == blockNrTail) && (blockNrHead != blockNrHead_new))
  {
    //erase 4kByte memory block (oldest data)
    flash_block4kErase(blockNrTail);

    tail = (blockNrTail + 1) << 12;
    if (tail >= (flashStartAddr_logData + flashSize_logData))
    {
      tail = flashStartAddr_logData; //wrap around
    }
    sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL, tail);

    //check if tail of readOut needs to be set new as well
    if (blockNrTailReadOut == blockNrTail)
    {
      //in the same block as old tail block number, so update it as well to new tail
      sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT, tail);
    }
  }
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_HEAD, head_new);

  //compact data first and then store them
  dataPointSmall.timeStamp.year = pDataPoint->timeStamp.year - 2000;
  dataPointSmall.timeStamp.month = pDataPoint->timeStamp.month;
  dataPointSmall.timeStamp.day = pDataPoint->timeStamp.day;
  dataPointSmall.timeStamp.hours = pDataPoint->timeStamp.hrs;
  dataPointSmall.timeStamp.minutes = pDataPoint->timeStamp.min;
  dataPointSmall.timeStamp.seconds = pDataPoint->timeStamp.sec;
  dataPointSmall.timeStamp.milliseconds = pDataPoint->timeStamp.ms;
  dataPointSmall.timeBasedMeas = pDataPoint->timeBasedMeas;

  //internal handling of NaN values for battery voltage (needed because of data compression)
  if (__isnanf(pDataPoint->vBat))
  {
    dataPointSmall.vBat = SYS_VBAT_ISNAN;
  }
  else
  {
    dataPointSmall.vBat = (uint16_t) (pDataPoint->vBat * 256.0f);                           //format 8.8, unsigned
  }

  //internal handling of NaN values for ADC_IN8 voltage (needed because of data compression)
  if (__isnanf(pDataPoint->adc_in8))
  {
    dataPointSmall.adc_in8 = ADC_IN_ISNAN;
  }
  else
  {
    dataPointSmall.adc_in8 = (uint16_t) (pDataPoint->adc_in8 * 8192.0f);                    //format 3.13, unsigned
  }

  //internal handling of NaN values for ADC_IN9 voltage (needed because of data compression)
  if (__isnanf(pDataPoint->adc_in9))
  {
    dataPointSmall.adc_in9 = ADC_IN_ISNAN;
  }
  else
  {
    dataPointSmall.adc_in9 = (uint16_t) (pDataPoint->adc_in9 * 8192.0f);                    //format 3.13, unsigned
  }

  //internal handling of NaN values for temperature (needed because of data compression)
  if (__isnanf(pDataPoint->bme280.temperature))
  {
    dataPointSmall.bme280_temperature = BME280_TEMPERATURE_ISNAN;
  }
  else
  {
    dataPointSmall.bme280_temperature = (int16_t) (pDataPoint->bme280.temperature * 256.0f);  //format 8.8, signed
  }

  dataPointSmall.bme280_pressure = pDataPoint->bme280.pressure;                             //format float

  //internal handling of NaN values for humidity (needed because of data compression)
  if (__isnanf(pDataPoint->bme280.humidity))
  {
    dataPointSmall.bme280_humidity = BME280_HUMIDITY_ISNAN;
  }
  else
  {
    dataPointSmall.bme280_humidity = (uint16_t) (pDataPoint->bme280.humidity * 512.0f);     //format 7.9, unsigned
  }

  //address check for memory location correctness
  if (head <= (flashSize_logData - sizeof(dataManager_dataPointSmall_t)))
  {
    flash_bytesWrite(head, &dataPointSmall.BYTE[0], sizeof(dataManager_dataPointSmall_t));
  }
  else
  {
    ///@TODO dataPoint could not be written, incorrect head cursor
    assert_param(0);
  }
}

/**
 ****************************************************************
 @brief   Get the number of not already read out measurement data points stored on the FLASH.
 @param   absolute  If true, the absolute number of measurement data points are returned,
 *        otherwise only the not already read out number
 @return  Return the number of measurement data points (0 ... 130'431)
 ****************************************************************
 */
uint32_t dataManager_flashNrOfMeasDataPointsGet (bool absolute)
{
  uint32_t tailAbsolute;
  uint32_t tailReadout;
  uint32_t head;
  uint32_t nrOfMeasDataPointsAbsolute;
  uint32_t nrOfMeasDataPointsReadout;

  //get the current memory addresses for the FLASH (circular memory)
  head = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_HEAD);
  tailAbsolute = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL);
  tailReadout = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT);

  //calculate the absolute number of measDataPoints
  if (head >= tailAbsolute)
  {
    nrOfMeasDataPointsAbsolute = head - tailAbsolute;
  }
  else
  {
    nrOfMeasDataPointsAbsolute = (flashSize_logData - tailAbsolute) + head;
  }
  nrOfMeasDataPointsAbsolute /= sizeof(dataManager_dataPointSmall_t);

  //calculate the newReadout number of measDataPoints
  if (head >= tailReadout)
  {
    nrOfMeasDataPointsReadout = head - tailReadout;
  }
  else
  {
    nrOfMeasDataPointsReadout = (flashSize_logData - tailReadout) + head;
  }
  nrOfMeasDataPointsReadout /= sizeof(dataManager_dataPointSmall_t);

  if (absolute)
  {
    return nrOfMeasDataPointsAbsolute;
  }
  else
  {
    //check for some error!
    if (nrOfMeasDataPointsReadout > nrOfMeasDataPointsAbsolute)
    {
      //this should never happens, otherwise there was a wrong handling with dataPointers!
      nrOfMeasDataPointsAbsolute = nrOfMeasDataPointsReadout;
    }

    return nrOfMeasDataPointsReadout;
  }
}

/**
 ****************************************************************
 @brief   Get a measurement data point out from the FLASH.
 @param   dataPointNr One of the possible dataPoint numbers (0 ... dataManager_nrOfMeasDataPointsGet())
 @param   pDataPoint  Pointer to the read out measurement data.
 @return  -
 ****************************************************************
 @note    The weekday of the RTC timeStamp is not stored!
 ****************************************************************
 */
void dataManager_flashMeasDataPointGet (uint32_t dataPointNr, dataManager_dataPoint_t *pDataPoint)
{
  uint32_t tail;
  uint32_t address;
  dataManager_dataPointSmall_t dataPointSmall;

  //get the current memory addresses for the FLASH (circular memory)
  tail = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL);

  address = tail + (sizeof(dataManager_dataPointSmall_t) * dataPointNr);
  if (address >= flashSize_logData)
  {
    address -= flashSize_logData; //wrap around
  }

  //read data
  flash_bytesRead(address, &dataPointSmall.BYTE[0], sizeof(dataManager_dataPointSmall_t));
  //and convert back the compact data system units
  pDataPoint->timeStamp.year = ((uint16_t) dataPointSmall.timeStamp.year) + 2000;
  pDataPoint->timeStamp.month = ((uint8_t) dataPointSmall.timeStamp.month);
  pDataPoint->timeStamp.day = ((uint8_t) dataPointSmall.timeStamp.day);
  pDataPoint->timeStamp.hrs = ((uint8_t) dataPointSmall.timeStamp.hours);
  pDataPoint->timeStamp.min = ((uint8_t) dataPointSmall.timeStamp.minutes);
  pDataPoint->timeStamp.sec = ((uint8_t) dataPointSmall.timeStamp.seconds);
  pDataPoint->timeStamp.ms = ((uint16_t) dataPointSmall.timeStamp.milliseconds);
  pDataPoint->timeBasedMeas = dataPointSmall.timeBasedMeas;

  //NaN number handling
  if (dataPointSmall.vBat == SYS_VBAT_ISNAN)
  {
    pDataPoint->vBat = NAN;
  }
  else
  {
    pDataPoint->vBat = ((float) dataPointSmall.vBat) / 256.0f;                            //format 8.8, unsigned
  }

  //NaN number handling
  if (dataPointSmall.adc_in8 == ADC_IN_ISNAN)
  {
    pDataPoint->adc_in8 = NAN;
  }
  else
  {
    pDataPoint->adc_in8 = ((float) dataPointSmall.adc_in8) / 8192.0f;                     //format 3.13, unsigned
  }

  //NaN number handling
  if (dataPointSmall.adc_in9 == ADC_IN_ISNAN)
  {
    pDataPoint->adc_in9 = NAN;
  }
  else
  {
    pDataPoint->adc_in9 = ((float) dataPointSmall.adc_in9) / 8192.0f;                     //format 3.13, unsigned
  }

  //NaN number handling
  if (dataPointSmall.bme280_temperature == BME280_TEMPERATURE_ISNAN)
  {
    pDataPoint->bme280.temperature = NAN;
  }
  else
  {
    pDataPoint->bme280.temperature = ((float) dataPointSmall.bme280_temperature) / 256.0f;  //format 8.8, signed
  }

  pDataPoint->bme280.pressure = dataPointSmall.bme280_pressure;                           //format float

  //NaN number handling
  if (dataPointSmall.bme280_humidity == BME280_HUMIDITY_ISNAN)
  {
    pDataPoint->bme280.humidity = NAN;
  }
  else
  {
    pDataPoint->bme280.humidity = ((float) dataPointSmall.bme280_humidity) / 512.0f;        //format 7.9, unsigned
  }
}

/**
 ****************************************************************
 @brief   Store a new event to the log data located in the corresponding FLASH section.
 @param   pTimeStamp  Pointer to the actual time information
 @param   evt One of the possible events
 @param   pAdditionalInfo Pointer to a string based additional information to be stored (max. 57 characters).
 @return  -
 ****************************************************************
 */
void dataManager_flashLogEvent (const rtcDateTime_t *const pTimeStamp, dataManager_logEvt_t evt, char *pAdditionalInfo)
{
  uint32_t tail;
  uint32_t head;
  uint32_t head_new;
  uint32_t blockNrTail;
  uint32_t blockNrHead;
  uint32_t blockNrHead_new;
  dataManager_logHistorySmall_t logHistorySmall;

  //get the current memory addresses for the FLASH (circular memory)
  tail = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGHIST_TAIL);
  head = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGHIST_HEAD);

  blockNrTail = tail >> 12;         //4kByte block size
  blockNrHead = head >> 12;         //4kByte block size

  head_new = head + sizeof(dataManager_logHistorySmall_t);
  if (head_new >= (flashStartAddr_logHist + flashSize_logHist))
  {
    //wrap around
    head_new = flashStartAddr_logHist;
  }
  blockNrHead_new = head_new >> 12; //4kByte block size

  //check if there is space left between tail and new-head
  if ((blockNrHead_new == blockNrTail) && (dataManager_flashNrOfLogEventsGet() >= 127))
  {
    //erase 4kByte memory block (oldest data)
    flash_block4kErase(blockNrTail);

    blockNrTail++;
    tail = blockNrTail << 12;
    if (tail >= (flashStartAddr_logHist + flashSize_logHist))
    {
      tail = flashStartAddr_logHist; //wrap around
    }
    sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGHIST_TAIL, tail);
  }
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGHIST_HEAD, head_new);

  //compact log informations and then store them
  logHistorySmall.timeStamp.year = pTimeStamp->year - 2000;
  logHistorySmall.timeStamp.month = pTimeStamp->month;
  logHistorySmall.timeStamp.day = pTimeStamp->day;
  logHistorySmall.timeStamp.hours = pTimeStamp->hrs;
  logHistorySmall.timeStamp.minutes = pTimeStamp->min;
  logHistorySmall.timeStamp.seconds = pTimeStamp->sec;
  logHistorySmall.timeStamp.milliseconds = pTimeStamp->ms;
  logHistorySmall.evt = (uint8_t) evt;
  strncpy(logHistorySmall.addInfo, pAdditionalInfo, sizeof(logHistorySmall.addInfo));

  //check if address is in correct range
  if ((head >= flashStartAddr_logHist)
    && (head <= ((flashStartAddr_logHist + flashSize_logHist) - sizeof(dataManager_logHistorySmall_t))))
  {
    flash_bytesWrite(head, &logHistorySmall.BYTE[0], sizeof(dataManager_logHistorySmall_t));
  }
  else
  {
    ///@TODO logEvent could not be written, incorrect head cursor
    assert_param(0);
  }
}

/**
 ****************************************************************
 @brief   Get the number of log events in the stored corresponding section on the FLASH.
 @param   -
 @return  Return the number of log events (0 ... 127)
 ****************************************************************
 */
uint32_t dataManager_flashNrOfLogEventsGet (void)
{
  uint32_t tail;
  uint32_t head;
  uint32_t nrOfLogEvents;

  //get the current memory addresses for the FLASH (circular memory)
  tail = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGHIST_TAIL);
  head = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGHIST_HEAD);

  if (head >= tail)
  {
    nrOfLogEvents = head - tail;
  }
  else
  {
    nrOfLogEvents = (flashSize_logHist - tail) + head;
  }
  nrOfLogEvents /= sizeof(dataManager_logHistorySmall_t);

  return nrOfLogEvents;
}

/**
 ****************************************************************
 @brief   Check if an other firmware version for this device is available
 *        on the SDCard.
 @param   pFileName The resulting path/filename of the new firmware image (max. 128Byte)
 @return  True if a other image was found, otherwise false
 ****************************************************************
 */
bool dataManager_sdCardCheckForNewFirmwareFile (char *pFileName)
{
  uint32_t idx;
  FRESULT fr;
  fwVersion_t fwVersion;
  char fileNamePattern[128];  //deviceName is limited to 92Byte
  bool otherVersionFound = false;
  bool otherVersion;
  uint8_t len;

  //search for binary files in firmware directory that matches the device name
  //get current firmware version
  dataLogger_fwVersionGet(&fwVersion);

  idx = 0;

  //add device name
  dataManager_cfgDeviceNameGet(&fileNamePattern[0], &len);
  idx += len;
  memcpy(&fileNamePattern[idx], "_v?_?_?.bin", 11);
  idx += 11;
  fileNamePattern[idx] = '\0';  //string termination

  if (!_dataManager_sdCardMount())
  {
    return false;
  }

  memcpy(&pFileName[0], "Firmware\\", 9);
  //start search for a matching file in Firmware directory
  fr = f_findfirst(&dj, &fno, "Firmware", fileNamePattern);

  //Repeat through all items that matches
  while ((fr == FR_OK) && (fno.fname[0]))
  {
    otherVersion = false;

    //check if this file is a other version
    // major
    if (fno.fname[idx - 9] != fwVersion.major)
    {
      otherVersion = true;
    }
    // minor
    else if (fno.fname[idx - 7] != fwVersion.minor)
    {
      otherVersion = true;
    }
    // patch
    else if (fno.fname[idx - 5] != fwVersion.patch)
    {
      otherVersion = true;
    }

    if (otherVersion)
    {
      fwVersion.major = fno.fname[idx - 9];
      fwVersion.minor = fno.fname[idx - 7];
      fwVersion.patch = fno.fname[idx - 5];

      otherVersionFound = true;

      //generate pathAndFileName
      strcpy(&pFileName[9], fno.fname);
    }
    fr = f_findnext(&dj, &fno); //Search for next item
  }
  f_closedir(&dj);

  _dataManager_sdCardUnmount();

  if (otherVersionFound)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   Store all not already readout measurement data from the FLASH to different files on the SD-Card.
 @param   -
 @return  True on success, otherwise false
 ****************************************************************
 */
bool dataManager_sdCardDataStore (void)
{
  uint32_t idx;
  rtcDateTime_t rtcDateTime;
  uint32_t nrOfNewDataPoints = dataManager_flashNrOfMeasDataPointsGet(false);
  uint32_t nrOfDataPoints = dataManager_flashNrOfMeasDataPointsGet(true);
  uint8_t len;
  bool success = true;
  char logText[48];

  //sleep for 50ms (toggle of sdCard-Detection GPIO signal
  sys_sleepForUs(50e3);

  //check if a SC-Card is inserted in the slot
  if (digIO_sdCardDetected())
  {
    if (!_dataManager_sdCardMount())
    {
      //unmount is called in mount function itself if an error occurs
      return false;
    }

    sys_rtcDateTimeGet(&rtcDateTime);

    idx = 0;

    //get the deviceName
    dataManager_cfgDeviceNameGet(&path[idx], &len);
    idx += len;

    //extend path by file ending
    path[idx++] = '.';
    path[idx++] = 'c';
    path[idx++] = 's';
    path[idx++] = 'v';
    path[idx] = '\0'; //string termination
    assert_param(idx < sizeof(path));

    if (!_dataManager_sdCardMeasDataFileStore(path, nrOfDataPoints, nrOfNewDataPoints))
    {
      //rewind data cursors to the start, if a error occurs
      dataManager_flashRewindDataReadoutCursor();

      sprintf(logText, "fail on *.csv: abs: %06u, new: %06u", (unsigned int) nrOfDataPoints,
        (unsigned int) nrOfNewDataPoints);
      dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_DATAREADOUT, logText);

      success = false;
    }

    //create the metaData file path
    idx -= 3;
    //extend path by file ending
    path[idx++] = 't';
    path[idx++] = 'x';
    path[idx] = 't';
    assert_param(idx < sizeof(path));

    if (!_dataManager_sdCardMetaDataFileStore(path))
    {
      dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_DATAREADOUT, "fail on *.txt");

      success = false;
    }

    //create the logData file path
    idx -= 2;
    //extend path by file ending
    path[idx++] = 'l';
    path[idx++] = 'o';
    path[idx] = 'g';
    assert_param(idx < sizeof(path));

    //save the timeStamp of this read out, save in log history if not allready marked with an error for this readout
    if (success)
    {
      sprintf(logText, "success: abs: %06u, new: %06u", (unsigned int) nrOfDataPoints,
        (unsigned int) nrOfNewDataPoints);
      dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_DATAREADOUT, logText);
    }

    if (!_dataManager_sdCardLogDataFileStore(path))
    {
      dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_DATAREADOUT, "fail on *.log");

      success = false;
    }

    _dataManager_sdCardUnmount();

    return success;
  }
  else
  {
    return false; //no SDCard detected
  }
}

/**
 ****************************************************************
 @brief   Read out all data on the FLASH and store them on the SD-Card.
 @param   -
 @return  True on success, otherwise false
 ****************************************************************
 @note    The informations are stored as hex-binary data in this format:
 @note    To dump the complete memory, it takes around 16Minutes!
 *          Details see in: datalogger_firmware\test\WABEsense_memoryDump.xlsx
 ****************************************************************
 */
bool dataManager_sdCardMemoryDump (void)
{
  UINT bytesLeftToWrite = 0;
  FRESULT iFResult;
  uint8_t len;
  uint32_t address;
  uint32_t idx;
  uint32_t nrOfBytesRead;

  //sleep for 50ms (toggle of sdCard-Detection GPIO signal
  sys_sleepForUs(50e3);

  //check if a SC-Card is inserted in the slot
  if (digIO_sdCardDetected())
  {
    if (!_dataManager_sdCardMount())
    {
      //unmount is called in mount function itself if an error occurs
      return false;
    }

    idx = 0;

    //get the deviceName
    dataManager_cfgDeviceNameGet(&path[idx], &len);
    idx += len;

    //extend path by file ending
    path[idx++] = '.';
    path[idx++] = 'd';
    path[idx++] = 'm';
    path[idx++] = 'p';
    path[idx] = '\0'; //string termination
    assert_param(idx < sizeof(path));

    //always create a new .dmp-File
    iFResult = f_open(&file, path, FA_CREATE_ALWAYS | FA_WRITE);
    if ((iFResult != FR_OK) && (iFResult != FR_EXIST))
    {
      return false;
    }

    //dump the complete FLASH memory line by line into the <deviceName>.dmp-File
    for (address = 0; address < 0x00400000;)
    {
      //read next data block (8192 blocks of each 512Bytes is read from FLASH)
      nrOfBytesRead = SD_CARD_BLOCK_SIZE;
      flash_bytesRead(address, _flashReadData, nrOfBytesRead);

      idx = 0;
      for (; nrOfBytesRead; nrOfBytesRead -= 16)
      {

        //write next line to .dmp-File (complete file have 262'144 lines)
        bytesLeftToWrite += (UINT) snprintf(&sdDataBuffer[bytesLeftToWrite], SD_CARD_BLOCK_SIZE,
          "%06x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n",
          (unsigned int) address, _flashReadData[idx], _flashReadData[idx + 1], _flashReadData[idx + 2],
          _flashReadData[idx + 3], _flashReadData[idx + 4], _flashReadData[idx + 5], _flashReadData[idx + 6],
          _flashReadData[idx + 7], _flashReadData[idx + 8], _flashReadData[idx + 9], _flashReadData[idx + 10],
          _flashReadData[idx + 11], _flashReadData[idx + 12], _flashReadData[idx + 13], _flashReadData[idx + 14],
          _flashReadData[idx + 15]);

        address += 16;
        idx += 16;

        //write data, but only if they are reaching the block size
        if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
        {
          return false;
        }
      }
    }

    //write remaining data anyway
    if (!_dataManager_sdCardWriteBlock(true, &bytesLeftToWrite))
    {
      return false;
    }

    //close .dmp data file
    iFResult = f_close(&file);
    if (iFResult != FR_OK)
    {
      return false;
    }

    _dataManager_sdCardUnmount();

    return true;
  }
  else
  {
    return false; //no SDCard detected
  }
}

/**
 ****************************************************************
 @brief   Internally function to mount the sdCard memory, init the
 *        FatFileSystem and turn on the 3v3 to the SD-Card.
 @param   -
 @return  Return true on success, otherwise false
 ****************************************************************
 */
static bool _dataManager_sdCardMount (void)
{
  FRESULT iFResult;

  MX_FATFS_Init();

  //switch the power on
  digIO_3V3PwrOn(DIG_IO_3V3UNIT_SDCARD);
  //a minimum of 250ms is needed before chip could be selected
  sys_sleepForUs(250e3);

  //Mount the file system immediately, using logical disk 0
  iFResult = f_mount(&fs, "", 1);
  if (iFResult != FR_OK)
  {
    _dataManager_sdCardUnmount();

    return false;
  }
  return true;
}

/**
 ****************************************************************
 @brief   Internally function to unmount the sdCard memory, deInit the
 *        FatFileSystem and request to turn off the 3v3 to the SD-Card.
 @param   -
 @return  -
 ****************************************************************
 */
static void _dataManager_sdCardUnmount (void)
{
  //unmount drive
  f_mount(0, "", 0);

  FATFS_DeInit();

  //request to switch the power off
  digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_SDCARD);
}

/**
 ****************************************************************
 @brief   Internally function to build the measurementData file and store it on the SD-Card.
 @param   pPath Pointer to the path of the file (including the file name/type)
 @param   nrOfDataPoints     The absolute number of available dataPoints
 @param   nrOfNewDataPoints  The number of new available dataPoints (if 0: only header will be stored)
 @return  -
 ****************************************************************
 @note    If nrOfDataPoints == nrOfNewDataPoints, a new file will be created and everything will be stored
 ****************************************************************
 */
static bool _dataManager_sdCardMeasDataFileStore (TCHAR *pPath, uint32_t nrOfDataPoints, uint32_t nrOfNewDataPoints)
{
  uint32_t dataPointNr;
  dataManager_dataPoint_t dataPoint;
  UINT bytesLeftToWrite = 0;
  uint32_t strLen;
  FRESULT iFResultExists = FR_NO_FILE;
  FRESULT iFResult;
  uint8_t len;
  uint32_t tailReadOut;

  if (nrOfDataPoints == nrOfNewDataPoints)
  {
    //always create a new .csv-File
    iFResult = f_open(&file, pPath, FA_CREATE_ALWAYS | FA_WRITE);
    if ((iFResult != FR_OK) && (iFResult != FR_EXIST))
    {
      return false;
    }
  }
  else
  {
    //create a new .csv-File only if not already exists, otherwise append new data
    iFResultExists = f_stat(pPath, &fno);
    iFResult = f_open(&file, pPath, FA_OPEN_APPEND | FA_WRITE);
    if ((iFResult != FR_OK) && (iFResult != FR_EXIST))
    {
      return false;
    }
  }

  //only write header if file not already exists
  if (iFResultExists == FR_NO_FILE)
  {
    //write header line (logData build up)
    strcpy(&sdDataBuffer[bytesLeftToWrite],
      "RTC_Datetime(YYYY.mm.dd HH:MM:SS.MS+0000);Measurement_Auto;Battery_Voltage(V);BME_Temperature(C);BME_Pressure(Pa);BME_Humidity(%%);");
    bytesLeftToWrite += strlen(sdDataBuffer);

    //append by dynamic header
    //analog channel0 (ADC_IN8 in schematic)
    len = strlen(_sysCfg.analogChannel0Name);
    memcpy(&sdDataBuffer[bytesLeftToWrite], _sysCfg.analogChannel0Name, len);
    bytesLeftToWrite += len;
    sdDataBuffer[bytesLeftToWrite++] = ';';
    //analog channel1 (ADC_IN9 in schematic)
    len = strlen(_sysCfg.analogChannel1Name);
    memcpy(&sdDataBuffer[bytesLeftToWrite], _sysCfg.analogChannel1Name, len);
    bytesLeftToWrite += len;
    sdDataBuffer[bytesLeftToWrite++] = '\r';
    sdDataBuffer[bytesLeftToWrite++] = '\n';

    //write data, but only if they are reaching the block size
    if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
    {
      return false;
    }

    //store all datapoint on it, even if these are not new once!
    nrOfNewDataPoints = nrOfDataPoints;
  }

  if (nrOfNewDataPoints)
  {
    //write .csv data file
    dataPointNr = nrOfDataPoints - nrOfNewDataPoints;
    do
    {
      //write next line to .csv-File
      dataManager_flashMeasDataPointGet(dataPointNr, &dataPoint);
      //format of data: year.Month.Day;Hrs:Min:Sec.MS;Boolean
      bytesLeftToWrite += (UINT) snprintf(&sdDataBuffer[bytesLeftToWrite], SD_CARD_BLOCK_SIZE,
        "%04u.%02u.%02u %02u:%02u:%02u.%03u;%d;", dataPoint.timeStamp.year, dataPoint.timeStamp.month,
        dataPoint.timeStamp.day, dataPoint.timeStamp.hrs, dataPoint.timeStamp.min, dataPoint.timeStamp.sec,
        dataPoint.timeStamp.ms, dataPoint.timeBasedMeas);

      //convert floating point numbers to strings
      //vBat(format x.6);BME_T(format x.6);BME_P(format x.6);BME_H(format x.6);ADC_IN8(format x.6);ADC_IN9(format x.6);
      strConv_floatToString_6(dataPoint.vBat, &sdDataBuffer[bytesLeftToWrite], &strLen);
      bytesLeftToWrite += strLen;
      sdDataBuffer[bytesLeftToWrite++] = ';';
      strConv_floatToString_6(dataPoint.bme280.temperature, &sdDataBuffer[bytesLeftToWrite], &strLen);
      bytesLeftToWrite += strLen;
      sdDataBuffer[bytesLeftToWrite++] = ';';
      strConv_floatToString_6(dataPoint.bme280.pressure, &sdDataBuffer[bytesLeftToWrite], &strLen);
      bytesLeftToWrite += strLen;
      sdDataBuffer[bytesLeftToWrite++] = ';';
      strConv_floatToString_6(dataPoint.bme280.humidity, &sdDataBuffer[bytesLeftToWrite], &strLen);
      bytesLeftToWrite += strLen;
      sdDataBuffer[bytesLeftToWrite++] = ';';
      strConv_floatToString_6(dataPoint.adc_in8, &sdDataBuffer[bytesLeftToWrite], &strLen);
      bytesLeftToWrite += strLen;
      sdDataBuffer[bytesLeftToWrite++] = ';';
      strConv_floatToString_6(dataPoint.adc_in9, &sdDataBuffer[bytesLeftToWrite], &strLen);
      bytesLeftToWrite += strLen;
      sdDataBuffer[bytesLeftToWrite++] = '\r';
      sdDataBuffer[bytesLeftToWrite++] = '\n';

      //write data, but only if they are reaching the block size
      if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
      {
        return false;
      }
      dataPointNr++;
    } while (dataPointNr != nrOfDataPoints);
  }

  //write remaining data anyway
  if (!_dataManager_sdCardWriteBlock(true, &bytesLeftToWrite))
  {
    return false;
  }

  //close .csv data file
  iFResult = f_close(&file);
  if (iFResult != FR_OK)
  {
    return false;
  }

  //set the readout flash data pointer (tail) to the head
  tailReadOut = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGDATA_HEAD);
  sys_backupRegisterSet(SYS_BACKUP_REG_FLASH_LOGDATA_TAIL_READOUT, tailReadOut);

  return true;
}

/**
 ****************************************************************
 @brief   Internally function to build the metaData file and store it on the SD-Card.
 @param   pPath Pointer to the path of the file (including the file name/type)
 @return  -
 ****************************************************************
 */
static bool _dataManager_sdCardMetaDataFileStore (TCHAR *pPath)
{
  sysID_t sysID;
  fwVersion_t fwVersion;
  UINT bytesLeftToWrite = 0;
  FRESULT iFResult;
  uint8_t len;

  //create this file all time new
  iFResult = f_open(&file, pPath, FA_CREATE_ALWAYS | FA_WRITE);
  if ((iFResult != FR_OK) && (iFResult != FR_EXIST))
  {
    return false;
  }

  //write meta data file content (.txt file)
  //line: device name
  strcpy(&sdDataBuffer[bytesLeftToWrite], "Device name: ");
  bytesLeftToWrite += 13;

  dataManager_cfgDeviceNameGet(&sdDataBuffer[bytesLeftToWrite], &len);
  bytesLeftToWrite += len;
  sdDataBuffer[bytesLeftToWrite++] = '\r';
  sdDataBuffer[bytesLeftToWrite++] = '\n';
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: timeBasedMeasurementActive
  if (dataLogger_measAutoCycleActivated())
  {
    strcpy(&sdDataBuffer[bytesLeftToWrite], "Time based measurement: enabled\r\n");
    bytesLeftToWrite += strlen("Time based measurement: enabled\r\n");
  }
  else
  {
    strcpy(&sdDataBuffer[bytesLeftToWrite], "Time based measurement: disabled\r\n");
    bytesLeftToWrite += strlen("Time based measurement: disabled\r\n");
  }
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: measurement interval
  bytesLeftToWrite += (UINT) snprintf(&sdDataBuffer[bytesLeftToWrite], SD_CARD_BLOCK_SIZE,
    "Measurement interval (minutes): %u\r\n", _sysCfg.measInterval_minutes);
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: battery nominal voltage
  bytesLeftToWrite += (UINT) snprintf(&sdDataBuffer[bytesLeftToWrite], SD_CARD_BLOCK_SIZE,
    "Battery nominal voltage (mV): %u\r\n", _sysCfg.bat_nominalCellMilivoltage);
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: battery number of cells
  bytesLeftToWrite += (UINT) snprintf(&sdDataBuffer[bytesLeftToWrite], SD_CARD_BLOCK_SIZE,
    "Battery number of cells: %u\r\n", _sysCfg.bat_nrOfCells);
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: analog input channel name mappings
  strcpy(&sdDataBuffer[bytesLeftToWrite], "Analog channel mappings:\r\n");
  bytesLeftToWrite += 26;
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: analog input channel0 name mapping
  strcpy(&sdDataBuffer[bytesLeftToWrite], " ADC_IN8 (HW): ");
  bytesLeftToWrite += 15;
  len = strlen(_sysCfg.analogChannel0Name);
  memcpy(&sdDataBuffer[bytesLeftToWrite], _sysCfg.analogChannel0Name, len);
  bytesLeftToWrite += len;
  sdDataBuffer[bytesLeftToWrite++] = '\r';
  sdDataBuffer[bytesLeftToWrite++] = '\n';
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: analog input channel1 name mapping
  strcpy(&sdDataBuffer[bytesLeftToWrite], " ADC_IN9 (HW): ");
  bytesLeftToWrite += 15;
  len = strlen(_sysCfg.analogChannel1Name);
  memcpy(&sdDataBuffer[bytesLeftToWrite], _sysCfg.analogChannel1Name, len);
  bytesLeftToWrite += len;
  sdDataBuffer[bytesLeftToWrite++] = '\r';
  sdDataBuffer[bytesLeftToWrite++] = '\n';
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: analog input channel2 name mapping
  strcpy(&sdDataBuffer[bytesLeftToWrite], " ADC_IN10 (HW): ");
  bytesLeftToWrite += 16;
  len = strlen(_sysCfg.analogChannel2Name);
  memcpy(&sdDataBuffer[bytesLeftToWrite], _sysCfg.analogChannel2Name, len);
  bytesLeftToWrite += len;
  sdDataBuffer[bytesLeftToWrite++] = '\r';
  sdDataBuffer[bytesLeftToWrite++] = '\n';
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: analog input channel3 name mapping
  strcpy(&sdDataBuffer[bytesLeftToWrite], " ADC_IN11 (HW): ");
  bytesLeftToWrite += 16;
  len = strlen(_sysCfg.analogChannel3Name);
  memcpy(&sdDataBuffer[bytesLeftToWrite], _sysCfg.analogChannel3Name, len);
  bytesLeftToWrite += len;
  sdDataBuffer[bytesLeftToWrite++] = '\r';
  sdDataBuffer[bytesLeftToWrite++] = '\n';
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: firmware version
  dataLogger_fwVersionGet(&fwVersion);
  bytesLeftToWrite += (UINT) snprintf(&sdDataBuffer[bytesLeftToWrite], SD_CARD_BLOCK_SIZE,
    "Firmware version: %c.%c.%c\r\n", fwVersion.major, fwVersion.minor, fwVersion.patch);
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: unique identification number
  sys_uniqueIdGet(&sysID);
  bytesLeftToWrite += (UINT) snprintf(&sdDataBuffer[bytesLeftToWrite], SD_CARD_BLOCK_SIZE,
    "Unique ID: 0x%08X%08X%08X\r\n", sysID.systemID[0], sysID.systemID[1], sysID.systemID[2]);
  //write remaining data anyway
  if (!_dataManager_sdCardWriteBlock(true, &bytesLeftToWrite))
  {
    return false;
  }

  //close metaData file
  iFResult = f_close(&file);
  if (iFResult != FR_OK)
  {
    return false;
  }
  return true;
}

/**
 ****************************************************************
 @brief   Internally function to build the logData file and store it on the SD-Card.
 @param   pPath Pointer to the path of the file (including the file name/type)
 @return  -
 ****************************************************************
 */
static bool _dataManager_sdCardLogDataFileStore (TCHAR *pPath)
{
  uint32_t tail;
  uint32_t head;
  FRESULT iFResult;
  UINT bytesLeftToWrite = 0;
  dataManager_logHistorySmall_t logHistorySmall;
  uint32_t nrOfMeasDataPoints;
  dataManager_dataPoint_t dataPoint;
  char evtTxt[32];
  uint32_t nrOfLogDataPoints;

  //create this file all time new
  iFResult = f_open(&file, pPath, FA_CREATE_ALWAYS | FA_WRITE);
  if ((iFResult != FR_OK) && (iFResult != FR_EXIST))
  {
    return false;
  }

  //line: info about csv-file (measurement data) - header
  strcpy(&sdDataBuffer[bytesLeftToWrite], "csv:\r\n");
  bytesLeftToWrite += 6;
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  nrOfMeasDataPoints = dataManager_flashNrOfMeasDataPointsGet(true);

  //line: info about .csv-file (measurement data) - time of first measurement dataPoint
  if (nrOfMeasDataPoints)
  {
    dataManager_flashMeasDataPointGet(0, &dataPoint);
  }
  else
  {
    dataPoint.timeStamp.year = 2000;
    dataPoint.timeStamp.month = 1;
    dataPoint.timeStamp.day = 1;
    dataPoint.timeStamp.hrs = 0;
    dataPoint.timeStamp.min = 0;
    dataPoint.timeStamp.sec = 0;
  }
  bytesLeftToWrite += (UINT) snprintf(&sdDataBuffer[bytesLeftToWrite], SD_CARD_BLOCK_SIZE,
    " start: %04u.%02u.%02u %02u:%02u:%02u.%03u\r\n", dataPoint.timeStamp.year, dataPoint.timeStamp.month,
    dataPoint.timeStamp.day, dataPoint.timeStamp.hrs, dataPoint.timeStamp.min, dataPoint.timeStamp.sec,
    dataPoint.timeStamp.ms);
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: info about csv-file (measurement data) - time of last measurement dataPoint
  if (nrOfMeasDataPoints)
  {
    dataManager_flashMeasDataPointGet(nrOfMeasDataPoints - 1, &dataPoint);
  }
  else
  {
    //nothing to do, dataPoint.timeStamp already set above
  }
  bytesLeftToWrite += (UINT) snprintf(&sdDataBuffer[bytesLeftToWrite], SD_CARD_BLOCK_SIZE,
    " stop: %04u.%02u.%02u %02u:%02u:%02u.%03u\r\n", dataPoint.timeStamp.year, dataPoint.timeStamp.month,
    dataPoint.timeStamp.day, dataPoint.timeStamp.hrs, dataPoint.timeStamp.min, dataPoint.timeStamp.sec,
    dataPoint.timeStamp.ms);
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: info about .csv-file (measurement data) - number of dataPoints
  bytesLeftToWrite += (UINT) snprintf(&sdDataBuffer[bytesLeftToWrite], SD_CARD_BLOCK_SIZE, " samples: %u\r\n",
    (unsigned int) nrOfMeasDataPoints);
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //line: logData read out history information
  strcpy(&sdDataBuffer[bytesLeftToWrite], "\r\nlog:\r\n");
  bytesLeftToWrite += 8;
  //write data, but only if they are reaching the block size
  if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
  {
    return false;
  }

  //write logHistory informations as well
  //get the current memory addresses for the FLASH (circular memory)
  tail = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGHIST_TAIL);
  head = sys_backupRegisterGet(SYS_BACKUP_REG_FLASH_LOGHIST_HEAD);

  nrOfLogDataPoints = dataManager_flashNrOfLogEventsGet();
  if (nrOfLogDataPoints)
  {
    if (nrOfLogDataPoints > 100)
    {
      //increase tail, that only 100 logDataPoints left (newest 100)
      tail += (sizeof(dataManager_logHistorySmall_t) * (nrOfLogDataPoints - 100));
      if (tail >= (flashStartAddr_logHist + flashSize_logHist))
      {
        tail -= flashSize_logHist; //wrap around
      }
    }

    do
    {
      flash_bytesRead(tail, &logHistorySmall.BYTE[0], sizeof(dataManager_logHistorySmall_t));

      // event text information
      switch (logHistorySmall.evt)
      {
        default:
          strcpy(evtTxt, "unknown");
          break;
        case LOG_EVT_DATAREADOUT:
          strcpy(evtTxt, "readout");
          break;
        case LOG_EVT_SYSRESTART:
          strcpy(evtTxt, "sysRestart");
          break;
        case LOG_EVT_FW_UPDATE:
          strcpy(evtTxt, "fwUpdate");
          break;
        case LOG_EVT_BME_ERROR:
          strcpy(evtTxt, "bmeError");
          break;
        case LOG_EVT_RTCBATTERY:
          strcpy(evtTxt, "rtcBattery");
          break;
        case LOG_EVT_CONFIGSTORE:
          strcpy(evtTxt, "configStore");
          break;
        case LOG_EVT_CLEARDATA:
          strcpy(evtTxt, "clearData");
          break;
        case LOG_EVT_AUTOMEAS:
          strcpy(evtTxt, "autoMeas");
          break;
      }

      //logHistory information
      bytesLeftToWrite += (UINT) snprintf(&sdDataBuffer[bytesLeftToWrite], SD_CARD_BLOCK_SIZE,
        " - %04u.%02u.%02u %02u:%02u:%02u.%03u, %s", logHistorySmall.timeStamp.year + 2000,
        logHistorySmall.timeStamp.month, logHistorySmall.timeStamp.day, logHistorySmall.timeStamp.hours,
        logHistorySmall.timeStamp.minutes, logHistorySmall.timeStamp.seconds, logHistorySmall.timeStamp.milliseconds,
        evtTxt);

      if (strlen(logHistorySmall.addInfo))
      {
        bytesLeftToWrite += (UINT) snprintf(&sdDataBuffer[bytesLeftToWrite], SD_CARD_BLOCK_SIZE, " %s\r\n",
          logHistorySmall.addInfo);
      }
      else
      {
        //only add the carriage return and newline
        sdDataBuffer[bytesLeftToWrite++] = '\r';
        sdDataBuffer[bytesLeftToWrite++] = '\n';
      }

      //write data, but only if they are reaching the block size
      if (!_dataManager_sdCardWriteBlock(false, &bytesLeftToWrite))
      {
        return false;
      }

      tail += sizeof(dataManager_logHistorySmall_t);
      if (tail >= (flashStartAddr_logHist + flashSize_logHist))
      {
        tail -= flashSize_logHist; //wrap around
      }
    } while (tail != head);
  }

  //write remaining data anyway
  if (!_dataManager_sdCardWriteBlock(true, &bytesLeftToWrite))
  {
    return false;
  }

  //close logData file
  iFResult = f_close(&file);
  if (iFResult != FR_OK)
  {
    return false;
  }
  return true;
}

/**
 ****************************************************************
 @brief   Write a block (512 bytes) of data to the current opened file on the SD-Card.
 @param   forceWrite  If true, the data needs to be written, even they are shorter than the block size
 @param   pBytesLeftToWrite  Pointer to the number of requested bytes to be written and the resulting left bytes to be written
 @return  True on success, otherwise false
 ****************************************************************
 */
static bool _dataManager_sdCardWriteBlock (bool forceWrite, UINT *pBytesLeftToWrite)
{
  FRESULT iFResult;
  UINT nrOfBytesToWrite;
  UINT nrOfBytesWritten;

  nrOfBytesToWrite = *pBytesLeftToWrite;
  if (nrOfBytesToWrite >= SD_CARD_BLOCK_SIZE)
  {
    //do the write of a complete block
    iFResult = f_write(&file, &sdDataBuffer[0], SD_CARD_BLOCK_SIZE, &nrOfBytesWritten);

    if ((iFResult != FR_OK) || (nrOfBytesWritten != SD_CARD_BLOCK_SIZE))
    {
      return false;
    }
    nrOfBytesToWrite -= nrOfBytesWritten;

    //copy others to be written data to the beginning of the SD-DataBuffer
    memcpy(&sdDataBuffer[0], &sdDataBuffer[SD_CARD_BLOCK_SIZE], nrOfBytesToWrite);
  }

  if (forceWrite && nrOfBytesToWrite)
  {
    //check if remaining data needs to be written anyway
    iFResult = f_write(&file, &sdDataBuffer[0], nrOfBytesToWrite, &nrOfBytesWritten);
    if ((iFResult != FR_OK) || (nrOfBytesWritten != nrOfBytesToWrite))
    {
      return false;
    }
    nrOfBytesToWrite -= nrOfBytesWritten;
  }
  *pBytesLeftToWrite = nrOfBytesToWrite;

  return true;
}

/**
 * @}
 */

/**
 * @}
 */
