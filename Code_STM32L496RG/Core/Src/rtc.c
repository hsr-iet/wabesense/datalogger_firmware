/**
  ******************************************************************************
  * File Name          : RTC.c
  * Description        : This file provides code for the configuration
  *                      of the RTC instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "rtc.h"

/* USER CODE BEGIN 0 */
#include <assert.h>
#include <stdbool.h>

static bool _rtcPowerUpReconfig = false;
static float _rtcSubSecondGranularity_ms;
static bool _rtcWakeUpTimerEvt = false;
/* USER CODE END 0 */

RTC_HandleTypeDef hrtc;

/* RTC init function */
void MX_RTC_Init(void)
{
  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};

  /** Initialize RTC Only 
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */
  RTC_TimeTypeDef timeStamp;
  HAL_RTC_GetTime(&hrtc, &timeStamp, RTC_FORMAT_BIN);

  _rtcSubSecondGranularity_ms = 1000.0f / (float) (timeStamp.SecondFraction + 1);
  if (__HAL_RTC_WAKEUPTIMER_GET_FLAG(&hrtc, RTC_FLAG_INITS))
  {
    //calendar is already initialized and powered at least from CR2032 battery
    _rtcPowerUpReconfig = false;

    return;
  }
  else
  {
    //Backup registers are not valid anymore, they must be recalculated (done in dataManager.c)
    _rtcPowerUpReconfig = true;
  }
  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date 
  */
  sTime.Hours = 0x8;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_WEDNESDAY;
  sDate.Month = RTC_MONTH_SEPTEMBER;
  sDate.Date = 0x15;
  sDate.Year = 0x21;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_RTC_MspInit(RTC_HandleTypeDef* rtcHandle)
{

  if(rtcHandle->Instance==RTC)
  {
  /* USER CODE BEGIN RTC_MspInit 0 */

  /* USER CODE END RTC_MspInit 0 */
    /* RTC clock enable */
    __HAL_RCC_RTC_ENABLE();

    /* RTC interrupt Init */
    HAL_NVIC_SetPriority(RTC_WKUP_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);
  /* USER CODE BEGIN RTC_MspInit 1 */

  /* USER CODE END RTC_MspInit 1 */
  }
}

void HAL_RTC_MspDeInit(RTC_HandleTypeDef* rtcHandle)
{

  if(rtcHandle->Instance==RTC)
  {
  /* USER CODE BEGIN RTC_MspDeInit 0 */

  /* USER CODE END RTC_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_RTC_DISABLE();

    /* RTC interrupt Deinit */
    HAL_NVIC_DisableIRQ(RTC_WKUP_IRQn);
  /* USER CODE BEGIN RTC_MspDeInit 1 */

  /* USER CODE END RTC_MspDeInit 1 */
  }
} 

/* USER CODE BEGIN 1 */
/**
 ****************************************************************
 @brief   Initialize the RTC unit (only enable clock gating and interrupts).
 @param   -
 @return  -
 ****************************************************************
 */
void HAL_RTC_init (void)
{
  RTC_TimeTypeDef timeStamp;

  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;

  hrtc.State = HAL_RTC_STATE_RESET;

  //Allocate lock resource and initialize it
  hrtc.Lock = HAL_UNLOCKED;

  //RTC clock enable
  __HAL_RCC_RTC_ENABLE();

  //RTC interrupt Init
  HAL_NVIC_SetPriority(RTC_WKUP_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);

  hrtc.State = HAL_RTC_STATE_READY;

  HAL_RTC_GetTime(&hrtc, &timeStamp, RTC_FORMAT_BIN);
  _rtcSubSecondGranularity_ms = 1000.0f / (float) (timeStamp.SecondFraction + 1);
}

/**
 ****************************************************************
 @brief   DeInitialize the RTC unit (only disable interrupts).
 @param   -
 @return  -
 ****************************************************************
 */
void HAL_RTC_deInit (void)
{
  HAL_NVIC_DisableIRQ(RTC_WKUP_IRQn);
}

/**
 ****************************************************************
 @brief   Check if the RTC module detects a powerUp and reconfiguration
 *        This means, all backup-registers where reset to zero and must be recalculated if possible
 @param   -
 @return  The value of the _rtcPowerUpReconfig flag (true, if a powerUp from zero power was detected)
 ****************************************************************
 */
bool HAL_RTC_PowerUpReconfigGetAndClear (void)
{
  bool retValue = _rtcPowerUpReconfig;

  _rtcPowerUpReconfig = false;
  return retValue;
}
/**
 ****************************************************************
 @brief   Check if a wakeUp timer event is pending and clear this flag directly.
 @param   -
 @return  Return true if a pending wakeUp Timer event was detected.
 ****************************************************************
 */
bool HAL_RTC_WakeUpEvtGetAndClear (void)
{
  bool wakeUpTimerEvt = _rtcWakeUpTimerEvt;

  if (wakeUpTimerEvt)
  {
    _rtcWakeUpTimerEvt = false;
    return true;
  }
  //check if masked wakeup timer interrupt flag was set
  if (__HAL_RTC_WAKEUPTIMER_GET_FLAG(&hrtc, RTC_FLAG_WUTF))
  {
    //Clear the WAKEUPTIMER interrupt pending bit
    __HAL_RTC_WAKEUPTIMER_CLEAR_FLAG(&hrtc, RTC_FLAG_WUTF);

    return true;
  }
  return false;
}

/**
 ****************************************************************
 @brief   Check if the wakeUpTimer is active or not
 @param   -
 @return  Return true, if the timer is activated, otherwise false
 ****************************************************************
 */
bool HAL_RTC_WakeUpTimerActivated (void)
{
  if (hrtc.Instance->CR & RTC_CR_WUTE)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/**
 ****************************************************************
 @brief   Set the real time calibration correction (0 to 511
 *        added or substitute clocks during this 32second window)
 @param   nrOfPulses  The number of pulses to add/sub during the 32 second window
 @param   substitute  If true, the above nrOfPulses are substitute, otherwise added
 @return  -
 ****************************************************************
 */
void HAL_RTC_SmoothCalibrationSet (uint32_t nrOfPulses, bool substitute)
{
  if (substitute)
  {
    HAL_RTCEx_SetSmoothCalib(&hrtc, RTC_SMOOTHCALIB_PERIOD_32SEC, RTC_SMOOTHCALIB_PLUSPULSES_RESET, nrOfPulses);
  }
  else
  {
    nrOfPulses = 512 - nrOfPulses;
    HAL_RTCEx_SetSmoothCalib(&hrtc, RTC_SMOOTHCALIB_PERIOD_32SEC, RTC_SMOOTHCALIB_PLUSPULSES_SET, nrOfPulses);
  }
}

/**
 ****************************************************************
 @brief   Enable or disable the RTC calibration output on PB2 pin.
 @param   enable  If true, set this pin as RTC_Calib_output, otherwise as GPIO output push-pull
 @return  -
 ****************************************************************
 */
void HAL_RTC_CalibrationOutputEnable (bool enable)
{
  GPIO_InitTypeDef GPIO_InitStruct =
  {
    0
  };

  if (enable)
  {
    //PB2     ------> RTC_OUT_CALIB
    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF0_RTC_50Hz;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
  }
  else
  {
    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);

    /*Configure GPIO pins : PBPin PBPin PBPin */
    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
  }
}

/**
 ****************************************************************
 @brief   Set a new RTC date/time information  (binary format).
 @param   pDateTime Pointer to the new date/time information data.
 @return  -
 ****************************************************************
 */
void HAL_RTC_DateTimeSet (rtcDateTime_t *pDateTime)
{
  RTC_TimeTypeDef sTime =
  {
    0
  };
  RTC_DateTypeDef sDate =
  {
    0
  };
  HAL_StatusTypeDef status;

  sTime.Hours = pDateTime->hrs;
  sTime.Minutes = pDateTime->min;
  sTime.Seconds = pDateTime->sec;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  status = HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
  assert_param(status == HAL_OK);

  sDate.WeekDay = pDateTime->weekday;
  sDate.Month = pDateTime->month;
  sDate.Date = pDateTime->day;
  sDate.Year = pDateTime->year % 100;
  status = HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
  assert_param(status == HAL_OK);
}

/**
 ****************************************************************
 @brief   Get the current RTC date/time information (binary format).
 @param   pDateTime Pointer to the current date/time information.
 @return  -
 ****************************************************************
 */
void HAL_RTC_DateTimeGet (rtcDateTime_t *pDateTime)
{
  RTC_TimeTypeDef timeStamp;
  RTC_DateTypeDef dateStamp;

  HAL_RTC_GetTime(&hrtc, &timeStamp, RTC_FORMAT_BIN);
  HAL_RTC_GetDate(&hrtc, &dateStamp, RTC_FORMAT_BIN);

  pDateTime->weekday = dateStamp.WeekDay;
  pDateTime->month = dateStamp.Month;
  pDateTime->day = dateStamp.Date;
  pDateTime->year = dateStamp.Year + 2000;
  pDateTime->hrs = timeStamp.Hours;
  pDateTime->min = timeStamp.Minutes;
  pDateTime->sec = timeStamp.Seconds;
  pDateTime->ms = ((float) (255 - timeStamp.SubSeconds)) * _rtcSubSecondGranularity_ms;
  assert_param(pDateTime->ms < 1000);
}

/**
 ****************************************************************
 @brief   Set the value of one of the possible backup registers, which
 *        are part of the RTC module.
 @param   backupRegNr The register number of the to be used backup register [0...31]
 @param   value       The to be set value
 @return  -
 ****************************************************************
 */
void HAL_RTC_BackupRegisterSet (uint32_t backupRegNr, uint32_t value)
{
  assert_param(backupRegNr <= 31);

  HAL_RTCEx_BKUPWrite(&hrtc, backupRegNr, value);
}

/**
 ****************************************************************
 @brief   Get a value out of one of the possible backup registers, which
 *        are part of the RTC module.
 @param   backupRegNr The register number of the to be used backup register [0...31]
 @return  The value read out of this register.
 ****************************************************************
 */
uint32_t HAL_RTC_BackupRegisterGet (uint32_t backupRegNr)
{
  assert_param(backupRegNr <= 31);

  return HAL_RTCEx_BKUPRead(&hrtc, backupRegNr);
}

/**
 * @brief  Wake Up Timer callback.
 * @param  hrtc RTC handle
 * @retval None
 */
void HAL_RTCEx_WakeUpTimerEventCallback (RTC_HandleTypeDef *hrtc)
{
  _rtcWakeUpTimerEvt = true;
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
