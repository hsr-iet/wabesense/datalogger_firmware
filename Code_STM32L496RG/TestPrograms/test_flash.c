/**
 ****************************************************************
 @file    test_flash.c
 ****************************************************************
 @brief   This module implements some tests for the external FLASH memory.
 *        The first memory block is erased, then erase is checked
 *        Some data are written to the first 64 bytes, then compared with the read back values
 *        The first memory block is erased & checked again
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-16
 ****************************************************************
 */

/** @addtogroup TESTPROGRAMMS
 * @{
 */

// --- Includes
#include "test_flash.h"

#include <assert.h>
#include <stdio.h>
#include "../HWO/FLASH.h"
#include "../HWO/RG_LED.h"
#include "../HWO/SYS.h"

// --- Defines

// --- Typedefs

// --- Variables

// --- Local Function Prototypes

/**
 ****************************************************************
 @brief	  Do all possible tests on the external flash memory.
 @param   -
 @return  -
 ****************************************************************
 */
void test_flash (void)
{
  uint32_t blockNr = 0;
  uint8_t flashDataRead[64];
  uint8_t flashDataWrite[64];
  uint32_t idx;

#ifdef SEMI_HOSTING
  printf("FLASH_Test: start\n\r");
#endif

#ifdef SEMI_HOSTING
  printf("FLASH_Test: erase and check erase");
#endif

  sys_sleepForUs(200);
  //read back 64 bytes and check them (address 0..63)
  flash_bytesRead(0, flashDataRead, 64);

  //erase and check erase
  flash_block4kErase(blockNr);

  flash_bytesRead((blockNr * 4096) + 0, flashDataRead, 1);
  assert_param(flashDataRead[0] == 0xFF);
  flash_bytesRead((blockNr * 4096) + 4095, flashDataRead, 1);
  assert_param(flashDataRead[0] == 0xFF);
#ifdef SEMI_HOSTING
  printf(" pass\n\r");
#endif

#ifdef SEMI_HOSTING
  printf("FLASH_Test: write some bytes and check by read");
#endif
  //write 32 bytes (address 0..31)
  for (idx = 0; idx < 32; idx++)
  {
    flashDataWrite[idx] = idx + 2;
  }
  //write 32 bytes (address 32..63)
  for (idx = 32; idx < 64; idx++)
  {
    flashDataWrite[idx] = 0xFF;
  }
  flash_bytesWrite(0, &flashDataWrite[0], 32);
  //write another single byte (address 49)
  flashDataWrite[49] = 0x55;
  flash_bytesWrite(49, &flashDataWrite[49], 1);
  //write another two bytes (address 57 & 58)
  flashDataWrite[57] = 0x88;
  flashDataWrite[58] = 0x99;
  flash_bytesWrite(57, &flashDataWrite[57], 2);
  //read back 64 bytes and check them (address 0..63)
  flash_bytesRead(0, flashDataRead, 64);
  for (idx = 0; idx < 64; idx++)
  {
    assert_param(flashDataRead[idx] == flashDataWrite[idx]);
  }
#ifdef SEMI_HOSTING
  printf(" pass\n\r");
#endif

#ifdef SEMI_HOSTING
  printf("FLASH_Test: erase and check erase again");
#endif
  //do it again: erase and check erase
  flash_block4kErase(blockNr);

  flash_bytesRead((blockNr * 4096) + 0, flashDataRead, 1);
  assert_param(flashDataRead[0] == 0xFF);
  flash_bytesRead((blockNr * 4096) + 4095, flashDataRead, 1);
  assert_param(flashDataRead[0] == 0xFF);
#ifdef SEMI_HOSTING
  printf(" pass\n\r");
#endif

#ifdef SEMI_HOSTING
  printf("FLASH_Test: ends\n\n\r");
#endif

  //mark test as okay
  rgLED_init();
  rgLED_luminositySet(10);
  rgLED_colorSet(RGLED_COLOR_GREEN);
  sys_sleepForUs(1e6);  //1s delay
  rgLED_luminositySet(0);
  rgLED_colorSet(RGLED_COLOR_NONE);
}

/**
 * @}
 */
