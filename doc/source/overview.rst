========
Overview
========

.. contents:: :local:

Datalogger PCB top side.

.. image:: https://gitlab.com/hsr-iet/wabesense/wabesense/-/raw/master/doc/img/datalogger_zeroSeries_PCB_Top_annotated.png
  :width: 400
  :alt: image of the top side
  
Datalogger PCB bottom side.

.. image:: https://gitlab.com/hsr-iet/wabesense/wabesense/-/raw/master/doc/img/datalogger_zeroSeries_PCB_Bottom_annotated.png
  :width: 400
  :alt: image of the bottom side

User manual
===========

The `User Manual`_ describe the basic handling/operation of the datalogger.

Main features
=============

**measurement data export by USB-Stick**

* **<deviceName>.csv**: measurement data
	RTC_Datetime(YYYY.mm.dd HH:MM:SS.MS+0000):	UTC-Time
	
	Measurement_Auto: '0': manual measurement, '1': RTC interval based measurement
	
	Battery_Voltage(V): measured battery voltage (connector J1) in voltage
	
	BME_Temperature(C): BME280 onBoard sensor absolute temperature in °C
	
	BME_Pressure(Pa): BME280 onBoard sensor absolute pressure in Pa
	
	BME_Humidity(%): BME280 onBoard sensor relative humidity in %
	
	ADC_IN8(V): Analog channel 8 measurement value in V (0...5)
	
	ADC_IN9(V): Analog channel 9 measurement value in V (0...5)
	
	ADC_IN10(V): Analog channel 10 measurement value in V (0...5)
	
	ADC_IN11(V): Analog channel 11 measurement value in V (0...5)
	
	example:
	
	.. code-block:: yaml
	
		RTC_Datetime(YYYY.mm.dd HH:MM:SS.MS+0000);Measurement_Auto;Battery_Voltage(V);BME_Temperature(C);BME_Pressure(Pa);BME_Humidity(%%);ADC_IN8(V);ADC_IN9(V);ADC_IN10(V);ADC_IN11(V)
		2021.03.22 09:16:22.199;0;7.132812;23.437500;96792.335937;36.806640;3.480591;0.000000;0.000000;0.000000
		2021.03.22 09:19:00.199;0;7.132812;23.621093;96785.671875;33.044921;3.475342;0.000000;0.000000;0.000000
		2021.03.22 09:19:01.199;0;7.148437;23.632812;96785.562500;33.658203;3.482300;0.000000;0.000000;0.000000
		2021.03.22 09:25:44.562;0;7.136719;23.542968;96799.164062;33.169921;3.481567;0.000000;0.000000;0.000000
		2021.03.22 09:27:57.042;0;7.097656;23.453125;96794.726562;31.058593;3.473389;0.000000;0.000000;0.000000
		2021.03.23 08:26:14.703;0;0.601563;23.808593;97245.085937;38.492187;0.000000;0.000000;0.000000;0.000000
		2021.03.23 08:26:19.691;0;0.601563;23.937500;97243.289062;39.496093;0.000000;0.000000;0.000000;0.000000

* **<deviceName>.log**: events logged by the datalogger like with optinal additional informations.
	* readout: Data was transfered from internal logger memory to USB-Stick.
		* fail on \*.csv: abs: "nrOfDataPoints", "nrOfNewDataPoints"
		* success: abs: "nrOfDataPoints", "nrOfNewDataPoints"
		* fail on \*.txt
		* fail on \*.log
	* sysRestart: The system was restarted (Power source inserted)
		* VDD powerUp
	* fwUpdate: An firmware file was detected on the memory stick, try to update.
		* USBStick - actual: "Version"
		* successful - new: "Version" --> status information after update
	* BME280 error: The BME280 sensor don't reported the measurement data (timeout)
		* Timeout during sysCheck --> BME sensor timed out (no data available)
		* Timeout --> BME sensor timed out (no data available)
	* rtcBattery: A low voltage measured on the RTC battery (CR2032) or it is not inserted/detected
		* low: "measuredVoltage" --> battery is low (less then 2.5V)
		* not inserted or empty: "measuredVoltage" --> no battery detected
	* configStore: A new logger configuration setting is stored to FLASH storage (persistent)
		* - default --> no configuration found on the device - use the default ones from firmware
		* - new config over serial Interface --> configuration store action by CSA command over serial interface - see `API documentation`_
		* - default all (RAM only) --> read back Tag information was not set, so device was never configured at all
		* - default autoMeasActive (RAM only) --> set default value for autoMeasActive flag (value was out of range), but only in RAM-variable
		* - default measInterval (RAM only) --> set default value for measInterval (value was out of range), but only in RAM-variable
	* clearData: All persistent stored measurement & logEvent data are cleared. Executed by DCA command over serial interface - see `API documentation`_
		* no additional informations
	* autoMeasure: State change of the auto measurement (RTC time based) function detected (deactivating or activating). Executed by ASS command over serial interface - see `API documentation`_
		* activate
		* deactivate
		* interval [min]: "minutes"
	* curorRestore: Cursors used for logMemory data are restored due to a complete power loss of the system (including CR2032 battery)
		* done
	* unknown: a event, which is not supported

	example:
	
	.. code-block:: yaml
	
		csv:
		 start: 2021.03.23 08:26:14.703
		 stop: 2021.03.23 08:26:19.691
		 samples: 2

		log:
		 - 2021.03.22 12:55:40.019, clearData
		 - 2021.03.22 12:56:23.328, configStore - new config
		 - 2021.03.22 12:56:33.015, readout success: abs: 001083, new: 001083
		 - 2021.03.23 08:26:09.000, sysRestart VDD powerUp
		 - 2021.03.23 08:27:24.675, readout success: abs: 001088, new: 000005
	
* **<deviceName>.txt**: current configuration informations about the device as simple text file.
		
	example:
	
	.. code-block:: yaml
	
		Device name: TEST2.SS.AB12641-002(2)
		Time based measurement: disabled
		Measurement interval (minutes): 10
		Battery nominal voltage (mV): 3700
		Battery number of cells: 3
		Analog channel mappings:
		 ADC_IN8 (HW): ADC_IN8(V)
		 ADC_IN9 (HW): ADC_IN9(V)
		 ADC_IN10 (HW): ADC_IN10(V)
		 ADC_IN11 (HW): ADC_IN11(V)
		Firmware version: 0.5.0
		Unique ID: 0x0032001F425350112035354E

**configurable via USB (virtual ComPort)** - see `API documentation`_
	* deviceName
	* sampling interval
	* analog channel name
	* RTC time
	* battery setup
	* data streaming
	
**user available I/Os**
	* 4 x 0-5V analog Inputs
	* 2 x 0-3.3V analog Inputs
	* 7 x digtial in/outputs
	* SPI or 4 additional digital in/outputs
	* I2C or 2 additional digital in/outputs
	
**User interface**
	* Two buttons via connectors
	* Two color LED via connector
	* USB-C Port (J2)
	* SD card slot (unused in this project)
	
**lowPower**
	* standby: only RTC domain on micorcontroller is powered
		* LSE: 32.768 kHz
		* HSI: off
		* HSI48: off
	* active:
		* LSE: 32.768 kHz
		* HSI: 16 MHz (directly used as SYSCLK, without PLL)
		* HSI48: 48 MHz

**Power supply options**
	* DC powersupply or Battery/Accumulator: via power Jack 2x5.5mm (J1, 5.0 to 28V, 0.5A)
	* USB: via USB Type C connector (J2)

Inline code documentation
=========================
The code is mainly documented by inline comments used together with `Doxygen`_. The part of this documentation
is generated on each push to the master branch of the firmware repository.

The resulting output are available here:

Firmware `Doxygen`_ documentation
---------------------------------

	* https://hsr-iet.gitlab.io/wabesense/datalogger_firmware/doxygen/index.html
	
Bootloader `Doxygen`_ documentation
-----------------------------------

	* https://hsr-iet.gitlab.io/wabesense/datalogger_firmware/doxygen_Bootloader/index.html

Generated report from the STMCubeIDE
====================================
:download:`Generated report of the STMCubeIDE as pdf<../sourceCode/WABEsense_Datalogger_STM32L496RGT6.pdf>`.

Hardware Overview
=================
The hardware reference manual is hosted at `WABEsense/DataLogger_Hardware Repository`_.

The picture below shows the pinout view of the microcontroller (view is part of the `Generated report from the STMCubeIDE`_ as well).

	.. image:: ../pictures/Microcontroller_PinoutView.jpg
	  :width: 750
	  :alt: Datalogger Microcontroller: Pinout view

:download:`Complete schematic of datalogger as pdf <https://gitlab.com/hsr-iet/wabesense/datalogger_hardware/-/blob/master/doc/pictures/schematic/WABEsense_Datalogger_Schematic.pdf>`.

Tools
=====
**Development Environment**
	`STM32CubeIDE`_ in version 1.3.0 (available for multiple operating system plattforms).
	
**Inline code documentation**
	* `Doxygen`_ in version 1.8.17
	* `Graphviz`_ in version 2.38 - used for graphics generation inside the `Doxygen`_.

The firmware package used for the basic HAL driver generation is: "STM32Cube FW_L4 V1.15.1". This code is generated with 
the STM32CubIDE, wich is a integrated part of the STM32CubeIDE.

Links
=====
.. _User Manual: https://hsr-iet.gitlab.io/wabesense/wabesense/
.. _API documentation: https://hsr-iet.gitlab.io/wabesense/dataloggerui/API.html
.. _Doxygen: https://www.doxygen.nl/index.html
.. _Graphviz: https://graphviz.org/
.. _STM32CubeIDE: https://www.st.com/en/development-tools/stm32cubeide.html
.. _WABEsense/DataLogger_Hardware Repository: https://hsr-iet.gitlab.io/wabesense/datalogger_hardware/

.. target-notes::