/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "fatfs.h"
#include "rtc.h"
#include "tim.h"
#include "usb_host.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "fatfs.h"
#include "../../HWO/SYS.h"
#include "../../HWO/DIG_IO.h"
#include "../../FWO/HMI.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef enum
{
  BOOTLOADER_STATE_INIT = 0,
  BOOTLOADER_STATE_ACTIVE,
  BOOTLOADER_STATE_ERROR,
  BOOTLOADER_STATE_SUCCESSFUL,
  BOOTLOADER_STATE_EXIT
} bootloaderState_t;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define APP_START_ADDRESS   0x08020000
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
dataForBootloader_t bootloaderData __attribute__ ((section ("dataToBootloaderFromUserApplication"))) =
{
  0
};
extern USBH_HandleTypeDef hUsbHostFS;
static FIL file;
//! data buffer for the usbStick - note: This must be a multiple of 64bits in size!
static uint8_t usbDataBuffer[512];
static bootloaderState_t bootloaderState = BOOTLOADER_STATE_INIT;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_USB_HOST_Process(void);

/* USER CODE BEGIN PFP */
static void _usbStickUnmount (void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  FRESULT iFResult;
  UINT nrOfBytesRead;
  uint32_t memAddr;
  uint32_t appResetVector;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  //check if flag for bootloading is set
  if ((bootloaderData.tag == BOOTLOADING_TAG_REQUEST) && (bootloaderData.pathAndFileName[bootloaderData.len] == '\0'))
  {
    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_TIM6_Init();

    if (!__HAL_RTC_WAKEUPTIMER_GET_FLAG(&hrtc, RTC_FLAG_INITS))
    {
      MX_RTC_Init();
    }

    sys_init();
    digIO_init();
    hmi_init();

    hmi_ledPatternSet(HMI_LED_PATTERN_BLINK_10HZ, RGLED_COLOR_RED);

    sys_sleepForUs(500e3);  //wait for some time, before check of VUSB
    //check if the USB-Power is present on the VBUS --> this is an error case!
    if (digIO_usbDetected())
    {
      bootloaderState = BOOTLOADER_STATE_ERROR;
    }
    else
    {
      //USB could interacts as host and should try to power the V-USB
      digIO_otgDeviceModeSet(false);
      digIO_5VExtPwrOn();       //turn power source V_Ext (connector J1) ON --> Device powered from J1 source
      digIO_5VtoUSBSwitchOn();  //use internal 5V to supply V-USB

      MX_USB_HOST_Init();
      while (USBH_MSC_IsReady(&hUsbHostFS) == 0)
      {
        MX_USB_HOST_Process();
      }

      MX_FATFS_Init();
      MX_USB_HOST_Process();

      //Mount the file system immediately, using logical disk 0
      iFResult = f_mount(&USBHFatFS, (TCHAR const*) USBHPath, 0);
      if (iFResult != FR_OK)
      {
        _usbStickUnmount();

        bootloaderState = BOOTLOADER_STATE_ERROR;
      }
      else
      {
        //read out file name from the RAM_BL_DATA section which is located in RAM
        //open the File
        iFResult = f_open(&file, bootloaderData.pathAndFileName, FA_OPEN_ALWAYS | FA_READ);
        if (iFResult != FR_OK)
        {
          _usbStickUnmount();

          bootloaderState = BOOTLOADER_STATE_ERROR;
        }
        else
        {
          bootloaderState = BOOTLOADER_STATE_ACTIVE;
        }
      }
    }
  }
  else
  {
    //no bootloading is requested
    bootloaderState = BOOTLOADER_STATE_EXIT;
  }

  while (1)
  {
    if (bootloaderState == BOOTLOADER_STATE_ACTIVE)
    {
      //try to clear flash pages where the user application is stored
      if (!sys_flashUserAppErase())
      {
        bootloaderState = BOOTLOADER_STATE_ERROR;
      }
      else
      {
        memAddr = APP_START_ADDRESS;
        do
        {
          MX_USB_HOST_Process();

          //read out data from firmware file
          iFResult = f_read(&file, usbDataBuffer, sizeof(usbDataBuffer), &nrOfBytesRead);
          if (iFResult != FR_OK)
          {
            f_close(&file);
            _usbStickUnmount();

            //flash is corrupted now!

            bootloaderState = BOOTLOADER_STATE_ERROR;
          }
          else
          {
            //update the user application flash sections with the readout data
            if (sys_flashUserAppWrite(memAddr, usbDataBuffer, nrOfBytesRead))
            {
              memAddr += nrOfBytesRead;
              if (nrOfBytesRead < sizeof(usbDataBuffer))
              {
                f_close(&file);

                //wait for USB buffers to flush
                while (USBH_MSC_IsReady(&hUsbHostFS) == 0)
                {
                  MX_USB_HOST_Process();
                }

                _usbStickUnmount();

                //last bytes read from file, bootloading finished
                bootloaderState = BOOTLOADER_STATE_SUCCESSFUL;
              }
            }
            else
            {
              f_close(&file);
              _usbStickUnmount();

              bootloaderState = BOOTLOADER_STATE_ERROR;
            }
          }
        } while (bootloaderState == BOOTLOADER_STATE_ACTIVE);
      }
    }

    switch (bootloaderState)
    {
      default:
      case BOOTLOADER_STATE_ERROR:
        hmi_ledPatternSet(HMI_LED_PATTERN_ON, RGLED_COLOR_RED);
        sys_sleepForUs(1e6);  //1s delay
        hmi_ledPatternSet(HMI_LED_PATTERN_OFF, RGLED_COLOR_NONE);
        break;
      case BOOTLOADER_STATE_SUCCESSFUL:
        bootloaderData.tag = BOOTLOADING_TAG_SUCCESS;
        break;
      case BOOTLOADER_STATE_EXIT:
        break;
    }

    //check if a valid firmware is installed, otherwise go to standby mode
    appResetVector = *(uint32_t*) (APP_START_ADDRESS + 4);
    if ((appResetVector == 0xFFFFFFFF) || (bootloaderState == BOOTLOADER_STATE_ERROR))
    {
      //no firmware installed, go to standby
      sys_standbyEnter();
    }
    else
    {
      //jump to user application code section
      sys_userAppStart(APP_START_ADDRESS);
    }
    /* USER CODE END WHILE */
    MX_USB_HOST_Process();

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSI
                              |RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_USB;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage 
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
/**
 ****************************************************************
 @brief   Internally function to unmount the usbStick memory, deInit the
 *        FatFileSystem and turn off the 5V to the USB-VBUS.
 @param   -
 @return  -
 ****************************************************************
 */
static void _usbStickUnmount (void)
{
  //unmount file system
  f_mount(0, (TCHAR const*) USBHPath, 1);
  MX_USB_HOST_Process();

  //wait again, in case some more buffers need flushing
  while (USBH_MSC_IsReady(&hUsbHostFS) == 0)
  {
    MX_USB_HOST_Process();
  }

  //unlink driver
  FATFS_DeInit();
  MX_USB_HOST_Process();

  //and wait again, just in case
  while (USBH_MSC_IsReady(&hUsbHostFS) == 0)
  {
    MX_USB_HOST_Process();
  }

  //stop the USB, and remove power
  USBH_Stop(&hUsbHostFS);

  /* De-Init */
  USBH_DeInit(&hUsbHostFS);

  //switch the power OFF to VUSB
  digIO_otgDeviceModeSet(true);   //interact as Device
  digIO_5VtoUSBSwitchOff();
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
   tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
