.. datalogger_firmware documentation master file, created by
   sphinx-quickstart on Wed Feb  3 12:07:54 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to WABEsense's datalogger_firmware reference Manual
===========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   overview
   
   structure
   
   memory
   
   opModes
   
   logData

   sourceCode
   
   debugEnvironment
   
   firmwareUpdate

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
