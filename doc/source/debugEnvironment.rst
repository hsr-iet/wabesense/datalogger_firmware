=================
Debug-Environment
=================

.. contents:: :local:

STM32CubeIDE
============
The `STM32CubeIDE`_ is used for the complete development process. This integrated development environment contains 
everything needed to be able to compile, link and debug an STM32-Microcontroller-based application.
Have a look in `Tools`_ to see the details about the used version.

STM32CubeMx
-----------
This Application, which is part of the `STM32CubeIDE`_ is very usefull and was used, to configure the functionality of the basic Microcontroller system. 
This includes the configuration of the Pins and there actual working functionality up to some high end functions like USB-Device setup as CDC USB-Device.

Corresponding File-Type: \*.ioc-File

Firmware Package
----------------
As part of the `STM32CubeIDE`_ a firmware package is needed, to generate a source-file based output, based on the graphical configuration. 
The used Firmware Package from ST is in Version: STM32Cube FW_L4 V1.15.1

Build
-----
There are two different kind of build configurations defined inside the Project. One is used for debugging (less code optimization) and another 
which is used as "Release".

Debugging
---------
The different \*.launch settings are part of the GIT-Repository. They contains the correct settings needed to be able to download and debug 
afterwards the application. There is a "Debug" and a "Release" setting. "Debug" setting should only be used during inhouse tests.
The "Release" compiled application is the one, which should normaly be programmed to a datalogger (including firmware-updates).

Firmware-Version
----------------
The Firmware-Version number's are part of the Project properties. Right-click on the project in "Project Explorer" and then select properties.
Inside the "C/C++ Build" select "Build Variables". Take care, that you select the configuration "All configuration", so that for Debug and for 
Release Builds, the Firmware-Version is the same!

.. image:: ../pictures/STM32CubeIDE_ProjectProperties_BuildVariables.JPG
		  :width: 750
		  :alt: Firmware-Version - STM32CubeIDE Build Variables in Project properties

Links
=====
.. _STM32CubeIDE: https://www.st.com/en/development-tools/stm32cubeide.html
.. _Tools: https://hsr-iet.gitlab.io/wabesense/datalogger_firmware/overview.html#tools

.. target-notes::