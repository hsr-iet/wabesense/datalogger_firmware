/**
 ****************************************************************
 @file    MMC.c
 ****************************************************************
 @brief   MMC/SDC (in SPI mode) control module.
 @brief   Low level disk interface module include file   (C)ChaN, 2013
 ****************************************************************
 @note    The used interface is the SPI1 unit (also used for FLASH and BME280)!
 *        SPI Interface of the sensor is compatible to (max. 20MHz)
 *          mode 00
 *            CPOL = 0, CPHA = 0
 *          or mode 11
 *            CPOL = 1, CPHA = 1
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-16
 ****************************************************************
 */

/** @addtogroup HWO
 * @{
 */

/** @addtogroup MMC
 * @{
 */

// --- Includes
#include "MMC.h"

#include <stdbool.h>
#include <stdint.h>
#include "spi.h"
#include "SYS.h"
#include "../DEF/GLOB_Types.h"

// --- Defines
//Definitions for MMC/SDC command
#define CMD0  	(0)     /* GO_IDLE_STATE */
#define CMD1  	(1)     /* SEND_OP_COND (MMC) */
#define ACMD41  (0x80+41) /* SEND_OP_COND (SDC) */
#define CMD8  	(8)     /* SEND_IF_COND */
#define CMD9  	(9)     /* SEND_CSD */
#define CMD10 	(10)    /* SEND_CID */
#define CMD12 	(12)    /* STOP_TRANSMISSION */
#define ACMD13  (0x80+13) /* SD_STATUS (SDC) */
#define CMD16 	(16)    /* SET_BLOCKLEN */
#define CMD17 (17)    /* READ_SINGLE_BLOCK */
#define CMD18 (18)    /* READ_MULTIPLE_BLOCK */
#define CMD23 (23)    /* SET_BLOCK_COUNT (MMC) */
#define ACMD23  (0x80+23) /* SET_WR_BLK_ERASE_COUNT (SDC) */
#define CMD24 (24)    /* WRITE_BLOCK */
#define CMD25 (25)    /* WRITE_MULTIPLE_BLOCK */
#define CMD32 (32)    /* ERASE_ER_BLK_START */
#define CMD33 (33)    /* ERASE_ER_BLK_END */
#define CMD38 (38)    /* ERASE */
#define CMD55 (55)    /* APP_CMD */
#define CMD58 (58)    /* READ_OCR */

// --- Typedefs

// --- Variables
static volatile DSTATUS Stat = STA_NOINIT;  //Disk status
static uint8_t CardType;                    //b0:MMC, b1:SDC, b2:Block addressing
static uint8_t PowerFlag = 0;               //indicates if "power" is on

// --- Local Function Prototypes
static uint8_t _mmc_wait_ready (uint32_t timeout_ms);
static void _mm_send_initial_clock_train (void);
static bool _mmc_rcvr_datablock (uint8_t *buff, uint32_t btr);
static bool _mmc_xmit_datablock (const uint8_t *buff, uint8_t token);
static uint8_t _mmc_send_cmd (uint8_t cmd, uint32_t arg);
static void _mmc_deselect (void);
static bool _mmc_select (void);

/**
 *****************************************************************
 @brief  Initialize Disk Drive.
 @param  drive        Physical drive number (0)
 @return The disk status
 ****************************************************************
 */
DSTATUS mmc_initialize (uint8_t drive)
{
  BYTE cmd;
  uint8_t txData[4];
  uint8_t n;
  uint8_t ty;
  uint8_t ocr[4];
  uint32_t timeoutCnt = 100;  //initialization timeout of 1000 millisecond (100 x 10ms)

  for (n = 0; n < 4; n++)
  {
    txData[n] = 0xFF;
  }

  if (drive)
  {
    return STA_NOINIT;                    //Supports only single drive
  }
  
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);	//Set CS# high
  sys_sleepForUs(10e3); /* 10ms */
  
  if (Stat & STA_NODISK)
  {
    return Stat;                          //No card in the socket
  }
  
  //set SPI clock to a lower frequency (100-400kHz)
  HAL_SPI1_clockSpeedFastSet(false);

  //Ensure the card is in SPI mode
  //Set DI and CS high and apply more than 74 pulses to SCLK for the card
  //to be able to accept a native command.
  _mm_send_initial_clock_train();
  PowerFlag = 1;

  ty = 0;
  if (_mmc_send_cmd(CMD0, 0) == 1)          //Enter Idle state
  {
    if (_mmc_send_cmd(CMD8, 0x1AA) == 1)    //SDC Ver2+
    {
      HAL_SPI1_transfer(txData, ocr, 4);

      if (ocr[2] == 0x01 && ocr[3] == 0xAA)
      {
        //The card can work at VDD range of 2.7-3.6V
        do
        {
          sys_sleepForUs(10e3);
          timeoutCnt--;
        } while (timeoutCnt && _mmc_send_cmd(ACMD41, 1UL << 30)); /* Wait for end of initialization with ACMD41(HCS) */

        if (timeoutCnt && _mmc_send_cmd(CMD58, 0) == 0)
        {
          /* Check CCS bit in the OCR */
          HAL_SPI1_transfer(txData, ocr, 4);
          ty = (ocr[0] & 0x40) ? 6 : 2; /* Card id SDv2 */
        }
      }
    }
    else  //Not SDv2 card
    {
      if (_mmc_send_cmd(ACMD41, 0) <= 1)
      { /* SDv1 or MMC? */
        ty = CT_SD1;
        cmd = ACMD41; /* SDv1 (ACMD41(0)) */
      }
      else
      {
        ty = CT_MMC;
        cmd = CMD1; /* MMCv3 (CMD1(0)) */
      }
      do
      {
        sys_sleepForUs(10e3);
        timeoutCnt--;
      } while (timeoutCnt && _mmc_send_cmd(cmd, 0)); /* Wait for end of initialization */

      if (!timeoutCnt || _mmc_send_cmd(CMD16, 512) != 0)  //_mmc_select R/W block length
      {
        ty = 0;
      }
    }
  }
  CardType = ty;
  _mmc_deselect();

  if (ty)
  {
    //Initialization succeeded
    Stat &= ~STA_NOINIT;                  //Clear STA_NOINIT
  }
  else
  {
    //Failed
    Stat = STA_NOINIT;
    //Initialization failed
    PowerFlag = 0;
  }

  //set SPI clock back to fast value
  HAL_SPI1_clockSpeedFastSet(true);

  return Stat;
}

/**
 *****************************************************************
 @brief  Get Disk Status.
 @param  drive  Physical drive number (0)
 @return The disk status.
 ****************************************************************
 */
DSTATUS mmc_status (uint8_t drive)
{
  if (drive)
  {
    return STA_NOINIT;                    //Supports only single drive */
  }
  return Stat;
}

/**
 *****************************************************************
 @brief  Read Sector(s).
 @param  drive  Physical drive number (0)
 @param  buff   Pointer to the data buffer to store read data
 @param  sector Start sector number (LBA)
 @param  count  Sector count (1..255)
 @return Result of disk function from enumeration.
 ****************************************************************
 */
DRESULT mmc_read (uint8_t drive, uint8_t *buff, uint32_t sector, uint8_t count)
{
  if (drive || !count)
  {
    return RES_PARERR;
  }
  if (Stat & STA_NOINIT)
  {
    return RES_NOTRDY;
  }

  if (!(CardType & 4))
  {
    sector *= 512;                        //Convert to byte address if needed
  }

  if (count == 1)
  {
    //Single block read
    if ((_mmc_send_cmd(CMD17, sector) == 0) && _mmc_rcvr_datablock(buff, 512))
    {
      count = 0;
    }
  }
  else
  {
    //Multiple block read
    if (_mmc_send_cmd(CMD18, sector) == 0)
    {
      do
      {
        if (!_mmc_rcvr_datablock(buff, 512))
          break;
        buff += 512;
      } while (--count);
      _mmc_send_cmd(CMD12, 0);                  //STOP_TRANSMISSION
    }
  }
  _mmc_deselect();

  return count ? RES_ERROR : RES_OK;
}

/**
 *****************************************************************
 @brief  Write Sector(s).
 @param  drive  Physical drive number (0)
 @param  buff   Pointer to the data to be written
 @param  sector Start sector number (LBA)
 @param  count  Sector count (1..255)
 @return Result of disk function from enumeration.
 ****************************************************************
 */
#if _READONLY == 0
DRESULT mmc_write (uint8_t drive, const uint8_t *buff, uint32_t sector, uint8_t count)
{
  if (drive || !count)
  {
    return RES_PARERR;
  }
  if (Stat & STA_NOINIT)
  {
    return RES_NOTRDY;
  }
  if (Stat & STA_PROTECT)
  {
    return RES_WRPRT;
  }

  if (!(CardType & 4))
  {
    sector *= 512;                        //Convert to byte address if needed
  }

  if (count == 1)
  {
    //Single block write
    if ((_mmc_send_cmd(CMD24, sector) == 0) && _mmc_xmit_datablock(buff, 0xFE))
    {
      count = 0;
    }
  }
  else
  {
    //Multiple block write
    if (CardType & 2)
    {
      _mmc_send_cmd(CMD55, 0);
      _mmc_send_cmd(CMD23, count);   //ACMD23
    }
    if (_mmc_send_cmd(CMD25, sector) == 0)
    {
      //WRITE_MULTIPLE_BLOCK
      do
      {
        if (!_mmc_xmit_datablock(buff, 0xFC))
        {
          break;
        }
        buff += 512;
      } while (--count);
      if (!_mmc_xmit_datablock(0, 0xFD))   //STOP_TRAN token
      {
        count = 1;
      }
    }
  }
  _mmc_deselect();

  return count ? RES_ERROR : RES_OK;
}
#endif //_READONLY

/**
 *****************************************************************
 @brief  Miscellaneous Functions.
 @param  drive  Physical drive number (0)
 @param  ctrl   Control code
 @param  buff   Buffer to send/receive control data
 @return Result of disk function from enumeration.
 ****************************************************************
 */
DRESULT mmc_ioctl (uint8_t drive, uint8_t ctrl, void *buff)
{
  DRESULT res;
  uint8_t n;
  uint8_t csd[16];
  uint8_t *ptr = buff;
  uint16_t csize;
  uint8_t txData[4];

  for (n = 0; n < 4; n++)
  {
    txData[n] = 0xFF;
  }

  if (drive)
    return RES_PARERR;

  res = RES_ERROR;

  if (ctrl == CTRL_POWER)
  {
    switch (*ptr)
    {
      case 0:   //Sub control code == 0 (POWER_OFF)
        if (PowerFlag)
        {
          PowerFlag = 0;                  //Power off
        }
        res = RES_OK;
        break;
      case 1:   //Sub control code == 1 (POWER_ON)
        //Ensure the card is in SPI mode
        //Set DI and CS high and apply more than 74 pulses to SCLK for the card
        //to be able to accept a native command.
        _mm_send_initial_clock_train();
        PowerFlag = 1;

        res = RES_OK;
        break;
      case 2:   //Sub control code == 2 (POWER_GET)
        *(ptr + 1) = PowerFlag;
        res = RES_OK;
        break;
      default:
        res = RES_PARERR;
    }
  }
  else
  {
    if (Stat & STA_NOINIT)
    {
      return RES_NOTRDY;
    }

    switch (ctrl)
    {
      case GET_SECTOR_COUNT:  //Get number of sectors on the disk (uint32_t)
        if ((_mmc_send_cmd(CMD9, 0) == 0) && _mmc_rcvr_datablock(csd, 16))
        {
          if ((csd[0] >> 6) == 1)
          {
            //SDC version 2.00
            csize = csd[9] + ((uint16_t) csd[8] << 8) + 1;
            *(uint32_t*) buff = (uint32_t) csize << 10;
          }
          else
          {
            //MMC or SDC version 1.XX
            n = (csd[5] & 15) + ((csd[10] & 128) >> 7) + ((csd[9] & 3) << 1) + 2;
            csize = (csd[8] >> 6) + ((uint16_t) csd[7] << 2) + ((uint16_t) (csd[6] & 3) << 10) + 1;
            *(uint32_t*) buff = (uint32_t) csize << (n - 9);
          }
          res = RES_OK;
        }
        break;

      case GET_SECTOR_SIZE:   //Get sectors on the disk (uint16_t)
        *(uint16_t*) buff = 512;
        res = RES_OK;
        break;

      case CTRL_SYNC:         //Make sure that data has been written
        if (_mmc_select())
        {
          res = RES_OK;
        }
        break;

      case MMC_GET_CSD:       //Receive CSD as a data block (16 bytes)
        //READ_CSD
        if (_mmc_send_cmd(CMD9, 0) == 0 && _mmc_rcvr_datablock(ptr, 16))
        {
          res = RES_OK;
        }
        break;

      case MMC_GET_CID:       //Receive CID as a data block (16 bytes)
        //READ_CID
        if (_mmc_send_cmd(CMD10, 0) == 0 && _mmc_rcvr_datablock(ptr, 16))
        {
          res = RES_OK;
        }
        break;

      case MMC_GET_OCR:       //Receive OCR as an R3 response (4 bytes)
        if (_mmc_send_cmd(CMD58, 0) == 0)
        {
          //READ_OCR
          HAL_SPI1_transfer(txData, ptr, 4);
          res = RES_OK;
        }
      case MMC_GET_TYPE:      //Get card type flags (1 byte)
        *ptr = CardType;
        res = RES_OK;
        break;

      default:
        res = RES_PARERR;
    }
  }
  
  _mmc_deselect();
  
  return res;
}

/**
 ****************************************************************
 @brief  Wait for card ready
 @param  timeout_ms	The maximum time to wait until timeout [ms]
 @return Return 0xFF if ready
 ****************************************************************
 */
static uint8_t _mmc_wait_ready (uint32_t timeout_ms)
{
  uint8_t res;
  uint8_t txData = 0xFF;

  HAL_SPI1_transmit(&txData, 1);           //Idle (Release DO)
  do
  {
    if (timeout_ms >= 100)
    {
      sys_sleepForUs(100e3);  //100ms
      timeout_ms -= 100;
    }
    else
    {
      sys_sleepForUs(timeout_ms * 1000);
      timeout_ms = 0;
    }
    HAL_SPI1_transfer(&txData, &res, 1);
  } while ((res != 0xFF) && timeout_ms);

  return res;
}

/**
 *****************************************************************
 @brief  Send 80 or so clock transitions with CS and DI held high.
 @brief  This is required after card power up to get it into SPI mode.
 @param  -
 @return -
 ****************************************************************
 */
static void _mm_send_initial_clock_train (void)
{
  uint8_t i;
  uint8_t txData = 0xFF;

  //Ensure CS is held high
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE);
  sys_sleepForUs(100e3);
  //Send 10 bytes over the SSI. This causes the clock to wiggle the
  //required number of times
  for (i = 0; i < 10; i++)
  {
    //Write DUMMY data. SSIDataPut() waits until there is room in the FIFO
    HAL_SPI1_transmit(&txData, 1);
  }
  sys_sleepForUs(100e3);
}

/**
 *****************************************************************
 @brief   Receive a data packet from MMC
 @param   buff  Data buffer to store received data
 @param   btr   Byte count (must be even number)
 @return  Return true if successful
 ****************************************************************
 */
static bool _mmc_rcvr_datablock (uint8_t *buff, uint32_t btr)
{
  uint8_t token;
  uint8_t txData = 0xFF;
  uint32_t timeoutCnt = 10;   //Wait for ready in timeout of 100ms (10 x 10ms)

  do
  {
    //Wait for data packet in timeout of 10ms
    HAL_SPI1_transfer(&txData, &token, 1);
    sys_sleepForUs(10e3);
    timeoutCnt--;
  } while ((token == 0xFF) && timeoutCnt);
  if (token != 0xFE)
  {
    return false;   //If not valid data token, return with error
  }

  do
  {
    //Receive the data block into buffer
    HAL_SPI1_transfer(&txData, buff++, 1);
    HAL_SPI1_transfer(&txData, buff++, 1);
  } while (btr -= 2);
  HAL_SPI1_transmit(&txData, 1);          //Discard CRC
  HAL_SPI1_transmit(&txData, 1);

  return true;   //Return with success
}

/**
 *****************************************************************
 @brief   Send a data packet to MMC.
 @param   buff    512 byte data block to be transmitted
 @param   token   Data/Stop token
 @return  return true if done without error
 ****************************************************************
 */
#if _READONLY == 0
static bool _mmc_xmit_datablock (const uint8_t *buff, uint8_t token)
{
  uint8_t resp;
  uint8_t txData = 0xFF;

  if (!_mmc_wait_ready(500))
  {
    return false;
  }

  HAL_SPI1_transmit(&token, 1);           //transmit data token
  if (token != 0xFD)
  {
    //Is data token
    //transmit the 512 byte data block to MMC
    HAL_SPI1_transmit((uint8_t*) buff, 512);

    HAL_SPI1_transmit(&txData, 1);        //CRC (Dummy)
    HAL_SPI1_transmit(&txData, 1);
    HAL_SPI1_transfer(&txData, &resp, 1); //Receive data response
    if ((resp & 0x1F) != 0x05)            //If not accepted, return with error
    {
      return false;
    }
  }
  return true;
}
#endif //_READONLY

/**
 *****************************************************************
 @brief   Send a command packet to MMC.
 @param   cmd     Command byte
 @param   arg     Argument
 @return  Return with the response value of the command.
 ****************************************************************
 */
static uint8_t _mmc_send_cmd (uint8_t cmd, uint32_t arg)
{
  uint8_t n;
  uint8_t res;
  uint8_t txData;

  if (cmd & 0x80)
  { /* Send a CMD55 prior to ACMD<n> */
    cmd &= 0x7F;
    res = _mmc_send_cmd(CMD55, 0);
    if (res > 1)
    {
      return res;
    }
  }

  /* _mmc_select the card and wait for ready except to stop multiple block read */
  if (cmd != CMD12)
  {
    _mmc_deselect();
    if (!_mmc_select())
      return 0xFF;
  }
  
  //Send command packet
  txData = cmd | 0x40;            //add start bit
  HAL_SPI1_transmit(&txData, 1);  //Command
  txData = (uint8_t) (arg >> 24); //Argument[31..24]
  HAL_SPI1_transmit(&txData, 1);
  txData = (uint8_t) (arg >> 16); //Argument[23..16]
  HAL_SPI1_transmit(&txData, 1);
  txData = (uint8_t) (arg >> 8);  //Argument[15..8]
  HAL_SPI1_transmit(&txData, 1);
  txData = (uint8_t) (arg);       //Argument[7..0]
  HAL_SPI1_transmit(&txData, 1);

  if (cmd == CMD0)
  {
    txData = 0x95; /* Valid CRC for CMD0(0) */
  }
  else if (cmd == CMD8)
  {
    txData = 0x87; /* Valid CRC for CMD8(0x1AA) */
  }
  else
  {
    txData = 0x01; /* Dummy CRC + Stop */
  }
  HAL_SPI1_transmit(&txData, 1);

  //Receive command response
  if (cmd == CMD12)
  {
    txData = 0xFF;
    HAL_SPI1_transmit(&txData, 1);  //Skip a stuff byte when stop reading
  }

  txData = 0xFF;
  n = 10;   //Wait for a valid response in timeout of 10 attempts
  do
  {
    HAL_SPI1_transfer(&txData, &res, 1);
  } while ((res & 0x80) && --n);

  return res;   //Return with the response value
}

/**
 *****************************************************************
 @brief   Internaly function to deselect the SD-Card chip.
 @param   -
 @return  -
 ****************************************************************
 */
static void _mmc_deselect (void)
{
  uint8_t txData = 0xFF;
  
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_NONE); 	//Set CS# high
  HAL_SPI1_transmit(&txData, 1);				//Dummy clock (force DO hi-z for multiple slave SPI)
}

/**
 *****************************************************************
 @brief   Internaly function to select the SD-Card chip after the chip is ready.
 @param   -
 @return  Returns true on success, otherwise false
 ****************************************************************
 */
static bool _mmc_select (void)
{
  uint8_t txData = 0xFF;
  
  HAL_SPI1_slaveSelect(SPI1_CHIPSELECT_SDCARD);
  HAL_SPI1_transmit(&txData, 1);	//Dummy clock (force DO enabled)
  if (_mmc_wait_ready(500))
  {
    return true; 		//Wait for card ready
  }
  else
  {
    _mmc_deselect();
    return false; 		//Timeout
  }
}
/**
 * @}
 */

/**
 * @}
 */
