/**
 ****************************************************************
 @file    BME280.h
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the onBoard BME280 sensor (temperature, pressure and humidity).
 @brief   The sensor is normally in sleep mode and wake up for a forced
 *        measurement (T, P & H).
 @brief   The maximum measurement time is:
 *        1.25ms + (2.3 * T_oversampling) + [(2.3 * P_oversampling) + 0.575] +
 *        [(2.3 * H_oversampling) + 0.5]
 *        Example1:
 *        osTemp = 1, osPres = 1, osHumidity = 1 (fix)
 *        t_meas = 1.25ms + 2.3ms + 2.875ms + 2.8ms = 9.225ms
 ****************************************************************
 @note    The used interface is the SPI1 unit (also used for FLASH and SDCard)!
 *        SPI Interface of the sensor is compatible to (max. 10MHz)
 *          mode 00
 *            CPOL = 0, CPHA = 0
 *          or mode 11
 *            CPOL = 1, CPHA = 1
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.2
 @date    2020-11-02
 ****************************************************************
 */
#ifndef __BME280_H
#define __BME280_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdbool.h>
#include <stdint.h>

//===============================================================
//defines

//===============================================================
//typedefs
//!over-sampling possibilities
typedef enum
{
  OS_NONE = 0,  //!< measurement is OFF
  OS_1 = 1,     //!< no oversampling (by 1)
  OS_2 = 2,     //!< oversampling by 2
  OS_4 = 3,     //!< oversampling by 4
  OS_8 = 4,     //!< oversampling by 8
  OS_16 = 5     //!< oversampling by 16
} bme280_oversampling_t;

//!BME280 sensor standby time after each measurement (only needed on normal sampling mode)
typedef enum
{
  TSB_0_5_MS = 0,     //!< 0.5ms standby time before next measurement cycle
  TSB_62_5_MS = 1,    //!< 62.5ms standby time before next measurement cycle
  TSB_125_MS = 2,     //!< 125ms standby time before next measurement cycle
  TSB_250_MS = 3,     //!< 250ms standby time before next measurement cycle
  TSB_500_MS = 4,     //!< 500ms standby time before next measurement cycle
  TSB_1000_MS = 5,    //!< 1000ms standby time before next measurement cycle
  TSB_10_MS = 6,      //!< 10ms standby time before next measurement cycle
  TSB_20_MS = 7       //!< 20ms standby time before next measurement cycle
} bme280_standbyTime_t;

//!BME280 sensor filter coefficient setting
typedef enum
{
  FILTER_OFF = 0,     //!< filter off, 1 sample to reach >= 75% of step response
  FILTER_COEFF2 = 1,  //!< filtered by 2, 2 sample to reach >= 75% of step response
  FILTER_COEFF4 = 2,  //!< filtered by 4, 5 sample to reach >= 75% of step response
  FILTER_COEFF8 = 3,  //!< filtered by 8, 11 sample to reach >= 75% of step response
  FILTER_COEFF16 = 4  //!< filtered by 16, 22 sample to reach >= 75% of step response
} bme280_filterCoef_t;

//!BME280 sensor configuration structure
typedef struct
{
  bme280_oversampling_t osTemp;     //!< temperature oversampling setting
  bme280_oversampling_t osPres;     //!< pressure oversampling setting
  bme280_oversampling_t osHumi;     //!< humidity oversampling setting
  bme280_standbyTime_t standbyTime; //!< standby time (only for normal operating mode)
  bme280_filterCoef_t filter;       //!< filter settings
} bme280_config_t;

//!BME280 sensor operating modes
typedef enum
{
  MODE_SLEEP = 0,   //!< sensor is in SLEEP mode
  MODE_FORCED = 1,  //!< sensor do one complete measurement cycle (measurement, sleep)
  MODE_NORMAL = 3   //!< sensor do endless measurement cycles (measurement, standby, measurement, standby, ...)
} bme280_mode_t;

//!BME280 sensor measurement data
typedef struct
{
  float temperature;  //!< temperature in °C unit [-40 ... +80°C]
  float pressure;     //!< pressure in Pa unit [30'000 ... 110'000 Pa]
  float humidity;     //!< humidity in % unit [0 ... 100%]
} bme280_measData_t;

//===============================================================
//function prototypes
void bme280_init (bme280_config_t config);
void bme280_modeSet (bme280_mode_t mode);
uint32_t bme280_measTimeGet (void);
bool bme280_dataGet (bme280_measData_t *pMeasData);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /*__BME280_H */
