/**
 ****************************************************************
 @file    DIG_IO.h
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        all the available digital In/Output pins on the data-logger.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-10
 ****************************************************************
 */
#ifndef __DIG_IO_H
#define __DIG_IO_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdbool.h>
#include "../DEF/GLOB_Types.h"

//===============================================================
//defines

//===============================================================
//typedefs
//! type definition for all available digital in/output pins on the data logger unit
typedef enum
{
  DIG_IO_PB12,    //!< digital in/output pin (optional SPI2-NSS), available on J5.17
  DIG_IO_PB13,    //!< digital in/output pin (optional SPI2-SCK), available on J5.19
  DIG_IO_PB14,    //!< digital in/output pin (optional SPI2-MISO), available on J5.21
  DIG_IO_PB15,    //!< digital in/output pin (optional SPI2-MOSI), available on J5.23
  DIG_IO_PC6,     //!< digital in/output pin, available on J5.18
  DIG_IO_PC7,     //!< digital in/output pin, available on J5.20
  DIG_IO_PC8,     //!< digital in/output pin, available on J5.22
  DIG_IO_PC9,     //!< digital in/output pin, available on J5.24
  DIG_IO_PC10,    //!< digital in/output pin, available on J5.26
  DIG_IO_PC11,    //!< digital in/output pin, available on J5.28
  DIG_IO_PB10,    //!< digital in/output pin (optional I2C2-SCL, check R48), available on J5.27
  DIG_IO_PB11,    //!< digital in/output pin (optional I2C2-SDA, check R49), available on J5.29
  DIG_IO_PC12,    //!< digital in/output pin, NO INTERRUPT, available on J5.30
} digIO_pin_t;

//! type definition for the possible in/output modes on the data logger unit pins
typedef enum
{
  DIG_IO_MODE_INPUT = 0x00000000,             //!< Digital floating input
  DIG_IO_MODE_OUTPUT_PP = 0x00000001,         //!< Output push pull
  DIG_IO_MODE_OUTPUT_OD = 0x00000011,         //!< Output open drain
  DIG_IO_MODE_IT_RISING = 0x10110000,         //!< External interrupt mode with rising edge trigger
  DIG_IO_MODE_IT_FALLING = 0x10210000,        //!< External interrupt mode with falling edge trigger
  DIG_IO_MODE_IT_RISING_FALLING = 0x10310000  //!< External interrupt mode with rising/falling edge trigger
} digIO_mode_t;

//! type definition for possible pull-up or pull-down resistors
typedef enum
{
  DIG_IO_PULL_NO = 0x00000000,  //!< no pull-up or pull-down
  DIG_IO_PULL_UP = 0x00000001,  //!< pull-up resistor
  DIG_IO_PULL_DOWN = 0x00000002 //!< pull-down resistor
} digIO_pull_t;

//! type definition for possible in/output speeds
typedef enum
{
  DIG_IO_SPEED_LOW = 0x00000000,      //!< range up to 5MHz
  DIG_IO_SPEED_MEDIUM = 0x00000001,   //!< range 5Mhz up to 25MHz
  DIG_IO_SPEED_HIGH = 0x00000002,     //!< range 25MHz up to 50MHz
  DIG_IO_SPEED_VERYHIGH = 0x00000003  //!< range 50MHz up to 80MHz
} digIO_speed_t;

//! Digital GPIO pin configuration structure definition
typedef struct
{
  digIO_mode_t mode;      //!< digital in/output pin configuration
  digIO_pull_t pull;      //!< pull-up or pull-down resistor selection
  digIO_speed_t speed;    //!< in/output maximum frequency configuration
} digIO_cfg_t;

//! type enumeration for 3V3 units
typedef enum
{
  DIG_IO_3V3UNIT_FLASH = 0x01,    //!< FLASH IC
  DIG_IO_3V3UNIT_SDCARD = 0x02,   //!< SDCard
  DIG_IO_3V3UNIT_EXT_I2C1 = 0x04, //!< External I2C1 communication interface
  DIG_IO_3V3UNIT_EXT_J5 = 0x08,   //!< External J5 connector
} digIO_3V3Unit_t;

//! type enumeration for 17V units
typedef enum
{
  DIG_IO_17VUNIT_EXT_SENSOR1 = 0x01,  //!< External sensor on screw connector J6
  DIG_IO_17VUNIT_EXT_SENSOR2 = 0x02,  //!< External sensor on screw connector J7
  DIG_IO_17VUNIT_EXT_J5 = 0x04,       //!< External J5 connector
} digIO_17VUnit_t;

//===============================================================
//function prototypes
void digIO_init (void);

//onBoard data logger inputs
bool digIO_usrBt1Pressed (void);
bool digIO_usrBt2Pressed (void);
bool digIO_usbDetected (void);
bool digIO_sdCardDetected (void);

//onBoard data logger outputs
void digIO_otgDeviceModeSet (bool device);
void digIO_5VtoUSBSwitchOn (void);
void digIO_5VtoUSBSwitchOff (void);
void digIO_5VExtPwrOn (void);
void digIO_5VExtPwrOff (void);
void digIO_3V3PwrOn (digIO_3V3Unit_t unit);
void digIO_3V3PwrReqOff (digIO_3V3Unit_t unit);
void digIO_17VPwrOn (digIO_17VUnit_t unit);
void digIO_17VPwrReqOff (digIO_17VUnit_t unit);

//external available data logger in/outputs
void digIO_setup (digIO_pin_t pin, digIO_cfg_t config);
void digIO_cbRegister (digIO_pin_t pin, pFV_V_t pCbFct);
bool digIO_read (digIO_pin_t pin);
void digIO_write (digIO_pin_t pin, bool state);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* #ifndef __DIG_IO_H */
