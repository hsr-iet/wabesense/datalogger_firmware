/**
 ****************************************************************
 @file    RG_LED.h
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        a red-green (yellow) LED (Q8F3BZZRYG02E).
 *        Timer1 is used to generate the PWM
 *          Channel 1: Red LED
 *          Cahnnel 2: Green LED
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.1
 @date    2021-09-20
 ****************************************************************
 */
#ifndef __RG_LED_H
#define __RG_LED_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdint.h>

//===============================================================
//defines

//===============================================================
//typedefs
//! RGB color type
typedef enum
{
  RGLED_COLOR_NONE = 0,   //!< RG LED OFF
  RGLED_COLOR_RED,        //!< RG LED red only
  RGLED_COLOR_GREEN,      //!< RG LED green only
  RGLED_COLOR_YELLOW      //!< RG LED yellow (red & green)
} rgColor_t;

//===============================================================
//function prototypes
void rgLED_init (void);
void rgLED_luminositySet (uint8_t percent);
void rgLED_colorSet (rgColor_t color);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /*__RGB_LED_H */
