/**
 ****************************************************************
 @file    RGB_LED.c
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the onBoard RGB_LED.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.1
 @date    2020-10-28
 ****************************************************************
 */

/** @addtogroup HWO
 * @{
 */

/** @addtogroup RGB_LED
 * @{
 */

// --- Includes
#include "RGB_LED.h"

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include "tim.h"

// --- Defines

// --- Typedefs

// --- Variables
static bool _initDone = false;
static volatile uint32_t _luminosity = 0;
static volatile rgbColor_t _color = RGBLED_COLOR_NONE;

// --- Local Function Prototypes

/**
 ****************************************************************
 @brief   Initialize the RGB LED
 @param   -
 @return  -
 ****************************************************************
 */
void rgbLED_init (void)
{
  if (!_initDone)
  {
    //setup timer1 (used for PWM generation)
    MX_TIM1_Init();

    _color = RGBLED_COLOR_NONE;
    rgbLED_luminositySet(0);

    _initDone = true;
  }
}

/**
 ****************************************************************
 @brief   Setup the luminosity value for the RGB LED
 @param   percent The to be used percent value [0..99]
 @return  -
 ****************************************************************
 */
void rgbLED_luminositySet (uint8_t percent)
{
  if (percent > 99)
  {
    percent = 99;
  }
  _luminosity = percent;

  rgbLED_colorSet(_color);
}

/**
 ****************************************************************
 @brief   Set the active RGB LED color
 @param   color One of the possible colors from enumeration
 @return  -
 ****************************************************************
 */
void rgbLED_colorSet (rgbColor_t color)
{
  HAL_TIMEx_PWMN_Stop(&htim1, TIM_CHANNEL_1);
  HAL_TIMEx_PWMN_Stop(&htim1, TIM_CHANNEL_2);
  HAL_TIMEx_PWMN_Stop(&htim1, TIM_CHANNEL_3);

  HAL_TIM_OC1_Set(&htim1, _luminosity);
  HAL_TIM_OC2_Set(&htim1, _luminosity);
  HAL_TIM_OC3_Set(&htim1, _luminosity);
  switch (color)
  {
    default:
      assert_param(0);
      break;
    case RGBLED_COLOR_NONE:
      _color = RGBLED_COLOR_NONE;
      break;
    case RGBLED_COLOR_RED:
      if (_luminosity)
      {
        HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_1);
      }
      break;
    case RGBLED_COLOR_GREEN:
      if (_luminosity)
      {
        HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_3);
      }
      break;
    case RGBLED_COLOR_BLUE:
      if (_luminosity)
      {
        HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_2);
      }
      break;
    case RGBLED_COLOR_YELLOW:
      if (_luminosity)
      {
        HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_1);
        HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_3);
      }
      break;
    case RGBLED_COLOR_PURPLE:
      if (_luminosity)
      {
        HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_1);
        HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_2);
      }
      break;
    case RGBLED_COLOR_BLUEGREEN:
      if (_luminosity)
      {
        HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_2);
        HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_3);
      }
      break;
  }
  _color = color;
}

/**
 * @}
 */

/**
 * @}
 */
