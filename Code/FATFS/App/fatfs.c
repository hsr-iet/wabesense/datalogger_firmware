/**
  ******************************************************************************
  * @file   fatfs.c
  * @brief  Code for fatfs applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

#include "fatfs.h"

uint8_t retUSER;    /* Return value for USER */
char USERPath[4];   /* USER logical drive path */
FATFS USERFatFS;    /* File system object for USER logical drive */
FIL USERFile;       /* File object for USER */

/* USER CODE BEGIN Variables */
#include <assert.h>
#include "../../HWO/SYS.h"
/* USER CODE END Variables */    

void MX_FATFS_Init(void) 
{
  /*## FatFS: Link the USER driver ###########################*/
  retUSER = FATFS_LinkDriver(&USER_Driver, USERPath);

  /* USER CODE BEGIN Init */
  assert_param(retUSER == 0);
  /* additional user code for init */
  /* USER CODE END Init */
}

/**
  * @brief  Gets Time from RTC 
  * @param  None
  * @retval Time in DWORD
  */
DWORD get_fattime(void)
{
  /* USER CODE BEGIN get_fattime */
  rtcDateTime_t dateTime;
  DWORD time = 0;

  sys_rtcDateTimeGet(&dateTime);

  //!< bit31:25   Year origin from the 1980 (0..127, e.g. 37 for 2017)
  //!< bit24:21   Month (1..12)
  //!< bit20:16   Day of the month (1..31)
  //!< bit15:11   Hour (0..23)
  //!< bit10:5    Minute (0..59)
  //!< bit4:0     Second / 2 (0..29, e.g. 25 for 50)
  time |= (dateTime.year - 1980) << 25;
  time |= (dateTime.month) << 21;
  time |= (dateTime.day) << 16;
  time |= (dateTime.hrs) << 11;
  time |= (dateTime.min) << 5;
  time |= dateTime.sec;

  return time;
  /* USER CODE END get_fattime */  
}

/* USER CODE BEGIN Application */
void FATFS_DeInit (void)
{
  retUSER = FATFS_UnLinkDriver(USERPath);

  assert_param(retUSER == 0);
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
