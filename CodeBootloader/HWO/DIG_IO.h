/**
 ****************************************************************
 @file    DIG_IO.h
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        all the available digital In/Output pins on the data-logger.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-10
 ****************************************************************
 */
#ifndef __DIG_IO_H
#define __DIG_IO_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdbool.h>
#include "../DEF/GLOB_Types.h"

//===============================================================
//defines

//===============================================================
//typedefs
//! type enumeration for 3V3 units
typedef enum
{
  DIG_IO_3V3UNIT_FLASH = 0x01,    //!< FLASH IC
  DIG_IO_3V3UNIT_SDCARD = 0x02,   //!< SDCard
  DIG_IO_3V3UNIT_RGB_LED = 0x04,  //!< RGB-LED
  DIG_IO_3V3UNIT_17VSW = 0x08,    //!< 17V Switched PowerSupply
  DIG_IO_3V3UNIT_EXT_I2C1 = 0x10  //!< External I2C1 communication interface
} digIO_3V3Unit_t;

//===============================================================
//function prototypes
void digIO_init (void);

//onBoard data logger inputs
bool digIO_usrBt1Pressed (void);
bool digIO_usrBt2Pressed (void);
bool digIO_usbDetected (void);
bool digIO_sdCardDetected (void);

//onBoard datag logger outputs
void digIO_3V3PwrOn (digIO_3V3Unit_t unit);
void digIO_3V3PwrReqOff (digIO_3V3Unit_t unit);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* #ifndef __DIG_IO_H */
