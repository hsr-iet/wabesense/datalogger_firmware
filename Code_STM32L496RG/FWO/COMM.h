/**
 ****************************************************************
 @file    COMM.h
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        a smart protocol over a USB interface.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-24
 ****************************************************************
 */
#ifndef __FWO_COMM_H
#define __FWO_COMM_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdint.h>
#include "dataManager.h"

//===============================================================
//defines

//===============================================================
//typedefs

//===============================================================
//function prototypes
void comm_init (void);
void comm_parseRx (void);
void comm_streamMeasDataPoint (dataManager_dataPoint_t *pDataPoint);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* #ifndef __FWO_COMM_H */
