/**
 ****************************************************************
 @file    dataManager.h
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the log data management (FLASH / USB-Memory-Stick).
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 1.0
 @date    2021-10-04
 ****************************************************************
 */
#ifndef __FWO_DATAMANAGER_H
#define __FWO_DATAMANAGER_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdbool.h>
#include <stdint.h>
#include "../HWO/SYS.h"
#include "../HWO/BME280.h"

//===============================================================
//defines

//===============================================================
//typedefs
//! all data of a single data point packed together
typedef struct
{
  rtcDateTime_t timeStamp;    //!< the timeStamp from RTC module
  bool timeBasedMeas;         //!< Flag to indicate, if this is a timeBaseMeasurement or not
  float vBat;                 //!< battery voltage [V]
  float adc_in8;              //!< voltage on ADC-input8 [V]
  float adc_in9;              //!< voltage on ADC-input9 [V]
  float adc_in10;             //!< voltage on ADC-input10 [V]
  float adc_in11;             //!< voltage on ADC-input11 [V]
  bme280_measData_t bme280;   //!< BME280 sensorData
} dataManager_dataPoint_t;

//! all possible log events as enumeration (a maximum of 256 different types is internally supported)
typedef enum
{
  LOG_EVT_DATAREADOUT = 0,    //!< data was transfered from internal logger memory to SD-card
  LOG_EVT_SYSRESTART,         //!< the system was restarted
  LOG_EVT_FW_UPDATE,          //!< a new firmware was detected, try to update it
  LOG_EVT_BME_ERROR,          //!< the BME280 sensor don't reported the measurement data (timeout)
  LOG_EVT_RTCBATTERY,         //!< a low voltage measured on the RTC battery (CR2032) or it is not inserted/detected
  LOG_EVT_CONFIGSTORE,        //!< a new logger configuration setting is stored to FLASH storage (persistent)
  LOG_EVT_CLEARDATA,          //!< clear all stored measurement & logEvent data
  LOG_EVT_AUTOMEAS,           //!< auto measurement (RTC time based) deactivating or activating
  LOG_EVT_CURSORRESTORE,      //!< cursors are restored due to a complete power loss of the system (including CR2032 battery)
} dataManager_logEvt_t;

//===============================================================
//function prototypes
void dataManager_init (void);

void dataManager_flashClearAll (void);
void dataManager_flashClearCfg (void);
void dataManager_flashClearMeasDataAndHistory (void);
void dataManager_flashRewindDataReadoutCursor (void);

bool dataManager_cfgDeviceNameSet (char *pDevName, uint8_t len);
void dataManager_cfgDeviceNameGet (char *pDevName, uint8_t *pLen);
bool dataManager_cfgMeasIntervalSet (uint32_t minutes);
uint32_t dataManager_cfgMeasIntervalGet (void);
bool dataManager_cfgBatterySettingSet (uint32_t nominalCellMiliVoltage, uint32_t nrOfCells);
void dataManager_cfgBatterySettingGet (uint32_t *pNominalCellMiliVoltage, uint32_t *pNrOfCells);
bool dataManager_cfgAnalogNameSet (uint8_t analogChannelNr, char *pAnalogChannelName, uint8_t len);
bool dataManager_cfgAnalogNameGet (uint8_t analogChannelNr, char *pAnalogChannelName, uint8_t *pLen);
void dataManager_cfgAutoMeasActiveSet (bool active);
bool dataManager_cfgAutoMeasActiveGet (void);
void dataManager_cfgStoreToFlash (void);

bool dataManager_flashNextMeasDataPointStore (dataManager_dataPoint_t *pDataPoint);
uint32_t dataManager_flashNrOfMeasDataPointsGet (bool absolute);
void dataManager_flashMeasDataPointGet (uint32_t dataPointNr, dataManager_dataPoint_t *pDataPoint);
bool dataManager_flashLogEvent (const rtcDateTime_t *const pTimeStamp, dataManager_logEvt_t evt, char* pAdditionalInfo);
uint32_t dataManager_flashNrOfLogEventsGet (void);

bool dataManager_usbStickCheckForNewFirmwareFile (char *pFileName);
bool dataManager_usbStickDataStore (void);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* #ifndef __FWO_DATAMANAGER_H */
