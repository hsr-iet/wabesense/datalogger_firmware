/**
 ****************************************************************
 @file    dataLogger.h
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the complete dataLogger device.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-23
 ****************************************************************
 */
#ifndef __FWO_DATALOGGER_H
#define __FWO_DATALOGGER_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdbool.h>
#include <stdint.h>
#include "../DEF/GLOB_Types.h"

//===============================================================
//defines

//===============================================================
//typedefs
typedef enum
{
  MEAS_REQUEST_MANUAL = 0,      //!< a manual measurement request -> include storing of data
  MEAS_REQUEST_RTC,             //!< a RTC measurement request -> include storing of data
  MEAS_REQUEST_STREAMING_ONLY   //!< used for streaming over virtual COM-Port, without storing of data
} measRequest_t;

//===============================================================
//function prototypes
void dataLogger_init (void);
void dataLogger_fwVersionGet (fwVersion_t *pfwVersion);

void dataLogger_setupWithUSB (void);
bool dataLogger_handleWithUSB (void);

void dataLogger_handleMeasAutoCycleActivation (void);
bool dataLogger_measAutoCycleActivated (void);
void dataLogger_measAutoCycleActivate (bool active);
void dataLogger_measAutoCycleActivatingToggle (void);
void dataLogger_measIntervalSet(uint32_t minutes);

bool dataLogger_sysCheck (void);
void dataLogger_firmwareUpdateByUsb (void);
bool dataLogger_checkForFirmwareOnSdCard (char* pPathAndFileName);
void dataLogger_firmwareUpdateBySdCard (char* pPathAndFileName);
void dataLogger_measStreaming (bool active);
void dataLogger_measCylce (measRequest_t requestType);
bool dataLogger_storeToSDCard (void);
bool dataLogger_memoryDumpToSDCard (void);
void dataLogger_flashConfigStore (bool defaultValues);
void dataLogger_flashClearDataAndHistory (void);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* #ifndef __FWO_DATALOGGER_H */
