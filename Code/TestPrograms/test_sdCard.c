/**
 ****************************************************************
 @file    test_sdCard.c
 ****************************************************************
 @brief   This module implements some tests for the SdCard interface.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-16
 ****************************************************************
 */

/** @addtogroup TESTPROGRAMMS
 * @{
 */

// --- Includes
#include "test_sdCard.h"

#include <assert.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "fatfs.h"
#include "../HWO/DIG_IO.h"
#include "../HWO/SYS.h"
#include "../HWO/RGB_LED.h"

// --- Defines

// --- Typedefs

// --- Variables

// --- Local Function Prototypes

/**
 ****************************************************************
 @brief	  Do all possible tests on the SD-Card.
 @param   -
 @return  -
 ****************************************************************
 */
void test_sdCard (void)
{
  FATFS fs;
  FIL file;
  UINT nrOfBytesWritten;
  FRESULT iFResult;
  FILINFO fno;
  FILINFO lastFno;
  DIR dir;
  TCHAR path[32] = "logs";
  static char sdDataBuffer[13];
  uint32_t idx;

  digIO_init();
  rgbLED_init();
  //sleep for 1 second
  sys_sleepForUs(1e6);

#ifdef SEMI_HOSTING
    printf("SDCard_Test: start\n\r");
#endif
  //check if a SC-Card is inserted to the slot
  if (digIO_sdCardDetected())
  {
#ifdef SEMI_HOSTING
    printf("SDCard_Test: Card detected\n\r");
#endif

    //switch the power on
    digIO_3V3PwrOn(DIG_IO_3V3UNIT_SDCARD);
    //a minimum of 100ms is needed before chip could be selected
    sys_sleepForUs(100e3);

#ifdef SEMI_HOSTING
    printf("SDCard_Test: mount drive");
#endif
    //Mount the file system immediately, using default drive
    iFResult = f_mount(&fs, "", 1);
    assert_param(iFResult == FR_OK);
#ifdef SEMI_HOSTING
    printf(" pass\n\r");
#endif

#ifdef SEMI_HOSTING
    printf("SDCard_Test: create directory");
#endif
    //Create a directory
    iFResult = f_mkdir(path);
    assert_param((iFResult == FR_OK) || (iFResult == FR_EXIST));
#ifdef SEMI_HOSTING
    printf(" pass\n\r");
#endif

#ifdef SEMI_HOSTING
    printf("SDCard_Test: open directory");
#endif
    iFResult = f_opendir(&dir, path);
    assert_param(iFResult == FR_OK);
#ifdef SEMI_HOSTING
    printf(" pass\n\r");
#endif

    lastFno.fname[0] = 0;
    while (1)
    {
      //read fileInformation from the current directory
      iFResult = f_readdir(&dir, &fno);
      if (iFResult != FR_OK || fno.fname[0] == 0)
      {
        break; /* Break on error or end of directory */
      }

      lastFno = fno;
    }
    if (iFResult == FR_OK)
    {
      path[4] = '/';
      path[5] = 'l';
      path[6] = 'o';
      path[7] = 'g';
      //index 8 up to 10: file-numbering
      path[11] = '.';
      path[12] = 'c';
      path[13] = 's';
      path[14] = 'v';
      if (lastFno.fname[0] == 0)
      {
        //no file existing, start with logIndex 000
        path[8] = '0';
        path[9] = '0';
        path[10] = '0';
      }
      else
      {
        //use next number, get last number out of lastFilename
        strcpy(&path[8], &lastFno.fname[3]);
        if (path[10] == '9')
        {
          path[10] = '0';
          if (path[9] == '9')
          {
            path[9] = '0';
            if (path[8] != '9')
            {
              path[10] += 1;
            }
          }
          else
          {
            path[9] += 1;
          }
        }
        else
        {
          path[10] += 1;
        }
      }
    }

#ifdef SEMI_HOSTING
    printf("SDCard_Test: create new log file");
#endif
    //create a new file
    iFResult = f_open(&file, path, FA_CREATE_ALWAYS | FA_WRITE);
    assert_param(iFResult == FR_OK);
#ifdef SEMI_HOSTING
    printf(" pass\n\r");
#endif

#ifdef SEMI_HOSTING
    printf("SDCard_Test: write some dummy data");
#endif
    //store some dummy data in sdCardBuffer
    for (idx = 0; idx < sizeof(sdDataBuffer); idx += 2)
    {
      sdDataBuffer[idx] = 'Z';
      if ((idx + 1) < sizeof(sdDataBuffer))
      {
        sdDataBuffer[idx + 1] = ';';
      }
    }

    //write file
    iFResult = f_write(&file, sdDataBuffer, sizeof(sdDataBuffer), &nrOfBytesWritten);
    assert_param(iFResult == FR_OK);
#ifdef SEMI_HOSTING
    printf(" pass\n\r");
#endif

#ifdef SEMI_HOSTING
    printf("SDCard_Test: close file");
#endif
    //close file
    iFResult = f_close(&file);
    assert_param(iFResult == FR_OK);
#ifdef SEMI_HOSTING
    printf(" pass\n\r");
#endif

    //unmount drive
    f_mount(0, "", 0);

    //request to switch the power off
    digIO_3V3PwrReqOff(DIG_IO_3V3UNIT_SDCARD);

    //mark test as okay
    rgbLED_luminositySet(10);
    rgbLED_colorSet(RGBLED_COLOR_GREEN);
    sys_sleepForUs(1e6);  //1s delay
    rgbLED_luminositySet(0);
    rgbLED_colorSet(RGBLED_COLOR_NONE);
  }
  else
  {
    //mark test as error
    rgbLED_luminositySet(10);
    rgbLED_colorSet(RGBLED_COLOR_RED);
    sys_sleepForUs(1e6);  //1s delay
    rgbLED_luminositySet(0);
    rgbLED_colorSet(RGBLED_COLOR_NONE);

#ifdef SEMI_HOSTING
    printf("SDCard_Test: No card detected!!!\n\r");
#endif
  }

#ifdef SEMI_HOSTING
  printf("SDCard_Test: ends\n\n\r");
#endif

}

/**
 * @}
 */
