/**
 ****************************************************************
 @file    test_aIN.c
 ****************************************************************
 @brief   This module implements some tests for the analog input channels.
 *
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-12-04
 ****************************************************************
 */

/** @addtogroup TESTPROGRAMMS
 * @{
 */

// --- Includes
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "../HWO/A_IN.h"
#include "../HWO/DIG_IO.h"
#include "../HWO/SYS.h"
#include "../FWO/stringConverter.h"
#include "../FWO/dataManager.h"
#include "test_aIN.h"

// --- Defines

// --- Typedefs

// --- Variables

// --- Local Function Prototypes

/**
 ****************************************************************
 @brief	  Do all possible tests on the analog inputs.
 @param   -
 @return  -
 ****************************************************************
 */
void test_aIN (void)
{
#ifdef DEBUG
  volatile float vBatCR2032[40];
  volatile float vBatPA1[40];
  volatile float adcIN8[40];
  volatile float adcIN9[40];
  volatile float adcIN10[40];
  volatile float adcIN11[40];
  aIn_samplingTime_t sampleTime;
  aIn_oversampling_t oversampling;
  unsigned int idx;
  float vBat_CR2032;
  rtcDateTime_t rtcDateTime;
  char additionalTxt[48];
  uint32_t float_strLen;
  uint32_t strLen;

  aIN_init();

  //cr2032 battery channel (on VBAT)
  oversampling = A_IN_OVERSAMPLING_NONE;
  idx = 0;
  do
  {
    sampleTime = A_IN_SAMPLETIME_2_5_CLK;
    do
    {
      //sample and get battery voltage
      vBatCR2032[idx] = aIN_sampleAndGetVBat(BAT_SOURCE_VBAT, oversampling, sampleTime);
      sampleTime++;
      idx++;
    } while (sampleTime <= A_IN_SAMPLETIME_640_5_CLK);
    oversampling++;
  } while (oversampling <= A_IN_OVERSAMPLING_16);

  vBat_CR2032 = vBatCR2032[0];
  //check battery against voltage thresholds
  if (vBat_CR2032 < ((float) 2.5f))
  {
    sys_rtcDateTimeGet(&rtcDateTime);
    if (vBat_CR2032 < 1.0f)
    {
      snprintf (&additionalTxt[0], 48, "not inserted or empty: ");

    }
    else
    {
      snprintf (&additionalTxt[0], 48, "low: ");
    }

    strLen = strlen(additionalTxt);
    //convert floating point numbers to strings (nothing, if NaN)
    strConv_floatToString_6(vBat_CR2032, (char*) &additionalTxt[strLen], &float_strLen);
    additionalTxt[strLen + float_strLen] = 0x00;

    //add a VBAT log on dataManager module
    dataManager_flashLogEvent(&rtcDateTime, LOG_EVT_RTCBATTERY, additionalTxt);
  }

  //battery channel (on PA1)
  oversampling = A_IN_OVERSAMPLING_NONE;
  idx = 0;
  do
  {
    sampleTime = A_IN_SAMPLETIME_2_5_CLK;
    do
    {
      //sample and get battery voltage
      vBatPA1[idx] = aIN_sampleAndGetVBat(BAT_SOURCE_PA1, oversampling, sampleTime);
      sampleTime++;
      idx++;
    } while (sampleTime <= A_IN_SAMPLETIME_640_5_CLK);
    oversampling++;
  } while (oversampling <= A_IN_OVERSAMPLING_16);

  //aIN8
  oversampling = A_IN_OVERSAMPLING_NONE;
  idx = 0;
  do
  {
    sampleTime = A_IN_SAMPLETIME_2_5_CLK;
    do
    {
      //sample and get battery voltage
      adcIN8[idx] = aIN_sampleAndGet(PA3_ADC_IN8, oversampling, sampleTime);
      sampleTime++;
      idx++;
    } while (sampleTime <= A_IN_SAMPLETIME_640_5_CLK);
    oversampling++;
  } while (oversampling <= A_IN_OVERSAMPLING_16);

  //aIN9
  oversampling = A_IN_OVERSAMPLING_NONE;
  idx = 0;
  do
  {
    sampleTime = A_IN_SAMPLETIME_2_5_CLK;
    do
    {
      //sample and get battery voltage
      adcIN9[idx] = aIN_sampleAndGet(PA4_ADC_IN9, oversampling, sampleTime);
      sampleTime++;
      idx++;
    } while (sampleTime <= A_IN_SAMPLETIME_640_5_CLK);
    oversampling++;
  } while (oversampling <= A_IN_OVERSAMPLING_16);

  //aIN10
  oversampling = A_IN_OVERSAMPLING_NONE;
  idx = 0;
  do
  {
    sampleTime = A_IN_SAMPLETIME_2_5_CLK;
    do
    {
      //sample and get battery voltage
      adcIN10[idx] = aIN_sampleAndGet(PA5_ADC_IN10, oversampling, sampleTime);
      sampleTime++;
      idx++;
    } while (sampleTime <= A_IN_SAMPLETIME_640_5_CLK);
    oversampling++;
  } while (oversampling <= A_IN_OVERSAMPLING_16);

  //aIN11
  oversampling = A_IN_OVERSAMPLING_NONE;
  idx = 0;
  do
  {
    sampleTime = A_IN_SAMPLETIME_2_5_CLK;
    do
    {
      //sample and get battery voltage
      adcIN11[idx] = aIN_sampleAndGet(PA6_ADC_IN11, oversampling, sampleTime);
      sampleTime++;
      idx++;
    } while (sampleTime <= A_IN_SAMPLETIME_640_5_CLK);
    oversampling++;
  } while (oversampling <= A_IN_OVERSAMPLING_16);
#endif
}

/**
 * @}
 */
