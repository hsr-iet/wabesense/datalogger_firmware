/**
 ****************************************************************
 @file    A_IN.h
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        all the available analog input pins on the data-logger.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-10
 ****************************************************************
 */
#ifndef __A_IN_H
#define __A_IN_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdint.h>

//===============================================================
//defines

//===============================================================
//typedefs
//! type definition for the different kind of battery sources
typedef enum
{
  BAT_SOURCE_PA1 = 0, //!< The connected battery/powerSupply to the datalogger on connector J1 (path switched by GPIO output V_SupplyMeasEna)
  BAT_SOURCE_VBAT,    //!< The connected battery on the datalogger on holder BT1 (CR2032 battery, used for RCT & BackupRegisters)
} aIn_batSrc_t;

//! type definition for the different USB CC pins
typedef enum
{
  USB_CC1 = 0,      //!< analog input of the USB CC1 signal
  USB_CC2 = 0,      //!< analog input of the USB CC2 signal
} aIn_usbCc_t;

//! type definition for all external available analog inputs on the data logger unit
typedef enum
{
  PA3_ADC_IN8 = 0,  //!< analog input (IN8), available on J5.1 or J6.2
  PA4_ADC_IN9,      //!< analog input (IN9), available on J5.3 or J6.3
  PA5_ADC_IN10,     //!< analog input (IN10), available on J5.5 or J7.2
  PA6_ADC_IN11,     //!< analog input (IN11), available on J5.7 or J7.3
} aIn_pin_t;

//! type definition for the hardware oversampling options
typedef enum
{
  A_IN_OVERSAMPLING_NONE = 0x00000000,  //!< no oversampling
  A_IN_OVERSAMPLING_2 = 0x00000001,     //!< a oversampling by two
  A_IN_OVERSAMPLING_4 = 0x00000002,     //!< a oversampling by four
  A_IN_OVERSAMPLING_8 = 0x00000003,     //!< a oversampling by eight
  A_IN_OVERSAMPLING_16 = 0x00000004     //!< a oversampling by sixteen
} aIn_oversampling_t;

//! type definition for the sampling time (ADC-Clock is 16MHz)
typedef enum
{
  A_IN_SAMPLETIME_2_5_CLK = 0x00000000,   //!< sampling time of 2.5 ADC clock cycles (0.16us)
  A_IN_SAMPLETIME_6_5_CLK = 0x00000001,   //!< sampling time of 6.5 ADC clock cycles (0.41us)
  A_IN_SAMPLETIME_12_5_CLK = 0x00000002,  //!< sampling time of 12.5 ADC clock cycles (0.79us)
  A_IN_SAMPLETIME_24_5_CLK = 0x00000003,  //!< sampling time of 24.5 ADC clock cycles (1.54ms)
  A_IN_SAMPLETIME_47_5_CLK = 0x00000004,  //!< sampling time of 47.5 ADC clock cycles (2.97ms)
  A_IN_SAMPLETIME_92_5_CLK = 0x00000005,  //!< sampling time of 92.5 ADC clock cycles (5.79ms)
  A_IN_SAMPLETIME_247_5_CLK = 0x00000006, //!< sampling time of 247.5 ADC clock cycles (15.47ms)
  A_IN_SAMPLETIME_640_5_CLK = 0x00000007  //!< sampling time of 640.5 ADC clock cycles (40.04ms)
} aIn_samplingTime_t;

//===============================================================
//function prototypes
void aIN_init (void);

//onBoard data logger inputs
float aIN_sampleAndGetVBat (aIn_batSrc_t source, aIn_oversampling_t osr, aIn_samplingTime_t samplingTime);
float aIN_sampleAndGetVUsbCc (aIn_usbCc_t source);

//external available data logger inputs
float aIN_sampleAndGet (aIn_pin_t input, aIn_oversampling_t osr, aIn_samplingTime_t samplingTime);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /* #ifndef __A_IN_H */
