/**
  ******************************************************************************
  * File Name          : SPI.h
  * Description        : This file provides code for the configuration
  *                      of the SPI instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __spi_H
#define __spi_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include <stdint.h>

typedef enum
{
  SPI1_CHIPSELECT_NONE,    //!< select no chip on the SPI1 interface
  SPI1_CHIPSELECT_SDCARD,  //!< select the sdCard on the SPI1 interface
  SPI1_CHIPSELECT_FLASH,   //!< select the FLASH (AT25SF321B) on the SPI1 interface
  SPI1_CHIPSELECT_BME280,  //!< select the BMP280 sensor on the SPI1 interface
} spiChipSelect_t;
/* USER CODE END Includes */

extern SPI_HandleTypeDef hspi1;

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

void MX_SPI1_Init(void);

/* USER CODE BEGIN Prototypes */
void HAL_SPI1_deInit (void);
void HAL_SPI1_slaveSelect (spiChipSelect_t chipSelect);
void HAL_SPI1_transmit (uint8_t *pTxData, uint16_t size);
void HAL_SPI1_receive (uint8_t *pRxData, uint16_t size);
void HAL_SPI1_transfer (uint8_t *pTxData, uint8_t *pRxData, uint16_t size);
/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ spi_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
