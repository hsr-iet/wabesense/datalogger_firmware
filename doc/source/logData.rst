========
Log data
========

.. contents:: :local:

Generated files
===============
All measured values are stored in a microcontroller externally FLASH-IC. Additional stored informations are events about the system and the configuration of it. 
With these informations, three different output-files will be exported durring `data-export operation`_. All files will be exported to the top directory on the 
USB-Stick.

Log-Data (.csv)
---------------
A semicolon (;) seperated file, that contains all permanently stored measurement information. If this file exists on the USB-Stick, it will only be extended by the 
new (not allready exported datapoints) measurement-values. Otherwise if the file is not located on the drive, the complete file will be written, independent of the 
state of the allready readout datapoints. The file will allways be located/searched on the top directory of the USB-Stick.

	* CSV file
	* The filename will be the name of the device (configurable).
	* Each line represent one sample point with the corresponding date-/timestamp and all measurement results

Column items
^^^^^^^^^^^^
Corresponding column items are (in this order):
	* RTC_Datetime(YYYY.mm.dd HH:MM:SS.MS+0000)
		The RTC system date and time in millisecond resolution.
	* Measurement_Auto
		Flag to indicate, if the measurement was a time based measurement (auto-flag: '1') or a manual measurement (auto-flag: '0').
	* Battery_Voltage(V)
		The system input voltage measured by the corresponding ADC channel.
	* BME_Temperature(C)
		The converted environment temperature, that was sampled by the BME280 sensor unit.
	* BME_Pressure(Pa)
		The converted environment pressure, that was sampled by the BME280 sensor unit.
	* BME_Humidity(%)
		The converted environment relative humidity, that was sampled by the BME280 sensor unit.
	* ADC_IN8(V)
		The converted input volate on the ADC_IN8 input as a resulting value of 0.0 ... 5.0V.
	* ADC_IN9(V)
		The converted input volate on the ADC_IN9 input as a resulting value of 0.0 ... 5.0V.
	* ADC_IN10(V)
		The converted input volate on the ADC_IN10 input as a resulting value of 0.0 ... 5.0V.
	* ADC_IN11(V)
		The converted input volate on the ADC_IN11 input as a resulting value of 0.0 ... 5.0V.

File example
^^^^^^^^^^^^
.. note::
	Not all columns of the header row are shown in the picture below!

.. image:: ../pictures/logData_dataLogExample.JPG
  :width: 750
  :alt: logData - measurement data log example
  
Internal memory management
^^^^^^^^^^^^^^^^^^^^^^^^^^
If the logger-internal memory-space for logData will be full, the oldest data's stored on it (4kByte block) will be automatically cleared and fill in the new 
measurement values. This rollover-process is handled by the datalogger independently. This 4kByte block size erase and store of the newest measurement process will 
then be active all time, once the Flash logData space was full. It then could be possible, that real measurment datas getting lost, if the user has not exported them. 
Then the interval time used for `data-export-operation`_ is to low --> user need to export the measurement data more often.
If this memory rollover process occours, the next time data are exported, only the not allready readout/exported measurement data will be exported 
(appended to the log-File).

Examples of internal memory blocks organisation for logData. Block number 0 up to 1018 are used for logData. One logDataPoint has a size of 32 Bytes. 
This gives 4096Bytes/Block divided by 32Byte per logDataPoint 128 DataPoints per 4kByte memory block. In total this gives 128 multiplied by 1019, what gives 
a total of 130'432 logDataPoint's which could be stored at maximum in the FLASH memory.

.. list-table:: Just some sample Points stored (total of 198 logDataPoint's stored) - no data readout until now
	:widths: 200 100 500
	:header-rows: 1

	*	- Internal measurement index
		- Block-Number
		- Content
	*	- 0
		- 0
		- new logDataPoint: not exported / readout (oldest logDataPoint)
	*	- 1 - 127
		- 0
		- new logDataPoint: not exported / readout
	*	- 128
		- 1
		- new logDataPoint: not exported / readout
	*	- 129 - 196
		- 1
		- new logDataPoint: not exported / readout
	*	- 197
		- 1
		- new logDataPoint: not exported / readout (newest logDataPoint)
	*	- 198 - 255
		- 1
		- erased / empty storage
	*	- 256 - 383
		- 2
		- erased / empty storage
	*	- 384 - 130'431
		- 3 - 1018
		- erased / empty storage

.. code-block:: rst
	
	**If csv-File exists**, corresponding csv-File will be **extended** by the following lines in that oder:
	  - Internal measurement index: 0 (oldest logDataPoint)
	  - Internal measurement index: 1
	  - Internal measurement index: 2
	  - ...
	  - Internal measurement index: 195
	  - Internal measurement index: 196
	  - Internal measurement index: 197 (newest logDataPoint)
	
.. code-block:: rst
	
	**If csv-File not extists**, the following lines will be created in the **new csv-File**:
	  - header
	  - Internal measurement index: 0 (oldest logDataPoint)
	  - Internal measurement index: 1
	  - Internal measurement index: 2
	  - ...
	  - Internal measurement index: 195
	  - Internal measurement index: 196
	  - Internal measurement index: 197 (newest logDataPoint)
	  
|

.. list-table:: Just some sample Points stored (total of 199 logDataPoint's stored, 198 allready exported)
	:widths: 200 100 500
	:header-rows: 1

	*	- Internal measurement index
		- Block-Number
		- Content
	*	- 0
		- 0
		- logDataPoint: exported / readout (oldest logDataPoint)
	*	- 1 - 127
		- 0
		- logDataPoint: exported / readout
	*	- 128
		- 1
		- logDataPoint: exported / readout
	*	- 129 - 197
		- 1
		- logDataPoint: exported / readout
	*	- 198
		- 1
		- new logDataPoint: not exported / readout (newest logDataPoint)
	*	- 199 - 255
		- 1
		- erased / empty storage
	*	- 256 - 383
		- 2
		- erased / empty storage
	*	- 384 - 130'431
		- 3 - 1018
		- erased / empty storage

.. code-block:: rst
	
	**If csv-File exists**, corresponding csv-File will be **extended** by the following lines in that oder:
	  - Internal measurement index: 198 (oldest not already read out logDataPoint --> in this case also the newest logDataPoint)
	
.. code-block:: rst
	
	**If csv-File not extists**, the following lines will be created in the **new csv-File**:
	  - header
	  - Internal measurement index: 0 (oldest logDataPoint)
	  - Internal measurement index: 1
	  - Internal measurement index: 2
	  - ...
	  - Internal measurement index: 196
	  - Internal measurement index: 197
	  - Internal measurement index: 198 (newest logDataPoint)

|

.. list-table:: LogData memory completly filled up and started with memory rollover - some of the logData have allready been exported		
	:widths: 200 100 500
	:header-rows: 1

	*	- Internal measurement index
		- Block-Number
		- Content
	*	- 0
		- 0
		- new logDataPoint: not exported / readout
	*	- 1 - 30
		- 0
		- new logDataPoint: not exported / readout
	*	- 31
		- 0
		- new logDataPoint: not exported / readout (newest logDataPoint)
	*	- 32 - 127
		- 0
		- erased / empty storage
	*	- 128
		- 1
		- logDataPoint: exported / readout (oldest logDataPoint)
	*	- 129 - 197
		- 1
		- logDataPoint: exported / readout
	*	- 198 - 255
		- 1
		- logDataPoint: exported / readout
	*	- 256 - 383
		- 2
		- logDataPoint: exported / readout
	*	- 384 - 130'303
		- 3 - 1017
		- logDataPoint: exported / readout
	*	- 130'304
		- 1018
		- logDataPoint: exported / readout
	*	- 130'305 - 130'411
		- 1018
		- logDataPoint: exported / readout
	*	- 130'412
		- 1018
		- new logDataPoint: not exported / readout (oldest not already read out logDataPoint)
	*	- 130'413 - 130'431
		- 1018
		- new logDataPoint: not exported / readout

.. code-block:: rst
	
	**If csv-File exists**, corresponding csv-File will be **extended** by the following lines in that oder:
	  - Internal measurement index: 130'412 (oldest not already read out logDataPoint)
	  - Internal measurement index: 130'413
	  - Internal measurement index: 130'414
	  - ...
	  - Internal measurement index: 130'430
	  - Internal measurement index: 130'431
	  - Internal measurement index: 0
	  - Internal measurement index: 1
	  - Internal measurement index: 2
	  - ...
	  - Internal measurement index: 29
	  - Internal measurement index: 30
	  - Internal measurement index: 31 (newest logDataPoint)
	
.. code-block:: rst
	
	**If csv-File not extists**, the following lines will be created in the **new csv-File**:
	  - header
	  - Internal measurement index: 128 (oldest logDataPoint)
	  - Internal measurement index: 129
	  - Internal measurement index: 130
	  - ...
	  - Internal measurement index: 130'430
	  - Internal measurement index: 130'431
	  - Internal measurement index: 0
	  - Internal measurement index: 1
	  - Internal measurement index: 2
	  - ...
	  - Internal measurement index: 29
	  - Internal measurement index: 30
	  - Internal measurement index: 31 (newest logDataPoint)

Log activity History (.log)
---------------------------
A activity history file about the last 100 events detected and executed by the datalogger. Each event could have additional information in plain text. This 
file will be written each time completely new to the USB-Stick, independent if it allready exitst on the Top-directory of the USB-Stick

	* Plain text file
	* The filename will be the name of the device (configurable).
	* First 6 lines are allways available, others depends on the number of stored activity events.

General file build up
^^^^^^^^^^^^^^^^^^^^^
	* First 4 lines gives information about
		* csv-File
		* Date-/Timestamp of first sample data point
		* Date-/Timestamp of last sample data point
		* The number of samples totaly stored in the datalogger
	
Possible Log-Events are
^^^^^^^^^^^^^^^^^^^^^^^
	* LOG_EVT_DATAREADOUT
		Data was transfered from internal logger memory to USB-Stick
	* LOG_EVT_SYSRESTART
		The system was restarted (Power source inserted)
	* LOG_EVT_FW_UPDATE
		An firmware file was detected on the memory stick, try to update.
	* LOG_EVT_BME_ERROR
		The BME280 sensor don't reported the measurement data (timeout)
	* LOG_EVT_RTCBATTERY
		A low voltage measured on the RTC battery (CR2032) or it is not inserted/detected
	* LOG_EVT_CONFIGSTORE
		* A new logger configuration setting is stored to FLASH storage (persistent)
		* FLASH content was not correct, restored only some parts (autoMeasActive and/or measInterval_minutes), but only in RAM
	* LOG_EVT_CLEARDATA
		All persistent stored measurement & logEvent data are cleared. Executed by DCA command over serial interface - see `API documentation`_
	* LOG_EVT_AUTOMEAS
		State change of the auto measurement (RTC time based) function detected (deactivating or activating)
	* LOG_EVT_CURSORRESTORE
		Cursors used for logMemory data are restored due to a complete power loss of the system (including CR2032 battery)

File example
^^^^^^^^^^^^
.. image:: ../pictures/logData_activityLogExample.JPG
  :width: 600
  :alt: logData - activity log example

Metadata and configuration (.txt)
---------------------------------
The metadata and configuration file contains the current system setup informations, which are permanently stored in FLASH.  This 
file will be written each time completely new to the USB-Stick, independent if it allready exitst on the Top-directory of the USB-Stick.

	* Plain text file
	* all these parts could be configured over the USB-Interface (virtual communication port - see ....)
	* The filename will be the name of the device (configurable)

General file build up
^^^^^^^^^^^^^^^^^^^^^
	* Device name
		default value is same as Unique ID (given in Hardware from microcontroller itself)
	* Time based measurement
		* default: 'enabled'
		* possible values:
			* 'enabled'	A automatic sampling with the given time interval will be triggered from RTC unit and then executed (short system wakeup)
			* 'disabled'	Automatic sampling is inactive.
	* Measurement interval (minutes)
		* default: 10
		* possible values: 1 - 1092
	* Battery nominal voltage (mV)
		* default: 3700
		* possible values: 0001 - 9999
	* Battery number of cells
		* default: 3
		* possible values: 1 - 9
	* Analog channel mappings
		* default:
			* ADC_IN8 (HW): ADC_IN8(V)
			* ADC_IN9 (HW): ADC_IN9(V)
			* ADC_IN10 (HW): ADC_IN10(V)
			* ADC_IN11 (HW): ADC_IN11(V)
		* e.g.
			* ADC_IN8 (HW): sensorAnalog0(V)
			* ADC_IN9 (HW): sensorAnalog1(V)
			* ADC_IN10 (HW): NA
			* ADC_IN11 (HW): NA
	* Firmware version:
		* 'Major'.'Minor'.'Bug' (each as a number form 0 - 9)
		* e.g. 0.2.0
	* Unique ID
		A unique identification number given from microcontroller hardware itself (only readable information, written by ST durring fabrication)
		e.g. 0x0032001F425350112035354E

File example
^^^^^^^^^^^^
.. image:: ../pictures/logData_metadataConfigurationExample.JPG
  :width: 400
  :alt: logData - activity log example
 
Exporting
=========

Log-Data (.csv)
---------------
If a file with the same name is allready present on the USB-Stick, where the file should be exported to, then the new not allready 
exported data will be appended to the existing file. So the .csv-file could contains more datapoints then the datalogger itself could 
store in the `FLASH-IC AT25SF321B`_.

Log activity History (.log)
---------------------------
This fill will be created on every export time new. If a same one allready exists, it will be overwritten with the new one.

Metadata and configuration (.txt)
---------------------------------
This fill will be created on every export time new. If a same one allready exists, it will be overwritten with the new one.

  
Example on USB-Stick (file explorer)
====================================

.. image:: ../pictures/logData_dataOnUSBStickExample.JPG
  :width: 600
  :alt: logData - USB Stick file export data
  
Links
=====
.. _FLASH-IC AT25SF321B: https://hsr-iet.gitlab.io/wabesense/datalogger_firmware/memory.html#datalogger-pcb-flash
.. _API documentation: https://hsr-iet.gitlab.io/wabesense/dataloggerui/API.html
.. _data-export operation: https://hsr-iet.gitlab.io/wabesense/wabesense/operation.html#data-export-operation

.. target-notes::