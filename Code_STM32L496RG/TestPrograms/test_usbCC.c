/**
 ****************************************************************
 @file    test_usbCC.c
 ****************************************************************
 @brief   This module implements some tests for the analog input channels
 *        of the USB interface.
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 1.0
 @date    2021-10-18
 ****************************************************************
 */

/** @addtogroup TESTPROGRAMMS
 * @{
 */

// --- Includes
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "../HWO/A_IN.h"
#include "../HWO/DIG_IO.h"
#include "../HWO/SYS.h"
#include "test_usbCC.h"

// --- Defines

// --- Typedefs

// --- Variables

// --- Local Function Prototypes

/**
 ****************************************************************
 @brief	  Do all possible tests on the analog inputs of the USB CC interface.
 @param   -
 @return  -
 ****************************************************************
 */
void test_usbCC (void)
{
  volatile float usbCC1[4];
  volatile float usbCC2[4];

  aIN_init();

  //values in default configuration (USB-Device configuration)
  digIO_otgDeviceModeSet(true);
  digIO_5VExtPwrOn();       //turn power source V_Ext (connector J1) ON --> Device powered from J1 source
  digIO_5VtoUSBSwitchOff();

  usbCC1[0] = aIN_sampleAndGetVUsbCc(USB_CC1);
  usbCC2[0] = aIN_sampleAndGetVUsbCc(USB_CC2);
  //results:
  // without USB-Stick: 0V
  // with USB-Stick: 0V

  //change to USB-Host
  digIO_otgDeviceModeSet(false);
  usbCC1[1] = aIN_sampleAndGetVUsbCc(USB_CC1);
  usbCC2[1] = aIN_sampleAndGetVUsbCc(USB_CC2);
  //results:
  // without USB-Stick: 3.3V
  // with USB-Stick: 0.376V

  //check if VUSB is not present actually
  if (!digIO_usbDetected())
  {
    //switch 5V on on VBUS
    digIO_5VtoUSBSwitchOn();  //use internal 5V to supply V-USB

    //change to USB-Host
    digIO_otgDeviceModeSet(false);
    usbCC1[2] = aIN_sampleAndGetVUsbCc(USB_CC1);
    usbCC2[2] = aIN_sampleAndGetVUsbCc(USB_CC2);
    //results:
    // without USB-Stick: 3.3V
    // with USB-Stick: 0.376V

    digIO_otgDeviceModeSet(true); //change back to USB-device
    usbCC1[3] = aIN_sampleAndGetVUsbCc(USB_CC1);
    usbCC2[3] = aIN_sampleAndGetVUsbCc(USB_CC2);
    //results:
    // without USB-Stick: 0V
    // with USB-Stick: 0V

    digIO_5VtoUSBSwitchOff();
  }
}

/**
 * @}
 */
