/**
 ****************************************************************
 @file    FLASH.h
 ****************************************************************
 @brief   This module offers a set of functions to handle
 *        the onBoard FLASH IC (AT25SF321B - 32Mbit).
 ****************************************************************
 @note    The used interface is the SPI1 unit (also used for BME280 and SDCard)!
 *        SPI Interface of the sensor is compatible to (max. 108MHz)
 *          mode 00
 *            CPOL = 0, CPHA = 0
 *          or mode 11
 *            CPOL = 1, CPHA = 1
 ****************************************************************
 @author  A. Tuescher, IMES/OST
 @version 0.0
 @date    2020-11-03
 ****************************************************************
 */
#ifndef __HWO_FLASH_H
#define __HWO_FLASH_H

//===============================================================
//If building with a C++ compiler, make all of the definitions in this header
//have a C binding.
#ifdef __cplusplus
extern "C"
{
#endif

//===============================================================
//includes
#include <stdint.h>

//===============================================================
//defines

//===============================================================
//typedefs

//===============================================================
//function prototypes
void flash_init (void);
uint32_t flash_memorySizeGet (void);
void flash_bytesRead (uint32_t address, uint8_t *pData, uint32_t nrOfBytes);
void flash_bytesWrite (uint32_t address, uint8_t *pData, uint32_t nrOfBytes);
void flash_block4kErase (uint32_t blockNr);
void flash_chipErase (void);

//===============================================================
//Mark the end of the C bindings section for C++ compilers
#ifdef __cplusplus
}
#endif

#endif /*__HWO_FLASH_H */
